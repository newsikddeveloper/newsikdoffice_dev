/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.office.ejb.dbapi;

import app.sikd.entity.backoffice.UploadFilePDF;
import app.sikd.entity.utilitas.UploadFile;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author detra
 */
public interface IUploadFileDAO {
    
    public void addUploadFile(UploadFile uploadFile, String tableName, Connection sikdConn) throws SQLException;
     public void deleteUploadFile(String tableName, String kodeSatker, short tahun, String ikd, Connection sikdConn) throws SQLException;
     public void deleteUploadFile(long id, String tableName, Connection sikdConn) throws SQLException;
     public void updateUploadFile(UploadFile uploadFile, String tableName, Connection sikdConn) throws SQLException;
     public List<UploadFile> getAllUploadFiles(String tableName, String kodeSatker, short tahun, Connection sikdConn) throws SQLException;
    
    public void addPdfUploadFile(UploadFilePDF uploadFile, String tableName, Connection sikdConn) throws SQLException;
    public void deletePdfUploadFile(long id, String tableName, Connection sikdConn) throws SQLException;
    public List<UploadFilePDF> getAllPdfUploadFiles(String tableName, String kodeSatker, Connection sikdConn) throws SQLException;
    
    
    
}
