/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.sikd.office.ejb.dbapi;

import app.sikd.dbapi.apbd.IAPBDConstants;
import app.sikd.entity.backoffice.APBD;
import app.sikd.entity.backoffice.ArusKas;
import app.sikd.entity.backoffice.ArusKasSaldo;
import app.sikd.entity.backoffice.KegiatanAPBD;
import app.sikd.entity.backoffice.KodeRekeningAPBD;
import app.sikd.entity.backoffice.Neraca;
import app.sikd.entity.backoffice.NeracaAkunJenis;
import app.sikd.entity.backoffice.NeracaAkunKelompok;
import app.sikd.entity.backoffice.NeracaAkunObjek;
import app.sikd.entity.backoffice.NeracaAkunUtama;
import app.sikd.entity.backoffice.ObjArusKasAkun;
import app.sikd.entity.backoffice.RealisasiAPBD;
import app.sikd.entity.backoffice.RealisasiKegiatanAPBD;
import app.sikd.entity.backoffice.RealisasiKodeRekeningAPBD;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author sora
 */
public class DMSQL implements IDMSQL {
    
    @Override
    public APBD getAPBDHeader(short year, String kodeSatker, short kodeData, short jenisCOA, Connection conn) throws SQLException {
        Statement stm = null;
        ResultSet rs = null;

        try {
            stm = conn.createStatement();
            rs = stm.executeQuery("select * from apbd "
                    + "where apbd.tahunanggaran= " + year + " "
                    + "and apbd.kodesatker = '" + kodeSatker + "' and apbd.kodedata= " + kodeData + " and apbd.jeniscoa=" + jenisCOA);
            if (rs.next()) {
               return new APBD(rs.getLong("apbdindex"), kodeSatker, rs.getString("kodepemda"), rs.getString("namapemda"), year, kodeData, jenisCOA, rs.getShort("statusdata"), 
                        rs.getString("nomorperda"), rs.getDate("tanggalperda")==null?null:new java.util.Date(rs.getDate("tanggalperda").getTime()), rs.getString("namaaplikasi"), rs.getString("pengembangaplikasi"));
            }

            return null;
        } catch (SQLException ex) {
            Logger.getLogger(DMSQL.class.getName()).log(Level.SEVERE, null, ex);
            throw new SQLException("SQL: Gagal mengambil data  apbd \n" + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    
    public APBD getLraHeader(short year, String kodeSatker, short kodeData, short jenisCOA, Connection conn) throws SQLException {
        Statement stm = null;
        ResultSet rs = null;

        try {
            stm = conn.createStatement();
            rs = stm.executeQuery("select * from apbd "
                    + "where apbd.tahunanggaran= " + year + " "
                    + "and apbd.kodesatker = '" + kodeSatker + "' and apbd.kodedata= " + kodeData + " and apbd.jeniscoa=" + jenisCOA);
            if (rs.next()) {
               return new APBD(rs.getLong("apbdindex"), kodeSatker, rs.getString("kodepemda"), rs.getString("namapemda"), year, kodeData, jenisCOA, rs.getShort("statusdata"), 
                        rs.getString("nomorperda"), rs.getDate("tanggalperda")==null?null:new java.util.Date(rs.getDate("tanggalperda").getTime()), rs.getString("namaaplikasi"), rs.getString("pengembangaplikasi"));
            }

            return null;
        } catch (SQLException ex) {
            Logger.getLogger(DMSQL.class.getName()).log(Level.SEVERE, null, ex);
            throw new SQLException("SQL: Gagal mengambil data  apbd \n" + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }

    @Override
    public Neraca getNeracaHeader(String kodeSatker, short tahun, short semester, String judul, Connection conn) throws SQLException {
        Statement stm = null;
        ResultSet rs = null;
        Neraca result = new Neraca();
        try {
            stm = conn.createStatement();
            rs = stm.executeQuery("select * from neraca  "
                    + "where kodesatker = '" + kodeSatker + "' and "
                    + "tahunanggaran= " + tahun + " and semester=" + semester
                    + " and judulneraca='" + judul + "'");
            result.setKodeSatker(kodeSatker);
            result.setTahunAnggaran(tahun);
            result.setSemester(semester);
            result.setJudulNeraca(judul);
            if (rs.next()) {
                result = new Neraca(rs.getLong("neracaindex"), rs.getString("kodesatker"), rs.getString("kodepemda"), rs.getString("namapemda"), tahun, semester, judul, rs.getShort("statusdata"));
            }

            return result;
        } catch (SQLException ex) {
            Logger.getLogger(DMSQL.class.getName()).log(Level.SEVERE, null, ex);
            throw new SQLException("SQL: Gagal mengambil data Neraca Header \n" + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }

    /**
     *
     * @param indexNeraca
     * @param conn
     * @return
     * @throws SQLException
     */
    public List<NeracaAkunUtama> getNeracaAkunStruktur(long indexNeraca, Connection conn) throws SQLException {
        Statement stm = null;
        ResultSet rs = null;
        List<NeracaAkunUtama> result = new ArrayList();
        try {
            stm = conn.createStatement();
            rs = stm.executeQuery("select u.akunutamaindex, u.kodeakunutama, u.namaakunutama, u.nilai, "
                    + "  k.akunkelompokindex, k.kodeakunkelompok, k.namaakunkelompok, "
                    + "       k.nilai nilaikelompok, "
                    + "  j.akunjenisindex, j.kodeakunjenis, j.namaakunjenis, "
                    + "       j.nilai nilaijenis, "
                    + "       o.akunobjekindex, o.kodeakunobjek, o.namaakunobjek, o.nilai nilaiobjek "
                    + "   from neracakodrekutama u "
                    + "   left join neracakodrekkelompok k on u.akunutamaindex=k.kodrekutamaindex "
                    + "   left join neracakodrekjenis j on k.akunkelompokindex=j.kodrekkelompokindex "
                    + "   left join neracakodrekobjek o on j.akunjenisindex=o.kodrekjenisindex "
                    + "	where neracaindex=" + indexNeraca + " order by kodeakunutama, kodeakunkelompok, kodeakunjenis, kodeakunobjek");
//            long indexU = 0, indexK =0, indexJ=0;
            List<NeracaAkunKelompok> kels = new ArrayList();
            List<NeracaAkunJenis> jeniss = new ArrayList();
            List<NeracaAkunObjek> objeks = new ArrayList();
            while (rs.next()) {
                if (result.isEmpty()) {
                    result.add(new NeracaAkunUtama(rs.getLong("akunutamaindex"), rs.getString("kodeakunutama"), rs.getString("namaakunutama"), rs.getDouble("nilai"), (short) 1));
                    if (rs.getString("kodeakunkelompok") != null && !rs.getString("kodeakunkelompok").trim().equals("")) {
                        kels.add(new NeracaAkunKelompok(rs.getLong("akunkelompokindex"), rs.getString("kodeakunkelompok"), rs.getString("namaakunkelompok"), rs.getDouble("nilaikelompok"), (short) 2));
                    }
                    if (rs.getString("kodeakunjenis") != null && !rs.getString("kodeakunjenis").trim().equals("")) {
                        jeniss.add(new NeracaAkunJenis(rs.getLong("akunjenisindex"), rs.getString("kodeakunjenis"), rs.getString("namaakunjenis"), rs.getDouble("nilaijenis"), (short) 3));
                    }
                    if (rs.getString("kodeakunobjek") != null && !rs.getString("kodeakunobjek").trim().equals("")) {
                        objeks.add(new NeracaAkunObjek(rs.getLong("akunobjekindex"), rs.getString("kodeakunobjek"), rs.getString("namaakunobjek"), rs.getDouble("nilaiobjek"), (short) 4));
                    }
                } else {
                    if (result.get(result.size() - 1).getIndex() != rs.getLong("akunutamaindex")) {
                        if (!jeniss.isEmpty()) {                            
                            List<NeracaAkunObjek> oos = new ArrayList();
                            oos.addAll(objeks);
                            jeniss.get(jeniss.size() - 1).setAkunObjeks(oos);
                        }
                        if (!kels.isEmpty()) {                            
                            List<NeracaAkunJenis> jjs = new ArrayList();
                            jjs.addAll(jeniss);
                            kels.get(kels.size() - 1).setAkunJeniss(jjs);
                        }
                        List<NeracaAkunKelompok> kks = new ArrayList();
                        kks.addAll(kels);
                        result.get(result.size() - 1).setAkunKelompoks(kks);
                        result.add(new NeracaAkunUtama(rs.getLong("akunutamaindex"), rs.getString("kodeakunutama"), rs.getString("namaakunutama"), rs.getDouble("nilai"), (short) 1));
                        kels.clear();
                        jeniss.clear();
                        objeks.clear();
                        if (rs.getString("kodeakunkelompok") != null && !rs.getString("kodeakunkelompok").trim().equals("")) {
                            kels.add(new NeracaAkunKelompok(rs.getLong("akunkelompokindex"), rs.getString("kodeakunkelompok"), rs.getString("namaakunkelompok"), rs.getDouble("nilaikelompok"), (short) 2));
                        }
                        if (rs.getString("kodeakunjenis") != null && !rs.getString("kodeakunjenis").trim().equals("")) {
                            jeniss.add(new NeracaAkunJenis(rs.getLong("akunjenisindex"), rs.getString("kodeakunjenis"), rs.getString("namaakunjenis"), rs.getDouble("nilaijenis"), (short) 3));
                        }
                        if (rs.getString("kodeakunobjek") != null && !rs.getString("kodeakunobjek").trim().equals("")) {
                            objeks.add(new NeracaAkunObjek(rs.getLong("akunobjekindex"), rs.getString("kodeakunobjek"), rs.getString("namaakunobjek"), rs.getDouble("nilaiobjek"), (short) 4));
                        }
                    } else {
                        if (!kels.isEmpty()) {
                            if (kels.get(kels.size() - 1).getIndex() != rs.getLong("akunkelompokindex")) {
                                if (!jeniss.isEmpty()) {
                                    List<NeracaAkunObjek> oos = new ArrayList();
                                    oos.addAll(objeks);
                                    jeniss.get(jeniss.size() - 1).setAkunObjeks(oos);
                                }
                                if (!kels.isEmpty()) {
                                    List<NeracaAkunJenis> jjs = new ArrayList();
                                    jjs.addAll(jeniss);
                                    kels.get(kels.size() - 1).setAkunJeniss(jjs);
                                }
                                if (rs.getString("kodeakunkelompok") != null && !rs.getString("kodeakunkelompok").trim().equals("")) {
                                    kels.add(new NeracaAkunKelompok(rs.getLong("akunkelompokindex"), rs.getString("kodeakunkelompok"), rs.getString("namaakunkelompok"), rs.getDouble("nilaikelompok"), (short) 2));
                                }
                                jeniss.clear();
                                objeks.clear();
                                if (rs.getString("kodeakunjenis") != null && !rs.getString("kodeakunjenis").trim().equals("")) {
                                    jeniss.add(new NeracaAkunJenis(rs.getLong("akunjenisindex"), rs.getString("kodeakunjenis"), rs.getString("namaakunjenis"), rs.getDouble("nilaijenis"), (short) 3));
                                }
                                if (rs.getString("kodeakunobjek") != null && !rs.getString("kodeakunobjek").trim().equals("")) {
                                    objeks.add(new NeracaAkunObjek(rs.getLong("akunobjekindex"), rs.getString("kodeakunobjek"), rs.getString("namaakunobjek"), rs.getDouble("nilaiobjek"), (short) 4));
                                }
                            } else {
                                if (!jeniss.isEmpty()) {
                                    if (jeniss.get(jeniss.size() - 1).getIndex() != rs.getLong("akunjenisindex")) {
                                        if (!jeniss.isEmpty()) {
                                            List<NeracaAkunObjek> oos = new ArrayList();
                                            oos.addAll(objeks);
                                            jeniss.get(jeniss.size() - 1).setAkunObjeks(oos);
                                        }
                                        objeks.clear();
                                        if (rs.getString("kodeakunjenis") != null && !rs.getString("kodeakunjenis").trim().equals("")) {
                                            jeniss.add(new NeracaAkunJenis(rs.getLong("akunjenisindex"), rs.getString("kodeakunjenis"), rs.getString("namaakunjenis"), rs.getDouble("nilaijenis"), (short) 3));
                                        }
                                        if (rs.getString("kodeakunobjek") != null && !rs.getString("kodeakunobjek").trim().equals("")) {
                                            objeks.add(new NeracaAkunObjek(rs.getLong("akunobjekindex"), rs.getString("kodeakunobjek"), rs.getString("namaakunobjek"), rs.getDouble("nilaiobjek"), (short) 4));
                                        }
                                    } else {
                                        if (!objeks.isEmpty()) {
                                            if (objeks.get(objeks.size() - 1).getIndex() != rs.getLong("akunobjekindex")) {
                                                if (rs.getString("kodeakunobjek") != null && !rs.getString("kodeakunobjek").trim().equals("")) {
                                                    objeks.add(new NeracaAkunObjek(rs.getLong("akunobjekindex"), rs.getString("kodeakunobjek"), rs.getString("namaakunobjek"), rs.getDouble("nilaiobjek"), (short) 4));
                                                }
                                            }
                                        }
                                    }
                                }

                            }
                        }
                    }
                }
            }
            if (!jeniss.isEmpty()) {
                jeniss.get(jeniss.size() - 1).setAkunObjeks(objeks);
            }
            if (!kels.isEmpty()) {
                kels.get(kels.size() - 1).setAkunJeniss(jeniss);
            }
            if (!result.isEmpty()) {
                result.get(result.size() - 1).setAkunKelompoks(kels);
            }

            return result;
        } catch (SQLException ex) {
            Logger.getLogger(DMSQL.class.getName()).log(Level.SEVERE, null, "Gagal mengambil data Neraca Akun Struktur " + ex);
            throw new SQLException("SQL: Gagal mengambil data Neraca Akun Struktur\n" + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }

    /**
     *
     * @param neraca
     * @param conn
     * @return
     * @throws SQLException
     */
    public Neraca createNeracaHeader(Neraca neraca, short iotype, Connection conn) throws SQLException {
        PreparedStatement stm = null;
        ResultSet rs = null;
        String sqlC = "with zz as ( ";
        sqlC = sqlC
                + "insert into neraca ("
                + "kodesatker, kodepemda, namapemda, tahunanggaran, semester, judulneraca, tanggalpengiriman, statusdata, namaaplikasi, pengembangaplikasi, iotype "
                + " ) "
                + "VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? ) ";
        sqlC = sqlC.concat(" returning * ) select max(neracaindex) as index from zz");

        String sqlE = "update neraca set judulneraca=?, statusdata=? where neracaindex=?";

        try {
            if (neraca.getIndex() > 0) {
                stm = conn.prepareStatement(sqlE);
                stm.setString(1, neraca.getJudulNeraca().trim());
                stm.setShort(2, neraca.getStatusData());
                stm.setLong(3, neraca.getIndex());
                stm.executeUpdate();
            } else {
                stm = conn.prepareStatement(sqlC);
                stm.setString(1, neraca.getKodeSatker().trim());
                stm.setString(2, neraca.getKodePemda().trim());
                stm.setString(3, neraca.getNamaPemda().trim());
                stm.setShort(4, neraca.getTahunAnggaran());
                stm.setShort(5, neraca.getSemester());
                stm.setString(6, neraca.getJudulNeraca());
                Timestamp time = new Timestamp(new java.util.Date().getTime());
                stm.setTimestamp(7, time);
                stm.setShort(8, neraca.getStatusData());
                stm.setString(9, "Input Data");
                stm.setString(10, "Input Data");
                stm.setShort(11, iotype);
                rs = stm.executeQuery();
                if (rs.next()) {
                    neraca.setIndex(rs.getLong("index"));
                }
            }
            return neraca;
        } catch (SQLException ex) {
            Logger.getLogger(DMSQL.class.getName()).log(Level.SEVERE, null, ex);
            throw new SQLException("SQL: Gagal input data Neraca Header \n" + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    public Neraca updateNeracaHeader(Neraca neraca, Connection conn) throws SQLException {
        PreparedStatement stm = null;
        ResultSet rs = null;        

        String sqlE = "update neraca set judulneraca=?, statusdata=? where neracaindex=?";

        try {
            if (neraca.getIndex() > 0) {
                stm = conn.prepareStatement(sqlE);
                stm.setString(1, neraca.getJudulNeraca().trim());
                stm.setShort(2, neraca.getStatusData());
                stm.setLong(3, neraca.getIndex());
                stm.executeUpdate();
            }
            return neraca;
        } catch (SQLException ex) {
            Logger.getLogger(DMSQL.class.getName()).log(Level.SEVERE, null, ex);
            throw new SQLException("SQL: Gagal merubah data Neraca Header \n" + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }

    public NeracaAkunUtama createNeracaAkunUtama(long indexNeraca, NeracaAkunUtama obj, Connection conn) throws SQLException {
        PreparedStatement stm = null;
        ResultSet rs = null;
        String sqlC = "with zz as ( ";
        sqlC = sqlC
                + "insert into neracakodrekutama ("
                + "neracaindex, kodeakunutama, namaakunutama, nilai "
                + " ) "
                + "VALUES ( ?, ?, ?, ? ) ";
        sqlC = sqlC.concat(" returning * ) select max(akunutamaindex) as index from zz");

        String sqlE = "update neracakodrekutama set kodeakunutama=?, namaakunutama=?, nilai=? where akunutamaindex=?";

        try {
            if (obj.getIndex() > 0) {
                stm = conn.prepareStatement(sqlE);
                stm.setString(1, obj.getKodeAkun().trim());
                stm.setString(2, obj.getNamaAkun());
                stm.setDouble(3, obj.getNilai());
                stm.setLong(4, obj.getIndex());
                stm.executeUpdate();
            } else {
                stm = conn.prepareStatement(sqlC);
                stm.setLong(1, indexNeraca);
                stm.setString(2, obj.getKodeAkun().trim());
                stm.setString(3, obj.getNamaAkun().trim());
                stm.setDouble(4, obj.getNilai());
                rs = stm.executeQuery();
                if (rs.next()) {
                    obj.setIndex(rs.getLong("index"));
                }
            }
            return obj;
        } catch (SQLException ex) {
            Logger.getLogger(DMSQL.class.getName()).log(Level.SEVERE, null, ex);
            throw new SQLException("SQL: Gagal input data Neraca Akun Utama\n" + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }

    public NeracaAkunKelompok createNeracaAkunKelompok(long indexAkunUtama, NeracaAkunKelompok obj, Connection conn) throws SQLException {
        PreparedStatement stm = null;
        ResultSet rs = null;
        String sqlC = "with zz as ( ";
        sqlC = sqlC
                + "insert into neracakodrekkelompok ("
                + "kodrekutamaindex, kodeakunkelompok, namaakunkelompok, nilai "
                + " ) "
                + "VALUES ( ?, ?, ?, ? ) ";
        sqlC = sqlC.concat(" returning * ) select max(akunkelompokindex) as index from zz");

        String sqlE = "update neracakodrekkelompok set kodeakunkelompok=?, namaakunkelompok=?, nilai=? where akunkelompokindex=?";

        try {
            if (obj.getIndex() > 0) {
                System.out.println(sqlE);
                stm = conn.prepareStatement(sqlE);
                stm.setString(1, obj.getKodeAkun().trim());
                stm.setString(2, obj.getNamaAkun());
                stm.setDouble(3, obj.getNilai());
                stm.setLong(4, obj.getIndex());
                stm.executeUpdate();
            } else {
                System.out.println(sqlC);
                stm = conn.prepareStatement(sqlC);
                stm.setLong(1, indexAkunUtama);
                stm.setString(2, obj.getKodeAkun().trim());
                stm.setString(3, obj.getNamaAkun().trim());
                stm.setDouble(4, obj.getNilai());
                rs = stm.executeQuery();
                if (rs.next()) {
                    obj.setIndex(rs.getLong("index"));
                }
            }
            return obj;
        } catch (SQLException ex) {
            Logger.getLogger(DMSQL.class.getName()).log(Level.SEVERE, null, ex);
            throw new SQLException("SQL: Gagal input data Neraca Akun Kelompok\n" + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }

    public NeracaAkunJenis createNeracaAkunJenis(long indexAkunKelompok, NeracaAkunJenis obj, Connection conn) throws SQLException {
        PreparedStatement stm = null;
        ResultSet rs = null;
        String sqlC = "with zz as ( ";
        sqlC = sqlC
                + "insert into neracakodrekjenis ("
                + "kodrekkelompokindex, kodeakunjenis, namaakunjenis, nilai "
                + " ) "
                + "VALUES ( ?, ?, ?, ? ) ";
        sqlC = sqlC.concat(" returning * ) select max(akunjenisindex) as index from zz");

        String sqlE = "update neracakodrekjenis set kodeakunjenis=?, namaakunjenis=?, nilai=? where akunjenisindex=?";

        try {
            if (obj.getIndex() > 0) {
                stm = conn.prepareStatement(sqlE);
                stm.setString(1, obj.getKodeAkun().trim());
                stm.setString(2, obj.getNamaAkun());
                stm.setDouble(3, obj.getNilai());
                stm.setLong(4, obj.getIndex());
                stm.executeUpdate();
            } else {
                stm = conn.prepareStatement(sqlC);
                stm.setLong(1, indexAkunKelompok);
                stm.setString(2, obj.getKodeAkun().trim());
                stm.setString(3, obj.getNamaAkun().trim());
                stm.setDouble(4, obj.getNilai());
                rs = stm.executeQuery();
                if (rs.next()) {
                    obj.setIndex(rs.getLong("index"));
                }
            }
            return obj;
        } catch (SQLException ex) {
            Logger.getLogger(DMSQL.class.getName()).log(Level.SEVERE, null, ex);
            throw new SQLException("SQL: Gagal input data Neraca Akun Jenis\n" + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }

    public NeracaAkunObjek createNeracaAkunObjek(long indexAkunJenis, NeracaAkunObjek obj, Connection conn) throws SQLException {
        PreparedStatement stm = null;
        ResultSet rs = null;
        String sqlC = "with zz as ( ";
        sqlC = sqlC
                + "insert into neracakodrekobjek ("
                + "kodrekjenisindex, kodeakunobjek, namaakunobjek, nilai "
                + " ) "
                + "VALUES ( ?, ?, ?, ? ) ";
        sqlC = sqlC.concat(" returning * ) select max(akunobjekindex) as index from zz");

        String sqlE = "update neracakodrekobjek set kodeakunobjek=?, namaakunobjek=?, nilai=? where akunobjekindex=?";

        try {
            if (obj.getIndex() > 0) {
                stm = conn.prepareStatement(sqlE);
                stm.setString(1, obj.getKodeAkun().trim());
                stm.setString(2, obj.getNamaAkun());
                stm.setDouble(3, obj.getNilai());
                stm.setLong(4, obj.getIndex());
                stm.executeUpdate();
            } else {
                stm = conn.prepareStatement(sqlC);
                stm.setLong(1, indexAkunJenis);
                stm.setString(2, obj.getKodeAkun().trim());
                stm.setString(3, obj.getNamaAkun().trim());
                stm.setDouble(4, obj.getNilai());
                rs = stm.executeQuery();
                if (rs.next()) {
                    obj.setIndex(rs.getLong("index"));
                }
            }
            return obj;
        } catch (SQLException ex) {
            Logger.getLogger(DMSQL.class.getName()).log(Level.SEVERE, null, ex);
            throw new SQLException("SQL: Gagal input data Neraca Akun Objek\n" + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }

    /**
     *
     * @param index
     * @param conn
     * @throws SQLException
     */
    public void deleteNeracaAkunObjek(long index, Connection conn) throws SQLException {
        PreparedStatement stm = null;
        String sql = "delete from neracakodrekobjek where akunobjekindex=?";

        try {
            stm = conn.prepareStatement(sql);
            stm.setLong(1, index);
            stm.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(DMSQL.class.getName()).log(Level.SEVERE, null, ex);
            throw new SQLException("SQL: Gagal hapus data Neraca Akun Objek\n" + ex.getMessage());
        } finally {
            if (stm != null) {
                stm.close();
            }
        }
    }

    public void deleteNeracaAkunJenis(long index, Connection conn) throws SQLException {
        PreparedStatement stm = null;
        String sql = "delete from neracakodrekjenis where akunjenisindex=?";

        try {
            stm = conn.prepareStatement(sql);
            stm.setLong(1, index);
            stm.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(DMSQL.class.getName()).log(Level.SEVERE, null, ex);
            throw new SQLException("SQL: Gagal hapus data Neraca Akun Jenis\n" + ex.getMessage());
        } finally {
            if (stm != null) {
                stm.close();
            }
        }
    }

    public void deleteNeracaAkunKelompok(long index, Connection conn) throws SQLException {
        PreparedStatement stm = null;
        String sql = "delete from neracakodrekkelompok where akunkelompokindex=?";

        try {
            stm = conn.prepareStatement(sql);
            stm.setLong(1, index);
            stm.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(DMSQL.class.getName()).log(Level.SEVERE, null, ex);
            throw new SQLException("SQL: Gagal hapus data Neraca Akun Kelompok\n" + ex.getMessage());
        } finally {
            if (stm != null) {
                stm.close();
            }
        }
    }

    public void deleteNeracaAkunUtama(long index, Connection conn) throws SQLException {
        PreparedStatement stm = null;
        String sql = "delete from neracakodrekutama where akunutamaindex=?";

        try {
            stm = conn.prepareStatement(sql);
            stm.setLong(1, index);
            stm.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(DMSQL.class.getName()).log(Level.SEVERE, null, ex);
            throw new SQLException("SQL: Gagal hapus data Neraca Akun Utama\n" + ex.getMessage());
        } finally {
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    /*
    ================================
    Arus Kas
    =================================
    */
    
    
    public void deleteArusKas(long index, Connection conn) throws SQLException{
        PreparedStatement stm = null;
        String sql = "delete from aruskas where aruskasindex=?";

        try {
            stm = conn.prepareStatement(sql);
            stm.setLong(1, index);
            stm.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(DMSQL.class.getName()).log(Level.SEVERE, null, ex);
            throw new SQLException("SQL: Gagal hapus data Arus Kas\n" + ex.getMessage());
        } finally {
            if (stm != null) {
                stm.close();
            }
        }
    }
    public void deleteArusKasRinci(long index, String table, String field, Connection conn) throws SQLException{
        PreparedStatement stm = null;
        String sql = "delete from " + table + " where " + field + "=?";

        try {
            stm = conn.prepareStatement(sql);
            stm.setLong(1, index);
            stm.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(DMSQL.class.getName()).log(Level.SEVERE, null, ex);
            throw new SQLException("SQL: Gagal hapus data Arus Kas Rincian\n" + ex.getMessage());
        } finally {
            if (stm != null) {
                stm.close();
            }
        }
    }
    public ArusKas createArusKasHeader(ArusKas arusKas, short iotype, Connection conn) throws SQLException{
        PreparedStatement stm = null;
        ResultSet rs = null;
        String sqlC = "with zz as ( ";
        sqlC = sqlC
                + "insert into aruskas ("
                + "kodesatker, kodepemda, namapemda, tahunanggaran, judularuskas, tglpengiriman, statusdata, namaaplikasi, pengembangaplikasi, iotype "
                + " ) "
                + "VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ? ) ";
        sqlC = sqlC.concat(" returning * ) select max(aruskasindex) as index from zz");

        String sqlE = "update aruskas set judularuskas=?, statusdata=? where aruskasindex=?";

        try {
            if (arusKas.getIndex() > 0) {
                stm = conn.prepareStatement(sqlE);
                stm.setString(1, arusKas.getJudulArusKas().trim());
                stm.setShort(2, arusKas.getStatusData());
                stm.setLong(3, arusKas.getIndex());
                stm.executeUpdate();
            } else {
                stm = conn.prepareStatement(sqlC);
                stm.setString(1, arusKas.getKodeSatker().trim());
                stm.setString(2, arusKas.getKodePemda().trim());
                stm.setString(3, arusKas.getNamaPemda().trim());
                stm.setShort(4, arusKas.getTahunAnggaran());
                stm.setString(5, arusKas.getJudulArusKas());
                Timestamp time = new Timestamp(new java.util.Date().getTime());
                stm.setTimestamp(6, time);
                stm.setShort(7, arusKas.getStatusData());
                stm.setString(8, "Input Data");
                stm.setString(9, "Input Data");
                stm.setShort(10, iotype);
                rs = stm.executeQuery();
                if (rs.next()) {
                    arusKas.setIndex(rs.getLong("index"));
                }
            }
            return arusKas;
        } catch (SQLException ex) {
            Logger.getLogger(DMSQL.class.getName()).log(Level.SEVERE, null, ex);
            throw new SQLException("SQL: Gagal input data Arus Kas Header\n" + ex.getMessage());
        } finally {
            if( rs!= null) rs.close();
            if (stm != null) { 
                stm.close();
            }
        }
    }
    public ObjArusKasAkun createArusKasRinci(long indexKas, ObjArusKasAkun obj, String table, String field, Connection conn) throws SQLException{
        PreparedStatement stm = null;
        String sql = "with zz as ( insert into " + table + " (aruskasindex, kodeakun, namaakun, nilai) VALUES (?, ?, ?, ?) returning *)"
                + " select max(" + field + ") as index from zz;";
        String sqlE = "update "+table+ " set kodeakun=?, namaakun=?, nilai=? where " + field + "=?";
        ResultSet rs = null;
        try {
            if (obj.getIndex() <= 0) {
                stm = conn.prepareStatement(sql);
                stm.setLong(1, indexKas);
                stm.setString(2, obj.getKodeAkun());
                stm.setString(3, obj.getNamaAkun());
                stm.setDouble(4, obj.getNilai());
                rs = stm.executeQuery();
                if (rs.next()) {
                    obj.setIndex(rs.getLong("index"));
                }
            }
            else{
                stm = conn.prepareStatement(sqlE);
                stm.setString(1, obj.getKodeAkun());
                stm.setString(2, obj.getNamaAkun());
                stm.setDouble(3, obj.getNilai());
                stm.setLong(4, obj.getIndex());
                stm.executeUpdate();
            }
            return obj;
        } catch (SQLException ex) {
            Logger.getLogger(DMSQL.class.getName()).log(Level.SEVERE, null, ex);
            throw new SQLException("SQL: Gagal input data Arus Kas rincian \n" + ex.getMessage());
        } finally {
            if( rs!=null ) rs.close();
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    public ArusKas getArusKasHeader(String kodeSatker, short tahun, String judul, Connection conn) throws SQLException{
        Statement stm = null;
        ResultSet rs = null;
        ArusKas result = new ArusKas();
        try {
            stm = conn.createStatement();
            rs = stm.executeQuery("select ak.*, aks.saldoindex, aks.kasbudawal, aks.kasbudakhir, "
                    + "aks.kasbendpengeluaranawal, aks.kasbendpenerimaanawal, aks.kaslainnya from aruskas ak "
                    + "left join aruskassaldo aks on ak.aruskasindex=aks.aruskasindex "
                    + "where kodesatker = '" + kodeSatker + "' and "
                    + "tahunanggaran= " + tahun 
                    + " and judularuskas='" + judul + "'");
            result.setKodeSatker(kodeSatker);
            result.setTahunAnggaran(tahun);
            result.setJudulArusKas(judul);
            if (rs.next()) {
                result = new ArusKas(rs.getLong("aruskasindex"), rs.getString("kodesatker"), rs.getString("kodepemda"), rs.getString("namapemda"), tahun, judul, rs.getShort("statusdata"));
                ArusKasSaldo sal = new ArusKasSaldo(rs.getLong("saldoindex"), rs.getDouble("kasbudawal"), rs.getDouble("kasbudakhir"), rs.getDouble("kasbendpengeluaranawal"), rs.getDouble("kasbendpenerimaanawal"), rs.getDouble("kaslainnya"));
                result.setArusKasSaldo(sal);
            }

            return result;
        } catch (SQLException ex) {
            Logger.getLogger(DMSQL.class.getName()).log(Level.SEVERE, null, ex);
            throw new SQLException("SQL: Gagal mengambil data Arus Kas Header \n" + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }
    public List<ObjArusKasAkun> getArusKasRinci(long indexArusKas, String table, String field, Connection conn) throws SQLException{
        PreparedStatement stm = null;
        String sql = "select * from " + table + " where aruskasindex=?";
        ResultSet rs = null;
        List<ObjArusKasAkun> result = new ArrayList();
        try {
            stm = conn.prepareStatement(sql);
            stm.setLong(1, indexArusKas);
            rs = stm.executeQuery();
            while( rs.next() ){
                result.add(new ObjArusKasAkun(rs.getLong(field), rs.getString("kodeakun"), rs.getString("namaakun"), rs.getDouble("nilai")));
            }
            return result;
        } catch (SQLException ex) {
            Logger.getLogger(DMSQL.class.getName()).log(Level.SEVERE, null, ex);
            throw new SQLException("SQL: Gagal mengambil data arus kas rincian\n" + ex.getMessage());
        } finally {
            if( rs != null ) rs.close();
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    
    public ArusKasSaldo createArusKasSaldo(long indexKas, ArusKasSaldo obj, Connection conn) throws SQLException{
        PreparedStatement stm = null;
        String sql = "with zz as ( insert into aruskassaldo "
                + "(aruskasindex, kasbudawal, kasbudakhir, kasbendpengeluaranawal, kasbendpenerimaanawal, kaslainnya) VALUES (?, ?, ?, ?, ?, ?) returning *)"
                + " select max(saldoindex) as index from zz;";
        String sqlE = "update aruskassaldo set kasbudawal=?, kasbudakhir=?, kasbendpengeluaranawal=?, kasbendpenerimaanawal=?, kaslainnya=? where saldoindex=?";
        ResultSet rs = null;
        try {
            if (obj.getIndex() <= 0) {
                stm = conn.prepareStatement(sql);
                stm.setLong(1, indexKas);
                stm.setDouble(2, obj.getKasBUDAwal());
                stm.setDouble(3, obj.getKasBUDAkhir());
                stm.setDouble(4, obj.getKasBendaharaPengeluaranAwal());
                stm.setDouble(5, obj.getKasBendaharaPenerimaanAwal());
                stm.setDouble(6, obj.getKasLainnya());
                rs = stm.executeQuery();
                if (rs.next()) {
                    obj.setIndex(rs.getLong("index"));
                }
            }
            else{
                stm = conn.prepareStatement(sqlE);
                stm.setDouble(1, obj.getKasBUDAwal());
                stm.setDouble(2, obj.getKasBUDAkhir());
                stm.setDouble(3, obj.getKasBendaharaPengeluaranAwal());
                stm.setDouble(4, obj.getKasBendaharaPenerimaanAwal());
                stm.setDouble(5, obj.getKasLainnya());
                stm.setLong(5, obj.getIndex());
                stm.executeUpdate();
            }
            return obj;
        } catch (SQLException ex) {
            Logger.getLogger(DMSQL.class.getName()).log(Level.SEVERE, null, ex);
            throw new SQLException("SQL: Gagal input data Arus Kas Saldo \n" + ex.getMessage());
        } finally {
            if( rs!=null ) rs.close();
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    /*
    =================================================================
    APBD
    =================================================================
    */
    
    public APBD getApbdStruktur(String kodeSatker, short tahun, short kodeData, short jenisCOA, Connection conn) throws SQLException{
        PreparedStatement stm = null, stmA = null;
        ResultSet rs = null, rsA = null;
        APBD result = new APBD();
        result.setTahunAnggaran(tahun);
        result.setKodeSatker(kodeSatker);
        result.setJenisCOA(jenisCOA);
        result.setKodeData(kodeData);
        
        String sql = "select * from apbd where kodesatker=? and tahunanggaran=? and kodedata=? and jeniscoa=? ";
        try{
            stm = conn.prepareStatement(sql);
            stm.setString(1, kodeSatker);
            stm.setShort(2, tahun);
            stm.setShort(3, kodeData);
            stm.setShort(4, jenisCOA);
            rs = stm.executeQuery();
            if( rs.next() ){                
                result = new APBD(rs.getLong("apbdindex"), kodeSatker, rs.getString("kodepemda"), rs.getString("namapemda"), tahun, kodeData, jenisCOA, rs.getShort("statusdata"), 
                        rs.getString("nomorperda"), rs.getDate("tanggalperda")==null?null:new java.util.Date(rs.getDate("tanggalperda").getTime()), rs.getString("namaaplikasi"), rs.getString("pengembangaplikasi"));
            }
            stm = conn.prepareStatement("select k.* from kegiatanapbd k  where apbdindex=? order by kodeurusanprogram, kodeurusanpelaksana, kodeskpd, kodeprogram, kodekegiatan");
            stm.setLong(1, result.getIndex());
            rs = stm.executeQuery();
            List<KegiatanAPBD> kegs = new ArrayList();
            while(rs.next()){
                long kegId = rs.getLong("kegiatanindex");
                stmA = conn.prepareStatement("select * from koderekapbd where kegiatanindex=? "
                        + " order by kodeakunutama, kodeakunkelompok, kodeakunjenis, kodeakunobjek, kodeakunrincian");
                stmA.setLong(1, kegId);
                rsA = stmA.executeQuery();
                List<KodeRekeningAPBD> akuns = new ArrayList();
                while(rsA.next()){
                    akuns.add(new KodeRekeningAPBD(rsA.getString("kodeakunutama"), rsA.getString("namaakunutama"), rsA.getString("kodeakunkelompok"), rsA.getString("namaakunkelompok"), rsA.getString("kodeakunjenis"), rsA.getString("namaakunjenis"), rsA.getString("kodeakunobjek"), rsA.getString("namaakunobjek"), rsA.getString("kodeakunrincian"), rsA.getString("namaakunrincian"), rsA.getString("kodeakunsub"), rsA.getString("namaakunsub"), rsA.getDouble("nilaianggaran"), kegId));
                }
                kegs.add(new KegiatanAPBD(kegId, rs.getString("kodeurusanprogram"), rs.getString("namaurusanprogram"), rs.getString("kodeurusanpelaksana"), rs.getString("namaurusanpelaksana"), rs.getString("kodeskpd"), rs.getString("namaskpd"), rs.getString("kodeprogram"), rs.getString("namaprogram"), rs.getString("kodekegiatan"), rs.getString("namakegiatan"), rs.getString("kodeFungsi"), rs.getString("namafungsi")));
                KodeRekeningAPBD[] ss = new KodeRekeningAPBD[akuns.size()];
                kegs.get(kegs.size()-1).setKodeRekenings(akuns);
            }
            result.setKegiatans(kegs);
            return result;
        } catch (SQLException ex){            
            Logger.getLogger(DMSQL.class.getName()).log(Level.SEVERE, null, ex);
            throw new SQLException(ex.getMessage());
        } 
        finally{
            if( rs != null ) rs.close();
            if( rsA!=null ) rsA.close();
            if( stm!=null ) stm.close();
            if( stmA!=null ) stmA.close();
        }        
    }

    @Override
    public APBD createAPBDHeader(APBD apbd, short iotype, Connection conn) throws SQLException {
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {            
            String sql = "with zz as ( ";
            sql = sql +
                    "insert into " + IAPBDConstants.TABLE_APBD + "("
                    + IAPBDConstants.ATTR_KODE_SATKER + ", "
                    + IAPBDConstants.ATTR_KODE_PEMDA + ", "
                    + IAPBDConstants.ATTR_NAMA_PEMDA + ", "
                    + IAPBDConstants.ATTR_TAHUN_ANGGARAN + ", "
                    + IAPBDConstants.ATTR_TANGGAL_PENGIRIMAN + ", "
                    + IAPBDConstants.ATTR_KODE_DATA 
                    + ", statusdata,jeniscoa,nomorperda,tanggalperda,namaaplikasi,pengembangaplikasi, iotype ) "
                    + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
            sql = sql + " returning * ) select max(apbdindex) as index from zz";            
            stm = conn.prepareStatement(sql);
            stm.setString(1, apbd.getKodeSatker().trim());
            stm.setString(2, apbd.getKodePemda().trim());
            stm.setString(3, apbd.getNamaPemda().trim());
            stm.setShort(4, apbd.getTahunAnggaran());
//            java.sql.Date da = new java.sql.Date(new java.util.Date().getTime());
            Timestamp time = new Timestamp(new java.util.Date().getTime());
            stm.setTimestamp(5, time);
            stm.setShort(6, apbd.getKodeData());
            stm.setShort(7, apbd.getStatusData());
            stm.setShort(8, apbd.getJenisCOA());
            stm.setString(9, apbd.getNomorPerda());
            if(apbd.getTanggalPerda()!=null)
            stm.setDate(10, new java.sql.Date(apbd.getTanggalPerda().getTime()));
            else stm.setNull(10, Types.DATE);
            stm.setString(11, apbd.getNamaAplikasi());
            stm.setString(12, apbd.getPengembangAplikasi());
            stm.setShort(13, iotype);
            rs = stm.executeQuery();
            
            if( rs.next()){
                apbd.setIndex(rs.getLong("index"));
            }
            return apbd;
        } catch (SQLException ex) {
            Logger.getLogger(DMSQL.class.getName()).log(Level.SEVERE, null, ex);
            throw new SQLException(ex.getMessage());
        } finally {
            if(rs!=null) rs.close();
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    @Override
    public APBD updateAPBDHeader(APBD apbd, Connection conn) throws SQLException {
        PreparedStatement stm = null;
        try {
            String sql = "update " + IAPBDConstants.TABLE_APBD + " set "
                    + IAPBDConstants.ATTR_KODE_SATKER + "=?, "
                    + IAPBDConstants.ATTR_KODE_PEMDA + "=?, "
                    + IAPBDConstants.ATTR_NAMA_PEMDA + "=?, "
                    + IAPBDConstants.ATTR_TAHUN_ANGGARAN + "=?, "
                    + IAPBDConstants.ATTR_TANGGAL_PENGIRIMAN + "=?, "
                    + IAPBDConstants.ATTR_KODE_DATA + "=?, statusdata= ?, "
                    + IAPBDConstants.ATTR_JENIS_COA + "=?, nomorperda= ?, tanggalperda=?, namaaplikasi=?, pengembangaplikasi=? "
                    + " WHERE "
                    + IAPBDConstants.ATTR_APBD_INDEX + "=?";
            
            stm = conn.prepareStatement(sql);
            stm.setString(1, apbd.getKodeSatker().trim());
            stm.setString(2, apbd.getKodePemda().trim());
            stm.setString(3, apbd.getNamaPemda().trim());
            stm.setShort(4, apbd.getTahunAnggaran());
//            java.sql.Date da = new java.sql.Date(new java.util.Date().getTime());
            Timestamp time = new Timestamp(new java.util.Date().getTime());
            stm.setTimestamp(5, time);
            stm.setShort(6, apbd.getKodeData());
            stm.setShort(7, apbd.getStatusData());
            stm.setShort(8, apbd.getJenisCOA());
            stm.setString(9, apbd.getNomorPerda());
            if( apbd.getTanggalPerda() != null )
            stm.setDate(10, new java.sql.Date(apbd.getTanggalPerda().getTime()));
            else stm.setNull(10, Types.DATE);
            stm.setString(11, apbd.getNamaAplikasi());
            stm.setString(12, apbd.getPengembangAplikasi());
            stm.setLong(13, apbd.getIndex());
            stm.executeUpdate();
            return apbd;
        } catch (SQLException ex) {
            Logger.getLogger(DMSQL.class.getName()).log(Level.SEVERE, null, ex);
            throw new SQLException(ex.getMessage());
        } finally {
            if (stm != null) {
                stm.close();
            }
        }
    }

    @Override
    public void deleteAPBD(long id, Connection conn) throws SQLException {
        PreparedStatement stm = null;
        try {
            String sql = "delete from " + IAPBDConstants.TABLE_APBD + " where apbdindex=? ";
            stm = conn.prepareStatement(sql);
            stm.setLong(1, id);
            stm.executeUpdate();
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    
    public KegiatanAPBD createKegiatanAPBD(long indexApbd, KegiatanAPBD kegiatan, Connection conn) throws SQLException {
        PreparedStatement pstm = null;
        ResultSet rs = null;
        try {
            String sql = "with zz as ( ";
            sql = sql + 
                    "insert into " + IAPBDConstants.TABLE_KEGIATAN_APBD + "("
                    + IAPBDConstants.ATTR_APBD_INDEX + ", "
                    + IAPBDConstants.ATTR_KODE_URUSAN_PROGRAM + ", "
                    + IAPBDConstants.ATTR_NAMA_URUSAN_PROGRAM + ", "
                    + IAPBDConstants.ATTR_KODE_URUSAN_PELAKSANA + ", "
                    + IAPBDConstants.ATTR_NAMA_URUSAN_PELAKSANA + ", "
                    + IAPBDConstants.ATTR_KODE_SKPD + ", "
                    + IAPBDConstants.ATTR_NAMA_SKPD + ", "
                    + IAPBDConstants.ATTR_KODE_PROGRAM + ", "
                    + IAPBDConstants.ATTR_NAMA_PROGRAM + ", "
                    + IAPBDConstants.ATTR_KODE_KEGIATAN + ", "
                    + IAPBDConstants.ATTR_NAMA_KEGIATAN + ", "
                    + IAPBDConstants.ATTR_KODE_FUNGSI + ", "
                    + IAPBDConstants.ATTR_NAMA_FUNGSI + ") "
                    + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
            sql = sql + " returning * ) select max(kegiatanindex) as index from zz";
            pstm = conn.prepareStatement(sql);
            pstm.setLong(1, indexApbd);
            pstm.setString(2, kegiatan.getKodeUrusanProgram().trim());
            pstm.setString(3, kegiatan.getNamaUrusanProgram().trim());
            pstm.setString(4, kegiatan.getKodeUrusanPelaksana().trim());
            pstm.setString(5, kegiatan.getNamaUrusanPelaksana().trim());
            pstm.setString(6, kegiatan.getKodeSKPD().trim());
            pstm.setString(7, kegiatan.getNamaSKPD().trim());
            pstm.setString(8, kegiatan.getKodeProgram().trim());
            pstm.setString(9, kegiatan.getNamaProgram().trim());
            pstm.setString(10, kegiatan.getKodeKegiatan().trim());
            pstm.setString(11, kegiatan.getNamaKegiatan().trim());
            pstm.setString(12, kegiatan.getKodeFungsi().trim());
            pstm.setString(13, kegiatan.getNamaFungsi().trim());
            rs= pstm.executeQuery();
            if(rs.next()) kegiatan.setIndex(rs.getLong("index"));
            return kegiatan;
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        }
        finally{
            if(rs!=null) rs.close();
            if( pstm!= null ) pstm.close();
        }
    }
    
    
    public KegiatanAPBD updateKegiatanAPBD(KegiatanAPBD kegiatan, Connection conn) throws SQLException {
        PreparedStatement pstm = null;
        ResultSet rs = null;
        try {
            String sql = "update " + IAPBDConstants.TABLE_KEGIATAN_APBD + " set "
                    + IAPBDConstants.ATTR_KODE_URUSAN_PROGRAM + "=?, "
                    + IAPBDConstants.ATTR_NAMA_URUSAN_PROGRAM + "=?, "
                    + IAPBDConstants.ATTR_KODE_URUSAN_PELAKSANA + "=?, "
                    + IAPBDConstants.ATTR_NAMA_URUSAN_PELAKSANA + "=?, "
                    + IAPBDConstants.ATTR_KODE_SKPD + "=?, "
                    + IAPBDConstants.ATTR_NAMA_SKPD + "=?, "
                    + IAPBDConstants.ATTR_KODE_PROGRAM + "=?, "
                    + IAPBDConstants.ATTR_NAMA_PROGRAM + "=?, "
                    + IAPBDConstants.ATTR_KODE_KEGIATAN + "=?, "
                    + IAPBDConstants.ATTR_NAMA_KEGIATAN + "=?, "
                    + IAPBDConstants.ATTR_KODE_FUNGSI + "=?, "
                    + IAPBDConstants.ATTR_NAMA_FUNGSI + "=? where kegiatanindex=?";
            int col=1;
            pstm = conn.prepareStatement(sql);
            pstm.setString(col++, kegiatan.getKodeUrusanProgram().trim());
            pstm.setString(col++, kegiatan.getNamaUrusanProgram().trim());
            pstm.setString(col++, kegiatan.getKodeUrusanPelaksana().trim());
            pstm.setString(col++, kegiatan.getNamaUrusanPelaksana().trim());
            pstm.setString(col++, kegiatan.getKodeSKPD().trim());
            pstm.setString(col++, kegiatan.getNamaSKPD().trim());
            pstm.setString(col++, kegiatan.getKodeProgram().trim());
            pstm.setString(col++, kegiatan.getNamaProgram().trim());
            pstm.setString(col++, kegiatan.getKodeKegiatan().trim());
            pstm.setString(col++, kegiatan.getNamaKegiatan().trim());
            pstm.setString(col++, kegiatan.getKodeFungsi().trim());
            pstm.setString(col++, kegiatan.getNamaFungsi().trim());
            pstm.setLong(col++, kegiatan.getIndex());
            pstm.executeUpdate();
            return kegiatan;
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        }
        finally{
            if(rs!=null) rs.close();
            if( pstm!= null ) pstm.close();
        }
    }
    
    
    @Override
    public void deleteKegiatanAPBD(long id, Connection conn) throws SQLException {
        PreparedStatement stm = null;
        try {
            String sql = "delete from " + IAPBDConstants.TABLE_KEGIATAN_APBD + " where kegiatanindex=?";
            stm = conn.prepareStatement(sql);
            stm.setLong(1, id);
            stm.executeUpdate();
            
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        }
        finally{
            if( stm!= null ) stm.close();
        }
    }

    @Override
    public void createRekeningAPBD(long indexKegiatan, KodeRekeningAPBD rekening, Connection conn) throws SQLException {
        PreparedStatement stm = null;
        try {
            String sql = "insert into " + IAPBDConstants.TABLE_KODE_REKENING_APBD + "("
                    + IAPBDConstants.ATTR_KEGIATAN_INDEX + ", "
                    + IAPBDConstants.ATTR_KODE_AKUN_UTAMA + ", "
                    + IAPBDConstants.ATTR_NAMA_AKUN_UTAMA + ", "
                    + IAPBDConstants.ATTR_KODE_AKUN_KELOMPOK + ", "
                    + IAPBDConstants.ATTR_NAMA_AKUN_KELOMPOK + ", "
                    + IAPBDConstants.ATTR_KODE_AKUN_JENIS + ", "
                    + IAPBDConstants.ATTR_NAMA_AKUN_JENIS + ", "
                    + IAPBDConstants.ATTR_KODE_AKUN_OBJEK + ", "
                    + IAPBDConstants.ATTR_NAMA_AKUN_OBJEK + ", "
                    + IAPBDConstants.ATTR_KODE_AKUN_RINCIAN + ", "
                    + IAPBDConstants.ATTR_NAMA_AKUN_RINCIAN + ", "
                    + IAPBDConstants.ATTR_KODE_AKUN_SUB + ", "
                    + IAPBDConstants.ATTR_NAMA_AKUN_SUB + ", "
                    + IAPBDConstants.ATTR_NILAI_ANGGARAN + ") "
                    + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
            stm = conn.prepareStatement(sql);
            stm.setLong(1, indexKegiatan);
            stm.setString(2, rekening.getKodeAkunUtama().trim());
            stm.setString(3, rekening.getNamaAkunUtama().trim());
            stm.setString(4, rekening.getKodeAkunKelompok().trim());
            stm.setString(5, rekening.getNamaAkunKelompok().trim());
            stm.setString(6, rekening.getKodeAkunJenis().trim());
            stm.setString(7, rekening.getNamaAkunJenis().trim());
            stm.setString(8, rekening.getKodeAkunObjek().trim());
            stm.setString(9, rekening.getNamaAkunObjek().trim());
            stm.setString(10, rekening.getKodeAkunRincian().trim());
            stm.setString(11, rekening.getNamaAkunRincian().trim());
            stm.setString(12, rekening.getKodeAkunSub());
            stm.setString(13, rekening.getNamaAkunSub());
            stm.setDouble(14, rekening.getNilaiAnggaran());
            stm.executeUpdate();
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    public void deleteRekeningAPBD(long indexKegiatan, KodeRekeningAPBD rekening, Connection conn) throws SQLException {
        PreparedStatement stm = null;
        try {
            String sql = "delete from  " + IAPBDConstants.TABLE_KODE_REKENING_APBD + " where "
                    + IAPBDConstants.ATTR_KEGIATAN_INDEX + " = ? and "
                    + IAPBDConstants.ATTR_KODE_AKUN_UTAMA + " =? and "
                    + IAPBDConstants.ATTR_KODE_AKUN_KELOMPOK + " =? and "
                    + IAPBDConstants.ATTR_KODE_AKUN_JENIS + " =? and "
                    + IAPBDConstants.ATTR_KODE_AKUN_OBJEK + " =? and "
                    + IAPBDConstants.ATTR_KODE_AKUN_RINCIAN + " =? and "
                    + IAPBDConstants.ATTR_KODE_AKUN_SUB + " =?";
            stm = conn.prepareStatement(sql);
            int col = 1;
            stm.setLong(col++, indexKegiatan);
            stm.setString(col++, rekening.getKodeAkunUtama().trim());
            stm.setString(col++, rekening.getKodeAkunKelompok().trim());
            stm.setString(col++, rekening.getKodeAkunJenis().trim());
            stm.setString(col++, rekening.getKodeAkunObjek().trim());
            stm.setString(col++, rekening.getKodeAkunRincian().trim());
            stm.setString(col++, rekening.getKodeAkunSub());
            stm.executeUpdate();
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    /*
    =================================================================
    LRA
    =================================================================
    */
    
    
    public RealisasiAPBD getRealisasiApbdStruktur(String kodeSatker, short tahun, short bulan, short kodeData, short jenisCOA, Connection conn) throws SQLException{
        PreparedStatement stm = null, stmA = null;
        ResultSet rs = null, rsA = null;
        RealisasiAPBD result = new RealisasiAPBD();
        result.setTahunAnggaran(tahun);
        result.setKodeSatker(kodeSatker);
        result.setJenisCOA(jenisCOA);
        result.setKodeData(kodeData);
        result.setPeriode(bulan);
        
        String sql = "select * from realisasiapbd where kodesatker=? and tahunanggaran=? and bulan=? and kodedata=? and jeniscoa=? ";
        try{
            stm = conn.prepareStatement(sql);
            stm.setString(1, kodeSatker);
            stm.setShort(2, tahun);
            stm.setShort(3, bulan);
            stm.setShort(4, kodeData);
            stm.setShort(5, jenisCOA);
            rs = stm.executeQuery();
            if( rs.next() ){                
                result = new RealisasiAPBD(rs.getLong("apbdindex"), kodeSatker, rs.getString("kodepemda"), rs.getString("namapemda"), tahun, bulan, kodeData, jenisCOA, rs.getShort("statusdata"), 
                        rs.getString("nomorperda"), rs.getDate("tanggalperda")==null?null:new java.util.Date(rs.getDate("tanggalperda").getTime()), rs.getString("namaaplikasi"), rs.getString("pengembangaplikasi"));
            }
            stm = conn.prepareStatement("select k.* from realisasikegiatanapbd k  where apbdindex=? order by kodeurusanprogram, kodeurusanpelaksana, kodeskpd, kodeprogram, kodekegiatan");
            stm.setLong(1, result.getIndex());
            rs = stm.executeQuery();
            List<RealisasiKegiatanAPBD> kegs = new ArrayList();
            while(rs.next()){
                long kegId = rs.getLong("kegiatanindex");
                stmA = conn.prepareStatement("select * from realisasikoderekapbd where kegiatanindex=? "
                        + " order by kodeakunutama, kodeakunkelompok, kodeakunjenis, kodeakunobjek, kodeakunrincian");
                stmA.setLong(1, kegId);
                rsA = stmA.executeQuery();
                List<RealisasiKodeRekeningAPBD> akuns = new ArrayList();
                while(rsA.next()){
                    akuns.add(new RealisasiKodeRekeningAPBD(rsA.getString("kodeakunutama"), rsA.getString("namaakunutama"), rsA.getString("kodeakunkelompok"), rsA.getString("namaakunkelompok"), rsA.getString("kodeakunjenis"), rsA.getString("namaakunjenis"), rsA.getString("kodeakunobjek"), rsA.getString("namaakunobjek"), rsA.getString("kodeakunrincian"), rsA.getString("namaakunrincian"), rsA.getString("kodeakunsub"), rsA.getString("namaakunsub"), rsA.getDouble("nilaianggaran"), kegId));
                }
                kegs.add(new RealisasiKegiatanAPBD(kegId, rs.getString("kodeurusanprogram"), rs.getString("namaurusanprogram"), rs.getString("kodeurusanpelaksana"), rs.getString("namaurusanpelaksana"), rs.getString("kodeskpd"), rs.getString("namaskpd"), rs.getString("kodeprogram"), rs.getString("namaprogram"), rs.getString("kodekegiatan"), rs.getString("namakegiatan"), rs.getString("kodeFungsi"), rs.getString("namafungsi")));
                kegs.get(kegs.size()-1).setKodeRekenings(akuns);
            }
            result.setKegiatans(kegs);
            return result;
        } catch (SQLException ex){            
            Logger.getLogger(DMSQL.class.getName()).log(Level.SEVERE, null, ex);
            throw new SQLException(ex.getMessage());
        } 
        finally{
            if( rs != null ) rs.close();
            if( rsA!=null ) rsA.close();
            if( stm!=null ) stm.close();
            if( stmA!=null ) stmA.close();
        }        
    }
    
    public RealisasiAPBD getRealisasiApbdHeader(String kodeSatker, short tahun, short bulan, short kodeData, short jenisCOA, Connection conn) throws SQLException{
        PreparedStatement stm = null;
        ResultSet rs = null;
        RealisasiAPBD result = new RealisasiAPBD();
        result.setTahunAnggaran(tahun);
        result.setKodeSatker(kodeSatker);
        result.setJenisCOA(jenisCOA);
        result.setKodeData(kodeData);
        result.setPeriode(bulan);
        
        String sql = "select * from realisasiapbd where kodesatker=? and tahunanggaran=? and bulan=? and kodedata=? and jeniscoa=? ";
        try{
            stm = conn.prepareStatement(sql);
            stm.setString(1, kodeSatker);
            stm.setShort(2, tahun);
            stm.setShort(3, bulan);
            stm.setShort(4, kodeData);
            stm.setShort(5, jenisCOA);
            rs = stm.executeQuery();
            if( rs.next() ){                
                result = new RealisasiAPBD(rs.getLong("apbdindex"), kodeSatker, rs.getString("kodepemda"), rs.getString("namapemda"), tahun, bulan, kodeData, jenisCOA, rs.getShort("statusdata"), 
                        rs.getString("nomorperda"), rs.getDate("tanggalperda")==null?null:new java.util.Date(rs.getDate("tanggalperda").getTime()), rs.getString("namaaplikasi"), rs.getString("pengembangaplikasi"));
            }
            return result;
        } catch (SQLException ex){            
            Logger.getLogger(DMSQL.class.getName()).log(Level.SEVERE, null, ex);
            throw new SQLException(ex.getMessage());
        } 
        finally{
            if( rs != null ) rs.close();
            if( stm!=null ) stm.close();
        }        
    }
    
    public RealisasiAPBD getRealisasiApbdHeader(String kodeSatker, short tahun, short bulan, short jenisCOA, Connection conn) throws SQLException{
        PreparedStatement stm = null;
        ResultSet rs = null;
        RealisasiAPBD result = new RealisasiAPBD();
        result.setTahunAnggaran(tahun);
        result.setKodeSatker(kodeSatker);
        result.setJenisCOA(jenisCOA);
//        result.setKodeData(kodeData);
        result.setPeriode(bulan);
        
        String sql = "select * from realisasiapbd where kodesatker=? and tahunanggaran=? and bulan=? and jeniscoa=? order by tglpengiriman desc limit 1";
        try{
            stm = conn.prepareStatement(sql);
            stm.setString(1, kodeSatker);
            stm.setShort(2, tahun);
            stm.setShort(3, bulan);
//            stm.setShort(4, kodeData);
            stm.setShort(4, jenisCOA);
            rs = stm.executeQuery();
            if( rs.next() ){                
                result = new RealisasiAPBD(rs.getLong("apbdindex"), kodeSatker, rs.getString("kodepemda"), rs.getString("namapemda"), tahun, bulan, rs.getShort("kodedata"), jenisCOA, rs.getShort("statusdata"), 
                        rs.getString("nomorperda"), rs.getDate("tanggalperda")==null?null:new java.util.Date(rs.getDate("tanggalperda").getTime()), rs.getString("namaaplikasi"), rs.getString("pengembangaplikasi"));
            }
            return result;
        } catch (SQLException ex){            
            Logger.getLogger(DMSQL.class.getName()).log(Level.SEVERE, null, ex);
            throw new SQLException(ex.getMessage());
        } 
        finally{
            if( rs != null ) rs.close();
            if( stm!=null ) stm.close();
        }        
    }

    @Override
    public RealisasiAPBD createRealiasiAPBDHeader(RealisasiAPBD apbd, short iotype, Connection conn) throws SQLException {
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {            
            String sql = "with zz as ( ";
            sql = sql +
                    "insert into " + IAPBDConstants.TABLE_REALISASI_APBD + "("
                    + IAPBDConstants.ATTR_KODE_SATKER + ", "
                    + IAPBDConstants.ATTR_KODE_PEMDA + ", "
                    + IAPBDConstants.ATTR_NAMA_PEMDA + ", "
                    + IAPBDConstants.ATTR_TAHUN_ANGGARAN + ", "
                    + IAPBDConstants.ATTR_BULAN + ", "
                    + IAPBDConstants.ATTR_TANGGAL_PENGIRIMAN + ", "
                    + IAPBDConstants.ATTR_KODE_DATA 
                    + ", statusdata,jeniscoa,nomorperda,tanggalperda,namaaplikasi,pengembangaplikasi, iotype ) "
                    + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
            sql = sql + " returning * ) select max(apbdindex) as index from zz";            
            stm = conn.prepareStatement(sql);
            stm.setString(1, apbd.getKodeSatker().trim());
            stm.setString(2, apbd.getKodePemda().trim());
            stm.setString(3, apbd.getNamaPemda().trim());
            stm.setShort(4, apbd.getTahunAnggaran());
            stm.setShort(5, apbd.getPeriode());
//            java.sql.Date da = new java.sql.Date(new java.util.Date().getTime());
            Timestamp time = new Timestamp(new java.util.Date().getTime());
            stm.setTimestamp(6, time);
            stm.setShort(7, apbd.getKodeData());
            stm.setShort(8, apbd.getStatusData());
            stm.setShort(9, apbd.getJenisCOA());
            stm.setString(10, apbd.getNomorPerda());
            if(apbd.getTanggalPerda()!=null)
            stm.setDate(11, new java.sql.Date(apbd.getTanggalPerda().getTime()));
            else stm.setNull(11, Types.DATE);
            stm.setString(12, apbd.getNamaAplikasi());
            stm.setString(13, apbd.getPengembangAplikasi());
            stm.setShort(14, iotype);
            rs = stm.executeQuery();
            
            if( rs.next()){
                apbd.setIndex(rs.getLong("index"));
            }
            return apbd;
        } catch (SQLException ex) {
            Logger.getLogger(DMSQL.class.getName()).log(Level.SEVERE, null, ex);
            throw new SQLException(ex.getMessage());
        } finally {
            if(rs!=null) rs.close();
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    @Override
    public RealisasiAPBD updateRealisasiAPBDHeader(RealisasiAPBD apbd, Connection conn) throws SQLException {
        PreparedStatement stm = null;
        try {
            String sql = "update " + IAPBDConstants.TABLE_REALISASI_APBD + " set "
                    + IAPBDConstants.ATTR_KODE_SATKER + "=?, "
                    + IAPBDConstants.ATTR_KODE_PEMDA + "=?, "
                    + IAPBDConstants.ATTR_NAMA_PEMDA + "=?, "
                    + IAPBDConstants.ATTR_TAHUN_ANGGARAN + "=?, bulan=?, "
                    + IAPBDConstants.ATTR_TANGGAL_PENGIRIMAN + "=?, "
                    + IAPBDConstants.ATTR_KODE_DATA + "=?, statusdata= ?, "
                    + IAPBDConstants.ATTR_JENIS_COA + "=?, nomorperda= ?, tanggalperda=?, namaaplikasi=?, pengembangaplikasi=? "
                    + " WHERE "
                    + IAPBDConstants.ATTR_APBD_INDEX + "=?";
            
            stm = conn.prepareStatement(sql);
            stm.setString(1, apbd.getKodeSatker().trim());
            stm.setString(2, apbd.getKodePemda().trim());
            stm.setString(3, apbd.getNamaPemda().trim());
            stm.setShort(4, apbd.getTahunAnggaran());
            stm.setShort(5, apbd.getPeriode());
            Timestamp time = new Timestamp(new java.util.Date().getTime());
            stm.setTimestamp(6, time);
            stm.setShort(7, apbd.getKodeData());
            stm.setShort(8, apbd.getStatusData());
            stm.setShort(9, apbd.getJenisCOA());
            stm.setString(10, apbd.getNomorPerda());
            if( apbd.getTanggalPerda() != null )
            stm.setDate(11, new java.sql.Date(apbd.getTanggalPerda().getTime()));
            else stm.setNull(11, Types.DATE);
            stm.setString(12, apbd.getNamaAplikasi());
            stm.setString(13, apbd.getPengembangAplikasi());
            stm.setLong(14, apbd.getIndex());
            stm.executeUpdate();
            return apbd;
        } catch (SQLException ex) {
            Logger.getLogger(DMSQL.class.getName()).log(Level.SEVERE, null, ex);
            throw new SQLException(ex.getMessage());
        } finally {
            if (stm != null) {
                stm.close();
            }
        }
    }

    @Override
    public void deleteRealisasiAPBD(long id, Connection conn) throws SQLException {
        PreparedStatement stm = null;
        try {
            String sql = "delete from " + IAPBDConstants.TABLE_REALISASI_APBD + " where apbdindex=? ";
            stm = conn.prepareStatement(sql);
            stm.setLong(1, id);
            stm.executeUpdate();
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    
    public RealisasiKegiatanAPBD createRealisasiKegiatanAPBD(long indexApbd, RealisasiKegiatanAPBD kegiatan, Connection conn) throws SQLException {
        PreparedStatement pstm = null;
        ResultSet rs = null;
        try {
            String sql = "with zz as ( ";
            sql = sql + 
                    "insert into " + IAPBDConstants.TABLE_REALISASI_KEGIATAN_APBD + "("
                    + IAPBDConstants.ATTR_APBD_INDEX + ", "
                    + IAPBDConstants.ATTR_KODE_URUSAN_PROGRAM + ", "
                    + IAPBDConstants.ATTR_NAMA_URUSAN_PROGRAM + ", "
                    + IAPBDConstants.ATTR_KODE_URUSAN_PELAKSANA + ", "
                    + IAPBDConstants.ATTR_NAMA_URUSAN_PELAKSANA + ", "
                    + IAPBDConstants.ATTR_KODE_SKPD + ", "
                    + IAPBDConstants.ATTR_NAMA_SKPD + ", "
                    + IAPBDConstants.ATTR_KODE_PROGRAM + ", "
                    + IAPBDConstants.ATTR_NAMA_PROGRAM + ", "
                    + IAPBDConstants.ATTR_KODE_KEGIATAN + ", "
                    + IAPBDConstants.ATTR_NAMA_KEGIATAN + ", "
                    + IAPBDConstants.ATTR_KODE_FUNGSI + ", "
                    + IAPBDConstants.ATTR_NAMA_FUNGSI + ") "
                    + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
            sql = sql + " returning * ) select max(kegiatanindex) as index from zz";
            pstm = conn.prepareStatement(sql);
            pstm.setLong(1, indexApbd);
            pstm.setString(2, kegiatan.getKodeUrusanProgram().trim());
            pstm.setString(3, kegiatan.getNamaUrusanProgram().trim());
            pstm.setString(4, kegiatan.getKodeUrusanPelaksana().trim());
            pstm.setString(5, kegiatan.getNamaUrusanPelaksana().trim());
            pstm.setString(6, kegiatan.getKodeSKPD().trim());
            pstm.setString(7, kegiatan.getNamaSKPD().trim());
            pstm.setString(8, kegiatan.getKodeProgram().trim());
            pstm.setString(9, kegiatan.getNamaProgram().trim());
            pstm.setString(10, kegiatan.getKodeKegiatan().trim());
            pstm.setString(11, kegiatan.getNamaKegiatan().trim());
            pstm.setString(12, kegiatan.getKodeFungsi().trim());
            pstm.setString(13, kegiatan.getNamaFungsi().trim());
            rs= pstm.executeQuery();
            if(rs.next()) kegiatan.setIndex(rs.getLong("index"));
            return kegiatan;
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        }
        finally{
            if(rs!=null) rs.close();
            if( pstm!= null ) pstm.close();
        }
    }
    
    
    public RealisasiKegiatanAPBD updateRealisasiKegiatanAPBD(RealisasiKegiatanAPBD kegiatan, Connection conn) throws SQLException {
        PreparedStatement pstm = null;
        ResultSet rs = null;
        try {
            String sql = "update " + IAPBDConstants.TABLE_REALISASI_KEGIATAN_APBD + " set "
                    + IAPBDConstants.ATTR_KODE_URUSAN_PROGRAM + "=?, "
                    + IAPBDConstants.ATTR_NAMA_URUSAN_PROGRAM + "=?, "
                    + IAPBDConstants.ATTR_KODE_URUSAN_PELAKSANA + "=?, "
                    + IAPBDConstants.ATTR_NAMA_URUSAN_PELAKSANA + "=?, "
                    + IAPBDConstants.ATTR_KODE_SKPD + "=?, "
                    + IAPBDConstants.ATTR_NAMA_SKPD + "=?, "
                    + IAPBDConstants.ATTR_KODE_PROGRAM + "=?, "
                    + IAPBDConstants.ATTR_NAMA_PROGRAM + "=?, "
                    + IAPBDConstants.ATTR_KODE_KEGIATAN + "=?, "
                    + IAPBDConstants.ATTR_NAMA_KEGIATAN + "=?, "
                    + IAPBDConstants.ATTR_KODE_FUNGSI + "=?, "
                    + IAPBDConstants.ATTR_NAMA_FUNGSI + "=? where kegiatanindex=?";
            int col=1;
            pstm = conn.prepareStatement(sql);
            pstm.setString(col++, kegiatan.getKodeUrusanProgram().trim());
            pstm.setString(col++, kegiatan.getNamaUrusanProgram().trim());
            pstm.setString(col++, kegiatan.getKodeUrusanPelaksana().trim());
            pstm.setString(col++, kegiatan.getNamaUrusanPelaksana().trim());
            pstm.setString(col++, kegiatan.getKodeSKPD().trim());
            pstm.setString(col++, kegiatan.getNamaSKPD().trim());
            pstm.setString(col++, kegiatan.getKodeProgram().trim());
            pstm.setString(col++, kegiatan.getNamaProgram().trim());
            pstm.setString(col++, kegiatan.getKodeKegiatan().trim());
            pstm.setString(col++, kegiatan.getNamaKegiatan().trim());
            pstm.setString(col++, kegiatan.getKodeFungsi().trim());
            pstm.setString(col++, kegiatan.getNamaFungsi().trim());
            pstm.setLong(col++, kegiatan.getIndex());
            pstm.executeUpdate();
            return kegiatan;
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        }
        finally{
            if(rs!=null) rs.close();
            if( pstm!= null ) pstm.close();
        }
    }
    
    
    @Override
    public void deleteRealisasiKegiatanAPBD(long id, Connection conn) throws SQLException {
        PreparedStatement stm = null;
        try {
            String sql = "delete from " + IAPBDConstants.TABLE_REALISASI_KEGIATAN_APBD + " where kegiatanindex=?";
            stm = conn.prepareStatement(sql);
            stm.setLong(1, id);
            stm.executeUpdate();
            
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        }
        finally{
            if( stm!= null ) stm.close();
        }
    }

    @Override
    public void createRealisasiRekeningAPBD(long indexKegiatan, RealisasiKodeRekeningAPBD rekening, Connection conn) throws SQLException {
        PreparedStatement stm = null;
        try {
            String sql = "insert into " + IAPBDConstants.TABLE_REALISASI_KODE_REKENING_APBD + "("
                    + IAPBDConstants.ATTR_KEGIATAN_INDEX + ", "
                    + IAPBDConstants.ATTR_KODE_AKUN_UTAMA + ", "
                    + IAPBDConstants.ATTR_NAMA_AKUN_UTAMA + ", "
                    + IAPBDConstants.ATTR_KODE_AKUN_KELOMPOK + ", "
                    + IAPBDConstants.ATTR_NAMA_AKUN_KELOMPOK + ", "
                    + IAPBDConstants.ATTR_KODE_AKUN_JENIS + ", "
                    + IAPBDConstants.ATTR_NAMA_AKUN_JENIS + ", "
                    + IAPBDConstants.ATTR_KODE_AKUN_OBJEK + ", "
                    + IAPBDConstants.ATTR_NAMA_AKUN_OBJEK + ", "
                    + IAPBDConstants.ATTR_KODE_AKUN_RINCIAN + ", "
                    + IAPBDConstants.ATTR_NAMA_AKUN_RINCIAN + ", "
                    + IAPBDConstants.ATTR_KODE_AKUN_SUB + ", "
                    + IAPBDConstants.ATTR_NAMA_AKUN_SUB + ", "
                    + IAPBDConstants.ATTR_NILAI_ANGGARAN + ") "
                    + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
            stm = conn.prepareStatement(sql);
            stm.setLong(1, indexKegiatan);
            stm.setString(2, rekening.getKodeAkunUtama().trim());
            stm.setString(3, rekening.getNamaAkunUtama().trim());
            stm.setString(4, rekening.getKodeAkunKelompok().trim());
            stm.setString(5, rekening.getNamaAkunKelompok().trim());
            stm.setString(6, rekening.getKodeAkunJenis().trim());
            stm.setString(7, rekening.getNamaAkunJenis().trim());
            stm.setString(8, rekening.getKodeAkunObjek().trim());
            stm.setString(9, rekening.getNamaAkunObjek().trim());
            stm.setString(10, rekening.getKodeAkunRincian().trim());
            stm.setString(11, rekening.getNamaAkunRincian().trim());
            stm.setString(12, rekening.getKodeAkunSub());
            stm.setString(13, rekening.getNamaAkunSub());
            stm.setDouble(14, rekening.getNilaiAnggaran());
            stm.executeUpdate();
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    public void deleteRealisasiRekeningAPBD(long indexKegiatan, RealisasiKodeRekeningAPBD rekening, Connection conn) throws SQLException {
        PreparedStatement stm = null;
        try {
            String sql = "delete from  " + IAPBDConstants.TABLE_REALISASI_KODE_REKENING_APBD + " where "
                    + IAPBDConstants.ATTR_KEGIATAN_INDEX + " = ? and "
                    + IAPBDConstants.ATTR_KODE_AKUN_UTAMA + " =? and "
                    + IAPBDConstants.ATTR_KODE_AKUN_KELOMPOK + " =? and "
                    + IAPBDConstants.ATTR_KODE_AKUN_JENIS + " =? and "
                    + IAPBDConstants.ATTR_KODE_AKUN_OBJEK + " =? and "
                    + IAPBDConstants.ATTR_KODE_AKUN_RINCIAN + " =? and "
                    + IAPBDConstants.ATTR_KODE_AKUN_SUB + " =?";
            stm = conn.prepareStatement(sql);
            int col = 1;
            stm.setLong(col++, indexKegiatan);
            stm.setString(col++, rekening.getKodeAkunUtama().trim());
            stm.setString(col++, rekening.getKodeAkunKelompok().trim());
            stm.setString(col++, rekening.getKodeAkunJenis().trim());
            stm.setString(col++, rekening.getKodeAkunObjek().trim());
            stm.setString(col++, rekening.getKodeAkunRincian().trim());
            stm.setString(col++, rekening.getKodeAkunSub());
            stm.executeUpdate();
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if (stm != null) {
                stm.close();
            }
        }
    }
}