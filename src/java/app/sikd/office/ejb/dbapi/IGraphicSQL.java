/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.office.ejb.dbapi;

import app.sikd.entity.backoffice.NilaiFungsiPemda;
import app.sikd.entity.graph.GraphGlobal;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author sora
 */
public interface IGraphicSQL {
    public List<GraphGlobal> getPadGraph(short year, short kodeData, short jenisCOA, List<String> kodePemdas, Connection conn) throws SQLException ;
    public List<GraphGlobal> getFiskalGraph(short year, short kodeData, short jenisCOA, List<String> kodePemdas, Connection conn) throws SQLException ;
    public List<GraphGlobal> getDanaBelanjaPengeluaranGraph(short year, short kodeData, short jenisCOA, List<String> kodePemdas, Connection conn) throws SQLException ;
    public List<GraphGlobal> getTingkatBelanjaModalGraph(short year, short kodeData, short jenisCOA, List<String> kodePemdas, Connection conn) throws SQLException ;
    public List<GraphGlobal> getTingkatBelanjaPegawaiTdkLangsungGraph(short year, short kodeData, short jenisCOA, List<String> kodePemdas, Connection conn) throws SQLException ;
    
    public List<GraphGlobal> getTop10Fungsi(short year, short kodeData, short jenisCOA, String kodeFungsi, Connection conn) throws SQLException;
    
    public List<NilaiFungsiPemda> getNilaiFungsiPemda(short year, String kodeSatker, short kodeData, short jenisCOA, String kodeFungsi, Connection conn) throws SQLException;
    public List<NilaiFungsiPemda> getKompilasiNilaiFungsiPemda(short year, List<String> kodePemdas, short kodeData, short jenisCOA, String kodeFungsi, Connection conn) throws SQLException;
    
    public int getJumlahDaerahKirimRealisasi(short year, short kodeData, short jenisCOA, Connection conn) throws SQLException;
    
}


