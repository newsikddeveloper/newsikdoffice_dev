/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.office.ejb.dbapi;

import app.sikd.entity.utilitas.SimpleAccount;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author sora
 */
public interface IGfsSQL {
    public List<SimpleAccount> getGfsAkunStruktur( Connection conn) throws SQLException;
    
}
