/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.sikd.office.ejb.dbapi;

import app.sikd.entity.apbd.LampiranIIIPerdaAPBD;
import app.sikd.entity.apbd.LampiranIIPerdaAPBD;
import app.sikd.entity.apbd.LampiranILra;
import app.sikd.entity.apbd.LampiranIPerdaAPBD;
import app.sikd.entity.apbd.LampiranIVPerdaAPBD;
import app.sikd.entity.apbd.LampiranVPerdaAPBD;
import app.sikd.entity.backoffice.APBDPerda;
import app.sikd.entity.backoffice.DTH;
import app.sikd.entity.backoffice.DataDaerah;
import app.sikd.entity.backoffice.DataDaerahDetail;
import app.sikd.entity.backoffice.NilaiFungsiPemda;
import app.sikd.entity.backoffice.PDRB;
import app.sikd.entity.backoffice.PDRBHarga;
import app.sikd.entity.backoffice.PerhitunganFihakKetiga;
import app.sikd.entity.backoffice.PinjamanDaerah;
import app.sikd.entity.backoffice.RTH;
import app.sikd.entity.backoffice.RealisasiAPBD;
import app.sikd.entity.backoffice.RealisasiKegiatanAPBD;
import app.sikd.entity.backoffice.RealisasiKodeRekeningAPBD;
import app.sikd.entity.backoffice.RekapitulasiGajiPegawai;
import app.sikd.entity.backoffice.RincianBPHTB;
import app.sikd.entity.backoffice.RincianHiburan;
import app.sikd.entity.backoffice.RincianHotel;
import app.sikd.entity.backoffice.RincianIMB;
import app.sikd.entity.backoffice.RincianIzinUsaha;
import app.sikd.entity.backoffice.RincianKendaraan;
import app.sikd.entity.backoffice.RincianPajakDanRetribusi;
import app.sikd.entity.backoffice.RincianPerhitunganFihakKetiga;
import app.sikd.entity.backoffice.RincianPinjamanDaerah;
import app.sikd.entity.backoffice.RincianRestoran;
import app.sikd.entity.utilitas.MappingAccount;
import app.sikd.entity.utilitas.MappingNonStandar;
import app.sikd.entity.utilitas.SimpleAccount;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

/**
 *
 * @author detra
 */
public interface IOfficeSQL {
    public APBDPerda getAPBDPerdaNumber(short year, String kodeSatker, short kodeData, short jenisCOA, Connection conn) throws SQLException;
    public APBDPerda getLRAPerdaNumber(short year, String kodeSatker, short kodeData, short jenisCOA, Connection conn) throws SQLException;

    public List<LampiranIPerdaAPBD> getLampiranIPerdaAPBDbyYear(short year, String kodeSatker, short kodeData, short jenisCOA, Connection conn) throws SQLException;
    public List<LampiranIIPerdaAPBD> getLampiranIIPerdaAPBDbyYear(short year, String kodeSatker, short kodeData, short jenisCOA, Connection conn) throws SQLException;
    public List<LampiranIIIPerdaAPBD> getLampiranIIIPerdaAPBD(short year, String kodeSatker, short kodeData, String kodeSKPD, short jenisCOA, Connection conn) throws SQLException;
    public List<LampiranIVPerdaAPBD> getLampiranIVPerdaAPBD(short year, String kodeSatker, short kodeData, short jenisCOA, Connection conn) throws SQLException;
    public List<LampiranVPerdaAPBD> getLampiranVPerdaAPBD(short year, String kodeSatker, short kodeData, short jenisCOA, Connection conn) throws SQLException;
    public List<LampiranIPerdaAPBD> getRingkasanPembiayaan(short year, String kodeSatker, short kodeData, short jenisCOA, Connection conn) throws SQLException;
    public List<LampiranIPerdaAPBD> getRingkasanPembiayaan(short year, List<String> kodepemdas, short kodeData, short jenisCOA, Connection conn) throws SQLException;
    public List<String> getDinasKirimAPBD(short year, String kodeSatker, short kodeData, short jenisCOA, Connection conn) throws SQLException;
    public Timestamp getTanggalDinasKirimAPBD(short year, String kodeSatker, short kodeData, short jenisCOA, Connection conn) throws SQLException ;
    
    public List<LampiranIPerdaAPBD> getKompilasiLampiranIPerdaAPBD(short year, List<String> kodePemdas, short kodeData, short jenisCOA, Connection conn) throws SQLException;
    public List<LampiranIIPerdaAPBD> getKompilasiLampiranIIPerdaAPBD(short year, List<String> kodePemdas, short kodeData, short jenisCOA, Connection conn) throws SQLException;

    public Timestamp getTanggalDinasKirimRealisasiAPBD(short year, String kodeSatker, short kodeData, short jenisCOA, Connection conn) throws SQLException;
    public List<String> getDinasKirimRealisasiAPBD(short year, String kodeSatker, short kodeData, short jenisCOA, Connection conn) throws SQLException;

    public String getUrusan(String kodeUrusan, Connection conn) throws SQLException;

    public List<LampiranILra> getRealisasiAPBD(short year, String kodeSatker, short month, short jenisCOA, Connection conn) throws SQLException;
    public List<LampiranILra> getRealisasiAPBD(short year, String kodeSatker, short kodeData, short month, short jenisCOA, Connection conn) throws SQLException;
    public List<LampiranIPerdaAPBD> getKompilasiRealisasiAPBD(short year, List<String> kodePemdas, short kodeData, short month, short jenisCOA, Connection conn) throws SQLException;
    
    public List<RekapitulasiGajiPegawai> getRekapitulasiGajiPegawai(short year, String kodeSatker, short month, Connection conn) throws SQLException ;

    public PerhitunganFihakKetiga getPFK(short year, String kodeSatker, Connection conn) throws SQLException;
    public long getPFKIndex(short year, String kodeSatker, Connection conn) throws SQLException;
    public List<RincianPerhitunganFihakKetiga> getRincianPFK(short year, String kodeSatker, Connection conn) throws SQLException;
    public List<RincianPerhitunganFihakKetiga> getRincianPFK(long indexPFK, Connection conn) throws SQLException;
    public long createPFK(PerhitunganFihakKetiga pfk, short iotype, Connection conn) throws SQLException;
    public void createRincianPFK(long indexPFK, RincianPerhitunganFihakKetiga rpfk, Connection conn) throws SQLException;
    public void deletePFK(PerhitunganFihakKetiga pfk, Connection conn) throws SQLException;
    public void deleteRincianPFK(long indexPFK, RincianPerhitunganFihakKetiga rpfk, Connection conn) throws SQLException;
    public void deleteRincianPFK(long indexPFK, Connection conn) throws SQLException;
    
    public long createPinjamanDaerah(PinjamanDaerah pinjaman, short iotype, Connection conn) throws SQLException;
    public void updatePinjamanDaerah(PinjamanDaerah pinjaman, long pinjamanIndex, Connection conn) throws SQLException;
    public void deletePinjamanDaerah(String kodeSatker, short thn, Connection conn) throws SQLException;
    public long getPinjamanDaerahIndex(String kodeSatker, short thn, Connection conn) throws SQLException;
    public String createRincianPinjamanDaerah(RincianPinjamanDaerah rincian, long indexPinjaman, Connection conn) throws SQLException;
    public void deleteRincianPinjamanDaerah(String kodeSatker, short thn, RincianPinjamanDaerah rincian, Connection conn) throws SQLException;
    public void updateRincianPinjamanDaerah(String kodeSatker, short thn, RincianPinjamanDaerah oldRincian, RincianPinjamanDaerah newRincian, Connection conn) throws SQLException;    
    public List<RincianPinjamanDaerah> getRincianPinjamanDaerahs(String kodeSatker, short thn, Connection conn) throws SQLException;
    
    public DTH getDTHReport(short year, String kodeSatker, short periode, String kodeSKPD, Connection conn) throws SQLException;
    public List<String> getDinasKirimDTH(short year, String kodeSatker, Connection conn) throws SQLException;
    public List<RTH> getRTHReport(short year, String kodeSatker, short periode, Connection conn) throws SQLException;
    
    public long createDataDaerah(DataDaerah obj, Connection conn) throws SQLException;
    public long getDataDaerahIndex(String kodeSatker, short tahun, Connection conn) throws SQLException;
    public long createDataDaerahDetail(DataDaerahDetail obj, long dataDaerahIndex, Connection conn) throws SQLException;
    public void updateDataDaerahDetail(DataDaerahDetail obj, long dataDaerahIndex, Connection conn) throws SQLException;
    public void deleteDataDaerah(String kodeSatker, short tahun, Connection conn) throws SQLException;
    public DataDaerahDetail getDataDaerahDetail(long dataDaerahIndex, Connection conn) throws SQLException;
    public long getDataDaerahDetailIndex(long dataDaerahIndex, Connection conn) throws SQLException;

    
    public long createPDRB(PDRB obj, Connection conn) throws SQLException;
    public long getPDRBIndex(String kodeSatker, short tahun, Connection conn) throws SQLException;
    public void deletePDRB(String kodeSatker, short tahun, Connection conn) throws SQLException;
    public long createPDRBHargaBerlaku(PDRBHarga obj, long pdrbIndex, Connection conn) throws SQLException;
    public long createPDRBHargaKonstant(PDRBHarga obj, long pdrbIndex, Connection conn) throws SQLException;
    public PDRBHarga getPDRBHargaKonstan(String kodeSatker, short tahun, Connection conn) throws SQLException;
    public PDRBHarga getPDRBHargaBerlaku(String kodeSatker, short tahun, Connection conn) throws SQLException;
    
    public List<RincianKendaraan> getRincianKendaraan(String kodeSatker, short tahun, Connection conn) throws SQLException;
    public List<RincianBPHTB> getRincianBPHTB(String kodeSatker, short tahun, Connection conn) throws SQLException;
    public List<RincianHiburan> getRincianHiburan(String kodeSatker, short tahun, Connection conn) throws SQLException;
    public List<RincianHotel> getRincianHotel(String kodeSatker, short tahun, Connection conn) throws SQLException;
    public List<RincianIMB> getRincianIMB(String kodeSatker, short tahun, Connection conn) throws SQLException ;
    public List<RincianRestoran> getRincianRestoran(String kodeSatker, short tahun, Connection conn) throws SQLException;
    public List<RincianIzinUsaha> getRincianIzinUsaha(String kodeSatker, short tahun, Connection conn) throws SQLException;
    
    public List<RincianPajakDanRetribusi> getRincianPajakDanRetribusi(String kodeSatker, short tahun, Connection conn) throws SQLException;
    
    public List<NilaiFungsiPemda> getRealisasiKegiatanBerdasarkanFungsi(short tahun, short bulan, String kodeSatker, short kodeData, String kodeFungsi, Connection conn) throws SQLException;
    public List<NilaiFungsiPemda> getRealisasiBelanjaBerdasarkanSumberDana(short tahun, short bulan, String kodeSatker, short sumberDana, Connection conn) throws SQLException;
    
    public List<LampiranIPerdaAPBD> getLampiranIPenjabaranAPBDbyYear(short year, String kodeSatker, short kodeData, short jenisCOA, Connection conn) throws SQLException;
    
    public void deleteMappingAccounts(MappingAccount ma, Connection conn) throws SQLException;
    public void createMappingAccounts(MappingAccount ma, Connection conn) throws SQLException;
    public List<MappingAccount> getMappingAccounts(short level, Connection conn) throws SQLException;
    public List<SimpleAccount> getAccrualAccounts(short[] level, Connection conn) throws SQLException;
    
    public void deleteGfsMappingAccounts(MappingAccount ma, Connection conn) throws SQLException;
    public void createGfsMappingAccounts(MappingAccount ma, Connection conn) throws SQLException;
    public List<MappingAccount> getGfsMappingAccounts(short[] levels, Connection conn) throws SQLException;
    public List<SimpleAccount> getGfsAccounts(short[] level, Connection conn) throws SQLException;
    
    
    public RealisasiAPBD getLRA(String kodeSatker, short tahun , short bulan, short kodeData, short jenisCOA, String userName, String password, Connection conn) throws SQLException;
    public List<RealisasiKegiatanAPBD> getLRAKegiatan(long indexLRA, Connection conn) throws SQLException;
    public List<RealisasiKodeRekeningAPBD> getLRAAkun(long indexKegiatanLRA, Connection conn) throws SQLException;
    
    public List<MappingNonStandar> getMappingNonStandarAccountApbds(short level, short tahun, String kodeSatker, Connection conn) throws SQLException;
    public List<MappingNonStandar> getMappingNonStandarAccountLras(short level, short tahun, String kodeSatker, Connection conn) throws SQLException;
    public List<MappingNonStandar> getMappingNonStandarAccountDths(short level, short tahun, String kodeSatker, Connection conn) throws SQLException;
    public List<MappingNonStandar> getMappingNonStandarAccountNeracas(short level, short tahun, String kodeSatker, Connection conn) throws SQLException;
    public MappingNonStandar createMappingAccountNonStandars(MappingNonStandar ma, Connection conn) throws SQLException;
    public void deleteMappingAccountNonStandars(MappingNonStandar ma, Connection conn) throws SQLException;
}
