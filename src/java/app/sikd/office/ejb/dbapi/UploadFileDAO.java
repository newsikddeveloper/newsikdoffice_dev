/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.office.ejb.dbapi;

import app.sikd.entity.backoffice.UploadFilePDF;
import app.sikd.entity.utilitas.UploadFile;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Dasep
 */
public class UploadFileDAO implements IUploadFileDAO{
     
    @Override
    public void addUploadFile(UploadFile uploadFile, String tableName, Connection sikdConn) throws SQLException{
             PreparedStatement preparedStatement = sikdConn
                     .prepareStatement("insert into " + tableName.trim() 
                             + " (kodesatker, tahunanggaran, ikd, filename, tglupload) values (?, ?, ?, ?, ? )");
             preparedStatement.setString(1, uploadFile.getKdsatker().trim());
             preparedStatement.setShort(2, uploadFile.getTahun());
             preparedStatement.setString(3, uploadFile.getIkd());
             preparedStatement.setString(4, uploadFile.getFilename());
             preparedStatement.setTimestamp(5, new Timestamp(uploadFile.getTglupload().getTime()));
             preparedStatement.executeUpdate();
 
       
     }
    
    @Override
     public void deleteUploadFile(String tableName, String kodeSatker, short tahun, String ikd, Connection sikdConn) throws SQLException {
             PreparedStatement preparedStatement = sikdConn
                     .prepareStatement("delete from " + tableName.trim() + " where kodeSatker=? and tahunanggaran=? and ikd = ?");
             preparedStatement.setString(1, kodeSatker);
             preparedStatement.setShort(2, tahun);
             preparedStatement.setString(3, ikd);
             preparedStatement.executeUpdate();
     }
 
    @Override
     public void deleteUploadFile(long Id, String tableName, Connection sikdConn) throws SQLException {
             PreparedStatement preparedStatement = sikdConn
                     .prepareStatement("delete from " + tableName.trim() + " where id=?");
             preparedStatement.setLong(1, Id);
             preparedStatement.executeUpdate();
     }
 
    @Override
     public void updateUploadFile(UploadFile uploadFile, String tableName, Connection sikdConn) throws SQLException {
             PreparedStatement preparedStatement = sikdConn
                     .prepareStatement("update " + tableName.trim() + " set filename=?, tglupload=? " +
                             "where id=?");
             
             preparedStatement.setString(1, uploadFile.getFilename());
             preparedStatement.setTimestamp(2, new Timestamp(uploadFile.getTglupload().getTime()));
             preparedStatement.setLong(3, uploadFile.getId());
             preparedStatement.executeUpdate();
 
         
     }
 
    @Override
     public List<UploadFile> getAllUploadFiles(String tableName, String kodeSatker, short tahun, Connection sikdConn) throws SQLException {
         List<UploadFile> uploadFiles = new ArrayList<>();
             Statement statement = sikdConn.createStatement();
             String sql = "select * from " + tableName.trim();
             
                 sql = sql +  " where kodesatker='" + kodeSatker + "' and tahunanggaran="+tahun;
             
             ResultSet rs = statement.executeQuery( sql );
             while (rs.next()) {
                 
                 uploadFiles.add(new UploadFile(rs.getLong("id"), rs.getShort("tahunanggaran")
                         , rs.getString("ikd"), rs.getString("kodeSatker"), rs.getString("filename"), rs.getTimestamp("tglupload")));
             }  
 
         return uploadFiles;
     }
     
     
     @Override
    public void addPdfUploadFile(UploadFilePDF uploadFile, String tableName, Connection sikdConn) throws SQLException{
             PreparedStatement preparedStatement = sikdConn
                     .prepareStatement("insert into " + tableName.trim() + " (kdsatker, filename, tglupload ) values (?, ?, ?)");
             preparedStatement.setString(1, uploadFile.getKdsatker());
             preparedStatement.setString(2, uploadFile.getFilename());
             preparedStatement.setTimestamp(3, new Timestamp(uploadFile.getTglupload().getTime()));

             preparedStatement.executeUpdate();
     }
 
    @Override
     public void deletePdfUploadFile(long id, String tableName, Connection sikdConn) throws SQLException {
             PreparedStatement preparedStatement = sikdConn
                     .prepareStatement("delete from " + tableName.trim() + " where id=?");
             preparedStatement.setLong(1, id);
             preparedStatement.executeUpdate();
     }   
 
    @Override
     public List<UploadFilePDF> getAllPdfUploadFiles(String tableName, String kodeSatker, Connection sikdConn) throws SQLException {
         List<UploadFilePDF> uploadFiles = new ArrayList<>();
             Statement statement = sikdConn.createStatement();
             String sql = "select * from " + tableName.trim();
             if(kodeSatker!=null && !kodeSatker.trim().equals("") ){
                 sql = sql +  " where kdsatker='" + kodeSatker + "'";
             }
             ResultSet rs = statement.executeQuery( sql );
             while (rs.next()) {
                 UploadFilePDF uploadFile = new UploadFilePDF();
                 uploadFile.setId(rs.getLong("id"));
                 uploadFile.setFilename(rs.getString("filename"));
                 uploadFile.setTglupload(rs.getTimestamp("tglupload"));
                 uploadFile.setKdsatker(rs.getString("kdsatker"));
                 uploadFiles.add(uploadFile);
             }  
 
         return uploadFiles;
     }
 
    
 }