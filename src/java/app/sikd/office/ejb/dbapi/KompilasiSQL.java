/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.sikd.office.ejb.dbapi;

import app.sikd.entity.backoffice.AkunKompilasi;
import app.sikd.entity.backoffice.AkunKompilasiGFS;
import app.sikd.entity.backoffice.KompilasiAkunApbd1364;
import app.sikd.entity.backoffice.KompilasiApbd1364;
import app.sikd.entity.backoffice.KompilasiKegiatanApbd1364;
import app.sikd.entity.utilitas.SimpleAccount;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author sora
 */
public class KompilasiSQL implements IKompilasiSQL {
    
    public List<KompilasiApbd1364> getApbd13Kompilasi64(short tahun, short kodeData, Connection conn) throws SQLException{
        String sql = " with start64 as( "
                + " select kodesatker, kodepemda, (kodeakunutama||'.'||kodeakunkelompok||'.'||kodeakunjenis||'.'||kodeakunobjek||'.'||kodeakunrincian) kode, sum(nilaianggaran) nilai , kodefungsi from koderekapbd r "
                + " inner join kegiatanapbd k on r.kegiatanindex=k.kegiatanindex "
                + " inner join apbd a on k.apbdindex=a.apbdindex "
                + " where tahunanggaran = " + tahun + " and kodedata="+kodeData+" and jeniscoa=2 "
                + " group by kodesatker, kodepemda, kode, kodefungsi"
                + " order by kodesatker, kode "
                + " ), "
                + " next64 as( "
                + " select kodesatker, kodepemda, kode, accountname, nilai, kodefungsi "
                + " from start64 "
                + " inner join accrualaccount aa on start64.kode=aa.accountcode "
                + " ), "
                + " kom as( "
                + " select kodesatker, kodepemda, (kodeakunutama||'.'||kodeakunkelompok||'.'||kodeakunjenis||'.'||kodeakunobjek||'.'||kodeakunrincian) kode, sum(nilaianggaran) nilai, kodefungsi from koderekapbd r "
                + " inner join kegiatanapbd k on r.kegiatanindex=k.kegiatanindex "
                + " inner join apbd a on k.apbdindex=a.apbdindex "
                + " where tahunanggaran = " + tahun +" and kodedata="+kodeData+" and jeniscoa=1  "
                + " and kodesatker not in (select kodesatker from apbd where tahunanggaran = "+tahun+" and kodedata="+kodeData+" and jeniscoa=2)  "
                + " group by kodesatker, kodepemda, kode, kodefungsi "
                + " order by kodesatker, kode "
                + " ), "
                + " prov1364 as( "
                + " select distinct substring(kodepemda from 1 for 2) as kodepemda from kom "
                + " union "
                + " select distinct substring(kodepemda from 1 for 2) as kodepemda from start64 "
                + " ), "
                + " prov as( "
                + " select distinct kodepemda from prov1364 "
                + " ), "
                + " ca as( "
                + " select kom.*, "
                + " case "
                + " when aa.accountcode is null then "
                + "  aa2.accountcode "
                + "  else aa.accountcode "
                + "  end as accrual "
                + " from kom "
                + " left join cashaccount ca on kom.kode=ca.accountcode "
                + " left join mappingaccount ma on ca.sequenceindex=ma.cashaccount "
                + " left join accrualaccount aa on ma.accrualaccount=aa.autoindex "
                + " left join mappingaccountnon mon on kom.kode=mon.accountcode and kom.kodesatker=mon.kodesatker "
                + " left join accrualaccount aa2 on mon.accrualaccount=aa2.autoindex "
                + " ) "
                + " , "
                + " yy as ( "
                + " select kodesatker, kodepemda, accrual kode, accountname, sum(nilai), kodefungsi from ca "
                + " inner join accrualaccount aa on ca.accrual=aa.accountcode "
                + " group by kodesatker, kodepemda,  accrual, accountname, kodefungsi "
                + " ), "
                + " zz as( "
                + " select * from next64 "
                + " union "
                + " select * from yy "
                + " ) "
                + " , "
                + " accrual4 as( "
                + " select autoindex ind64, accountcode, accountname from accrualaccount where "
                + " accounttype=4 "
                + " ) "
                + " , "
                + " laststate as( "
                + " select kodesatker, kodepemda, ind64, kode, acc.accountcode, acc.accountname, nilai, kodefungsi from  "
                + " zz left join accrual4 acc on zz.kode like acc.accountcode||'%' "
                + " order by kode "
                + " ), "
                + " apbd4 as( "
                + " select kodesatker, kodepemda, ind64, accountcode, accountname, sum(nilai) nilai, kodefungsi "
                + "  from laststate "
                + "  group by kodesatker, kodepemda, ind64, accountcode, accountname, kodefungsi "
                + "  order by kodesatker, accountcode "
                + " ) "
                + " select prov.kodepemda||'.00' as pemda, ind64, apbd4.accountcode, apbd4.accountname, sum(nilai) nilai, kodefungsi "
                + " from prov "
                + " inner join apbd4 on apbd4.kodepemda like prov.kodepemda||'.%' "
                + " group by pemda, ind64, accountcode, accountname, kodefungsi "
                + " order by pemda, accountcode"; 
        List<KompilasiApbd1364> result = new ArrayList();
        List<KompilasiKegiatanApbd1364> kegiatans = new ArrayList();
        List<KompilasiAkunApbd1364> akuns = new ArrayList();
        Statement stm = null;
        ResultSet rs = null;
        try {
            stm = conn.createStatement();
            rs = stm.executeQuery(sql);
            java.util.Date tglPenyusunan = new java.util.Date();
            while (rs.next()) {
                if(result.isEmpty()){
                    result.add(new KompilasiApbd1364(rs.getString("pemda"), tahun, kodeData, tglPenyusunan));
                    kegiatans.add(new KompilasiKegiatanApbd1364(rs.getString("kodefungsi")));
                    akuns.add(new KompilasiAkunApbd1364(rs.getString("accountcode"), rs.getDouble("nilai")));
                }
                else{
                    if( !result.get(result.size()-1).getKodePemda().equals(rs.getString("pemda")) ){
                        kegiatans.get(kegiatans.size()-1).setAkuns(akuns);
                        result.get(result.size()-1).setKegiatans(kegiatans);
                        result.add(new KompilasiApbd1364(rs.getString("pemda"), tahun, kodeData, tglPenyusunan));
                        kegiatans = new ArrayList();
                        akuns = new ArrayList();
                        kegiatans.add(new KompilasiKegiatanApbd1364(rs.getString("kodefungsi")));
                        akuns.add(new KompilasiAkunApbd1364(rs.getString("accountcode"), rs.getDouble("nilai")));
                    }
                    else{
                        if(!kegiatans.get(kegiatans.size()-1).getKodeFungsi().equals(rs.getString("kodefungsi"))){
                            kegiatans.get(kegiatans.size()-1).setAkuns(akuns);
                            akuns = new ArrayList();
                            kegiatans.add(new KompilasiKegiatanApbd1364(rs.getString("kodefungsi")));
                            akuns.add(new KompilasiAkunApbd1364(rs.getString("accountcode"), rs.getDouble("nilai")));
                        }
                        else {
                            akuns.add(new KompilasiAkunApbd1364(rs.getString("accountcode"), rs.getDouble("nilai")));
                        }
                            
                    }
                }                
            }
            
            if( !result.isEmpty() ){
                if( !kegiatans.isEmpty() ){
                    kegiatans.get(kegiatans.size()-1).setAkuns(akuns);
                }
                result.get(result.size()-1).setKegiatans(kegiatans);
            }

            return result;
        } catch (SQLException ex) {
            Logger.getLogger(KompilasiSQL.class.getName()).log(Level.SEVERE, null, ex);
            throw new SQLException("SQL: Gagal mengambil data Kompilasi APBD \n" + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    public void deleteApbdKompilasi64(short tahun, short kodeData, Connection conn) throws SQLException{
        Statement stm=null;
        try {
            stm = conn.createStatement();
            stm.executeUpdate("delete from apbd64 where tahunanggaran=" + tahun + " and kodedata="+kodeData);
        } catch (SQLException ex) {
            Logger.getLogger(KompilasiSQL.class.getName()).log(Level.SEVERE, null, ex);
            throw new SQLException("SQL: Gagal menghapus data Kompilasi APBD \n" + ex.getMessage());
        } finally {
            if(stm!=null) stm.close();         
        }
    }
    public KompilasiApbd1364 createApbdKompilasi64(KompilasiApbd1364 apbd1364, Connection conn) throws SQLException{
        PreparedStatement pstm=null;
        
        String sql = "with zz as ( INSERT INTO apbd64( kodepemda, tahunanggaran, kodedata, tanggalpenyusunan) "
                + " VALUES (?, ?, ?, ?) returning *) select max(apbdindex) as id from zz";
        ResultSet rs = null;
        try {
            pstm = conn.prepareStatement(sql);
            pstm.setString(1, apbd1364.getKodePemda().trim());
            pstm.setShort(2, apbd1364.getTahun());
            pstm.setShort(3, apbd1364.getKodeData());
            pstm.setDate(4, new java.sql.Date(apbd1364.getTanggalPenyusunan().getTime()));
            rs = pstm.executeQuery();
            if (rs.next()) {
                apbd1364.setIndex(rs.getLong("id"));
                if(apbd1364.getKegiatans()!= null && !apbd1364.getKegiatans().isEmpty()){
                    for (int i = 0; i < apbd1364.getKegiatans().size(); i++) {
                        KompilasiKegiatanApbd1364 keg = createKegiatanApbdKompilasi64(apbd1364.getIndex(), apbd1364.getKegiatans().get(i), conn);
                        apbd1364.getKegiatans().set(i, keg);                        
                    }
                }
            }

            return apbd1364;
        } catch (SQLException ex) {
            Logger.getLogger(KompilasiSQL.class.getName()).log(Level.SEVERE, null, ex);
            throw new SQLException("SQL: Gagal input data Kompilasi APBD \n" + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (pstm != null) {
                pstm.close();
            }
        }
    }
    
    public KompilasiKegiatanApbd1364 createKegiatanApbdKompilasi64(long indexApbd, KompilasiKegiatanApbd1364 kegiatan, Connection conn) throws SQLException{
        Statement stm = null;
        PreparedStatement pstm = null;
        String sql = "with zz as ( INSERT INTO kegiatanapbd64( apbdindex, kodefungsi) "
                + " VALUES (" + indexApbd + ", '" + kegiatan.getKodeFungsi() + "') returning *) select max(kegiatanindex) as id from zz";
        String sql2 = "insert into koderekapbd64 VALUES(?, ?, ?)";
        ResultSet rs = null;
        try {
            stm = conn.createStatement();
            rs = stm.executeQuery(sql);
            
            if (rs.next()) {
                long id = rs.getLong("id");
                kegiatan.setIndex(id);
                pstm = conn.prepareStatement(sql2);
                if(kegiatan.getAkuns()!=null && !kegiatan.getAkuns().isEmpty()){
                    for (int i = 0; i < kegiatan.getAkuns().size(); i++) {
                        pstm.setLong(1, id);
                        pstm.setString(2, kegiatan.getAkuns().get(i).getKodeAkun());
                        pstm.setDouble(3, kegiatan.getAkuns().get(i).getNilai());
                        pstm.executeUpdate();
                    }
                }                
            }

            return kegiatan;
        } catch (SQLException ex) {
            Logger.getLogger(KompilasiSQL.class.getName()).log(Level.SEVERE, null, ex);
            throw new SQLException("SQL: Gagal input data Kompilasi Kegiatan APBD \n" + ex.getMessage());
        } finally {
            if(pstm!=null){
                pstm.close();
            }
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    
    public List<AkunKompilasiGFS> getApbdKompilasi64(short tahun, short kodeData, String kodepemda, Connection conn) throws SQLException{
        String sql = "with utama as(  "
                + " select substring(rek.accountcode from 1 for 1) code, sum(nilai) nilai  "
                + " from koderekapbd64 rek  "
                + " inner join kegiatanapbd64 keg on rek.kegiatanindex=keg.kegiatanindex  "
                + " inner join apbd64 on keg.apbdindex=apbd64.apbdindex  "
                + " where tahunanggaran=2013 and kodedata=0  ";
        if(kodepemda!=null && !kodepemda.trim().equals(""))
                sql = sql + " and kodepemda='" + kodepemda + "' ";
        sql = sql + " group by substring(rek.accountcode from 1 for 1)  "
                + "),  "
                + "kel as(  "
                + " select substring(rek.accountcode from 1 for 3) code, sum(nilai) nilai  "
                + " from koderekapbd64 rek  "
                + " inner join kegiatanapbd64 keg on rek.kegiatanindex=keg.kegiatanindex  "
                + " inner join apbd64 on keg.apbdindex=apbd64.apbdindex  "
                + " where tahunanggaran=2013 and kodedata=0  ";
        if(kodepemda!=null && !kodepemda.trim().equals(""))
                sql = sql + " and kodepemda='" + kodepemda + "' ";
        sql = sql + " group by substring(rek.accountcode from 1 for 3)  "
                + "),  "
                + "jenis as(  "
                + " select substring(rek.accountcode from 1 for 5) code, sum(nilai) nilai  "
                + " from koderekapbd64 rek  "
                + " inner join kegiatanapbd64 keg on rek.kegiatanindex=keg.kegiatanindex  "
                + " inner join apbd64 on keg.apbdindex=apbd64.apbdindex  "
                + " where tahunanggaran=2013 and kodedata=0  ";
        if(kodepemda!=null && !kodepemda.trim().equals(""))
                sql = sql + " and kodepemda='" + kodepemda + "' ";
        sql = sql + " group by substring(rek.accountcode from 1 for 5)  "                
                + "),  "
                + "objek as(  "
                + " select substring(rek.accountcode from 1 for 8) code, sum(nilai) nilai  "
                + " from koderekapbd64 rek  "
                + " inner join kegiatanapbd64 keg on rek.kegiatanindex=keg.kegiatanindex  "
                + " inner join apbd64 on keg.apbdindex=apbd64.apbdindex  "
                + " where tahunanggaran=2013 and kodedata=0  ";
        if(kodepemda!=null && !kodepemda.trim().equals(""))
                sql = sql + " and kodepemda='" + kodepemda + "' ";
        sql = sql + " group by substring(rek.accountcode from 1 for 8)  "                
                + "),  "                
                + "gabung as(  "
                + " select * from utama  "
                + " union  "
                + " select * from kel  "
                + " union  "
                + " select * from jenis  "
                + " union  "
                + " select * from objek  "
                + ")," 
                + " hasil64 as( " 
                + " select acc.autoindex accindex, g.code, accountname, nilai " 
                + " from gabung g " 
                + " inner join accrualaccount acc on g.code=acc.accountcode " 
                + " order by code ) " 
                + " select hasil64.*, gfs.accountcode||' '||gfs.accountname gfs from hasil64 " 
                + " left join mappingaccrualgfs map on hasil64.accindex=map.accrualaccount " 
                + " left join gfsaccount gfs on map.gfsaccount=gfs.autoindex";
        List<AkunKompilasiGFS> result = new ArrayList();
        Statement stm = null;
        ResultSet rs = null;
        try {
            stm = conn.createStatement();
            rs = stm.executeQuery(sql);
            while (rs.next()) {
                AkunKompilasiGFS acc = new AkunKompilasiGFS(rs.getString("gfs"), rs.getString("code"), rs.getString("accountname"), 
                        rs.getDouble("nilai"));
                if(acc.getKodeAkun().trim().length()==1 || acc.getKodeAkun().trim().length()==3)
                    acc.setStyle("font-weight: bold;");
                result.add(acc);
            }
            
            return result;
        } catch (SQLException ex) {
            Logger.getLogger(KompilasiSQL.class.getName()).log(Level.SEVERE, null, ex);
            throw new SQLException("SQL: Gagal mengambil data Kompilasi APBD 64 \n" + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }
    

    /**
     *
     * @param tahun
     * @param pemdaCodes
     * @param conn
     * @return
     * @throws SQLException
     */
    @Override
    public List<AkunKompilasi> getApbdKompilasiLangsungs(short tahun, List<String> pemdaCodes, Connection conn) throws SQLException {        
        List<AkunKompilasi> result = new ArrayList();
        SimpleAccount s = new SimpleAccount();
        Statement stm = null;
        ResultSet rs = null;
        String sql = "with kom as( "
                + "select kodesatker, (kodeakunutama||'.'||kodeakunkelompok||'.'||kodeakunjenis||'.'||kodeakunobjek||'.'||kodeakunrincian) kode, sum(nilaianggaran) nilai "
                + " from koderekapbd r "
                + "inner join kegiatanapbd k on r.kegiatanindex=k.kegiatanindex "
                + "inner join apbd a on k.apbdindex=a.apbdindex "
                + "where tahunanggaran = " + tahun + " and kodedata=0 and jeniscoa=1 ";
        String where = "";
        if (pemdaCodes != null && !pemdaCodes.isEmpty()) {
            for (int i = 0; i < pemdaCodes.size(); i++) {
                if (!where.trim().equals("")) {
                    where = where + " or ";
                }
                where = where + " kodepemda= '" + pemdaCodes.get(i) + "' ";
            }
        }
        if (!where.trim().equals("")) {
            sql = sql + " and (" + where + " )";
        }
        sql = sql + " group by kodesatker, kode "
                + "order by kodesatker, kode "
                + "), "
                + "ca as( "
                + "select kom.*, ca.accountcode cash, "
                + "case "
                + "when aa.accountcode is null then "
                + " aa2.accountcode "
                + " else aa.accountcode "
                + " end as accrual "
                + "from kom "
                + "left join cashaccount ca on kom.kode=ca.accountcode "
                + "left join mappingaccount ma on ca.sequenceindex=ma.cashaccount "
                + "left join accrualaccount aa on ma.accrualaccount=aa.autoindex "
                + "left join mappingaccountnon mon on kom.kode=mon.accountcode and kom.kodesatker=mon.kodesatker "
                + "left join accrualaccount aa2 on mon.accrualaccount=aa2.autoindex "
                + "), "
                + "zz as ( "
                + "select aa.autoindex ind, accrual, accountname, sum(nilai) nilai from ca "
                + "inner join accrualaccount aa on ca.accrual=aa.accountcode "
                + "group by ind, accrual, accountname "
                + "), "
                + "result as( "
                + "select aa.accountcode, aa.accountname, aa.accounttype, sum(nilai) nilai from accrualaccount aa "
                + "left join zz on zz.accrual like aa.accountcode||'%' "
                + "group by aa.accountcode, aa.accountname, aa.accounttype "
                + "order by accountcode "
                + ") "
                + "select accountcode, accountname, accounttype, nilai from result "
                + "where (accounttype=1 or accounttype=2 or accounttype=3 or accounttype=4) "
                + "and "
                + "(accountcode like '4%'  or accountcode like '5%' or accountcode like '6%' or accountcode like '7%' or accountcode like '8%' or accountcode like '9%')";
        try {
            stm = conn.createStatement();
            rs = stm.executeQuery(sql);
            while (rs.next()) {
                result.add(new AkunKompilasi(rs.getString("accountcode"), rs.getString("accountname"), rs.getDouble("nilai")));
                short atype = rs.getShort("accounttype");
                if(atype==(short)1 || atype==(short)2 || atype==(short)3)
                    result.get(result.size()-1).setStyle("font-weight: bold;");
            }

            return result;
        } catch (SQLException ex) {
            Logger.getLogger(KompilasiSQL.class.getName()).log(Level.SEVERE, null, ex);
            throw new SQLException("SQL: Gagal mengambil data Kompilasi APBD \n" + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }

}
