/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.sikd.office.ejb.dbapi;

import app.sikd.entity.backoffice.ArusKasReport;
import app.sikd.entity.backoffice.LODefisitNonOperasional;
import app.sikd.entity.backoffice.LOPosLuarBiasa;
import app.sikd.entity.backoffice.LOReport;
import app.sikd.entity.backoffice.LOSurplusNonOperasional;
import app.sikd.entity.backoffice.Neraca;
import app.sikd.entity.backoffice.NeracaAkunJenis;
import app.sikd.entity.backoffice.NeracaAkunKelompok;
import app.sikd.entity.backoffice.NeracaAkunObjek;
import app.sikd.entity.backoffice.NeracaAkunUtama;
import app.sikd.entity.backoffice.NeracaReport;
import app.sikd.entity.backoffice.PerubahanEkuitas;
import app.sikd.entity.backoffice.PerubahanSAL;
import app.sikd.util.SIKDUtil;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author sora
 */
public class LaporanSQL implements ILaporanSQL {

    @Override
    public List<NeracaReport> getNeracaReports(short year, short semester, String kodeSatker, String judulNeraca, Connection conn) throws SQLException {
        Statement stm = null;
        ResultSet rs = null;
        String sql = "with zz as "
                + "(select u.kodeakunutama, u.namaakunutama, k.kodeakunkelompok, k.namaakunkelompok, j.kodeakunjenis, j.namaakunjenis, sum(o.nilai) nilai "
                + "from neracakodrekutama u "
                + "inner join neraca on u.neracaindex=neraca.neracaindex "
                + "left join neracakodrekkelompok k on u.akunutamaindex=k.kodrekutamaindex "
                + "left join neracakodrekjenis j on k.akunkelompokindex=j.kodrekkelompokindex "
                + "left join neracakodrekobjek o on j.akunjenisindex=o.kodrekjenisindex "
                + "where neraca.kodesatker = '" + kodeSatker.trim() + "' and neraca.tahunanggaran=" + year 
                + " and neraca.semester=" + semester 
                + " and neraca.judulneraca='" + judulNeraca.trim() + "' "
                + "group by u.kodeakunutama, u.namaakunutama, k.kodeakunkelompok, k.namaakunkelompok, j.kodeakunjenis, j.namaakunjenis  " 
                + "order by u.kodeakunutama, k.kodeakunkelompok, j.kodeakunjenis  "
                + "), "
                + "zz2 as "
                + "(select u.kodeakunutama, u.namaakunutama, k.kodeakunkelompok, k.namaakunkelompok, j.kodeakunjenis, j.namaakunjenis, sum(o.nilai) nilai "
                + "from neracakodrekutama u "
                + "inner join neraca on u.neracaindex=neraca.neracaindex "
                + "left join neracakodrekkelompok k on u.akunutamaindex=k.kodrekutamaindex "
                + "left join neracakodrekjenis j on k.akunkelompokindex=j.kodrekkelompokindex "
                + "left join neracakodrekobjek o on j.akunjenisindex=o.kodrekjenisindex "
                + "where neraca.kodesatker = '" + kodeSatker.trim() + "' and neraca.tahunanggaran=" + (year-1) 
                + " and neraca.semester=" + semester 
                + " and neraca.judulneraca='" + judulNeraca.trim() + "' "
                + "group by u.kodeakunutama, u.namaakunutama, k.kodeakunkelompok, k.namaakunkelompok, j.kodeakunjenis, j.namaakunjenis " 
                + " order by u.kodeakunutama, k.kodeakunkelompok, j.kodeakunjenis "
                + ") "
                + "select zz.*, zz2.nilai nilaipra from zz  "
                + "left join zz2 on zz.namaakunutama=zz2.namaakunutama and zz.namaakunkelompok=zz2.namaakunkelompok and zz.namaakunjenis=zz2.namaakunjenis "
                + "order by zz.kodeakunutama, zz.kodeakunkelompok, zz.kodeakunjenis";
        try {
            stm = conn.createStatement();
            rs = stm.executeQuery(sql);
            String u = "";
            String kel = "";
            List<NeracaReport> result = new ArrayList();
            double jumlahu1=0, jumlahu2=0, jumlahk1=0, jumlahk2=0;
            while (rs.next()) {
                if( u.trim().equals("")){
                    result.add(new NeracaReport(rs.getString("namaakunutama"), 0, 0));
                    result.add(new NeracaReport(rs.getString("namaakunkelompok"), 0, 0));
                    result.add(new NeracaReport(rs.getString("namaakunjenis"), rs.getDouble("nilai"), rs.getDouble("nilaipra"), (short)3));
//                    jumlahu1=rs.getDouble("nilai");
//                    jumlahu2=rs.getDouble("nilaipra");
                    jumlahk1=rs.getDouble("nilai");
                    jumlahk2=rs.getDouble("nilaipra");
                    u = rs.getString("namaakunutama");                    
                    kel = rs.getString("namaakunkelompok");
                }
                else{
                    if (!u.trim().equalsIgnoreCase(rs.getString("namaakunutama").trim())) {
                        if( kel != null)
                            result.add(new NeracaReport("Jumlah " + kel, jumlahk1, jumlahk2, (short) 1));
                        jumlahu1+=jumlahk1;
                        jumlahu2+=jumlahk2;                        
                        result.add(new NeracaReport("", 0, 0));                        
                        result.add(new NeracaReport("JUMLAH " + u.toUpperCase(), jumlahu1, jumlahu2, (short) 2));
                        result.add(new NeracaReport("", 0, 0));
                        result.add(new NeracaReport(rs.getString("namaakunutama"), 0, 0));
                        result.add(new NeracaReport(rs.getString("namaakunkelompok"), 0, 0));
                        result.add(new NeracaReport(rs.getString("namaakunjenis"), rs.getDouble("nilai"), rs.getDouble("nilaipra"), (short) 3));
//                        jumlahu1 = rs.getDouble("nilai");
//                        jumlahu2 = rs.getDouble("nilaipra");
                        jumlahu1 = 0;//rs.getDouble("nilai");
                        jumlahu2 = 0;//rs.getDouble("nilaipra");
                        jumlahk1 = rs.getDouble("nilai");
                        jumlahk2 = rs.getDouble("nilaipra");
                        u = rs.getString("namaakunutama");
                        kel = rs.getString("namaakunkelompok");
                    }
                    else if( !kel.trim().equalsIgnoreCase(rs.getString("namaakunkelompok").trim())){
                        if( kel != null)
                            result.add(new NeracaReport("Jumlah " + kel, jumlahk1, jumlahk2, (short) 1));
                         jumlahu1+=jumlahk1;
                         jumlahu2+=jumlahk2;
                         result.add(new NeracaReport("", 0, 0));
                         result.add(new NeracaReport(rs.getString("namaakunkelompok"), 0, 0));
                         result.add(new NeracaReport(rs.getString("namaakunjenis"), rs.getDouble("nilai"), rs.getDouble("nilaipra"), (short) 3));
                         jumlahk1 = rs.getDouble("nilai");
                        jumlahk2 = rs.getDouble("nilaipra");
                        kel = rs.getString("namaakunkelompok");
                    }
                    else{
                        result.add(new NeracaReport(rs.getString("namaakunjenis"), rs.getDouble("nilai"), rs.getDouble("nilaipra"), (short) 3));
                        jumlahk1 += rs.getDouble("nilai");
                        jumlahk2 += rs.getDouble("nilaipra");
                    }
                }                
            }
            if( result.size() > 0 ){
                if( kel != null)
                    result.add(new NeracaReport("Jumlah " + kel, jumlahk1, jumlahk2, (short) 1));
                result.add(new NeracaReport("", 0, 0));
                jumlahu1+=jumlahk1;
                jumlahu2+=jumlahk2;
                result.add(new NeracaReport("JUMLAH " + u.toUpperCase(), jumlahu1, jumlahu2, (short) 2));
            }
            return result;
        } catch (SQLException ex) {
            throw new SQLException("Gagal mengambil data Neraca Report");
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    /**
     *
     * @param year
     * @param semester
     * @param kodeSatker
     * @param judulNeraca
     * @param conn
     * @return
     * @throws SQLException
     */
    @Override
    public Neraca getNeraca(short year, short semester, String kodeSatker, String judulNeraca, Connection conn) throws SQLException {
        Statement stm = null;
        ResultSet rs = null;
        String sql = "select * from neraca "
                + "where kodesatker = '" + kodeSatker.trim() + "' and tahunanggaran=" + year 
                + " and semester=" + semester
                + " and judulneraca='" + judulNeraca.trim() + "' ";
        try {
            stm = conn.createStatement();
            rs = stm.executeQuery(sql);
            if (rs.next()) {
                return new Neraca(rs.getLong("neracaindex"), rs.getString("kodesatker"), rs.getString("kodepemda"), rs.getString("namapemda"), rs.getShort("tahunanggaran"), rs.getShort("semester"), rs.getString("judulneraca"), rs.getShort("statusdata"));
            }
            return null;
        } catch (SQLException ex) {
            throw new SQLException("Gagal mengambil data Neraca");
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }

    @Override
    public List<String> getJudulNeracas(short year, short semester, String kodeSatker, Connection conn) throws SQLException {
        Statement stm = null;
        ResultSet rs = null;
        try {
            stm = conn.createStatement();
            rs = stm.executeQuery("select distinct judulneraca judul from neraca where kodesatker='" + kodeSatker.trim() + "' and tahunanggaran=" + year
            + " and semester=" + semester + " order by judulneraca");
            List<String> result = new ArrayList();
            String u = "";
            while (rs.next()) {
                result.add(rs.getString("judul").trim());
            }

            return result;
        } catch (SQLException ex) {
            throw new SQLException("Gagal mengambil data Judul Neraca");
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    /**
     *
     * @param neraca
     * @param conn
     * @return
     * @throws SQLException
     */
    @Override
    public long createNeraca(Neraca neraca, short iotype, Connection conn) throws SQLException{
        PreparedStatement pstm= null;
        Statement stm=null;
        ResultSet rs = null;
        long result = 0;
        String sql=  "insert into neraca(kodesatker,kodepemda,namapemda,tahunanggaran,semester, judulneraca, iotype) "
                + "values(?, ?, ?, ?, ?, ?, ?)";
        try {
            pstm = conn.prepareStatement(sql);
            pstm.setString(1, neraca.getKodeSatker().trim());
            pstm.setString(2, neraca.getKodePemda().trim());
            pstm.setString(3, neraca.getNamaPemda().trim());
            pstm.setShort(4, neraca.getTahunAnggaran());
            pstm.setShort(5, neraca.getSemester());
            pstm.setString(6, neraca.getJudulNeraca().trim()); 
            pstm.setShort(7, iotype); 
            pstm.executeUpdate();
            
            stm = conn.createStatement();
            rs = stm.executeQuery("select max (neracaindex) id from neraca");
            if( rs.next() ) result = rs.getLong("id");  
            return result;
        } catch (SQLException ex) {
            throw new SQLException("Gagal menambah data Neraca");
        }
        finally{
            if( rs != null ) rs.close();
            if( stm!=null ) stm.close();
            if( pstm!=null ) pstm.close();
        }
    }
    
    /**
     *
     * @param neraca
     * @param conn
     * @throws SQLException
     */
    @Override
    public void updateNeraca(Neraca neraca, Connection conn) throws SQLException{
        PreparedStatement pstm= null;
        String sql=  "update neraca set tahunanggaran= ?, judulneraca=? "
                + "where neracaindex=?";
        try {
            pstm = conn.prepareStatement(sql);
            pstm.setShort(1, neraca.getTahunAnggaran());
            pstm.setString(2, neraca.getJudulNeraca().trim()); 
            pstm.setLong(3, neraca.getIndex()); 
            pstm.executeUpdate();
            
        } catch (SQLException ex) {
            throw new SQLException("Gagal Merubah data Neraca");
        }
        finally{
            if( pstm!=null ) pstm.close();
        }
    }
    
    /**
     *
     * @param neraca
     * @param conn
     * @throws SQLException
     */
    @Override
    public void deleteNeraca(Neraca neraca, Connection conn) throws SQLException{
        PreparedStatement pstm= null;
        String sql=  "delete from neraca "
                + "where neracaindex=?";
        try {
            pstm = conn.prepareStatement(sql);
            pstm.setLong(1, neraca.getIndex());
            pstm.executeUpdate();
            
        } catch (SQLException ex) {
            throw new SQLException("Gagal Menghapus data Neraca");
        }
        finally{
            if( pstm!=null ) pstm.close();
        }
    }
    
    /**
     *
     * @param indexNeraca
     * @param obj
     * @param conn
     * @return
     * @throws SQLException
     */
    @Override
    public long createNeracaAkunUtama(long indexNeraca, NeracaAkunUtama obj, Connection conn) throws SQLException{
        PreparedStatement pstm= null;
        Statement stm=null;
        ResultSet rs = null;
        long result = 0;
        String sql=  "insert into neracakodrekutama(neracaindex, kodeakunutama, namaakunutama, nilai) "
                + "values(?, ?, ?, ?)";
        try {
            pstm = conn.prepareStatement(sql);
            pstm.setLong(1, indexNeraca);
            pstm.setString(2, obj.getKodeAkun().trim());
            pstm.setString(3, obj.getNamaAkun().trim());
            pstm.setDouble(4, obj.getNilai());
            pstm.executeUpdate();
            
            stm = conn.createStatement();
            rs = stm.executeQuery("select max (akunutamaindex) id from neracakodrekutama");
            if( rs.next() ) result = rs.getLong("id");  
            return result;
        } catch (SQLException ex) {
            throw new SQLException("Gagal menambah data Akun Utama Neraca");
        }
        finally{
            if( rs != null ) rs.close();
            if( stm!=null ) stm.close();
            if( pstm!=null ) pstm.close();
        }
    }
    
    /**
     *
     * @param indexNeraca
     * @param conn
     * @return
     * @throws SQLException
     */
    @Override
    public List<NeracaAkunUtama> getNeracaAkunUtamas(long indexNeraca, Connection conn) throws SQLException{
        PreparedStatement pstm= null;
        ResultSet rs = null;
        String sql=  "select * from neracakodrekutama where neracaindex=? order by kodeakunutama";
        try {
            pstm = conn.prepareStatement(sql);
            pstm.setLong(1, indexNeraca);
            rs = pstm.executeQuery();
            List<NeracaAkunUtama> result = new ArrayList();
            while( rs.next() ) {
               result.add(new NeracaAkunUtama(rs.getLong("akunutamaindex"), rs.getString("kodeakunutama"), rs.getString("namaakunutama"), rs.getDouble("nilai")));
            }  
            return result;
        } catch (SQLException ex) {
            throw new SQLException("Gagal Mengambil data Akun Utama Neraca");
        }
        finally{
            if( rs != null ) rs.close();
            if( pstm!=null ) pstm.close();
        }
    }
    
    /**
     *
     * @param obj
     * @param conn
     * @throws SQLException
     */
    @Override
    public void updateNeracaAkunUtama(NeracaAkunUtama obj, Connection conn) throws SQLException{
        PreparedStatement pstm= null;
        String sql=  "update neracakodrekutama set  kodeakunutama= ? , namaakunutama= ? , nilai=? "
                + "where akunutamaindex=?";
        try {
            pstm = conn.prepareStatement(sql);
            pstm.setString(1, obj.getKodeAkun());
            pstm.setString(2, obj.getNamaAkun());
            pstm.setDouble(3, obj.getNilai()); 
            pstm.setLong(4, obj.getIndex()); 
            pstm.executeUpdate();
            
        } catch (SQLException ex) {
            throw new SQLException("Gagal Merubah data Akun Utama Neraca ");
        }
        finally{
            if( pstm!=null ) pstm.close();
        }
    }
    
    /**
     *
     * @param obj
     * @param conn
     * @throws SQLException
     */
    @Override
    public void deleteNeracaAkunUtama(NeracaAkunUtama obj, Connection conn) throws SQLException{
        PreparedStatement pstm= null;
        String sql=  "delete from neracakodrekutama "
                + "where akunutamaindex=?";
        try {
            pstm = conn.prepareStatement(sql);
            pstm.setLong(1, obj.getIndex());
            pstm.executeUpdate();
            
        } catch (SQLException ex) {
            throw new SQLException("Gagal Menghapus data Akun Utama Neraca");
        }
        finally{
            if( pstm!=null ) pstm.close();
        }
    }
    
    /**
     *
     * @param indexNeracaAkunUtama
     * @param obj
     * @param conn
     * @return
     * @throws SQLException
     */
    @Override
    public long createNeracaAkunKelompok(long indexNeracaAkunUtama, NeracaAkunKelompok obj, Connection conn) throws SQLException{
        PreparedStatement pstm= null;
        Statement stm=null;
        ResultSet rs = null;
        long result = 0;
        String sql=  "insert into neracakodrekkelompok(kodrekutamaindex, kodeakunkelompok, namaakunkelompok, nilai) "
                + "values(?, ?, ?, ?)";
        try {
            pstm = conn.prepareStatement(sql);
            pstm.setLong(1, indexNeracaAkunUtama);
            pstm.setString(2, obj.getKodeAkun().trim());
            pstm.setString(3, obj.getNamaAkun().trim());
            pstm.setDouble(4, obj.getNilai());
            pstm.executeUpdate();
            
            stm = conn.createStatement();
            rs = stm.executeQuery("select max (akunkelompokindex) id from neracakodrekkelompok");
            if( rs.next() ) result = rs.getLong("id");  
            return result;
        } catch (SQLException ex) {
            throw new SQLException("Gagal menambah data Akun Kelompok Neraca");
        }
        finally{
            if( rs != null ) rs.close();
            if( stm!=null ) stm.close();
            if( pstm!=null ) pstm.close();
        }
    }
    
    /**
     *
     * @param indexNeracaAkunUtama
     * @param conn
     * @return
     * @throws SQLException
     */
    @Override
    public List<NeracaAkunKelompok> getNeracaAkunKelompoks(long indexNeracaAkunUtama, Connection conn) throws SQLException{
        PreparedStatement pstm= null;
        ResultSet rs = null;
        String sql=  "select * from neracakodrekkelompok where kodrekutamaindex=? order by kodeakunkelompok";
        try {
            pstm = conn.prepareStatement(sql);
            pstm.setLong(1, indexNeracaAkunUtama);
            rs = pstm.executeQuery();
            List<NeracaAkunKelompok> result = new ArrayList();
            while( rs.next() ) {
               result.add(new NeracaAkunKelompok(rs.getLong("akunkelompokindex"), rs.getString("kodeakunkelompok"), rs.getString("namaakunkelompok"), rs.getDouble("nilai")));
            }  
            return result;
        } catch (SQLException ex) {
            throw new SQLException("Gagal Mengambil data Akun Kelompok Neraca");
        }
        finally{
            if( rs != null ) rs.close();
            if( pstm!=null ) pstm.close();
        }
    }
    
    /**
     *
     * @param obj
     * @param conn
     * @throws SQLException
     */
    @Override
    public void updateNeracaAkunKelompok(NeracaAkunKelompok obj, Connection conn) throws SQLException{
        PreparedStatement pstm= null;
        String sql=  "update neracakodrekkelompok set kodeakunkelompok= ? , namaakunkelompok= ? , nilai=? "
                + "where akunkelompokindex=?";
        try {
            pstm = conn.prepareStatement(sql);
            pstm.setString(1, obj.getKodeAkun());
            pstm.setString(2, obj.getNamaAkun());
            pstm.setDouble(3, obj.getNilai()); 
            pstm.setLong(4, obj.getIndex()); 
            pstm.executeUpdate();
            
        } catch (SQLException ex) {
            throw new SQLException("Gagal Merubah data Akun Kelompok Neraca ");
        }
        finally{
            if( pstm!=null ) pstm.close();
        }
    }
    
    /**
     *
     * @param obj
     * @param conn
     * @throws SQLException
     */
    @Override
    public void deleteNeracaAkunKelompok(NeracaAkunKelompok obj, Connection conn) throws SQLException{
        PreparedStatement pstm= null;
        String sql=  "delete from neracakodrekkelompok "
                + "where akunkelompokindex=?";
        try {
            pstm = conn.prepareStatement(sql);
            pstm.setLong(1, obj.getIndex());
            pstm.executeUpdate();
            
        } catch (SQLException ex) {
            throw new SQLException("Gagal Menghapus data Akun Kelompok Neraca");
        }
        finally{
            if( pstm!=null ) pstm.close();
        }
    }
    
    /**
     *
     * @param indexNeracaAkunKelompok
     * @param obj
     * @param conn
     * @return
     * @throws SQLException
     */
    @Override
    public long createNeracaAkunJenis(long indexNeracaAkunKelompok, NeracaAkunJenis obj, Connection conn) throws SQLException{
        PreparedStatement pstm= null;
        Statement stm=null;
        ResultSet rs = null;
        long result = 0;
        String sql=  "insert into neracakodrekjenis(kodrekkelompokindex, kodeakunjenis, namaakunjenis, nilai) "
                + "values(?, ?, ?, ?)";
        try {
            pstm = conn.prepareStatement(sql);
            pstm.setLong(1, indexNeracaAkunKelompok);
            pstm.setString(2, obj.getKodeAkun().trim());
            pstm.setString(3, obj.getNamaAkun().trim());
            pstm.setDouble(4, obj.getNilai());
            pstm.executeUpdate();
            
            stm = conn.createStatement();
            rs = stm.executeQuery("select max (akunjenisindex) id from neracakodrekjenis");
            if( rs.next() ) result = rs.getLong("id");  
            return result;
        } catch (SQLException ex) {
            throw new SQLException("Gagal menambah data Akun Jenis Neraca");
        }
        finally{
            if( rs != null ) rs.close();
            if( stm!=null ) stm.close();
            if( pstm!=null ) pstm.close();
        }
    }
    
    /**
     *
     * @param indexNeracaAkunKelompok
     * @param conn
     * @return
     * @throws SQLException
     */
    @Override
    public List<NeracaAkunJenis> getNeracaAkunJeniss(long indexNeracaAkunKelompok, Connection conn) throws SQLException{
        PreparedStatement pstm= null;
        ResultSet rs = null;
        String sql=  "select * from neracakodrekjenis where kodrekkelompokindex=? order by kodeakunjenis";
        try {
            pstm = conn.prepareStatement(sql);
            pstm.setLong(1, indexNeracaAkunKelompok);
            rs = pstm.executeQuery();
            List<NeracaAkunJenis> result = new ArrayList();
            while( rs.next() ) {
               result.add(new NeracaAkunJenis(rs.getLong("akunjenisindex"), rs.getString("kodeakunjenis"), rs.getString("namaakunjenis"), rs.getDouble("nilai")));
            }  
            return result;
        } catch (SQLException ex) {
            throw new SQLException("Gagal Mengambil data Akun Jenis Neraca");
        }
        finally{
            if( rs != null ) rs.close();
            if( pstm!=null ) pstm.close();
        }
    }
    
    /**
     *
     * @param obj
     * @param conn
     * @throws SQLException
     */
    @Override
    public void updateNeracaAkunJenis(NeracaAkunJenis obj, Connection conn) throws SQLException{
        PreparedStatement pstm= null;
        String sql=  "update neracakodrekjenis set kodeakunjenis= ? , namaakunjenis= ? , nilai=? "
                + "where akunjenisindex=?";
        try {
            pstm = conn.prepareStatement(sql);
            pstm.setString(1, obj.getKodeAkun());
            pstm.setString(2, obj.getNamaAkun());
            pstm.setDouble(3, obj.getNilai()); 
            pstm.setLong(4, obj.getIndex()); 
            pstm.executeUpdate();
            
        } catch (SQLException ex) {
            throw new SQLException("Gagal Merubah data Akun Jenis Neraca ");
        }
        finally{
            if( pstm!=null ) pstm.close();
        }
    }
    
    /**
     *
     * @param obj
     * @param conn
     * @throws SQLException
     */
    @Override
    public void deleteNeracaAkunJenis(NeracaAkunJenis obj, Connection conn) throws SQLException{
        PreparedStatement pstm= null;
        String sql=  "delete from neracakodrekjenis "
                + "where akunjenisindex=?";
        try {
            pstm = conn.prepareStatement(sql);
            pstm.setLong(1, obj.getIndex());
            pstm.executeUpdate();
            
        } catch (SQLException ex) {
            throw new SQLException("Gagal Menghapus data Akun Jenis Neraca");
        }
        finally{
            if( pstm!=null ) pstm.close();
        }
    }
    
    /**
     *
     * @param indexNeracaAkunJenis
     * @param obj
     * @param conn
     * @return
     * @throws SQLException
     */
    @Override
    public long createNeracaAkunObjek(long indexNeracaAkunJenis, NeracaAkunObjek obj, Connection conn) throws SQLException{
        PreparedStatement pstm= null;
        Statement stm=null;
        ResultSet rs = null;
        long result = 0;
        String sql=  "insert into neracakodrekobjek(kodrekjenisindex, kodeakunobjek, namaakunobjek, nilai) "
                + "values(?, ?, ?, ?)";
        try {
            pstm = conn.prepareStatement(sql);
            pstm.setLong(1, indexNeracaAkunJenis);
            pstm.setString(2, obj.getKodeAkun().trim());
            pstm.setString(3, obj.getNamaAkun().trim());
            pstm.setDouble(4, obj.getNilai());
            pstm.executeUpdate();
            
            stm = conn.createStatement();
            rs = stm.executeQuery("select max (akunobjekindex) id from neracakodrekobjek");
            if( rs.next() ) result = rs.getLong("id");  
            return result;
        } catch (SQLException ex) {
            throw new SQLException("Gagal menambah data Akun Objek Neraca");
        }
        finally{
            if( rs != null ) rs.close();
            if( stm!=null ) stm.close();
            if( pstm!=null ) pstm.close();
        }
    }
    
    /**
     *
     * @param indexNeracaAkunJenis
     * @param conn
     * @return
     * @throws SQLException
     */
    @Override
    public List<NeracaAkunObjek> getNeracaAkunObjeks(long indexNeracaAkunJenis, Connection conn) throws SQLException{
        PreparedStatement pstm= null;
        ResultSet rs = null;
        String sql=  "select * from neracakodrekobjek where kodrekjenisindex=? order by kodeakunobjek";
        try {
            pstm = conn.prepareStatement(sql);
            pstm.setLong(1, indexNeracaAkunJenis);
            rs = pstm.executeQuery();
            List<NeracaAkunObjek> result = new ArrayList();
            while( rs.next() ) {
               result.add(new NeracaAkunObjek(rs.getLong("akunobjekindex"), rs.getString("kodeakunobjek"), rs.getString("namaakunobjek"), rs.getDouble("nilai")));
            }  
            return result;
        } catch (SQLException ex) {
            throw new SQLException("Gagal Mengambil data Akun Objek Neraca");
        }
        finally{
            if( rs != null ) rs.close();
            if( pstm!=null ) pstm.close();
        }
    }
    
    /**
     *
     * @param obj
     * @param conn
     * @throws SQLException
     */
    @Override
    public void updateNeracaAkunObjek(NeracaAkunObjek obj, Connection conn) throws SQLException{
        PreparedStatement pstm= null;
        String sql=  "update neracakodrekobjek set kodeakunobjek= ? , namaakunobjek= ? , nilai=? "
                + "where akunobjekindex=?";
        try {
            pstm = conn.prepareStatement(sql);
            pstm.setString(1, obj.getKodeAkun());
            pstm.setString(2, obj.getNamaAkun());
            pstm.setDouble(3, obj.getNilai()); 
            pstm.setLong(4, obj.getIndex()); 
            pstm.executeUpdate();
            
        } catch (SQLException ex) {
            throw new SQLException("Gagal Merubah data Akun Objek Neraca ");
        }
        finally{
            if( pstm!=null ) pstm.close();
        }
    }
    
    /**
     *
     * @param obj
     * @param conn
     * @throws SQLException
     */
    @Override
    public void deleteNeracaAkunObjek(NeracaAkunObjek obj, Connection conn) throws SQLException{
        PreparedStatement pstm= null;
        String sql=  "delete from neracakodrekobjek "
                + "where akunobjekindex=?";
        try {
            pstm = conn.prepareStatement(sql);
            pstm.setLong(1, obj.getIndex());
            pstm.executeUpdate();
            
        } catch (SQLException ex) {
            throw new SQLException("Gagal Menghapus data Akun Objek Neraca");
        }
        finally{
            if( pstm!=null ) pstm.close();
        }
    }
    
    
    public List<ArusKasReport> getArusKasOperasiReports(short year, String kodeSatker, String judulArusKas, List<ArusKasReport> result, Connection conn) throws SQLException {
        Statement stm = null;
        ResultSet rs = null;
        String sql = "with akun as "
                + "(select distinct kodeakun kode, namaakun uraian from arusmasukoperasi i  "
                + "inner join aruskas a on i.aruskasindex=a.aruskasindex  "
                + "where kodeSatker='"+kodeSatker+"' and (tahunanggaran="+(year-1)+" or tahunanggaran="+year+" ) and judularuskas='" + judulArusKas + "' "
                + "order by kodeakun), "
                + "nilai as "
                + "( select i.kodeakun kode, sum(nilai) nilai from arusmasukoperasi i  "
                + "inner join aruskas a on i.aruskasindex=a.aruskasindex  "
                + "where kodeSatker='"+kodeSatker+"' and tahunanggaran=" + year + " and judularuskas='" + judulArusKas + "' "
                + "group by kode order by kode ), "
                + "pranilai as "
                + "( select i.kodeakun kode, sum(nilai) nilai from arusmasukoperasi i "
                + "inner join aruskas a on i.aruskasindex=a.aruskasindex "
                + "where kodeSatker='"+kodeSatker+"' and tahunanggaran=" + (year-1) + " and judularuskas='" + judulArusKas + "' "
                + "group by kode order by kode ) "
                + "select a.kode, a.uraian, n.nilai nilai, pn.nilai pranilai from akun a "
                + "left join nilai n on a.kode=n.kode "
                + "left join pranilai pn on a.kode=pn.kode";
        
        try {
            stm = conn.createStatement();
            rs = stm.executeQuery(sql);
            
            double jumlahm1=0, jumlahk1=0;
            double jumlahm2=0, jumlahk2=0;
            result.add(new ArusKasReport("Arus Kas Dari Aktifitas Operasi", "","", ArusKasReport.TYPE_BOLD));
            result.add(new ArusKasReport("", "","", (short)0));
            result.add(new ArusKasReport("Arus Masuk Kas", "","", ArusKasReport.TYPE_BOLD));
            while (rs.next()) {                
                    result.add(new ArusKasReport(rs.getString("uraian"), 
                            (rs.getString("kode").trim().length()==1 || rs.getString("kode").trim().length()==3) ? "":SIKDUtil.doubleToString(rs.getDouble("nilai")), 
                            (rs.getString("kode").trim().length()==1 || rs.getString("kode").trim().length()==3) ? "":SIKDUtil.doubleToString(rs.getDouble("pranilai")), 
                            (rs.getString("kode").trim().length()==1 || rs.getString("kode").trim().length()==3) ? ArusKasReport.TYPE_BOLD:ArusKasReport.TYPE_PLAIN));
                    if (rs.getString("kode").trim().length() > 3) {                  
                        jumlahm1 += rs.getDouble("nilai");
                        jumlahm2 += rs.getDouble("pranilai");
                    }
            }
            result.add(new ArusKasReport("Jumlah Arus Masuk Kas", SIKDUtil.doubleToString(jumlahm1), SIKDUtil.doubleToString(jumlahm2), ArusKasReport.TYPE_BOLD));
            result.add(new ArusKasReport("", "","", ArusKasReport.TYPE_PLAIN));
            
            sql = "with akun as "
                + "(select distinct kodeakun kode, namaakun uraian from aruskeluaroperasi i  "
                + "inner join aruskas a on i.aruskasindex=a.aruskasindex  "
                + "where kodeSatker='"+kodeSatker+"' and (tahunanggaran="+(year-1)+" or tahunanggaran="+year+" ) and judularuskas='" + judulArusKas + "' "
                + "order by kodeakun), "
                + "nilai as "
                + "( select i.kodeakun kode, sum(nilai) nilai from aruskeluaroperasi i  "
                + "inner join aruskas a on i.aruskasindex=a.aruskasindex  "
                + "where kodeSatker='"+kodeSatker+"' and tahunanggaran=" + year + " and judularuskas='" + judulArusKas + "' "
                + "group by kode order by kode ), "
                + "pranilai as "
                + "( select i.kodeakun kode, sum(nilai) nilai from aruskeluaroperasi i "
                + "inner join aruskas a on i.aruskasindex=a.aruskasindex "
                + "where kodeSatker='"+kodeSatker+"' and tahunanggaran=" + (year-1) + " and judularuskas='" + judulArusKas + "' "
                + "group by kode order by kode ) "
                + "select a.kode, a.uraian, n.nilai nilai, pn.nilai pranilai from akun a "
                + "left join nilai n on a.kode=n.kode "
                + "left join pranilai pn on a.kode=pn.kode";
            
            rs = stm.executeQuery(sql);
            
            result.add(new ArusKasReport("Arus Keluar Kas", "", "", ArusKasReport.TYPE_BOLD));
            while(rs.next()){
                result.add(new ArusKasReport(rs.getString("uraian"), 
                            (rs.getString("kode").trim().length()==1 || rs.getString("kode").trim().length()==3) ? "":SIKDUtil.doubleToString(rs.getDouble("nilai")), 
                            (rs.getString("kode").trim().length()==1 || rs.getString("kode").trim().length()==3) ? "":SIKDUtil.doubleToString(rs.getDouble("pranilai")), 
                            (rs.getString("kode").trim().length()==1 || rs.getString("kode").trim().length()==3) ? ArusKasReport.TYPE_BOLD:ArusKasReport.TYPE_PLAIN));
                    if (rs.getString("kode").trim().length() > 3) {
                        jumlahk1 += rs.getDouble("nilai");
                        jumlahk2 += rs.getDouble("pranilai");
                    }
            }
            result.add(new ArusKasReport("Jumlah Arus Keluar Kas", SIKDUtil.doubleToString(jumlahk1), SIKDUtil.doubleToString(jumlahk2), ArusKasReport.TYPE_BOLD));
            result.add(new ArusKasReport("", "","", ArusKasReport.TYPE_PLAIN));
            result.add(new ArusKasReport("Arus Kas Bersih Dari Aktifitas Operasi", SIKDUtil.doubleToString(jumlahm1-jumlahk1), SIKDUtil.doubleToString(jumlahm2-jumlahk2), ArusKasReport.TYPE_BOLD_BACKGROUND));
            
            return result;
        } catch (SQLException ex) {
            throw new SQLException("Gagal mengambil data Arus Kas Aktifitas Operasi");
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    public List<ArusKasReport> getArusKasInvestasiReports(short year, String kodeSatker, String judulArusKas, List<ArusKasReport> result, Connection conn) throws SQLException {
        Statement stm = null;
        ResultSet rs = null;
        
        String sql = "with akun as "
                + "(select distinct kodeakun kode, namaakun uraian from arusmasukinvestasi i  "
                + "inner join aruskas a on i.aruskasindex=a.aruskasindex  "
                + "where kodeSatker='"+kodeSatker+"' and (tahunanggaran="+(year-1)+" or tahunanggaran="+year+" ) and judularuskas='" + judulArusKas + "' "
                + "order by kodeakun), "
                + "nilai as "
                + "( select i.kodeakun kode, sum(nilai) nilai from arusmasukinvestasi i  "
                + "inner join aruskas a on i.aruskasindex=a.aruskasindex  "
                + "where kodeSatker='"+kodeSatker+"' and tahunanggaran=" + year + " and judularuskas='" + judulArusKas + "' "
                + "group by kode order by kode ), "
                + "pranilai as "
                + "( select i.kodeakun kode, sum(nilai) nilai from arusmasukinvestasi i "
                + "inner join aruskas a on i.aruskasindex=a.aruskasindex "
                + "where kodeSatker='"+kodeSatker+"' and tahunanggaran=" + (year-1) + " and judularuskas='" + judulArusKas + "' "
                + "group by kode order by kode ) "
                + "select a.kode, a.uraian, n.nilai nilai, pn.nilai pranilai from akun a "
                + "left join nilai n on a.kode=n.kode "
                + "left join pranilai pn on a.kode=pn.kode";
        try {
            stm = conn.createStatement();
            rs = stm.executeQuery(sql);
            
            double jumlahm1=0, jumlahk1=0;
            double jumlahm2=0, jumlahk2=0;
            result.add(new ArusKasReport("Arus Kas Dari Aktifitas Investasi Aset Non Keuangan", "","", ArusKasReport.TYPE_BOLD));
            result.add(new ArusKasReport("", "","", (short)0));
            result.add(new ArusKasReport("Arus Masuk Kas", "","", ArusKasReport.TYPE_BOLD));
            while (rs.next()) {                
                    result.add(new ArusKasReport(rs.getString("uraian"), 
                            (rs.getString("kode").trim().length()==1 || rs.getString("kode").trim().length()==3) ? "":SIKDUtil.doubleToString(rs.getDouble("nilai")), 
                            (rs.getString("kode").trim().length()==1 || rs.getString("kode").trim().length()==3) ? "":SIKDUtil.doubleToString(rs.getDouble("pranilai")), 
                            (rs.getString("kode").trim().length()==1 || rs.getString("kode").trim().length()==3) ? ArusKasReport.TYPE_BOLD:ArusKasReport.TYPE_PLAIN));
                    if (rs.getString("kode").trim().length() > 3) {                  
                        jumlahm1 += rs.getDouble("nilai");
                        jumlahm2 += rs.getDouble("pranilai");
                    }
            }
            result.add(new ArusKasReport("Jumlah Arus Masuk Kas", SIKDUtil.doubleToString(jumlahm1), SIKDUtil.doubleToString(jumlahm2), ArusKasReport.TYPE_BOLD));
            result.add(new ArusKasReport("", "", "", ArusKasReport.TYPE_PLAIN));
            
            sql = "with akun as "
                + "(select distinct kodeakun kode, namaakun uraian from aruskeluarinvestasi i  "
                + "inner join aruskas a on i.aruskasindex=a.aruskasindex  "
                + "where kodeSatker='"+kodeSatker+"' and (tahunanggaran="+(year-1)+" or tahunanggaran="+year+" ) and judularuskas='" + judulArusKas + "' "
                + "order by kodeakun), "
                + "nilai as "
                + "( select i.kodeakun kode, sum(nilai) nilai from aruskeluarinvestasi i  "
                + "inner join aruskas a on i.aruskasindex=a.aruskasindex  "
                + "where kodeSatker='"+kodeSatker+"' and tahunanggaran=" + year + " and judularuskas='" + judulArusKas + "' "
                + "group by kode order by kode ), "
                + "pranilai as "
                + "( select i.kodeakun kode, sum(nilai) nilai from aruskeluarinvestasi i "
                + "inner join aruskas a on i.aruskasindex=a.aruskasindex "
                + "where kodeSatker='"+kodeSatker+"' and tahunanggaran=" + (year-1) + " and judularuskas='" + judulArusKas + "' "
                + "group by kode order by kode ) "
                + "select a.kode, a.uraian, n.nilai nilai, pn.nilai pranilai from akun a "
                + "left join nilai n on a.kode=n.kode "
                + "left join pranilai pn on a.kode=pn.kode";
            rs = stm.executeQuery(sql);
            
            result.add(new ArusKasReport("Arus Keluar Kas",  "","", ArusKasReport.TYPE_BOLD));
            while(rs.next()){
                result.add(new ArusKasReport(rs.getString("uraian"), 
                            (rs.getString("kode").trim().length()==1 || rs.getString("kode").trim().length()==3) ? "":SIKDUtil.doubleToString(rs.getDouble("nilai")), 
                            (rs.getString("kode").trim().length()==1 || rs.getString("kode").trim().length()==3) ? "":SIKDUtil.doubleToString(rs.getDouble("pranilai")), 
                            (rs.getString("kode").trim().length()==1 || rs.getString("kode").trim().length()==3) ? ArusKasReport.TYPE_BOLD:ArusKasReport.TYPE_PLAIN));
                    if (rs.getString("kode").trim().length() > 3) {                  
                        jumlahk1 += rs.getDouble("nilai");
                        jumlahk2 += rs.getDouble("pranilai");
                    }
            }
            result.add(new ArusKasReport("Jumlah Arus Keluar Kas", SIKDUtil.doubleToString(jumlahk1), SIKDUtil.doubleToString(jumlahk2), ArusKasReport.TYPE_BOLD));
            result.add(new ArusKasReport("", "", "", ArusKasReport.TYPE_PLAIN));
            result.add(new ArusKasReport("Arus Kas Bersih Dari Aktifitas Investasi Non Keuangan", SIKDUtil.doubleToString(jumlahm1-jumlahk1), SIKDUtil.doubleToString(jumlahm2-jumlahk2), ArusKasReport.TYPE_BOLD_BACKGROUND));
            
            
            return result;
        } catch (SQLException ex) {
            throw new SQLException("Gagal mengambil data Arus Kas Investasi Non Keuangan");
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    public List<ArusKasReport> getArusKasPembiayaanReports(short year, String kodeSatker, String judulArusKas, List<ArusKasReport> result, Connection conn) throws SQLException {
        Statement stm = null;
        ResultSet rs = null;
        
        String sql = "with akun as "
                + "(select distinct kodeakun kode, namaakun uraian from arusmasukpembiayaan i  "
                + "inner join aruskas a on i.aruskasindex=a.aruskasindex  "
                + "where kodeSatker='"+kodeSatker+"' and (tahunanggaran="+(year-1)+" or tahunanggaran="+year+" ) and judularuskas='" + judulArusKas + "' "
                + "order by kodeakun), "
                + "nilai as "
                + "( select i.kodeakun kode, sum(nilai) nilai from arusmasukpembiayaan i  "
                + "inner join aruskas a on i.aruskasindex=a.aruskasindex  "
                + "where kodeSatker='"+kodeSatker+"' and tahunanggaran=" + year + " and judularuskas='" + judulArusKas + "' "
                + "group by kode order by kode ), "
                + "pranilai as "
                + "( select i.kodeakun kode, sum(nilai) nilai from arusmasukpembiayaan i "
                + "inner join aruskas a on i.aruskasindex=a.aruskasindex "
                + "where kodeSatker='"+kodeSatker+"' and tahunanggaran=" + (year-1) + " and judularuskas='" + judulArusKas + "' "
                + "group by kode order by kode ) "
                + "select a.kode, a.uraian, n.nilai nilai, pn.nilai pranilai from akun a "
                + "left join nilai n on a.kode=n.kode "
                + "left join pranilai pn on a.kode=pn.kode";
        try {
            stm = conn.createStatement();
            rs = stm.executeQuery(sql);
            
            double jumlahm1=0, jumlahk1=0;
            double jumlahm2=0, jumlahk2=0;
            result.add(new ArusKasReport("Arus Kas Dari Aktifitas Pembiayaan", "","", ArusKasReport.TYPE_BOLD));
            result.add(new ArusKasReport("", "","", (short)0));
            result.add(new ArusKasReport("Arus Masuk Kas", "","", ArusKasReport.TYPE_BOLD));
            while (rs.next()) {                
                    result.add(new ArusKasReport(rs.getString("uraian"), 
                            (rs.getString("kode").trim().length()==1 || rs.getString("kode").trim().length()==3) ? "":SIKDUtil.doubleToString(rs.getDouble("nilai")), 
                            (rs.getString("kode").trim().length()==1 || rs.getString("kode").trim().length()==3) ? "":SIKDUtil.doubleToString(rs.getDouble("pranilai")), 
                            (rs.getString("kode").trim().length()==1 || rs.getString("kode").trim().length()==3) ? ArusKasReport.TYPE_BOLD:ArusKasReport.TYPE_PLAIN));
                    if (rs.getString("kode").trim().length() > 3) {                  
                        jumlahm1 += rs.getDouble("nilai");
                        jumlahm2 += rs.getDouble("pranilai");
                    }
            }
            result.add(new ArusKasReport("Jumlah Arus Masuk Kas", SIKDUtil.doubleToString(jumlahm1), SIKDUtil.doubleToString(jumlahm2), ArusKasReport.TYPE_BOLD));
            result.add(new ArusKasReport("", "", "", ArusKasReport.TYPE_PLAIN));
            
            sql = "with akun as "
                + "(select distinct kodeakun kode, namaakun uraian from aruskeluarpembiayaan i  "
                + "inner join aruskas a on i.aruskasindex=a.aruskasindex  "
                + "where kodeSatker='"+kodeSatker+"' and (tahunanggaran="+(year-1)+" or tahunanggaran="+year+" ) and judularuskas='" + judulArusKas + "' "
                + "order by kodeakun), "
                + "nilai as "
                + "( select i.kodeakun kode, sum(nilai) nilai from aruskeluarpembiayaan i  "
                + "inner join aruskas a on i.aruskasindex=a.aruskasindex  "
                + "where kodeSatker='"+kodeSatker+"' and tahunanggaran=" + year + " and judularuskas='" + judulArusKas + "' "
                + "group by kode order by kode ), "
                + "pranilai as "
                + "( select i.kodeakun kode, sum(nilai) nilai from aruskeluarpembiayaan i "
                + "inner join aruskas a on i.aruskasindex=a.aruskasindex "
                + "where kodeSatker='"+kodeSatker+"' and tahunanggaran=" + (year-1) + " and judularuskas='" + judulArusKas + "' "
                + "group by kode order by kode ) "
                + "select a.kode, a.uraian, n.nilai nilai, pn.nilai pranilai from akun a "
                + "left join nilai n on a.kode=n.kode "
                + "left join pranilai pn on a.kode=pn.kode";
            rs = stm.executeQuery(sql);
            
            result.add(new ArusKasReport("Arus Keluar Kas",  "","", ArusKasReport.TYPE_BOLD));
            while(rs.next()){
                result.add(new ArusKasReport(rs.getString("uraian"), 
                            (rs.getString("kode").trim().length()==1 || rs.getString("kode").trim().length()==3) ? "":SIKDUtil.doubleToString(rs.getDouble("nilai")), 
                            (rs.getString("kode").trim().length()==1 || rs.getString("kode").trim().length()==3) ? "":SIKDUtil.doubleToString(rs.getDouble("pranilai")), 
                            (rs.getString("kode").trim().length()==1 || rs.getString("kode").trim().length()==3) ? ArusKasReport.TYPE_BOLD:ArusKasReport.TYPE_PLAIN));
                    if (rs.getString("kode").trim().length() > 3) {                  
                        jumlahk1 += rs.getDouble("nilai");
                        jumlahk2 += rs.getDouble("pranilai");
                    }
            }
            result.add(new ArusKasReport("Jumlah Arus Keluar Kas", SIKDUtil.doubleToString(jumlahk1), SIKDUtil.doubleToString(jumlahk2), ArusKasReport.TYPE_BOLD));
            result.add(new ArusKasReport("", "", "", ArusKasReport.TYPE_PLAIN));
            result.add(new ArusKasReport("Arus Kas Bersih Dari Aktifitas Pembiayaan", SIKDUtil.doubleToString(jumlahm1-jumlahk1), SIKDUtil.doubleToString(jumlahm2-jumlahk2), ArusKasReport.TYPE_BOLD_BACKGROUND));
            
            return result;
        } catch (SQLException ex) {
            throw new SQLException("Gagal mengambil data Arus Kas Pembiayaan");
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    
    public List<ArusKasReport> getArusKasNonAnggaranReports(short year, String kodeSatker, String judulArusKas, List<ArusKasReport> result, Connection conn) throws SQLException {
        Statement stm = null;
        ResultSet rs = null;
        
        String sql = "with akun as "
                + "(select distinct kodeakun kode, namaakun uraian from arusmasuknonanggaran i  "
                + "inner join aruskas a on i.aruskasindex=a.aruskasindex  "
                + "where kodeSatker='"+kodeSatker+"' and (tahunanggaran="+(year-1)+" or tahunanggaran="+year+" ) and judularuskas='" + judulArusKas + "' "
                + "order by kodeakun), "
                + "nilai as "
                + "( select i.kodeakun kode, sum(nilai) nilai from arusmasuknonanggaran i  "
                + "inner join aruskas a on i.aruskasindex=a.aruskasindex  "
                + "where kodeSatker='"+kodeSatker+"' and tahunanggaran=" + year + " and judularuskas='" + judulArusKas + "' "
                + "group by kode order by kode ), "
                + "pranilai as "
                + "( select i.kodeakun kode, sum(nilai) nilai from arusmasuknonanggaran i "
                + "inner join aruskas a on i.aruskasindex=a.aruskasindex "
                + "where kodeSatker='"+kodeSatker+"' and tahunanggaran=" + (year-1) + " and judularuskas='" + judulArusKas + "' "
                + "group by kode order by kode ) "
                + "select a.kode, a.uraian, n.nilai nilai, pn.nilai pranilai from akun a "
                + "left join nilai n on a.kode=n.kode "
                + "left join pranilai pn on a.kode=pn.kode";
        try {
            stm = conn.createStatement();
            rs = stm.executeQuery(sql);
            
            double jumlahm1=0, jumlahk1=0;
            double jumlahm2=0, jumlahk2=0;
            result.add(new ArusKasReport("Arus Kas Dari Aktifitas Non Anggaran", "","", ArusKasReport.TYPE_BOLD));
            result.add(new ArusKasReport("", "","", (short)0));
            result.add(new ArusKasReport("Arus Masuk Kas", "","", ArusKasReport.TYPE_BOLD));
            while (rs.next()) {                
                    result.add(new ArusKasReport(rs.getString("uraian"), 
                            (rs.getString("kode").trim().length()==1 || rs.getString("kode").trim().length()==3) ? "":SIKDUtil.doubleToString(rs.getDouble("nilai")), 
                            (rs.getString("kode").trim().length()==1 || rs.getString("kode").trim().length()==3) ? "":SIKDUtil.doubleToString(rs.getDouble("pranilai")), 
                            (rs.getString("kode").trim().length()==1 || rs.getString("kode").trim().length()==3) ? ArusKasReport.TYPE_BOLD:ArusKasReport.TYPE_PLAIN));
                    if (rs.getString("kode").trim().length() > 3) {                  
                        jumlahm1 += rs.getDouble("nilai");
                        jumlahm2 += rs.getDouble("pranilai");
                    }
            }
            result.add(new ArusKasReport("Jumlah Arus Masuk Kas", SIKDUtil.doubleToString(jumlahm1), SIKDUtil.doubleToString(jumlahm2), ArusKasReport.TYPE_BOLD));
            result.add(new ArusKasReport("", "", "", ArusKasReport.TYPE_PLAIN));
            
            sql = "with akun as "
                + "(select distinct kodeakun kode, namaakun uraian from aruskeluarnonanggaran i  "
                + "inner join aruskas a on i.aruskasindex=a.aruskasindex  "
                + "where kodeSatker='"+kodeSatker+"' and (tahunanggaran="+(year-1)+" or tahunanggaran="+year+" ) and judularuskas='" + judulArusKas + "' "
                + "order by kodeakun), "
                + "nilai as "
                + "( select i.kodeakun kode, sum(nilai) nilai from aruskeluarnonanggaran i  "
                + "inner join aruskas a on i.aruskasindex=a.aruskasindex  "
                + "where kodeSatker='"+kodeSatker+"' and tahunanggaran=" + year + " and judularuskas='" + judulArusKas + "' "
                + "group by kode order by kode ), "
                + "pranilai as "
                + "( select i.kodeakun kode, sum(nilai) nilai from aruskeluarnonanggaran i "
                + "inner join aruskas a on i.aruskasindex=a.aruskasindex "
                + "where kodeSatker='"+kodeSatker+"' and tahunanggaran=" + (year-1) + " and judularuskas='" + judulArusKas + "' "
                + "group by kode order by kode ) "
                + "select a.kode, a.uraian, n.nilai nilai, pn.nilai pranilai from akun a "
                + "left join nilai n on a.kode=n.kode "
                + "left join pranilai pn on a.kode=pn.kode";
            rs = stm.executeQuery(sql);
            
            result.add(new ArusKasReport("Arus Keluar Kas",  "","", ArusKasReport.TYPE_BOLD));
            while(rs.next()){
                result.add(new ArusKasReport(rs.getString("uraian"), 
                            (rs.getString("kode").trim().length()==1 || rs.getString("kode").trim().length()==3) ? "":SIKDUtil.doubleToString(rs.getDouble("nilai")), 
                            (rs.getString("kode").trim().length()==1 || rs.getString("kode").trim().length()==3) ? "":SIKDUtil.doubleToString(rs.getDouble("pranilai")), 
                            (rs.getString("kode").trim().length()==1 || rs.getString("kode").trim().length()==3) ? ArusKasReport.TYPE_BOLD:ArusKasReport.TYPE_PLAIN));
                    if (rs.getString("kode").trim().length() > 3) {                  
                        jumlahk1 += rs.getDouble("nilai");
                        jumlahk2 += rs.getDouble("pranilai");
                    }
            }
            result.add(new ArusKasReport("Jumlah Arus Keluar Kas", SIKDUtil.doubleToString(jumlahk1), SIKDUtil.doubleToString(jumlahk2), ArusKasReport.TYPE_BOLD));
            result.add(new ArusKasReport("", "", "", ArusKasReport.TYPE_PLAIN));
            result.add(new ArusKasReport("Arus Kas Bersih Dari Aktifitas Non Anggaran", SIKDUtil.doubleToString(jumlahm1-jumlahk1), SIKDUtil.doubleToString(jumlahm2-jumlahk2), ArusKasReport.TYPE_BOLD_BACKGROUND));
            
            return result;
        } catch (SQLException ex) {
            throw new SQLException("Gagal mengambil data Arus Kas Non Anggaran");
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    public List<ArusKasReport> getArusKasSaldoReports(short year, String kodeSatker, String judulArusKas, List<ArusKasReport> result, Connection conn) throws SQLException {
        Statement stm = null;
        ResultSet rs = null;
        double nil=0, pranil=0;
        if(result.size() > 0 ){
            ArusKasReport akr = result.get(result.size()-1);
            nil=SIKDUtil.stringToDouble(akr.getStrNilai());
            pranil = SIKDUtil.stringToDouble(akr.getStrNilaiPra());
        }

        String sql = "with awal as ( select text 'kas awal' nama,  "
                + " ( "
                + "  select kasbudawal from aruskassaldo s  "
                + "  inner join aruskas kas on s.aruskasindex=kas.aruskasindex "
                + "  where kodesatker='"+kodeSatker+"' and tahunanggaran="+(year-1)+" and judularuskas='"+judulArusKas+"' "
                + "  ) praawal, "
                + "  ( "
                + "  select kasbudawal from aruskassaldo s  "
                + "  inner join aruskas kas on s.aruskasindex=kas.aruskasindex "
                + "  where kodesatker='"+kodeSatker+"' and tahunanggaran="+year+" and judularuskas='"+judulArusKas+"' "
                + "  ) awal "
                + "), "
                + "pengeluaran as ( select text 'kas bendahara keluar' nama,  "
                + " ( "
                + "  select kasbendpengeluaranawal from aruskassaldo s  "
                + "  inner join aruskas kas on s.aruskasindex=kas.aruskasindex "
                + "  where kodesatker='"+kodeSatker+"' and tahunanggaran="+(year-1)+" and judularuskas='"+judulArusKas+"' "
                + "  ) praawal, "
                + "  ( "
                + "  select kasbendpengeluaranawal from aruskassaldo s  "
                + "  inner join aruskas kas on s.aruskasindex=kas.aruskasindex "
                + "  where kodesatker='"+kodeSatker+"' and tahunanggaran="+year+" and judularuskas='"+judulArusKas+"' "
                + "  ) awal "
                + "), "
                + "penerimaan as ( select text 'kas bendahara terima' nama,  "
                + " ( "
                + "  select kasbendpenerimaanawal from aruskassaldo s  "
                + "  inner join aruskas kas on s.aruskasindex=kas.aruskasindex "
                + "  where kodesatker='"+kodeSatker+"' and tahunanggaran="+(year-1)+" and judularuskas='"+judulArusKas+"' "
                + "  ) praawal, "
                + "  ( "
                + "  select kasbendpenerimaanawal from aruskassaldo s  "
                + "  inner join aruskas kas on s.aruskasindex=kas.aruskasindex "
                + "  where kodesatker='"+kodeSatker+"' and tahunanggaran="+year+" and judularuskas='"+judulArusKas+"' "
                + "  ) awal "
                + "), "
                + "lain as ( select text 'kas lain' nama,  "
                + " ( "
                + "  select kaslainnya from aruskassaldo s  "
                + "  inner join aruskas kas on s.aruskasindex=kas.aruskasindex "
                + "  where kodesatker='"+kodeSatker+"' and tahunanggaran="+(year-1)+" and judularuskas='"+judulArusKas+"' "
                + "  ) praawal, "
                + "  ( "
                + "  select kaslainnya from aruskassaldo s  "
                + "  inner join aruskas kas on s.aruskasindex=kas.aruskasindex "
                + "  where kodesatker='"+kodeSatker+"' and tahunanggaran="+year+" and judularuskas='"+judulArusKas+"' "
                + "  ) awal "
                + ") "
                + "select * from awal union  select * from pengeluaran "
                + "union select * from penerimaan union  select * from lain "
                + "order by nama";
        
        try {
            stm = conn.createStatement();
            rs = stm.executeQuery(sql);
            while(rs.next()){
                if(rs.getString("nama").equals("kas awal") ){
                    result.add(new ArusKasReport("Saldo Awal Kas", SIKDUtil.doubleToString(rs.getDouble("awal")), SIKDUtil.doubleToString(rs.getDouble("praawal")), ArusKasReport.TYPE_BOLD));
                }
                else if(rs.getString("nama").equals("kas bendahara keluar") ){
                    result.add(new ArusKasReport("Saldo Akhir Kas di Bendahara Pengeluaran", SIKDUtil.doubleToString(rs.getDouble("awal")), SIKDUtil.doubleToString(rs.getDouble("praawal")), ArusKasReport.TYPE_BOLD));
                }
                else if(rs.getString("nama").equals("kas bendahara terima") ){
                    result.add(new ArusKasReport("Saldo Akhir Kas di Bendahara Penerimaan", SIKDUtil.doubleToString(rs.getDouble("awal")), SIKDUtil.doubleToString(rs.getDouble("praawal")), ArusKasReport.TYPE_BOLD));
                }
                else if(rs.getString("nama").equals("kas lain") ){
                    result.add(new ArusKasReport("Saldo Akhir Kas Lainnya", SIKDUtil.doubleToString(rs.getDouble("awal")), SIKDUtil.doubleToString(rs.getDouble("praawal")), ArusKasReport.TYPE_BOLD));
                }
                
                nil+=rs.getDouble("awal");
                pranil+=rs.getDouble("praawal");                
            }
            result.add(new ArusKasReport("", "", "", ArusKasReport.TYPE_BOLD));
            result.add(new ArusKasReport("SALDO AKHIR KAS", SIKDUtil.doubleToString(nil), SIKDUtil.doubleToString(pranil), ArusKasReport.TYPE_BOLD_BACKGROUND));
            
            return result;
        } catch (SQLException ex) {
            throw new SQLException("Gagal mengambil data Saldo Arus Kas ");
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    /**
     *
     * @param year
     * @param kodeSatker
     * @param judulArusKas
     * @param conn
     * @return
     * @throws SQLException
     */
    @Override
    public List<ArusKasReport> getArusKasReports(short year, String kodeSatker, String judulArusKas, Connection conn) throws SQLException {
        List<ArusKasReport> result = new ArrayList();
        if (judulArusKas != null && !judulArusKas.trim().equals("")) {
            getArusKasOperasiReports(year, kodeSatker, judulArusKas, result, conn);
            ArusKasReport akpOp = result.get(result.size()-1);
            double op = SIKDUtil.stringToDouble(akpOp.getStrNilai());
            double praOp = SIKDUtil.stringToDouble(akpOp.getStrNilaiPra());
            result.add(new ArusKasReport("","","",ArusKasReport.TYPE_PLAIN));
            
            getArusKasInvestasiReports(year, kodeSatker, judulArusKas, result, conn);            
            ArusKasReport akpInv = result.get(result.size()-1);
            double inv = SIKDUtil.stringToDouble(akpInv.getStrNilai());
            double praInv = SIKDUtil.stringToDouble(akpInv.getStrNilaiPra());
            result.add(new ArusKasReport("","","",ArusKasReport.TYPE_PLAIN));
            
            getArusKasPembiayaanReports(year, kodeSatker, judulArusKas, result, conn);
            ArusKasReport akpBi = result.get(result.size()-1);
            double bi = SIKDUtil.stringToDouble(akpBi.getStrNilai());
            double praBi = SIKDUtil.stringToDouble(akpBi.getStrNilaiPra());
            result.add(new ArusKasReport("","","",ArusKasReport.TYPE_PLAIN));
            
            getArusKasNonAnggaranReports(year, kodeSatker, judulArusKas, result, conn);
            ArusKasReport akpNa = result.get(result.size()-1);
            double na = SIKDUtil.stringToDouble(akpNa.getStrNilai());
            double praNa = SIKDUtil.stringToDouble(akpNa.getStrNilaiPra());
            result.add(new ArusKasReport("","","",ArusKasReport.TYPE_PLAIN));
            double naik = op+inv+bi+na;
            double praNaik = praOp+praInv+praBi+praNa;
            
            result.add(new ArusKasReport("Kenaikan/Penurunan Kas", SIKDUtil.doubleToString(naik), SIKDUtil.doubleToString(praNaik), ArusKasReport.TYPE_BOLD));
            
            getArusKasSaldoReports(year, kodeSatker, judulArusKas, result, conn);
        }
        return result;
        
    }
    
    /**
     *
     * @param year
     * @param kodeSatker
     * @param conn
     * @return
     * @throws SQLException
     */
    @Override
    public List<String> getJudulArusKas(short year, String kodeSatker, Connection conn) throws SQLException {
        Statement stm = null;
        ResultSet rs = null;
        try {
            stm = conn.createStatement();
            rs = stm.executeQuery("select distinct judularuskas judul from aruskas "
                    + "where kodesatker='" + kodeSatker.trim() + "' "
                    + "and tahunanggaran=" + year);
            List<String> result = new ArrayList();
            String u = "";
            while (rs.next()) {
                result.add(rs.getString("judul").trim());
            }

            return result;
        } catch (SQLException ex) {
            throw new SQLException("Gagal mengambil data Judul Arus Kas");
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    /**
     *
     * @param year
     * @param kodeSatker
     * @param conn
     * @return
     * @throws SQLException
     */
    @Override
    public PerubahanSAL getPerubahanSalReports(short year, String kodeSatker, Connection conn) throws SQLException {
        Statement stm = null;
        ResultSet rs = null;

        String sql = "SELECT sal.kodepemda, sal.namapemda, det.* "
                + "  FROM perubahansaldetail det inner join perubahansal sal"
                + "  on det.salindex=sal.salindex"
                + "  where sal.kodesatker='" + kodeSatker + "' and "
                + "  sal.tahunanggaran=" + year ;
        try {
            stm = conn.createStatement();
            rs = stm.executeQuery(sql);
            PerubahanSAL result = null;
            if(rs.next()){
                result = new PerubahanSAL();
                result.setIndex(rs.getLong("salindex"));
                result.setKodePemda(rs.getString("kodepemda"));
                result.setNamaPemda(rs.getString("namapemda"));
                result.setKodeSatker(kodeSatker);
                result.setTahunAnggaran(year);
                result.setSalAwal(rs.getDouble("salawal"));
                result.setPenggunaanSal(rs.getDouble("penggunaansal"));
                result.setSilpa(rs.getDouble("silpa"));                
                result.setKoreksi(rs.getDouble("koreksi"));
                result.setLainLain(rs.getDouble("lainlain"));
            }
            
            return result;
        } catch (SQLException ex) {
            throw new SQLException("Gagal mengambil data Perubahan Sal");
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    /**
     *
     * @param sal
     * @param conn
     * @return
     * @throws SQLException
     */
    @Override
    public long createPerubahanSalReports(PerubahanSAL sal, short iotype, Connection conn) throws SQLException {
        PreparedStatement stm = null;
        ResultSet rs = null;
        String sql = " with zz as ( ";
            sql = sql + "insert into perubahansal "
                    + "(kodesatker, kodepemda, namapemda, tahunanggaran, tanggalpengiriman, statusdata, namaaplikasi, pengembangaplikasi, iotype) "
                    + "values(?, ?, ?, ?, ?, ?, ?, ?, ?)";
            sql = sql + " returning * ) select max(salindex) as id from zz";
        
        try {
            if(sal.getIndex()>0) deletePerubahanSalReports(sal.getTahunAnggaran(), sal.getKodeSatker().trim(), conn);
            stm = conn.prepareStatement(sql);
            long id = 0;
            stm.setString(1, sal.getKodeSatker().trim());
            stm.setString(2, sal.getKodePemda().trim());
            stm.setString(3, sal.getNamaPemda().trim());
            stm.setShort(4, sal.getTahunAnggaran());
            java.util.Date dd = new java.util.Date();
            stm.setDate(5, new java.sql.Date(dd.getTime()));
            stm.setShort(6, (short)0);
            stm.setString(7, "Input Data");
            stm.setString(8, "Input Data");
            stm.setShort(9, iotype);
            rs = stm.executeQuery();
            if(rs.next()){
                id= rs.getLong("id");
            }
            if (id > 0) {
                stm = conn.prepareStatement("INSERT INTO perubahansaldetail VALUES(?, ?, ?, ?, ?, ?)");
                stm.setLong(1, id);
                stm.setDouble(2, sal.getSalAwal());
                stm.setDouble(3, sal.getPenggunaanSal());
                stm.setDouble(4, sal.getSilpa());
                stm.setDouble(5, sal.getKoreksi());
                stm.setDouble(6, sal.getLainLain());
                stm.executeUpdate();
            }
            return id;
        } catch (SQLException ex) {
            throw new SQLException("Gagal input data Perubahan Sal");
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    /**
     *
     * @param year
     * @param kodeSatker
     * @param conn
     * @throws SQLException
     */
    @Override
    public void deletePerubahanSalReports(short year, String kodeSatker, Connection conn) throws SQLException {
        Statement stm = null;
        String sql = "DELETE FROM perubahansal sal"
                + "  where sal.kodesatker='" + kodeSatker + "' and "
                + "  sal.tahunanggaran=" + year ;
        try {
            stm = conn.createStatement();
            stm.executeUpdate(sql);
            
        } catch (SQLException ex) {
            throw new SQLException("Gagal menghapus data Perubahan Sal");
        } finally {
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    /**
     *
     * @param year
     * @param kodeSatker
     * @param conn
     * @return
     * @throws SQLException
     */
    @Override
    public PerubahanEkuitas getPerubahanEkuitasReports(short year, String kodeSatker, Connection conn) throws SQLException {
        Statement stm = null;
        ResultSet rs = null;

        String sql = "SELECT ek.kodepemda, ek.namapemda, det.* "
                + "  FROM perubahanekuitasdetail det inner join perubahanekuitas ek"
                + "  on det.ekuitasindex=ek.ekuitasindex"
                + "  where ek.kodesatker='" + kodeSatker + "' and "
                + "  ek.tahunanggaran=" + year ;
        try {
            stm = conn.createStatement();
            rs = stm.executeQuery(sql);
            PerubahanEkuitas result = null;
            if(rs.next()){
                result = new PerubahanEkuitas();
                result.setIndex(rs.getLong("ekuitasindex"));
                result.setKodePemda(rs.getString("kodepemda"));
                result.setNamaPemda(rs.getString("namapemda"));
                result.setKodeSatker(kodeSatker);
                result.setTahunAnggaran(year);
                result.setEkuitasAwal(rs.getDouble("ekuitasawal"));
                result.setSurplusDefisitLO(rs.getDouble("surplusdefisit"));
                result.setKoreksiNilaiPersediaan(rs.getDouble("koreksipersediaan"));
                result.setSelisihRevaluasiAset(rs.getDouble("selisihrevaluasiaset"));                
                result.setLainLain(rs.getDouble("lainlain"));
            }
            
            return result;
        } catch (SQLException ex) {
            throw new SQLException("Gagal mengambil data Perubahan Ekuitas");
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    /**
     *
     * @param lpe
     * @param conn
     * @return
     * @throws SQLException
     */
    @Override
    public long createPerubahanEkuitasReports(PerubahanEkuitas lpe, short iotype, Connection conn) throws SQLException {
        PreparedStatement stm = null;
        ResultSet rs = null;
        String sql = " with zz as ( ";
            sql = sql + "insert into perubahanekuitas "
                    + "(kodesatker, kodepemda, namapemda, tahunanggaran, tanggalpengiriman, statusdata, namaaplikasi, pengembangaplikasi, iotype) "
                    + "values(?, ?, ?, ?, ?, ?, ?, ?, ?)";
            sql = sql + " returning * ) select max(ekuitasindex) as id from zz";
        
        try {
            if(lpe.getIndex()>0) deletePerubahanEkuitasReports(lpe.getTahunAnggaran(), lpe.getKodeSatker().trim(), conn);
            stm = conn.prepareStatement(sql);
            long id = 0;
            stm.setString(1, lpe.getKodeSatker().trim());
            stm.setString(2, lpe.getKodePemda().trim());
            stm.setString(3, lpe.getNamaPemda().trim());
            stm.setShort(4, lpe.getTahunAnggaran());
            Date dd = new Date();
            stm.setDate(5, new java.sql.Date(dd.getTime()));
            stm.setShort(6, (short)0);
            stm.setString(7, "Input Data");
            stm.setString(8, "Input Data");
            stm.setShort(9, iotype);
            rs = stm.executeQuery();
            if(rs.next()){
                id= rs.getLong("id");
            }
            if (id > 0) {
                
                stm = conn.prepareStatement("INSERT INTO perubahanekuitasdetail VALUES(?, ?, ?, ?, ?, ?)");
                stm.setLong(1, id);
                stm.setDouble(2, lpe.getEkuitasAwal());
                stm.setDouble(3, lpe.getSurplusDefisitLO());
                stm.setDouble(4, lpe.getKoreksiNilaiPersediaan());
                stm.setDouble(5, lpe.getSelisihRevaluasiAset());
                stm.setDouble(6, lpe.getLainLain());
                stm.executeUpdate();
            }
            return id;
        } catch (SQLException ex) {
            throw new SQLException("Gagal input data Perubahan Ekuitas");
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    /**
     *
     * @param year
     * @param kodeSatker
     * @param conn
     * @throws SQLException
     */
    @Override
    public void deletePerubahanEkuitasReports(short year, String kodeSatker, Connection conn) throws SQLException {
        Statement stm = null;
        String sql = "DELETE FROM perubahanekuitas"
                + "  where kodesatker='" + kodeSatker + "' and "
                + "  tahunanggaran=" + year ;
        try {
            stm = conn.createStatement();
            stm.executeUpdate(sql);
            
        } catch (SQLException ex) {
            throw new SQLException("Gagal menghapus data Perubahan Ekuitas");
        } finally {
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    /**
     *
     * @param year
     * @param triwulan
     * @param kodeSatker
     * @param conn
     * @return
     * @throws SQLException
     */
    @Override
    public List<LOReport> getLaporanOperasionalReports(short year, short triwulan, String kodeSatker, Connection conn) throws SQLException{
        List<LOReport> result = new ArrayList();
        
        getLOReportAkuns(year, triwulan, kodeSatker, result, conn);
        double pend=0, prapend =0;
        String strpend = "";
        if( result.size() > 0 ){
            LOReport r = result.get(result.size()-1);
            strpend = r.getUraian();
            pend = SIKDUtil.stringToDouble(r.getNilai());
            prapend = SIKDUtil.stringToDouble(r.getNilaiMin());      
        }
        if( strpend == null ) strpend = "";
        else if( !strpend.trim().equals("SURPLUS/DEFISIT DARI OPERASI") ) strpend = "";
        double jumlahsurplus =0,  jumlahprasurplus =0;
        getLOSurplusNonOPReports(kodeSatker, year, triwulan, result, conn);
        String strsur = "";
        if (result.size() > 0) {
            strsur = result.get(result.size()-1).getUraian();
            if (strsur != null && strsur.trim().equals("Jumlah Surplus Non Operasional")) {
                jumlahsurplus = SIKDUtil.stringToDouble(result.get(result.size() - 1).getNilai());
                jumlahprasurplus = SIKDUtil.stringToDouble(result.get(result.size() - 1).getNilaiMin());
            }
            else strsur = "";
        }
        
        double jumlahdef =0,  jumlahpradef =0;
        getLODefisitNonOPReports(kodeSatker, year, triwulan, result, conn);
        String strdef = "";        
        if (result.size() > 0) {
            strdef = result.get(result.size()-1).getUraian();
            if (strdef != null && strdef.trim().equals("Jumlah Defisit Non Operasional")) {
                jumlahdef = SIKDUtil.stringToDouble(result.get(result.size() - 1).getNilai());
                jumlahpradef = SIKDUtil.stringToDouble(result.get(result.size() - 1).getNilaiMin());
            }
            else strdef = "";
        }
        
        int no = 1;
        if(result.size() > 0 ){
            no = Integer.parseInt(result.get(result.size()-1).getNo());
            result.get(result.size()-1).setNo("");
        }
        
        if(!strsur.trim().equals("") || !strdef.trim().equals("")){
            result.add(new LOReport());
            double jumsd = jumlahsurplus+jumlahdef;
            double jumprasd = jumlahprasurplus+jumlahpradef;
            double seldef = Math.abs(jumsd-jumprasd);
            double perdef = 0;
            if(jumsd>jumprasd && jumsd > 0 ) perdef = (seldef/jumsd)*100;
            else if(jumprasd>jumsd && jumprasd > 0 ) perdef = (seldef/jumprasd)*100;
            result.add(new LOReport("", "JUMLAH SURPLUS/DEFISIT DARI KEGIATAN NON OPERASIONAL", SIKDUtil.doubleToString(jumsd), 
                    SIKDUtil.doubleToString(jumprasd), SIKDUtil.doubleToString(seldef), SIKDUtil.doubleToString(perdef)+" %", "font-weight: bold;"));
        }
        if(!strsur.trim().equals("") || !strdef.trim().equals("") || !strpend.trim().equals("")){
            result.add(new LOReport());
            double jumsd = jumlahsurplus+jumlahdef+pend;
            double jumprasd = jumlahprasurplus+jumlahpradef+prapend;
            double seldef = Math.abs(jumsd-jumprasd);
            double perdef = 0;
            if(jumsd>jumprasd && jumsd > 0 ) perdef = (seldef/jumsd)*100;
            else if(jumprasd>jumsd && jumprasd > 0 ) perdef = (seldef/jumprasd)*100;
            result.add(new LOReport(""+no, "SURPLUS/DEFISIT SEBELUM POS LUAR BIASA", SIKDUtil.doubleToString(jumsd), 
                    SIKDUtil.doubleToString(jumprasd), SIKDUtil.doubleToString(seldef), SIKDUtil.doubleToString(perdef)+" %", "font-weight: bold;"));
        }
        
        
        getLOPosLBReports(kodeSatker, year, triwulan, result, conn);
        return result;
    }
    
    /**
     *
     * @param year
     * @param triwulan
     * @param kodeSatker
     * @param result
     * @param conn
     * @return
     * @throws SQLException
     */
    public List<LOReport> getLOReportAkuns(short year, short triwulan, String kodeSatker, List<LOReport> result, Connection conn) throws SQLException {
        Statement stm = null;
        ResultSet rs = null;

        String sql = "with utama as( "
                + "  select distinct u.kodeakunutama kode, u.namaakunutama nama from opkodrekutama u  "
                + "  inner join lo on u.loindex=lo.loindex "
                + "  where lo.kodesatker='" + kodeSatker + "' and (lo.tahunanggaran=" + (year-1) + " or lo.tahunanggaran=" + year + ") "
                + " and lo.triwulan= " + triwulan
                + "  order by kode), "
                + "kelompok as "
                + "  (select distinct u.kodeakunutama || '.' || k.kodeakunkelompok kode, upper(k.namaakunkelompok) nama from opkodrekkelompok k "
                + "  inner join opkodrekutama u on u.akunutamaindex=k.kodrekutamaindex "
                + "  inner join lo on u.loindex=lo.loindex "
                + "  where lo.kodesatker='" + kodeSatker + "' and (lo.tahunanggaran=" + (year-1) + " or lo.tahunanggaran=" + year + ") "
                + " and lo.triwulan="+triwulan
                + "  order by kode), "
                + "  jenis as "
                + "  (select distinct u.kodeakunutama || '.' || k.kodeakunkelompok || '.' ||  j.kodeakunjenis kode, "
                + "  upper(j.namaakunjenis) nama from opkodrekjenis j "
                + "  inner join opkodrekkelompok k on j.kodrekkelompokindex=k.akunkelompokindex "
                + "  inner join opkodrekutama u on u.akunutamaindex=k.kodrekutamaindex "
                + "  inner join lo on u.loindex=lo.loindex "
                + "  where lo.kodesatker='" + kodeSatker + "' and (lo.tahunanggaran=" + (year-1) + " or lo.tahunanggaran=" + year + ") "
                + " and lo.triwulan=" + triwulan
                + "  order by kode), "
                + "akun as "
                + "(select * from utama union select * from kelompok union select jenis.kode, initcap(jenis.nama) from jenis "
                + "order by kode), "
                + "jumlah as "
                + "(select u.kodeakunutama || '.' || k.kodeakunkelompok || '.' ||  j.kodeakunjenis kode, "
                + "  upper(j.namaakunjenis) nama, sum(o.nilai) nilai from opkodrekjenis j "
                + "  inner join opkodrekobjek o on j.akunjenisindex=o.kodrekjenisindex "
                + "  inner join opkodrekkelompok k on j.kodrekkelompokindex=k.akunkelompokindex "
                + "  inner join opkodrekutama u on u.akunutamaindex=k.kodrekutamaindex "
                + "  inner join lo on u.loindex=lo.loindex "
                + "  where lo.kodesatker='" + kodeSatker + "' and lo.tahunanggaran= " + year 
                + " and lo.triwulan=" + triwulan 
                + "  group by kode, nama "
                + "  order by kode), "
                + "  jumlahmin as "
                + "  (select u.kodeakunutama || '.' || k.kodeakunkelompok || '.' ||  j.kodeakunjenis kode, "
                + "  upper(j.namaakunjenis) nama, sum(o.nilai) nilai from opkodrekjenis j "
                + "  inner join opkodrekobjek o on j.akunjenisindex=o.kodrekjenisindex "
                + "  inner join opkodrekkelompok k on j.kodrekkelompokindex=k.akunkelompokindex "
                + "  inner join opkodrekutama u on u.akunutamaindex=k.kodrekutamaindex "
                + "  inner join lo on u.loindex=lo.loindex "
                + "  where lo.kodesatker='" + kodeSatker + "' and lo.tahunanggaran= " + (year-1) 
                + " and lo.triwulan=" + triwulan
                + "  group by kode, nama "
                + "  order by kode) "
                + "  "
                + "select a.*, jumlahmin.nilai pranilai, jumlah.nilai from akun a "
                + "left join jumlah on a.kode=jumlah.kode "
                + "left join jumlahmin on a.kode=jumlahmin.kode "
                + "order by a.kode";
        
        try {
            stm = conn.createStatement();
            rs = stm.executeQuery(sql);
            int no = 1;
            double jum4 = 0, prajum4=0;
            double jum5 = 0, prajum5=0;
            double j1 = 0, praj1=0;
            double j3 = 0, praj3=0;
            String nama1 ="", nama3 = "";
            String kode1="";
            while(rs.next()){
                double nilai = rs.getDouble("nilai");
                double pranilai = rs.getDouble("pranilai");
                double naik = Math.abs(nilai-pranilai);
                double persen = 0;
                if(nilai >= pranilai && nilai > 0 ) persen = (naik/nilai) *100;
                else if(pranilai > nilai && pranilai> 0) persen = (naik/pranilai)*100;
                
                if( rs.getString("kode").trim().length() == 1 ||  rs.getString("kode").trim().length() == 3 ){
                    if( rs.getString("kode").trim().length() == 3 ){
                        if(!nama3.trim().equals("")){
                            double na = Math.abs(j3-praj3);
                            double per = 0;
                            if(j3 >= praj3 && j3 > 0 ) per = (na/j3) *100;
                            else if(praj3 > j3 && praj3> 0) per = (na/praj3)*100;
                            
                            result.add(new LOReport("", "Jumlah " + SIKDUtil.initCap(nama3), 
                                    SIKDUtil.doubleToString(j3), SIKDUtil.doubleToString(praj3), 
                                    SIKDUtil.doubleToString(na), SIKDUtil.doubleToString(per) + " %", "font-weight:bold;"));
                            result.add(new LOReport());
                            j3 = nilai; praj3 = pranilai;
                        }
                        nama3=rs.getString("nama").trim();
                    }
                    else if( rs.getString("kode").trim().length() == 1 ){                        
                        if(!nama1.trim().equals("")){
                            double na = Math.abs(j3-praj3);
                            double per = 0;
                            if(j3 >= praj3 && j3 > 0 ) per = (na/j3) *100;
                            else if(praj3 > j3 && praj3> 0) per = (na/praj3)*100;
                            result.add(new LOReport("", "Jumlah " + SIKDUtil.initCap(nama3), 
                                    SIKDUtil.doubleToString(j3), SIKDUtil.doubleToString(praj3), 
                                    SIKDUtil.doubleToString(na), SIKDUtil.doubleToString(per) + " %", "font-weight:bold;"));
                            result.add(new LOReport());
                            j3 = nilai; praj3 = pranilai;
                            
                            na = Math.abs(j1-praj1);
                            per = 0;
                            if(j1 >= praj1 && j1 > 0 ) per = (na/j1) *100;
                            else if(praj1 > j1 && praj1> 0) per = (na/praj1)*100;
                            result.add(new LOReport("", "JUMLAH " + nama1, 
                                    SIKDUtil.doubleToString(j1), SIKDUtil.doubleToString(praj1),
                                    SIKDUtil.doubleToString(na), SIKDUtil.doubleToString(per) + " %", "font-weight:bold;"));
                            result.add(new LOReport());
                            if(kode1.trim().equals("4")) {
                                jum4 = j1; prajum4 = praj1;
                            }
                            j1 = nilai; praj1 = pranilai;                
                            nama3="";
                        }
                        nama1=rs.getString("nama").trim();
                        kode1 = rs.getString("kode").trim();
                    }
                    result.add(new LOReport(String.valueOf(no), rs.getString("nama").trim(), 
                        "", "", 
                        "", "", ""));
                    result.get(result.size()-1).setFontStyle("font-weight: bold;");
                }
                else{
                    result.add(new LOReport(String.valueOf(no), rs.getString("nama").trim(), 
                        SIKDUtil.doubleToString(nilai), SIKDUtil.doubleToString(pranilai), 
                        SIKDUtil.doubleToString(naik), SIKDUtil.doubleToString(persen) + " %", ""));
                    j3+=nilai; praj3+=pranilai;
                    j1+=nilai; praj1+=pranilai;
                }                
                no++;
            }
            if( result.size() > 0 ){
                double na = Math.abs(j3-praj3);
                double per = 0;
                if(j3 >= praj3 && j3 > 0 ) per = (na/j3) *100;
                else if(praj3 > j3 && praj3> 0) per = (na/praj3)*100;
                result.add(new LOReport("", "Jumlah " + SIKDUtil.initCap(nama3),
                        SIKDUtil.doubleToString(j3), SIKDUtil.doubleToString(praj3),
                        SIKDUtil.doubleToString(na), SIKDUtil.doubleToString(per) + " %", "font-weight:bold;"));
                result.add(new LOReport());
                na = Math.abs(j1-praj1);
                per = 0;
                if(j1 >= praj1 && j1 > 0 ) per = (na/j1) *100;
                else if(praj1 > j1 && praj1> 0) per = (na/praj1)*100;
                result.add(new LOReport("", "JUMLAH " + nama1,
                        SIKDUtil.doubleToString(j1), SIKDUtil.doubleToString(praj1),
                        SIKDUtil.doubleToString(na), SIKDUtil.doubleToString(per) + " %", "font-weight:bold;"));
                result.add(new LOReport());
                na = Math.abs((jum4-j1)-(prajum4-praj1));
                per = 0;
                if(Math.abs(jum4-j1) >= Math.abs(prajum4-praj1) && Math.abs(jum4-j1) > 0 ) per = (na/Math.abs(jum4-j1)) *100;
                else if(Math.abs(prajum4-praj1) > Math.abs(jum4-j1) && Math.abs(prajum4-praj1)> 0) per = (na/Math.abs(prajum4-praj1))*100;
                result.add(new LOReport(""+no, "SURPLUS/DEFISIT DARI OPERASI",
                        SIKDUtil.doubleToString(jum4-j1), SIKDUtil.doubleToString(prajum4-praj1),
                        SIKDUtil.doubleToString(na), 
                        SIKDUtil.doubleToString(per) + " %", "font-weight:bold;"));
                
            }
            
            return result;
        } catch (SQLException ex) {
            throw new SQLException("Gagal mengambil data Perubahan Ekuitas");
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    public List<LOReport> getLOSurplusNonOPReports(String kodeSatker, short  year, short triwulan, List<LOReport> result, Connection conn) throws SQLException {
        Statement stm = null;
        ResultSet rs = null;

        String sql = "SELECT * FROM surplusnonop s "
                + "  where loindex in (select loindex from lo "
                + "  where kodesatker='" + kodeSatker + "' and tahunanggaran= " + year 
                + " and triwulan=" + triwulan
                + " ) " ;
        try {
            stm = conn.createStatement();
            rs = stm.executeQuery(sql);
            LOSurplusNonOperasional sur = null;
            if(rs.next()){
                sur = new LOSurplusNonOperasional(rs.getLong("surplusnonopindex"), rs.getDouble("penjualanasetnonlancar"), rs.getDouble("kewajibanjangkapanjang"), rs.getDouble("surpluslainnya"));
            }
            rs=stm.executeQuery("SELECT * FROM surplusnonop s "
                + "  where loindex in (select loindex from lo "
                + "  where kodesatker='" + kodeSatker + "' and tahunanggaran= " + (short)(year-1) 
                    +" and triwulan=" + triwulan
                    + " ) ");
            LOSurplusNonOperasional prasur = null;
            if(rs.next()){
                prasur = new LOSurplusNonOperasional(rs.getLong("surplusnonopindex"), rs.getDouble("penjualanasetnonlancar"), rs.getDouble("kewajibanjangkapanjang"), rs.getDouble("surpluslainnya"));
            }
            
            if (sur != null || prasur != null) {
                int no = 1;
                if (result.size() > 0) {
                    LOReport r = result.get(result.size() - 1);                    
                    no = Integer.parseInt(r.getNo());
                    result.get(result.size() - 1).setNo("");
                }
                result.add(new LOReport());

                result.add(new LOReport("", "SURPLUS/DEFISIT DARI KEGIATAN NON OPERASIONAL", "", "", "", "", "font-weight: bold;"));
                result.add(new LOReport("", "   SURPLUS NON OPERASIONAL", "", "", "", "", "font-weight: bold;"));
                if (sur == null) {
                    sur = new LOSurplusNonOperasional();
                }
                if (prasur == null) {
                    prasur = new LOSurplusNonOperasional();
                }
                double jumlahsurplus = sur.getKewajibanJangkaPanjang() + sur.getPenjualanAsetNonLancar() + sur.getSurplusLainnya();
                double jumlahprasurplus = prasur.getKewajibanJangkaPanjang() + prasur.getPenjualanAsetNonLancar() + prasur.getSurplusLainnya();
                double sel = Math.abs(sur.getPenjualanAsetNonLancar() - prasur.getPenjualanAsetNonLancar());
                double per = 0;
                if (sur.getPenjualanAsetNonLancar() > prasur.getPenjualanAsetNonLancar() && sur.getPenjualanAsetNonLancar() > 0) {
                    per = (sel / sur.getPenjualanAsetNonLancar()) * 100;
                } else if (prasur.getPenjualanAsetNonLancar() > sur.getPenjualanAsetNonLancar() && prasur.getPenjualanAsetNonLancar() > 0) {
                    per = (sel / prasur.getPenjualanAsetNonLancar()) * 100;
                }
                result.add(new LOReport(String.valueOf(no++), "      Surplus Penjualan Aset Nonlancar", SIKDUtil.doubleToString(sur.getPenjualanAsetNonLancar()),
                        SIKDUtil.doubleToString(prasur.getPenjualanAsetNonLancar()), SIKDUtil.doubleToString(sel), SIKDUtil.doubleToString(per), ""));

                sel = Math.abs(sur.getKewajibanJangkaPanjang() - prasur.getKewajibanJangkaPanjang());
                per = 0;
                if (sur.getKewajibanJangkaPanjang() > prasur.getKewajibanJangkaPanjang() && sur.getKewajibanJangkaPanjang() > 0) {
                    per = (sel / sur.getKewajibanJangkaPanjang()) * 100;
                } else if (prasur.getKewajibanJangkaPanjang() > sur.getKewajibanJangkaPanjang() && prasur.getKewajibanJangkaPanjang() > 0) {
                    per = (sel / prasur.getKewajibanJangkaPanjang()) * 100;
                }
                result.add(new LOReport(String.valueOf(no++), "      Surplus Penyelesaian Kewajiban Jangka Panjang", SIKDUtil.doubleToString(sur.getKewajibanJangkaPanjang()),
                        SIKDUtil.doubleToString(prasur.getKewajibanJangkaPanjang()), SIKDUtil.doubleToString(sel), SIKDUtil.doubleToString(per), ""));

                sel = Math.abs(sur.getSurplusLainnya() - prasur.getSurplusLainnya());
                per = 0;
                if (sur.getSurplusLainnya() > prasur.getSurplusLainnya() && sur.getSurplusLainnya() > 0) {
                    per = (sel / sur.getSurplusLainnya()) * 100;
                } else if (prasur.getSurplusLainnya() > sur.getSurplusLainnya() && prasur.getSurplusLainnya() > 0) {
                    per = (sel / prasur.getSurplusLainnya()) * 100;
                }
                result.add(new LOReport(String.valueOf(no++), "      Surplus Dari Kegiatan Non Operasional Lainnya", SIKDUtil.doubleToString(sur.getSurplusLainnya()),
                        SIKDUtil.doubleToString(prasur.getSurplusLainnya()), SIKDUtil.doubleToString(sel), SIKDUtil.doubleToString(per), ""));

                sel = Math.abs(jumlahsurplus - jumlahprasurplus);
                per = 0;
                if (jumlahsurplus > jumlahprasurplus && jumlahsurplus > 0) {
                    per = (sel / jumlahsurplus) * 100;
                } else if (jumlahprasurplus > jumlahsurplus && jumlahprasurplus > 0) {
                    per = (sel / jumlahprasurplus) * 100;
                }
                result.add(new LOReport(""+no, "   Jumlah Surplus Non Operasional", SIKDUtil.doubleToString(jumlahsurplus),
                        SIKDUtil.doubleToString(jumlahprasurplus), SIKDUtil.doubleToString(sel), SIKDUtil.doubleToString(per), "font-weight: bold;"));
            }
            
            return result;
        } catch (SQLException ex) {
            throw new SQLException("Gagal mengambil data Surplus Non Operasional");
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    public List<LOReport> getLODefisitNonOPReports(String kodeSatker, short  year, short triwulan, List<LOReport> result, Connection conn) throws SQLException {
        Statement stm = null;
        ResultSet rs = null;

        String sql = "SELECT * FROM defisitnonop "
                + "  where loindex in (select loindex from lo "
                + "  where kodesatker='" + kodeSatker + "' and tahunanggaran= " + year 
                + " and triwulan=" + triwulan
                + " ) " ;
        try {
            stm = conn.createStatement();
            rs = stm.executeQuery(sql);
            LODefisitNonOperasional def = null;
            if(rs.next()){
                def = new LODefisitNonOperasional(rs.getLong("defisitnonopindex"), rs.getDouble("penjualanasetnonlancar"), rs.getDouble("kewajibanjangkapanjang"), rs.getDouble("defisitlainnya"));
            }
            
            rs = stm.executeQuery("SELECT * FROM defisitnonop "
                + "  where loindex in (select loindex from lo "
                + "  where kodesatker='" + kodeSatker + "' and tahunanggaran= " + (short)(year-1) 
                    + " and triwulan=" + triwulan
                    + " ) ");
            LODefisitNonOperasional pradef = null;
            if(rs.next()){
                pradef = new LODefisitNonOperasional(rs.getLong("defisitnonopindex"), rs.getDouble("penjualanasetnonlancar"), rs.getDouble("kewajibanjangkapanjang"), rs.getDouble("defisitlainnya"));
            }
            
            if(def!=null || pradef!=null){
                int no = 1;
                String str = "";

                if (result.size() > 0) {
                    LOReport r = result.get(result.size() - 1);                    
                    no = Integer.parseInt(r.getNo());
                    str = r.getUraian();
                    result.get(result.size() - 1).setNo("");
                }
                result.add(new LOReport());
                if (str != null && !str.trim().equals("Jumlah Surplus Non Operasional")) {    
                    result.add(new LOReport("", "SURPLUS/DEFISIT DARI KEGIATAN NON OPERASIONAL", "", "", "", "", "font-weight: bold;"));
                }
                
                result.add(new LOReport("", "DEFISIT NON OPERASIONAL", "", "", "", "", "font-weight: bold;"));
                if (def == null) {
                    def = new LODefisitNonOperasional();
                }
                if (pradef == null) {
                    pradef = new LODefisitNonOperasional();
                }
                double jumlahdef = def.getKewajibanJangkaPanjang() + def.getPenjualanAsetNonLancar() + def.getDefisitLainnya();
                double jumlahpradef = pradef.getKewajibanJangkaPanjang() + pradef.getPenjualanAsetNonLancar() + pradef.getDefisitLainnya();
                double sel = Math.abs(def.getPenjualanAsetNonLancar() - pradef.getPenjualanAsetNonLancar());
                double per = 0;
                if (def.getPenjualanAsetNonLancar() > pradef.getPenjualanAsetNonLancar() && def.getPenjualanAsetNonLancar() > 0) {
                    per = (sel / def.getPenjualanAsetNonLancar()) * 100;
                } else if (pradef.getPenjualanAsetNonLancar() > def.getPenjualanAsetNonLancar() && pradef.getPenjualanAsetNonLancar() > 0) {
                    per = (sel / pradef.getPenjualanAsetNonLancar()) * 100;
                }
                result.add(new LOReport(String.valueOf(no++), "      Defisit Penjualan Aset Nonlancar", SIKDUtil.doubleToString(def.getPenjualanAsetNonLancar()),
                        SIKDUtil.doubleToString(pradef.getPenjualanAsetNonLancar()), SIKDUtil.doubleToString(sel), SIKDUtil.doubleToString(per), ""));

                sel = Math.abs(def.getKewajibanJangkaPanjang() - pradef.getKewajibanJangkaPanjang());
                per = 0;
                if (def.getKewajibanJangkaPanjang() > pradef.getKewajibanJangkaPanjang() && def.getKewajibanJangkaPanjang() > 0) {
                    per = (sel / def.getKewajibanJangkaPanjang()) * 100;
                } else if (pradef.getKewajibanJangkaPanjang() > def.getKewajibanJangkaPanjang() && pradef.getKewajibanJangkaPanjang() > 0) {
                    per = (sel / pradef.getKewajibanJangkaPanjang()) * 100;
                }
                result.add(new LOReport(String.valueOf(no++), "      Defisit Penyelesaian Kewajiban Jangka Panjang", SIKDUtil.doubleToString(def.getKewajibanJangkaPanjang()),
                        SIKDUtil.doubleToString(pradef.getKewajibanJangkaPanjang()), SIKDUtil.doubleToString(sel), SIKDUtil.doubleToString(per), ""));

                sel = Math.abs(def.getDefisitLainnya() - pradef.getDefisitLainnya());
                per = 0;
                if (def.getDefisitLainnya() > pradef.getDefisitLainnya()&& def.getDefisitLainnya()> 0) {
                    per = (sel / def.getDefisitLainnya()) * 100;
                } else if (pradef.getDefisitLainnya() > def.getDefisitLainnya()&& pradef.getDefisitLainnya()> 0) {
                    per = (sel / pradef.getDefisitLainnya()) * 100;
                }
                result.add(new LOReport(String.valueOf(no++), "      Defisit Dari Kegiatan Non Operasional Lainnya", SIKDUtil.doubleToString(def.getDefisitLainnya()),
                        SIKDUtil.doubleToString(pradef.getDefisitLainnya() ), SIKDUtil.doubleToString(sel), SIKDUtil.doubleToString(per), ""));

                sel = Math.abs(jumlahdef - jumlahpradef);
                per = 0;
                if (jumlahdef > jumlahpradef && jumlahdef > 0) {
                    per = (sel / jumlahdef) * 100;
                } else if (jumlahpradef > jumlahdef && jumlahpradef > 0) {
                    per = (sel / jumlahpradef) * 100;
                }
                result.add(new LOReport(""+no, "   Jumlah Defisit Non Operasional", SIKDUtil.doubleToString(jumlahdef),
                        SIKDUtil.doubleToString(jumlahpradef), SIKDUtil.doubleToString(sel), SIKDUtil.doubleToString(per), "font-weight: bold;"));                                
            }
            
            return result;
        } catch (SQLException ex) {
            throw new SQLException("Gagal mengambil data Defisit Non Operasional");
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    
    public List<LOReport> getLOPosLBReports(String kodeSatker, short  year, short triwulan, List<LOReport> result, Connection conn) throws SQLException {
        Statement stm = null;
        ResultSet rs = null;

        String sql = "SELECT * FROM posluarbiasaop "
                + "  where loindex in (select loindex from lo "
                + "  where kodesatker='" + kodeSatker + "' and tahunanggaran= " + year 
                + " and triwulan=" + triwulan
                + " ) " ;
        try {
            stm = conn.createStatement();
            rs = stm.executeQuery(sql);
            LOPosLuarBiasa pos = null;
            if(rs.next()){
                pos = new LOPosLuarBiasa(rs.getLong("posluarbiasaindex"), rs.getDouble("pendapatan"), rs.getDouble("beban"));
            }
            
            rs = stm.executeQuery("SELECT * FROM posluarbiasaop "
                + "  where loindex in (select loindex from lo "
                + "  where kodesatker='" + kodeSatker + "' and tahunanggaran= " + (short)(year-1) 
                    + " and triwulan= " + triwulan
                    + " ) ");
            LOPosLuarBiasa prapos = null;
            if( rs.next() ){
                prapos = new LOPosLuarBiasa(rs.getLong("posluarbiasaindex"), rs.getDouble("pendapatan"), rs.getDouble("beban"));
            }
            
            if(pos!= null || prapos != null){
                if(pos == null) pos = new LOPosLuarBiasa();
                if(prapos==null ) prapos = new LOPosLuarBiasa();
                int no = 1;
                double jum =0, prajum=0;
                if(result.size() > 0 ){
                    LOReport lo = result.get(result.size()-1);
                    no = Integer.parseInt(lo.getNo());
                    jum=SIKDUtil.stringToDouble(lo.getNilai());
                    prajum = SIKDUtil.stringToDouble(lo.getNilaiMin());
                    result.get(result.size()-1).setNo("");
                }
                result.add(new LOReport());
                result.add(new LOReport("", "POS LUAR BIASA", "", "", "", "", "font-weight: bold;"));
                result.add(new LOReport("", "PENDAPATAN LUAR BIASA", "", "", "", "", "font-weight: bold;"));
                double j = pos.getPendapatan();
                double praj = prapos.getPendapatan();
                double sel = Math.abs(j-praj);
                double per = 0;
                if( j>praj && j > 0 ) per = (sel/j)*100;
                else if(praj>j && praj>0) per = (sel/praj)*100;
                result.add(new LOReport(String.valueOf(no++), "Pendapatan Luar Biasa", SIKDUtil.doubleToString(j), SIKDUtil.doubleToString(praj), SIKDUtil.doubleToString(sel), SIKDUtil.doubleToString(per)+" %", ""));
                result.add(new LOReport("", "Jumlah Pendapatan Luar Biasa", SIKDUtil.doubleToString(j), SIKDUtil.doubleToString(praj), SIKDUtil.doubleToString(sel), SIKDUtil.doubleToString(per)+" %", "font-weight: bold;"));
                
                result.add(new LOReport());
                result.add(new LOReport("", "BEBAN LUAR BIASA", "", "", "", "", "font-weight: bold;"));
                double b = pos.getBeban();
                double prab = prapos.getBeban();
                double selb = Math.abs(b-prab);
                double perb = 0;
                if( b>prab && b > 0 ) perb = (selb/b)*100;
                else if(prab>b && prab>0) perb = (selb/prab)*100;
                result.add(new LOReport(String.valueOf(no++), "Beban Luar Biasa", SIKDUtil.doubleToString(b), SIKDUtil.doubleToString(prab), SIKDUtil.doubleToString(selb), SIKDUtil.doubleToString(perb)+" %", ""));
                result.add(new LOReport("", "Jumlah Beban Luar Biasa", SIKDUtil.doubleToString(b), SIKDUtil.doubleToString(prab), SIKDUtil.doubleToString(selb), SIKDUtil.doubleToString(perb)+" %", "font-weight: bold;"));
                result.add(new LOReport());
                b=b+j;
                prab=prab+praj;
                selb = Math.abs(b-prab);
                perb = 0;
                if( b>prab && b > 0 ) perb = (selb/b)*100;
                else if(prab>b && prab>0) perb = (selb/prab)*100;
                result.add(new LOReport("", "JUMLAH POS LUAR BIASA", SIKDUtil.doubleToString(b), SIKDUtil.doubleToString(prab), SIKDUtil.doubleToString(selb), SIKDUtil.doubleToString(perb)+" %", "font-weight: bold;"));
                result.add(new LOReport());
                b=b+jum;
                prab=prab+prajum;
                selb = Math.abs(b-prab);
                perb = 0;
                if( b>prab && b > 0 ) perb = (selb/b)*100;
                else if(prab>b && prab>0) perb = (selb/prab)*100;
                result.add(new LOReport("", "SURPLUS/DEFISIT-LO", SIKDUtil.doubleToString(b), SIKDUtil.doubleToString(prab), SIKDUtil.doubleToString(selb), SIKDUtil.doubleToString(perb)+" %", "font-weight: bold; background-color: activecaption;"));
            }
            return result;
        } catch (SQLException ex) {
            throw new SQLException("Gagal mengambil data Pos Luar Biasa");
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }
    
}