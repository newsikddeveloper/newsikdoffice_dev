/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.sikd.office.ejb.dbapi;

import app.sikd.entity.apbd.LampiranIPerdaAPBD;
import app.sikd.entity.utilitas.SimpleAccount;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author sora
 */
public class GfsSQL implements IGfsSQL {

    @SuppressWarnings("null")
    public List<SimpleAccount> getGfsAkunStruktur(Connection conn) throws SQLException {
        Statement stm = null;
        ResultSet rs = null;
        List<SimpleAccount> result = new ArrayList();
        try {
            stm = conn.createStatement();
            rs = stm.executeQuery("SELECT g.autoindex, g.accountcode, g.accountname, g.accounttype, gf.accountcode parentcode "
                    + "  FROM gfsaccount g "
                    + "  left join gfsaccountstructure gs on g.autoindex=gs.subaccount "
                    + "  left join gfsaccount gf on gs.parentaccount=gf.autoindex "
//                    + "  where g.accountcode like '1%'"
                    + "  order by g.accountcode ");
            while (rs.next()) {
                String code = rs.getString("accountcode");                
                SimpleAccount sa = new SimpleAccount(rs.getLong("autoindex"), code.trim(), rs.getString("accountname"), rs.getShort("accounttype"));
                String parentCode = rs.getString("parentcode");
                if (parentCode != null && !parentCode.trim().equals("") ) {
                    sa.setStyle(parentCode.trim());
                }
                result.add(sa);
            }
            
            return result;
        } catch (SQLException ex) {
//            ex.printStackTrace();
            Logger.getLogger(GfsSQL.class.getName()).log(Level.SEVERE, null, "Gagal mengambil data GFS Akun Struktur " + ex);
            throw new SQLException("SQL: Gagal mengambil data GFS Akun Struktur\n" + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }
    
  /*  public List<LampiranIPerdaAPBD> getGfsStatement1(short tahun, Connection conn) throws SQLException {
        Statement stm = null;
        ResultSet rs = null;
        List<LampiranIPerdaAPBD> result = new ArrayList();
        try {
            stm = conn.createStatement();
            rs = stm.executeQuery("SELECT g.accountcode, g.accountname, g.accounttype, gf.accountcode parentcode "
                    + "  FROM gfsaccount g "
                    + "  left join gfsaccountstructure gs on g.autoindex=gs.subaccount "
                    + "  left join gfsaccount gf on gs.parentaccount=gf.autoindex "
//                    + "  where g.accountcode like '1%'"
                    + "  order by g.accountcode ");
            while (rs.next()) {
                String code = rs.getString("accountcode");                
                SimpleAccount sa = new SimpleAccount(rs.getLong("autoindex"), code.trim(), rs.getString("accountname"), rs.getShort("accounttype"));
                String parentCode = rs.getString("parentcode");
                if (parentCode != null && !parentCode.trim().equals("") ) {
                    sa.setStyle(parentCode.trim());
                }
                result.add(sa);
            }
            
            return result;
        } catch (SQLException ex) {
//            ex.printStackTrace();
            Logger.getLogger(GfsSQL.class.getName()).log(Level.SEVERE, null, "Gagal mengambil data GFS Akun Struktur " + ex);
            throw new SQLException("SQL: Gagal mengambil data GFS Akun Struktur\n" + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }
*/
}
