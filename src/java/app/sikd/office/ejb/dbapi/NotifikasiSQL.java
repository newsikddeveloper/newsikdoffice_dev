/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.sikd.office.ejb.dbapi;

import app.sikd.entity.utilitas.MappingAccount;
import app.sikd.util.SIKDUtil;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author sora
 */
public class NotifikasiSQL implements INotifikasiSQL {

    /**
     *
     * @param tahun
     * @param kodeSatker
     * @param kodeData
     * @param jeniscoa
     * @param conn
     * @return
     * @throws SQLException
     */
    @Override
    public short getApbdNotifikasi(short tahun, String kodeSatker, short kodeData, short jeniscoa, Connection conn) throws SQLException {
        short result = -1;
        Statement stm = null;
        ResultSet rs = null;
        String sql = "select statusdata from apbd where tahunanggaran=" + tahun + " and kodesatker='" + kodeSatker + "' and "
                + " kodedata=" + kodeData + " and jeniscoa=" + jeniscoa;
        try {
            stm = conn.createStatement();
            rs = stm.executeQuery(sql);
            if (rs.next()) {
                return rs.getShort("statusdata");
            }
            return result;
        } catch (SQLException e) {
            throw new SQLException(e.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }

    /**
     *
     * @param tahun
     * @param periode
     * @param kodeSatker
     * @param kodeData
     * @param jeniscoa
     * @param conn
     * @return
     * @throws SQLException
     */
    @Override
    public short getLraNotifikasi(short tahun, short periode, String kodeSatker, short kodeData, short jeniscoa, Connection conn) throws SQLException {
        short result = -1;
        Statement stm = null;
        ResultSet rs = null;
        String sql = "select statusdata from realisasiapbd where tahunanggaran=" + tahun + " and  bulan=" + periode
                + " and kodesatker='" + kodeSatker + "' and "
                + " kodedata=" + kodeData + " and jeniscoa=" + jeniscoa;
        try {
            stm = conn.createStatement();
            rs = stm.executeQuery(sql);
            if (rs.next()) {
                return rs.getShort("statusdata");
            }
            return result;
        } catch (SQLException e) {
            throw new SQLException(e.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }

    /**
     *
     * @param tahun
     * @param periode
     * @param kodeSatker
     * @param conn
     * @return
     * @throws SQLException
     */
    @Override
    public short getDthNotifikasi(short tahun, short periode, String kodeSatker, Connection conn) throws SQLException {
        short result = -1;
        Statement stm = null;
        ResultSet rs = null;
        String sql = "select statusdata from dth where tahunanggaran=" + tahun + " and  periode=" + periode
                + " and kodesatker='" + kodeSatker + "' ";
        try {
            stm = conn.createStatement();
            rs = stm.executeQuery(sql);
            if (rs.next()) {
                return rs.getShort("statusdata");
            }
            return result;
        } catch (SQLException e) {
            throw new SQLException(e.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }

    /**
     *
     * @param tahun
     * @param semester
     * @param kodeSatker
     * @param conn
     * @return
     * @throws SQLException
     */
    @Override
    public short getNeracaNotifikasi(short tahun, short semester, String kodeSatker, Connection conn) throws SQLException {
        short result = -1;
        Statement stm = null;
        ResultSet rs = null;
        String sql = "select statusdata from neraca where tahunanggaran=" + tahun + " and  semester=" + semester
                + " and kodesatker='" + kodeSatker + "' ";
        try {
            stm = conn.createStatement();
            rs = stm.executeQuery(sql);
            if (rs.next()) {
                return rs.getShort("statusdata");
            }
            return result;
        } catch (SQLException e) {
            throw new SQLException(e.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }

    /**
     *
     * @param tahun
     * @param kodeSatker
     * @param conn
     * @return
     * @throws SQLException
     */
    @Override
    public short getArusKasNotifikasi(short tahun, String kodeSatker, Connection conn) throws SQLException {
        short result = -1;
        Statement stm = null;
        ResultSet rs = null;
        String sql = "select statusdata from aruskas where tahunanggaran=" + tahun
                + " and kodesatker='" + kodeSatker + "' ";
        try {
            stm = conn.createStatement();
            rs = stm.executeQuery(sql);
            if (rs.next()) {
                return rs.getShort("statusdata");
            }
            return result;
        } catch (SQLException e) {
            throw new SQLException(e.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }

    /**
     *
     * @param tahun
     * @param triwulan
     * @param kodeSatker
     * @param conn
     * @return
     * @throws SQLException
     */
    @Override
    public short getLONotifikasi(short tahun, short triwulan, String kodeSatker, Connection conn) throws SQLException {
        short result = -1;
        Statement stm = null;
        ResultSet rs = null;
        String sql = "select statusdata from lo where tahunanggaran=" + tahun + " and  triwulan=" + triwulan
                + " and kodesatker='" + kodeSatker + "' ";
        try {
            stm = conn.createStatement();
            rs = stm.executeQuery(sql);
            if (rs.next()) {
                return rs.getShort("statusdata");
            }
            return result;
        } catch (SQLException e) {
            throw new SQLException(e.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }

    /**
     *
     * @param tahun
     * @param kodeSatker
     * @param conn
     * @return
     * @throws SQLException
     */
    @Override
    public short getEkuitasNotifikasi(short tahun, String kodeSatker, Connection conn) throws SQLException {
        short result = -1;
        Statement stm = null;
        ResultSet rs = null;
        String sql = "select statusdata from perubahanekuitas where tahunanggaran=" + tahun
                + " and kodesatker='" + kodeSatker + "' ";
        try {
            stm = conn.createStatement();
            rs = stm.executeQuery(sql);
            if (rs.next()) {
                return rs.getShort("statusdata");
            }
            return result;
        } catch (SQLException e) {
            throw new SQLException(e.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }

    /**
     *
     * @param tahun
     * @param kodeSatker
     * @param conn
     * @return
     * @throws SQLException
     */
    @Override
    public short getSalNotifikasi(short tahun, String kodeSatker, Connection conn) throws SQLException {
        short result = -1;
        Statement stm = null;
        ResultSet rs = null;
        String sql = "select statusdata from perubahansal where tahunanggaran=" + tahun
                + " and kodesatker='" + kodeSatker + "' ";
        try {
            stm = conn.createStatement();
            rs = stm.executeQuery(sql);
            if (rs.next()) {
                return rs.getShort("statusdata");
            }
            return result;
        } catch (SQLException e) {
            throw new SQLException(e.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }

    /**
     *
     * @param tahun
     * @param kodeSatker
     * @param conn
     * @return
     * @throws SQLException
     */
    @Override
    public short getPFKNotifikasi(short tahun, String kodeSatker, Connection conn) throws SQLException {
        short result = -1;
        Statement stm = null;
        ResultSet rs = null;
        String sql = "select statusdata from pfk where tahunanggaran=" + tahun
                + " and kodesatker='" + kodeSatker + "' ";
        try {
            stm = conn.createStatement();
            rs = stm.executeQuery(sql);
            if (rs.next()) {
                return rs.getShort("statusdata");
            }
            return result;
        } catch (SQLException e) {
            throw new SQLException(e.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }

    /**
     *
     * @param tahun
     * @param kodeSatker
     * @param conn
     * @return
     * @throws SQLException
     */
    @Override
    public short getPinjamanDaerahNotifikasi(short tahun, String kodeSatker, Connection conn) throws SQLException {
        short result = -1;
        Statement stm = null;
        ResultSet rs = null;
        String sql = "select statusdata from pinjamandaerah where tahunanggaran=" + tahun
                + " and kodesatker='" + kodeSatker + "' ";
        try {
            stm = conn.createStatement();
            rs = stm.executeQuery(sql);
            if (rs.next()) {
                return rs.getShort("statusdata");
            }
            return result;
        } catch (SQLException e) {
            throw new SQLException(e.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }

    public List<MappingAccount> getAllNotifikasi(short year, String kodesatker, short jenisCOA, Connection conn) throws SQLException {
        List<MappingAccount> result = new ArrayList();
        Statement stm = null;
        ResultSet rs = null;
        String sql = "with bulan as( "
                + "select 1 as bln union select 2 as bln "
                + "union select 3 as bln union select 4 as bln "
                + "union select 5 as bln union select 6 as bln "
                + "union select 7 as bln union select 8 as bln "
                + "union select 9 as bln union select 10 as bln "
                + "union select 11 as bln union select 12 as bln "
                + "order by bln "
                + "), "
                + "semester as( select 1 as smstr union select 2 as smstr ), "
                + "triwulan as( "
                + " select 1 as tri union select 2 as tri union select 3 as tri union select 4 as tri "
                + "), "
                + "apbdmtitle as( select 'APBD Murni'::character(255) apbdtitle ), "
                + "apbdM as( "
                + " select 'APBD Murni'::character(255) as title, statusdata apbdmstat from apbd "
                + " where tahunanggaran=" + year + " and kodesatker='" + kodesatker + "' and "
                + " kodedata=0 and jeniscoa= " + jenisCOA
                + "), "
                + "apbdptitle as( select 'APBD Perubahan'::character(255) apbdtitle ), "
                + "apbdp as( "
                + " select 'APBD Perubahan'::character(255) as title, statusdata apbdpstat from apbd "
                + " where tahunanggaran=" + year + " and kodesatker='" + kodesatker + "' and "
                + " kodedata=1 and jeniscoa= " + jenisCOA
                + "), "
                + "apbdMurni as( "
                + " select 1 as id, apbdtitle as ikd, 0::integer as bln, apbdmstat status from apbdmtitle "
                + " left join apbdm on apbdmtitle.apbdtitle=apbdM.title "
                + "), "
                + "apbdRubah as( "
                + " select 2 as id, apbdtitle as ikd, 0::integer as bln, apbdpstat status from apbdptitle "
                + " left join apbdp on apbdptitle.apbdtitle=apbdP.title "
                + "), "
                + "lraM as( "
                + " select bulan, statusdata statuslraM from realisasiapbd "
                + " where tahunanggaran=" + year + " and kodesatker='" + kodesatker + "' and "
                + " kodedata=0 and jeniscoa= " + jenisCOA
                + "), "
                + "lraP as( "
                + " select bulan, statusdata statuslraP from realisasiapbd "
                + " where tahunanggaran=" + year + " and kodesatker='" + kodesatker + "' and "
                + " kodedata=1 and jeniscoa= " + jenisCOA
                + "), "
                + "lraAll as( "
                + " select bulan.bln as bln, statuslraM, statuslraP  from bulan "
                + " left join lraM on bulan.bln=lraM.bulan "
                + " left join lraP on bulan.bln=lraP.bulan "
                + "), "
                + "lra as( "
                + " select 3 as id, 'lra bulan '||bln as ikd, bln::integer, "
                + " CASE WHEN statuslraM>=0 THEN (select statuslraM) "
                + "            WHEN statuslraM<0 and bln >6 THEN (select statuslraP) "
                + "            ELSE (select statuslraM) "
                + "       END as status "
                + "       from lraAll "
                + "), "
                + "dtht as( "
                + " select periode, statusdata from dth "
                + " where tahunanggaran=" + year + " and kodesatker='" + kodesatker + "'"
                + "),"
                + "dth2 as( "
                + " select 4 as id, 'DTH Periode '||bln as ikd, bln::integer, statusdata from bulan "
                + " left join dtht on bulan.bln=dtht.periode "
                + "), "
                + "nrct as( "
                + " select semester, statusdata from neraca "
                + " where tahunanggaran=" + year + " and kodesatker='" + kodesatker + "'"
                + "), "
                + "nrc2 as( "
                + " select 5 as id, 'Neraca Semester ' || smstr, smstr::integer as bln, statusdata from semester "
                + " left join nrct on semester.smstr=nrct.semester "
                + "), "
                + "kastitle as( select 'Arus Kas'::character(255) as ikd  ), "
                + "kast as( "
                + " select 'Arus Kas'::character(255) as ikd, statusdata from aruskas "
                + " where tahunanggaran=" + year + " and kodesatker='" + kodesatker + "'"
                + "), "
                + "kas2 as( "
                + " select 6 as id, kastitle.ikd, 0::integer as bln, kast.statusdata from kastitle "
                + " left join kast on kastitle.ikd=kast.ikd "
                + "), "
                + "lot as( "
                + " select triwulan, statusdata from lo "
                + " where tahunanggaran=" + year + " and kodesatker='" + kodesatker + "'"
                + "), "
                + "lo2 as ( "
                + " select 7 as id, 'LO Triwulan ' || tri, tri::integer as bln, statusdata from triwulan "
                + " left join lot on triwulan.tri=lot.triwulan "
                + "), "
                + "lpetitle as( select 'Laporan Perubahan Ekuitas'::character(255) as ikd ), "
                + "lpet as( "
                + " select 'Laporan Perubahan Ekuitas'::character(255) as ikd, statusdata from perubahanekuitas "
                + " where tahunanggaran=" + year + " and kodesatker='" + kodesatker + "'"
                + "), "
                + "lpe2 as( "
                + " select 8 as id, lpetitle.ikd, 0::integer as bln, lpet.statusdata from lpetitle "
                + " left join lpet on lpetitle.ikd=lpet.ikd "
                + "), "
                + "lpstitle as( select 'Laporan Perubahan Saldo'::character(255) as ikd ), "
                + "lpst as( "
                + " select 'Laporan Perubahan Saldo'::character(255) as ikd, statusdata from perubahansal "
                + " where tahunanggaran=" + year + " and kodesatker='" + kodesatker + "'"
                + "), "
                + "lps2 as( "
                + " select 9 as id, lpstitle.ikd, 0::integer as bln, lpst.statusdata from lpstitle "
                + " left join lpst on lpstitle.ikd=lpst.ikd "
                + "), "
                + "pfktitle as( select 'Perhitungan Fihak Ketiga'::character(255) as ikd ), "
                + "pfkt as( "
                + " select 'Perhitungan Fihak Ketiga'::character(255) as ikd, statusdata from pfk "
                + " where tahunanggaran=" + year + " and kodesatker='" + kodesatker + "'"
                + "), "
                + "pfk2 as( "
                + " select 10 as id, pfktitle.ikd, 0::integer as bln, pfkt.statusdata from pfktitle "
                + " left join pfkt on pfktitle.ikd=pfkt.ikd "
                + "), "
                + "pdtitle as( select 'Pinjaman Daerah'::character(255) as ikd ), "
                + "pdt as( "
                + " select 'Pinjaman Daerah'::character(255) as ikd, statusdata from pinjamandaerah "
                + " where tahunanggaran=" + year + " and kodesatker='" + kodesatker + "'"
                + "), "
                + "pd2 as( "
                + " select 11 as id, pdtitle.ikd, 0::integer as bln, pdt.statusdata from pdtitle "
                + " left join pdt on pdtitle.ikd=pdt.ikd "
                + "), "
                + "hasil as( "
                + " select * from apbdMurni union all "
                + " select * from apbdRubah union all "
                + " select * from lra union all "
                + " select * from dth2 union all "
                + " select * from nrc2 union "
                + " select * from kas2 union "
                + " select * from lo2 union "
                + " select * from lpe2 union "
                + " select * from lps2 union "
                + " select * from pfk2 union "
                + " select * from pd2 "
                + " order by id, bln "
                + ") "
                + " select id, ikd, bln, "
                + " CASE WHEN status>=0 THEN 'Sudah  mengirimkan data dengan status ' || status "
                + "            ELSE 'Belum Mengirimkan data' "
                + "       END as status  "
                + "       from hasil ";
        try {
            stm = conn.createStatement();
            rs = stm.executeQuery(sql);
            while (rs.next()) {
                String stat = rs.getString("status");
                String style = "";
                if(stat.trim().toLowerCase().startsWith("sudah")){
                    short tt = Short.valueOf(""+stat.trim().charAt(stat.trim().length()-1));
                    String ss = getStatusData(tt);
                    stat = stat.trim().substring(0, stat.trim().length()-2)+ " " + ss;
                }
                else{
                    style = " color: red;";
                }
                result.add(new MappingAccount(0, rs.getString("ikd"), stat));
                result.get(result.size()-1).setStyle(style);
            }
            return result;

        } catch (SQLException e) {
            throw new SQLException("Gagal Mengambil seluruh status data IKD \n " + e.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    public String getStatusData(short statusd){
        String result = "";
        if(statusd==SIKDUtil.STATUS_BELUM_SIAP) result = SIKDUtil.STATUS_BELUM_SIAP_STRING;
        else if(statusd==SIKDUtil.STATUS_SIAP) result = SIKDUtil.STATUS_SIAP_STRING;
        else if(statusd==SIKDUtil.STATUS_SEDANG) result = SIKDUtil.STATUS_SEDANG_STRING;
        else if(statusd==SIKDUtil.STATUS_TELAH_OK) result = SIKDUtil.STATUS_TELAH_OK_STRING;
        else if(statusd==SIKDUtil.STATUS_TELAH_NOT_OK) result = SIKDUtil.STATUS_TELAH_NOT_OK_STRING;
        return result;
    }

}
