/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.office.ejb.dbapi;

import app.sikd.entity.backoffice.NilaiFungsiPemda;
import app.sikd.entity.graph.GraphGlobal;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author sora
 */
public class GraphicSQL implements IGraphicSQL{
    
    /**
     *
     * @param year
     * @param kodeData
     * @param jenisCOA
     * @param kodePemdas
     * @param conn
     * @return
     * @throws SQLException
     */
    @Override
    public List<GraphGlobal> getPadGraph(short year, short kodeData, short jenisCOA, List<String> kodePemdas, Connection conn) throws SQLException {
        Statement stm = null;
        ResultSet rs = null;
        String sql = "with x as( " 
                    + " SELECT kodepemda, namapemda, sum(nilaianggaran) nilai1 from realisasikoderekapbd  " 
                    + " inner join realisasikegiatanapbd on realisasikoderekapbd.kegiatanindex=realisasikegiatanapbd.kegiatanindex " 
                    + " inner join realisasiapbd on realisasikegiatanapbd.apbdindex=realisasiapbd.apbdindex " 
                    + " where kodeakunutama='4' and kodeakunkelompok='1'  " 
                    + " and tahunanggaran=" + year + " and kodedata=" + kodeData + " and jeniscoa=" + jenisCOA ;
        String qpemdas="";
        if(kodePemdas!=null && !kodePemdas.isEmpty()){
            for (String kodePemda : kodePemdas) {
                if(!qpemdas.trim().equals("")) qpemdas = qpemdas + " or ";
                qpemdas=qpemdas + " kodepemda = '" + kodePemda.trim() + "' ";
            }
        }
        if(!qpemdas.trim().equals("")){
            qpemdas = " ( " + qpemdas + " ) ";
            sql = sql + " and " + qpemdas;
        }
        
        sql = sql + " group by kodepemda, namapemda " 
              + " order by kodepemda " 
              + " ), " 
              + " z as( " 
              + " SELECT kodepemda, namapemda, sum(nilaianggaran) nilai2 from realisasikoderekapbd " 
              + " inner join realisasikegiatanapbd on realisasikoderekapbd.kegiatanindex=realisasikegiatanapbd.kegiatanindex" 
              + " inner join realisasiapbd on realisasikegiatanapbd.apbdindex=realisasiapbd.apbdindex" 
              + " where kodeakunutama='4'" 
              + " and tahunanggaran=" + year + " and kodedata=" + kodeData + " and jeniscoa=" + jenisCOA;
        if(!qpemdas.trim().equals("")){
            sql = sql + " and " + qpemdas;
        }
        sql = sql + " group by kodepemda, namapemda" 
              + " order by kodepemda" 
              + " ) " 
              + " select x.kodepemda, x.namapemda, (nilai1/nilai2) nilai from x "
              + " inner join z on x.kodepemda=z.kodepemda";
        try {
            stm = conn.createStatement();
            rs = stm.executeQuery(sql);
            List<GraphGlobal> result = new ArrayList();
            while (rs.next()) {
                result.add(new GraphGlobal(rs.getString("kodepemda"),rs.getString("namapemda"), rs.getDouble("nilai")));
            }
            return result;
        } catch (SQLException ex) {
            Logger.getLogger(GraphicSQL.class.getName()).log(Level.SEVERE, null, ex);
            throw new SQLException("SQL: Gagal mengambil data untuk Grafik tingkat kemandirian daerah yaitu kemampuan daerah dalam mendanai belanjanya \n" + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    /**
     *
     * @param year
     * @param kodeData
     * @param jenisCOA
     * @param kodePemdas
     * @param conn
     * @return
     * @throws SQLException
     */
    @Override
    public List<GraphGlobal> getFiskalGraph(short year, short kodeData, short jenisCOA, List<String> kodePemdas, Connection conn) throws SQLException {
        Statement stm = null;
        ResultSet rs = null;
        String qpemdas="";
        if(kodePemdas!=null && !kodePemdas.isEmpty()){
            for (String kodePemda : kodePemdas) {
                if(!qpemdas.trim().equals("")) qpemdas = qpemdas + " or ";
                qpemdas=qpemdas + " kodepemda = '" + kodePemda.trim() + "' ";
            }
        }
        if(!qpemdas.trim().equals("")){
            qpemdas = " ( " + qpemdas + " ) ";
        }
        String sql = "with x as( " 
                    + " SELECT kodepemda, namapemda, sum(nilaianggaran) nilai1 from realisasikoderekapbd " 
                    + " inner join realisasikegiatanapbd on realisasikoderekapbd.kegiatanindex=realisasikegiatanapbd.kegiatanindex " 
                    + " inner join realisasiapbd on realisasikegiatanapbd.apbdindex=realisasiapbd.apbdindex where " 
                    + " tahunanggaran=" + year + " and kodedata=" + kodeData + " and jeniscoa=" + jenisCOA;
        
        if(!qpemdas.trim().equals("")){
            sql = sql + " and " + qpemdas;
        }
        sql = sql + " and ((kodeakunutama='4' and kodeakunkelompok='2' and kodeakunjenis='3') or " 
                    + " (kodeakunutama='4' and kodeakunkelompok='3' and kodeakunjenis='1') or " 
                    + " (kodeakunutama='4' and kodeakunkelompok='3' and kodeakunjenis='2') or " 
                    + " (kodeakunutama='4' and kodeakunkelompok='3' and kodeakunjenis='4' and kodeakunobjek='01') or " 
                    + " (kodeakunutama='5' and kodeakunkelompok='1' and kodeakunjenis='1') or " 
                    + " (kodeakunutama='5' and kodeakunkelompok='1' and kodeakunjenis='2')) " 
                    + " group by kodepemda, namapemda " 
                    + " order by kodepemda " 
                    + " ), " 
                    + " z as( " 
                    + " SELECT kodepemda, namapemda, sum(nilaianggaran) nilai2 from realisasikoderekapbd " 
                    + " inner join realisasikegiatanapbd on realisasikoderekapbd.kegiatanindex=realisasikegiatanapbd.kegiatanindex " 
                    + " inner join realisasiapbd on realisasikegiatanapbd.apbdindex=realisasiapbd.apbdindex " 
                    + " where kodeakunutama='4' " 
                    + " and tahunanggaran=" + year + " and kodedata=" + kodeData + " and jeniscoa=" + jenisCOA;
        if(!qpemdas.trim().equals("")){
            sql = sql + " and " + qpemdas;
        }
        sql= sql + " group by kodepemda, namapemda " 
                    + " order by kodepemda " 
                    + " ) " 
                    + " select (nilai1/nilai2) nilai , nilai1, nilai2, x.kodepemda, x.namapemda from x " 
                    + " inner join z on x.kodepemda=z.kodepemda";
        
        try {
            stm = conn.createStatement();
            rs = stm.executeQuery(sql);
            List<GraphGlobal> result = new ArrayList();
            while (rs.next()) {
                result.add(new GraphGlobal(rs.getString("kodepemda"),rs.getString("namapemda"), rs.getDouble("nilai")));
            }
            return result;
        } catch (SQLException ex) {
            Logger.getLogger(GraphicSQL.class.getName()).log(Level.SEVERE, null, ex);
            throw new SQLException("SQL: Gagal mengambil data untuk Grafik tingkat kemampuan daerah dalam mendanai program dan kegiatan yang menjadi prioritas daerahnya \n" + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    /**
     *
     * @param year
     * @param kodeData
     * @param jenisCOA
     * @param kodePemdas
     * @param conn
     * @return
     * @throws SQLException
     */
    @Override
    public List<GraphGlobal> getDanaBelanjaPengeluaranGraph(short year, short kodeData, short jenisCOA, List<String> kodePemdas, Connection conn) throws SQLException {
        Statement stm = null;
        ResultSet rs = null;
        String qpemdas="";
        if(kodePemdas!=null && !kodePemdas.isEmpty()){
            for (String kodePemda : kodePemdas) {
                if(!qpemdas.trim().equals("")) qpemdas = qpemdas + " or ";
                qpemdas=qpemdas + " kodepemda = '" + kodePemda.trim() + "' ";
            }
        }
        if(!qpemdas.trim().equals("")){
            qpemdas = " ( " + qpemdas + " ) ";
        }
        
        String sql = "with x as( " 
                    + " SELECT kodepemda, namapemda, sum(nilaianggaran) nilai1 from realisasikoderekapbd " 
                    + " inner join realisasikegiatanapbd on realisasikoderekapbd.kegiatanindex=realisasikegiatanapbd.kegiatanindex " 
                    + " inner join realisasiapbd on realisasikegiatanapbd.apbdindex=realisasiapbd.apbdindex " 
                    + " where tahunanggaran=" + year + " and kodedata=" + kodeData + " and jeniscoa=" + jenisCOA;
        if(!qpemdas.trim().equals("")){
            sql = sql + " and " + qpemdas;
        }
        sql = sql + " and  ((kodeakunutama='4') or " 
                    + " (kodeakunutama='6' and kodeakunkelompok='1')) " 
                    + " group by kodepemda, namapemda " 
                    + " order by kodepemda " 
                    + " ), " 
                    + " z as( " 
                    + " SELECT kodepemda, namapemda, sum(nilaianggaran) nilai2 from realisasikoderekapbd " 
                    + " inner join realisasikegiatanapbd on realisasikoderekapbd.kegiatanindex=realisasikegiatanapbd.kegiatanindex " 
                    + " inner join realisasiapbd on realisasikegiatanapbd.apbdindex=realisasiapbd.apbdindex " 
                    + " where tahunanggaran=" + year + " and kodedata=" + kodeData + " and jeniscoa=" + jenisCOA;
        if(!qpemdas.trim().equals("")){
            sql = sql + " and " + qpemdas;
        }
        sql = sql + " and ((kodeakunutama='5') or " 
                    + " (kodeakunutama='6' and kodeakunkelompok='2')) " 
                    + " group by kodepemda, namapemda " 
                    + " order by kodepemda " 
                    + " ) " 
                    + " select (nilai1/nilai2) nilai , nilai1, nilai2, x.kodepemda, x.namapemda from x " 
                    + " inner join z on x.kodepemda=z.kodepemda";
        try {
            stm = conn.createStatement();
            rs = stm.executeQuery(sql);
            List<GraphGlobal> result = new ArrayList();
            while (rs.next()) {
                result.add(new GraphGlobal(rs.getString("kodepemda"),rs.getString("namapemda"), rs.getDouble("nilai")));
            }
            return result;
        } catch (SQLException ex) {
            Logger.getLogger(GraphicSQL.class.getName()).log(Level.SEVERE, null, ex);
            throw new SQLException("SQL: Gagal mengambil data untuk Grafik Tingkat kemampuan keuangan daerah dalam mendanai belanja dan pengeluaran daerah \n" + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    /**
     *
     * @param year
     * @param kodeData
     * @param jenisCOA
     * @param kodePemdas
     * @param conn
     * @return
     * @throws SQLException
     */
    @Override
    public List<GraphGlobal> getTingkatBelanjaModalGraph(short year, short kodeData, short jenisCOA, List<String> kodePemdas, Connection conn) throws SQLException {
        Statement stm = null;
        ResultSet rs = null;
        String qpemdas="";
        if(kodePemdas!=null && !kodePemdas.isEmpty()){
            for (String kodePemda : kodePemdas) {
                if(!qpemdas.trim().equals("")) qpemdas = qpemdas + " or ";
                qpemdas=qpemdas + " kodepemda = '" + kodePemda.trim() + "' ";
            }
        }
        if(!qpemdas.trim().equals("")){
            qpemdas = " ( " + qpemdas + " ) ";
        }
        
        String sql = "with x as( " 
                    + " SELECT kodepemda, namapemda, sum(nilaianggaran) nilai1 from realisasikoderekapbd " 
                    + " inner join realisasikegiatanapbd on realisasikoderekapbd.kegiatanindex=realisasikegiatanapbd.kegiatanindex " 
                    + " inner join realisasiapbd on realisasikegiatanapbd.apbdindex=realisasiapbd.apbdindex " 
                    + " where tahunanggaran=" + year + " and kodedata=" + kodeData + " and jeniscoa=" + jenisCOA;
        if(!qpemdas.trim().equals("")){
            sql = sql + " and " + qpemdas;
        }
        sql = sql + " and (kodeakunutama='5' and kodeakunkelompok='2' and kodeakunjenis='3') " 
                    + " group by kodepemda, namapemda " 
                    + " order by kodepemda " 
                    + " ), " 
                    + " z as( " 
                    + " SELECT kodepemda, namapemda, sum(nilaianggaran) nilai2 from realisasikoderekapbd " 
                    + " inner join realisasikegiatanapbd on realisasikoderekapbd.kegiatanindex=realisasikegiatanapbd.kegiatanindex " 
                    + " inner join realisasiapbd on realisasikegiatanapbd.apbdindex=realisasiapbd.apbdindex " 
                    + " where tahunanggaran=" + year + " and kodedata=" + kodeData + " and jeniscoa=" + jenisCOA;
        if(!qpemdas.trim().equals("")){
            sql = sql + " and " + qpemdas;
        }
        sql = sql + " and kodeakunutama='5' " 
                    + " group by kodepemda, namapemda " 
                    + " order by kodepemda " 
                    + " ) " 
                    + " select (nilai1/nilai2) nilai , nilai1, nilai2, x.kodepemda, x.namapemda from x " 
                    + " inner join z on x.kodepemda=z.kodepemda";

        try {
            stm = conn.createStatement();
            rs = stm.executeQuery(sql );
            List<GraphGlobal> result = new ArrayList();
            while (rs.next()) {
                result.add(new GraphGlobal(rs.getString("kodepemda"),rs.getString("namapemda"), rs.getDouble("nilai")));
            }
            return result;
        } catch (SQLException ex) {
            Logger.getLogger(GraphicSQL.class.getName()).log(Level.SEVERE, null, ex);
            throw new SQLException("SQL: Gagal mengambil data untuk Grafik Seberapa besar daerah mengalokasikan seluruh belanjanya untuk belanja modal\n" + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    /**
     *
     * @param year
     * @param kodeData
     * @param jenisCOA
     * @param kodePemdas
     * @param conn
     * @return
     * @throws SQLException
     */
    @Override
    public List<GraphGlobal> getTingkatBelanjaPegawaiTdkLangsungGraph(short year, short kodeData, short jenisCOA, List<String> kodePemdas, Connection conn) throws SQLException {
        Statement stm = null;
        ResultSet rs = null;
        String qpemdas="";
        if(kodePemdas!=null && !kodePemdas.isEmpty()){
            for (String kodePemda : kodePemdas) {
                if(!qpemdas.trim().equals("")) qpemdas = qpemdas + " or ";
                qpemdas=qpemdas + " kodepemda = '" + kodePemda.trim() + "' ";
            }
        }
        if(!qpemdas.trim().equals("")){
            qpemdas = " ( " + qpemdas + " ) ";
        }
        
        String sql = "with x as( " 
                    + " SELECT kodepemda, namapemda, sum(nilaianggaran) nilai1 from realisasikoderekapbd " 
                    + " inner join realisasikegiatanapbd on realisasikoderekapbd.kegiatanindex=realisasikegiatanapbd.kegiatanindex " 
                    + " inner join realisasiapbd on realisasikegiatanapbd.apbdindex=realisasiapbd.apbdindex " 
                    + " where tahunanggaran=" + year + " and kodedata=" + kodeData + " and jeniscoa=" + jenisCOA ;
        if(!qpemdas.trim().equals("")){
            sql = sql + " and " + qpemdas;
        }
        sql = sql + " and (kodeakunutama='5' and kodeakunkelompok='1' and kodeakunjenis='1') " 
                    + " group by kodepemda, namapemda " 
                    + " order by kodepemda " 
                    + " ), " 
                    + " z as( " 
                    + " SELECT kodepemda, namapemda, sum(nilaianggaran) nilai2 from realisasikoderekapbd " 
                    + " inner join realisasikegiatanapbd on realisasikoderekapbd.kegiatanindex=realisasikegiatanapbd.kegiatanindex " 
                    + " inner join realisasiapbd on realisasikegiatanapbd.apbdindex=realisasiapbd.apbdindex " 
                    + " where tahunanggaran=" + year + " and kodedata=" + kodeData + " and jeniscoa=" + jenisCOA;
        if(!qpemdas.trim().equals("")){
            sql = sql + " and " + qpemdas;
        }
        sql= sql + " and kodeakunutama='5' " 
                    + " group by kodepemda, namapemda " 
                    + " order by kodepemda " 
                    + " ) " 
                    + " select (nilai1/nilai2) nilai , nilai1, nilai2, x.kodepemda, x.namapemda from x " 
                    + " inner join z on x.kodepemda=z.kodepemda";

        try {
            stm = conn.createStatement();
            rs = stm.executeQuery(sql );
            List<GraphGlobal> result = new ArrayList();
            while (rs.next()) {
                result.add(new GraphGlobal(rs.getString("kodepemda"),rs.getString("namapemda"), rs.getDouble("nilai")));
            }
            return result;
        } catch (SQLException ex) {
            Logger.getLogger(GraphicSQL.class.getName()).log(Level.SEVERE, null, ex);
            throw new SQLException("SQL: Gagal mengambil data untuk Grafik Seberapa besar daerah mengalokasikan seluruh belanjanya untuk belanja pegawai tidak langsung \n" + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    /**
     *
     * @param year
     * @param kodeData
     * @param jenisCOA
     * @param kodeFungsi
     * @param conn
     * @return
     * @throws SQLException
     */
    @Override
    public List<GraphGlobal> getTop10Fungsi(short year, short kodeData, short jenisCOA, String kodeFungsi, Connection conn) throws SQLException {
        Statement stm = null;
        ResultSet rs = null;
            
        String sql = "select kodepemda, namapemda, kodefungsi, namafungsi, sum (nilaianggaran) "
                + " nilai from realisasikoderekapbd rek " 
                + " inner join realisasikegiatanapbd keg on rek.kegiatanindex=keg.kegiatanindex " 
                + " inner join realisasiapbd apbd on keg.apbdindex=apbd.apbdindex " 
                + " where kodefungsi='"+kodeFungsi+"' and tahunanggaran="+year+" and kodedata="+kodeData+" and jeniscoa=" + jenisCOA 
                + " group by kodepemda, namapemda, kodefungsi, namafungsi " 
                + " order by nilai limit 10" ;
        

        try {
            stm = conn.createStatement();
            rs = stm.executeQuery(sql );
            List<GraphGlobal> result = new ArrayList();
            while (rs.next()) {
                result.add(new GraphGlobal(rs.getString("kodepemda"),rs.getString("namapemda"), rs.getDouble("nilai")));
            }
            return result;
        } catch (SQLException ex) {
            Logger.getLogger(GraphicSQL.class.getName()).log(Level.SEVERE, null, ex);
            throw new SQLException("SQL: Gagal mengambil data untuk Grafik Top 10 Fungsi\n" + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    
    /**
     *
     * @param year
     * @param kodeSatker
     * @param kodeData
     * @param jenisCOA
     * @param kodeFungsi
     * @param conn
     * @return
     * @throws SQLException
     */
    @Override
    public List<NilaiFungsiPemda> getNilaiFungsiPemda(short year, String kodeSatker, short kodeData, short jenisCOA, String kodeFungsi, Connection conn) throws SQLException {
        List<NilaiFungsiPemda> result = new ArrayList();
        Statement stm = null;
        ResultSet rs = null;
        String sql = "select kodepemda, namapemda, sum(nilaianggaran) nilai from realisasikoderekapbd rek "
                + "  inner join realisasikegiatanapbd keg on rek.kegiatanindex=keg.kegiatanindex "
                + "  inner join realisasiapbd apbd on keg.apbdindex=apbd.apbdindex "
                + "  where kodefungsi='" + kodeFungsi + "' and "
                + "  tahunanggaran=" + year + " and kodedata=" + kodeData + " and jeniscoa=" + jenisCOA;        
        if(!kodeSatker.trim().equals("000000"))
            sql = sql + "and apbd.kodesatker = '" + kodeSatker + "' ";
        sql = sql + " group by kodepemda, namapemda "
                + "  order by kodepemda";
        try {
            stm = conn.createStatement();
            rs = stm.executeQuery(sql);
            while (rs.next()) {
                result.add(new NilaiFungsiPemda(rs.getString("kodepemda"), rs.getString("namapemda"), rs.getDouble("nilai")));
            }

            return result;
        } catch (SQLException ex) {
            Logger.getLogger(GraphicSQL.class.getName()).log(Level.SEVERE, null, ex);
            throw new SQLException("SQL: Gagal mengambil data Nilai Fungsi Pemda \n" + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    /**
     *
     * @param year
     * @param kodePemdas
     * @param kodeData
     * @param jenisCOA
     * @param kodeFungsi
     * @param conn
     * @return
     * @throws SQLException
     */
    @Override
    public List<NilaiFungsiPemda> getKompilasiNilaiFungsiPemda(short year, List<String> kodePemdas, short kodeData, short jenisCOA, String kodeFungsi, Connection conn) throws SQLException {
        List<NilaiFungsiPemda> result = new ArrayList();
        Statement stm = null;
        ResultSet rs = null;
        String sql = "select kodepemda, namapemda, sum(nilaianggaran) nilai from realisasikoderekapbd rek "
                + "  inner join realisasikegiatanapbd keg on rek.kegiatanindex=keg.kegiatanindex "
                + "  inner join realisasiapbd apbd on keg.apbdindex=apbd.apbdindex "
                + "  where kodefungsi='" + kodeFungsi + "' and "
                + "  tahunanggaran=" + year + " and kodedata=" + kodeData + " and jeniscoa=" + jenisCOA;        
        
        String qPemdas= "";
        if(kodePemdas!=null && kodePemdas.size() > 0 ){
            for (String kodePemda : kodePemdas) {
                if(!qPemdas.trim().equals("")) qPemdas = qPemdas + " or ";
                qPemdas = qPemdas + "kodepemda='" + kodePemda.trim() + "' ";
            }
        }
        if(!qPemdas.trim().equals("")) {
            qPemdas = "(" + qPemdas + ") ";
            sql = sql + " and " + qPemdas;
        }
        
        sql = sql + " group by kodepemda, namapemda "
                + "  order by kodepemda";
        
        try {
            stm = conn.createStatement();
            rs = stm.executeQuery(sql);
            while (rs.next()) {
                result.add(new NilaiFungsiPemda(rs.getString("kodepemda"), rs.getString("namapemda"), rs.getDouble("nilai")));
            }
            return result;
        } catch (SQLException ex) {
            Logger.getLogger(OfficeSQL.class.getName()).log(Level.SEVERE, null, ex);
            throw new SQLException("SQL: Gagal mengambil data Kompilasi Nilai Fungsi Pemda\n" + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    /**
     *
     * @param year
     * @param kodeData
     * @param jenisCOA
     * @param conn
     * @return
     * @throws SQLException
     */
    @Override
    public int getJumlahDaerahKirimRealisasi(short year, short kodeData, short jenisCOA, Connection conn) throws SQLException{
        Statement stm = null;
        ResultSet rs = null;
            
        String sql = "with ss as( " 
                + "  select distinct kodesatker, namapemda from realisasiapbd " 
                + "  where tahunanggaran="+year+" and bulan<=12 and kodedata="+kodeData+" and jeniscoa="+jenisCOA+") "
                + "  select count(kodesatker) jumlah from ss" ;
        

        try {
            stm = conn.createStatement();
            rs = stm.executeQuery(sql );
            int result = 0;
            if (rs.next()) {
                result=rs.getInt("jumlah");
            }
            return result;
        } catch (SQLException ex) {
            Logger.getLogger(GraphicSQL.class.getName()).log(Level.SEVERE, null, ex);
            throw new SQLException("SQL: Gagal mengambil data Jumlah Pemda Mengirim Realisasi \n" + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }
}
