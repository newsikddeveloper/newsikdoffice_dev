/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 ` */
package app.sikd.office.ejb.dbapi;

import app.sikd.dbapi.apbd.IAPBDConstants;
import app.sikd.entity.apbd.LampiranIIIPerdaAPBD;
import app.sikd.entity.apbd.LampiranIIPerdaAPBD;
import app.sikd.entity.apbd.LampiranILra;
import app.sikd.entity.apbd.LampiranIPerdaAPBD;
import app.sikd.entity.apbd.LampiranIVPerdaAPBD;
import app.sikd.entity.apbd.LampiranVPerdaAPBD;
import app.sikd.entity.backoffice.APBDPerda;
import app.sikd.entity.backoffice.AkunDTHSKPD;
import app.sikd.entity.backoffice.DTH;
import app.sikd.entity.backoffice.DTHSKPD;
import app.sikd.entity.backoffice.DataDaerah;
import app.sikd.entity.backoffice.DataDaerahDetail;
import app.sikd.entity.backoffice.NilaiFungsiPemda;
import app.sikd.entity.backoffice.PDRB;
import app.sikd.entity.backoffice.PDRBHarga;
import app.sikd.entity.backoffice.PajakDTHSKPD;
import app.sikd.entity.backoffice.PerhitunganFihakKetiga;
import app.sikd.entity.backoffice.PinjamanDaerah;
import app.sikd.entity.backoffice.PotonganDTHSKPD;
import app.sikd.entity.backoffice.RTH;
import app.sikd.entity.backoffice.RealisasiAPBD;
import app.sikd.entity.backoffice.RealisasiKegiatanAPBD;
import app.sikd.entity.backoffice.RealisasiKodeRekeningAPBD;
import app.sikd.entity.backoffice.RekapitulasiGajiPegawai;
import app.sikd.entity.backoffice.RincianBPHTB;
import app.sikd.entity.backoffice.RincianDTHSKPD;
import app.sikd.entity.backoffice.RincianHiburan;
import app.sikd.entity.backoffice.RincianHotel;
import app.sikd.entity.backoffice.RincianIMB;
import app.sikd.entity.backoffice.RincianIzinUsaha;
import app.sikd.entity.backoffice.RincianKendaraan;
import app.sikd.entity.backoffice.RincianPajakDanRetribusi;
import app.sikd.entity.backoffice.RincianPerhitunganFihakKetiga;
import app.sikd.entity.backoffice.RincianPinjamanDaerah;
import app.sikd.entity.backoffice.RincianRestoran;
import app.sikd.entity.utilitas.MappingAccount;
import app.sikd.entity.utilitas.MappingNonStandar;
import app.sikd.entity.utilitas.SimpleAccount;
import app.sikd.entity.ws.fault.SIKDServiceException;
import java.sql.Connection;
import java.sql.PreparedStatement;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author detra
 */
public class OfficeSQL implements IOfficeSQL {
    
    @Override
    public APBDPerda getAPBDPerdaNumber(short year, String kodeSatker, short kodeData, short jenisCOA, Connection conn) throws SQLException {
        Statement stm = null;
        ResultSet rs = null;

        try {
            stm = conn.createStatement();
            rs = stm.executeQuery("select nomorperda, tanggalperda from apbd "
                    + "where apbd.tahunanggaran= " + year + " "
                    + "and apbd.kodesatker = '" + kodeSatker + "' and apbd.kodedata= " + kodeData + " and apbd.jeniscoa=" + jenisCOA);
            if (rs.next()) {
               return new APBDPerda(rs.getString("nomorperda"), rs.getDate("tanggalperda")); 
            }

            return null;
        } catch (SQLException ex) {
            Logger.getLogger(OfficeSQL.class.getName()).log(Level.SEVERE, null, ex);
            throw new SQLException("SQL: Gagal mengambil data Nomor dan Tanggal Perda apbd \n" + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    @Override
    public APBDPerda getLRAPerdaNumber(short year, String kodeSatker, short kodeData, short jenisCOA, Connection conn) throws SQLException {
        Statement stm = null;
        ResultSet rs = null;

        try {
            stm = conn.createStatement();
            rs = stm.executeQuery("select nomorperda, tanggalperda from realisasiapbd apbd "
                    + "where apbd.tahunanggaran= " + year + " "
                    + "and apbd.kodesatker = '" + kodeSatker + "' and apbd.kodedata= " + kodeData + " and apbd.jeniscoa=" + jenisCOA);
            if (rs.next()) {
               return new APBDPerda(rs.getString("nomorperda"), rs.getDate("tanggalperda")); 
            }

            return null;
        } catch (SQLException ex) {
            Logger.getLogger(OfficeSQL.class.getName()).log(Level.SEVERE, null, ex);
            throw new SQLException("SQL: Gagal mengambil data Nomor dan Tanggal Perda Realisasi APBD \n" + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }

    @Override
    public List<LampiranIPerdaAPBD> getLampiranIPerdaAPBDbyYear(short year, String kodeSatker, short kodeData, short jenisCOA, Connection conn) throws SQLException {
        List<LampiranIPerdaAPBD> result = new ArrayList();
        Statement stm = null;
        ResultSet rs = null;
        String sql = "with utama as " 
                + " (select rek.kodeakunutama kode, ";
        if(!kodeSatker.trim().equals("000000"))
            sql = sql + " rek.namaakunutama nama, ";
        else
            sql = sql + " accountname nama, ";
        sql = sql + " sum(rek.nilaianggaran) jumlah " 
                + " from koderekapbd rek " 
                + " inner join kegiatanapbd keg on rek.kegiatanindex=keg.kegiatanindex " 
                + " inner join apbd on keg.apbdindex=apbd.apbdindex " ;
        if(kodeSatker.trim().equals("000000"))
            sql = sql + " inner join cashaccount ca on rek.kodeakunutama = ca.accountcode " ;
        sql = sql + " where apbd.tahunanggaran= " + year
                + " and apbd.kodedata= " + kodeData + " and apbd.jeniscoa= " + jenisCOA;
        if(!kodeSatker.trim().equals("000000"))
            sql = sql + "and apbd.kodesatker = '" + kodeSatker + "' ";
        sql = sql + " group by kode, nama " 
                + " order by kode), " 
                + " kelompok as " 
                + " (select (rek.kodeakunutama || '.' || rek.kodeakunkelompok) kode, ";
        if(!kodeSatker.trim().equals("000000"))
            sql = sql + " rek.namaakunkelompok nama, ";
        else
            sql = sql + " accountname nama, ";
        
        sql = sql + " sum(rek.nilaianggaran) jumlah  " 
                + " from koderekapbd rek " 
                + " inner join kegiatanapbd keg on rek.kegiatanindex=keg.kegiatanindex " 
                + " inner join apbd on keg.apbdindex=apbd.apbdindex " ;
        if(kodeSatker.trim().equals("000000"))
            sql = sql + " inner join cashaccount ca on (rek.kodeakunutama || '.' || rek.kodeakunkelompok) = ca.accountcode " ;
        sql = sql + " where apbd.tahunanggaran= " + year
                + " and apbd.kodedata= " + kodeData + " and apbd.jeniscoa= " + jenisCOA;
        if(!kodeSatker.trim().equals("000000"))
            sql = sql + "and apbd.kodesatker = '" + kodeSatker + "' ";
        
        sql = sql + " group by kode, nama " 
                + " order by kode), " 
                + " jenis as " 
                + " (select (rek.kodeakunutama || '.' || rek.kodeakunkelompok || '.' || rek.kodeakunjenis) kode, ";
        if(!kodeSatker.trim().equals("000000"))
            sql = sql + " rek.namaakunjenis nama, ";
        else
            sql = sql + " accountname nama, ";
        
        sql = sql + " sum(rek.nilaianggaran) jumlah " 
                + " from koderekapbd rek " 
                + " inner join kegiatanapbd keg on rek.kegiatanindex=keg.kegiatanindex " 
                + " inner join apbd on keg.apbdindex=apbd.apbdindex " ;
        if(kodeSatker.trim().equals("000000"))
            sql = sql + " inner join cashaccount ca on (rek.kodeakunutama || '.' || rek.kodeakunkelompok || '.' || rek.kodeakunjenis) = ca.accountcode " ;
        sql = sql + " where apbd.tahunanggaran= " + year
                + " and apbd.kodedata= " + kodeData + " and apbd.jeniscoa= " + jenisCOA;
        if(!kodeSatker.trim().equals("000000"))
            sql = sql + "and apbd.kodesatker = '" + kodeSatker + "' ";
        
        sql = sql + " group by kode, nama " 
                + " order by kode) " 
                + " select * from utama " 
                + " union select * from kelompok " 
                + " union select * from jenis " 
                + " order by kode ";
        
        /*String sql = "select rek.kodeakunutama, rek.namaakunutama, "
                    + "rek.kodeakunkelompok, rek.namaakunkelompok, "
                    + "rek.kodeakunjenis, rek.namaakunjenis, "
                    + "sum(rek.nilaianggaran) jumlah from koderekapbd rek "
                    + "inner join kegiatanapbd keg on rek.kegiatanindex=keg.kegiatanindex "
                    + "inner join apbd on keg.apbdindex=apbd.apbdindex "
                    + "where apbd.tahunanggaran= " + year + " ";
        if(!kodeSatker.trim().equals("000000"))
            sql = sql + "and apbd.kodesatker = '" + kodeSatker + "' ";
        sql= sql + " and apbd.kodedata= " + kodeData + " and apbd.jeniscoa=" + jenisCOA + " "
                    + "group by rek.kodeakunutama, rek.namaakunutama, "
                    + "rek.kodeakunkelompok, rek.namaakunkelompok, "
                    + "rek.kodeakunjenis, rek.namaakunjenis "
                    + "order by rek.kodeakunutama, rek.namaakunutama, "
                    + "rek.kodeakunkelompok, rek.namaakunkelompok, "
                    + "rek.kodeakunjenis, rek.namaakunjenis";
        */
        try {
            stm = conn.createStatement();
            rs = stm.executeQuery(sql);
//            String utama = "", kelompok = "", jenis;
//            double jumlahKelompok = 0;
//            List<String> indexKelompok = new ArrayList();
            while (rs.next()) {
                result.add(new LampiranIPerdaAPBD(rs.getString("kode"), rs.getString("nama"), rs.getDouble("jumlah")));
  /*              if (utama.trim().equals("")) {
                    utama = rs.getString("kodeakunutama").trim();
                    kelompok = utama.trim() + "." + rs.getString("kodeakunkelompok").trim();
                    jenis = kelompok.trim() + "." + rs.getString("kodeakunjenis").trim();
                    double jumlah = jumlahKelompok = rs.getDouble("jumlah");
                    result.add(new LampiranIPerdaAPBD(utama, rs.getString("namaakunutama"), 0));
                    result.add(new LampiranIPerdaAPBD(kelompok, rs.getString("namaakunkelompok"), jumlah));
                    indexKelompok.add(String.valueOf(result.size() - 1));
                    result.add(new LampiranIPerdaAPBD(jenis, rs.getString("namaakunjenis"), jumlah));
                } else {
                    if (!utama.trim().equals(rs.getString("kodeakunutama").trim())) {
                        utama = rs.getString("kodeakunutama").trim();
                        kelompok = utama.trim() + "." + rs.getString("kodeakunkelompok").trim();
                        jenis = kelompok.trim() + "." + rs.getString("kodeakunjenis").trim();
                        double jumlah = jumlahKelompok = rs.getDouble("jumlah");
                        result.add(new LampiranIPerdaAPBD(utama, rs.getString("namaakunutama"), 0));
                        result.add(new LampiranIPerdaAPBD(kelompok, rs.getString("namaakunkelompok"), jumlahKelompok));
                        indexKelompok.add(String.valueOf(result.size() - 1));
                        result.add(new LampiranIPerdaAPBD(jenis, rs.getString("namaakunjenis"), jumlah));
                    } else {
                        if (kelompok.trim().equals(utama.trim() + "." + rs.getString("kodeakunkelompok").trim())) {
                            jenis = kelompok.trim() + "." + rs.getString("kodeakunjenis");
                            double jumlah = rs.getDouble("jumlah");
                            jumlahKelompok = jumlahKelompok + jumlah;
                            result.add(new LampiranIPerdaAPBD(jenis, rs.getString("namaakunjenis"), jumlah));
                            int ii = 0;
                            boolean find = false;
                            while (ii < indexKelompok.size() && find == false) {
                                if (result.get(Integer.parseInt(indexKelompok.get(ii).trim())).getKode().trim().equals(kelompok.trim())) {
                                    result.get(Integer.parseInt(indexKelompok.get(ii).trim())).setJumlah(jumlahKelompok);
                                    find = true;
                                }
                                ii++;
                            }
                        } else {
                            //Kelompok Beda
                            kelompok = utama.trim() + "." + rs.getString("kodeakunkelompok").trim();
                            jenis = kelompok.trim() + "." + rs.getString("kodeakunjenis").trim();
                            double jumlah = jumlahKelompok = rs.getDouble("jumlah");
                            result.add(new LampiranIPerdaAPBD(kelompok, rs.getString("namaakunkelompok"), jumlahKelompok));
                            indexKelompok.add(String.valueOf(result.size() - 1));
                            result.add(new LampiranIPerdaAPBD(jenis, rs.getString("namaakunjenis"), jumlah));
                        }
                    }
                }
*/
            }

            return result;
        } catch (SQLException ex) {
            Logger.getLogger(OfficeSQL.class.getName()).log(Level.SEVERE, null, ex);
            throw new SQLException("SQL: Gagal mengambil data Lampiran I Perda apbd \n" + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    
    @Override
    public List<LampiranIPerdaAPBD> getKompilasiLampiranIPerdaAPBD(short year, List<String> kodePemdas, short kodeData, short jenisCOA, Connection conn) throws SQLException {
        List<LampiranIPerdaAPBD> result = new ArrayList();
        Statement stm = null;
        ResultSet rs = null;
        String qPemdas= "";
        if(kodePemdas!=null && kodePemdas.size() > 0 ){
            for (String kodePemda : kodePemdas) {
                if(!qPemdas.trim().equals("")) qPemdas = qPemdas + " or ";
                qPemdas = qPemdas + "kodepemda='" + kodePemda.trim() + "' ";
            }
        }
        if(!qPemdas.trim().equals("")) {
            qPemdas = "(" + qPemdas + ") ";
        }
        String sql = "with utama as " 
                + " (select rek.kodeakunutama kode, accountname, sum(rek.nilaianggaran) jumlah " 
                + " from koderekapbd rek " 
                + " inner join kegiatanapbd keg on rek.kegiatanindex=keg.kegiatanindex " 
                + " inner join apbd on keg.apbdindex=apbd.apbdindex  " 
                + " inner join cashaccount ca on rek.kodeakunutama = ca.accountcode " 
                + " where apbd.tahunanggaran= " + year 
                + " and apbd.kodedata= " + kodeData + " and apbd.jeniscoa=" + jenisCOA;
        if( qPemdas!=null && !qPemdas.trim().equals(""))
            sql = sql + " and " + qPemdas;
        
        sql = sql + " group by rek.kodeakunutama, accountname " 
                + " order by rek.kodeakunutama), " 
                + " kelompok as " 
                + " (select (rek.kodeakunutama || '.' || rek.kodeakunkelompok) kode, accountname, sum(rek.nilaianggaran) jumlah " 
                + " from koderekapbd rek " 
                + " inner join kegiatanapbd keg on rek.kegiatanindex=keg.kegiatanindex " 
                + " inner join apbd on keg.apbdindex=apbd.apbdindex " 
                + " inner join cashaccount ca on (rek.kodeakunutama || '.' || rek.kodeakunkelompok) = ca.accountcode " 
                + " where apbd.tahunanggaran= " + year 
                + " and apbd.kodedata= " + kodeData + " and apbd.jeniscoa=" + jenisCOA;
        if( qPemdas!=null && !qPemdas.trim().equals(""))
            sql = sql + " and " + qPemdas;
                sql = sql + " group by kode, accountname " 
                + " order by kode), " 
                + " jenis as " 
                + " (select (rek.kodeakunutama || '.' || rek.kodeakunkelompok || '.' || rek.kodeakunjenis) kode, accountname, sum(rek.nilaianggaran) jumlah " 
                + " from koderekapbd rek " 
                + " inner join kegiatanapbd keg on rek.kegiatanindex=keg.kegiatanindex " 
                + " inner join apbd on keg.apbdindex=apbd.apbdindex  " 
                + " inner join cashaccount ca on (rek.kodeakunutama || '.' || rek.kodeakunkelompok || '.' || rek.kodeakunjenis) = ca.accountcode " 
                + " where apbd.tahunanggaran= " + year 
                + " and apbd.kodedata= " + kodeData + " and apbd.jeniscoa=" + jenisCOA;
        if( qPemdas!=null && !qPemdas.trim().equals(""))
            sql = sql + " and " + qPemdas;
        
        sql = sql + " group by kode, accountname " 
                + " order by kode) " 
                + " select * from utama " 
                + " union select * from kelompok " 
                + " union select * from jenis " 
                + " order by kode ";
        
        try {
            stm = conn.createStatement();
            rs = stm.executeQuery(sql);
            String utama = "", kelompok = "", jenis;
            double jumlahKelompok = 0;
            List<String> indexKelompok = new ArrayList();
            while (rs.next()) {
                result.add(new LampiranIPerdaAPBD(rs.getString("kode"), rs.getString("accountname"), rs.getDouble("jumlah")));
                /*
                if (utama.trim().equals("")) {
                    utama = rs.getString("kodeakunutama").trim();
                    kelompok = utama.trim() + "." + rs.getString("kodeakunkelompok").trim();
                    jenis = kelompok.trim() + "." + rs.getString("kodeakunjenis").trim();
                    double jumlah = jumlahKelompok = rs.getDouble("jumlah");
                    result.add(new LampiranIPerdaAPBD(utama, rs.getString("namaakunutama"), 0));
                    result.add(new LampiranIPerdaAPBD(kelompok, rs.getString("namaakunkelompok"), jumlah));
                    indexKelompok.add(String.valueOf(result.size() - 1));
                    result.add(new LampiranIPerdaAPBD(jenis, rs.getString("namaakunjenis"), jumlah));
                } else {
                    if (!utama.trim().equals(rs.getString("kodeakunutama").trim())) {
                        utama = rs.getString("kodeakunutama").trim();
                        kelompok = utama.trim() + "." + rs.getString("kodeakunkelompok").trim();
                        jenis = kelompok.trim() + "." + rs.getString("kodeakunjenis").trim();
                        double jumlah = jumlahKelompok = rs.getDouble("jumlah");
                        result.add(new LampiranIPerdaAPBD(utama, rs.getString("namaakunutama"), 0));
                        result.add(new LampiranIPerdaAPBD(kelompok, rs.getString("namaakunkelompok"), jumlahKelompok));
                        indexKelompok.add(String.valueOf(result.size() - 1));
                        result.add(new LampiranIPerdaAPBD(jenis, rs.getString("namaakunjenis"), jumlah));
                    } else {
                        if (kelompok.trim().equals(utama.trim() + "." + rs.getString("kodeakunkelompok").trim())) {
                            jenis = kelompok.trim() + "." + rs.getString("kodeakunjenis");
                            double jumlah = rs.getDouble("jumlah");
                            jumlahKelompok = jumlahKelompok + jumlah;
                            result.add(new LampiranIPerdaAPBD(jenis, rs.getString("namaakunjenis"), jumlah));
                            int ii = 0;
                            boolean find = false;
                            while (ii < indexKelompok.size() && find == false) {
                                if (result.get(Integer.parseInt(indexKelompok.get(ii).trim())).getKode().trim().equals(kelompok.trim())) {
                                    result.get(Integer.parseInt(indexKelompok.get(ii).trim())).setJumlah(jumlahKelompok);
                                    find = true;
                                }
                                ii++;
                            }
                        } else {
                            //Kelompok Beda
                            kelompok = utama.trim() + "." + rs.getString("kodeakunkelompok").trim();
                            jenis = kelompok.trim() + "." + rs.getString("kodeakunjenis").trim();
                            double jumlah = jumlahKelompok = rs.getDouble("jumlah");
                            result.add(new LampiranIPerdaAPBD(kelompok, rs.getString("namaakunkelompok"), jumlahKelompok));
                            indexKelompok.add(String.valueOf(result.size() - 1));
                            result.add(new LampiranIPerdaAPBD(jenis, rs.getString("namaakunjenis"), jumlah));
                        }
                    }
                }
                */
            }

            return result;
        } catch (SQLException ex) {
            Logger.getLogger(OfficeSQL.class.getName()).log(Level.SEVERE, null, ex);
            throw new SQLException("SQL: Gagal mengambil data Lampiran I Perda apbd \n" + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }


    @Override
    public List<LampiranIIPerdaAPBD> getLampiranIIPerdaAPBDbyYear(short year, String kodeSatker, short kodeData, short jenisCOA, Connection conn) throws SQLException {
        List<LampiranIIPerdaAPBD> result = new ArrayList();
        Statement stm = null;
        ResultSet rs = null;
        try {
            String sql = "with indexnya as( "
                    + "select apbdindex from apbd "
                    + "where kodesatker='"+kodeSatker.trim()+"' and tahunanggaran="+year+" and kodedata="+kodeData+" and jeniscoa=" + jenisCOA
                    + "), "
                    + "urusan as( "
                    + "select '1'::character(15) as kode, 'URUSAN WAJIB'::character(255) as nama "
                    + "union "
                    + "select '2'::character(15) as kode, 'URUSAN PILIHAN'::character(255) as nama "
                    + "), "
                    + "pelaksana as( "
                    + "select distinct kodeurusanpelaksana kode, upper(namaurusanpelaksana) nama from kegiatanapbd where apbdindex in (select apbdindex from indexnya) "
                    + "), "
                    + "skpd as( "
                    + "select distinct (kodeurusanpelaksana||'.'||kodeskpd) kode, namaskpd nama from kegiatanapbd where apbdindex in (select apbdindex from indexnya) "
                    + "), "
                    + "jumlahurusan4 as( "
                    + "select kode, nama, sum(nilaianggaran) nilai4, 0::double precision nilai51, 0::double precision nilai52, 0::double precision nilai61, 0::double precision nilai62 from koderekapbd rek "
                    + "inner join kegiatanapbd keg on rek.kegiatanindex=keg.kegiatanindex "
                    + "inner join urusan on substring(keg.kodeurusanpelaksana from 1 for 1)=urusan.kode "
                    + "where kodeakunutama='4' and apbdindex in (select apbdindex from indexnya) "
                    + "group by kode, nama "
                    + "), "
                    + "jumlahpelaksana4 as( "
                    + "select kode, nama, sum(nilaianggaran) nilai4, 0::double precision nilai51, 0::double precision nilai52, 0::double precision nilai61, 0::double precision nilai62 from pelaksana "
                    + "left join kegiatanapbd keg on pelaksana.kode=keg.kodeurusanpelaksana "
                    + "left join koderekapbd rek on keg.kegiatanindex=rek.kegiatanindex "
                    + "where kodeakunutama='4' and apbdindex in (select apbdindex from indexnya) "
                    + "group by kode, nama "
                    + "), "
                    + "jumlahskpd4 as( "
                    + "select kode, nama, sum(nilaianggaran) nilai4, 0::double precision nilai51, 0::double precision nilai52, 0::double precision nilai61, 0::double precision nilai62 from skpd "
                    + "left join kegiatanapbd keg on skpd.kode=(kodeurusanpelaksana||'.'||kodeskpd) "
                    + "left join koderekapbd rek on keg.kegiatanindex=rek.kegiatanindex "
                    + "where kodeakunutama='4' and apbdindex in (select apbdindex from indexnya) "
                    + "group by kode, nama "
                    + "), "
                    + "jumlahurusan51 as( "
                    + "select kode, nama, 0::double precision nilai4, sum(nilaianggaran) nilai51, 0::double precision nilai52, 0::double precision nilai61, 0::double precision nilai62 from koderekapbd rek "
                    + "inner join kegiatanapbd keg on rek.kegiatanindex=keg.kegiatanindex "
                    + "inner join urusan on substring(keg.kodeurusanpelaksana from 1 for 1)=urusan.kode "
                    + "where kodeakunutama='5' and kodeakunkelompok='1' and apbdindex in (select apbdindex from indexnya) "
                    + "group by kode, nama "
                    + "), "
                    + "jumlahpelaksana51 as( "
                    + "select kode, nama, 0::double precision nilai4, sum(nilaianggaran) nilai51, 0::double precision nilai52, 0::double precision nilai61, 0::double precision nilai62 from pelaksana "
                    + "left join kegiatanapbd keg on pelaksana.kode=keg.kodeurusanpelaksana "
                    + "left join koderekapbd rek on keg.kegiatanindex=rek.kegiatanindex "
                    + "where kodeakunutama='5' and kodeakunkelompok='1' and apbdindex in (select apbdindex from indexnya) "
                    + "group by kode, nama "
                    + "), "
                    + "jumlahskpd51 as( "
                    + "select kode, nama, 0::double precision nilai4, sum(nilaianggaran) nilai51, 0::double precision nilai52, 0::double precision nilai61, 0::double precision nilai62 from skpd "
                    + "left join kegiatanapbd keg on skpd.kode=(kodeurusanpelaksana||'.'||kodeskpd) "
                    + "left join koderekapbd rek on keg.kegiatanindex=rek.kegiatanindex "
                    + "where kodeakunutama='5' and kodeakunkelompok='1' and apbdindex in (select apbdindex from indexnya) "
                    + "group by kode, nama "
                    + "), "
                    + "jumlahurusan52 as( "
                    + "select kode, nama, 0::double precision nilai4, 0::double precision nilai51, sum(nilaianggaran) nilai52, 0::double precision nilai61, 0::double precision nilai62 from koderekapbd rek "
                    + "inner join kegiatanapbd keg on rek.kegiatanindex=keg.kegiatanindex "
                    + "inner join urusan on substring(keg.kodeurusanpelaksana from 1 for 1)=urusan.kode "
                    + "where kodeakunutama='5' and kodeakunkelompok='2' and apbdindex in (select apbdindex from indexnya) "
                    + "group by kode, nama "
                    + "), "
                    + "jumlahpelaksana52 as( "
                    + "select kode, nama, 0::double precision nilai4, 0::double precision nilai51, sum(nilaianggaran) nilai52, 0::double precision nilai61, 0::double precision nilai62 from pelaksana "
                    + "left join kegiatanapbd keg on pelaksana.kode=keg.kodeurusanpelaksana "
                    + "left join koderekapbd rek on keg.kegiatanindex=rek.kegiatanindex "
                    + "where kodeakunutama='5' and kodeakunkelompok='2' and apbdindex in (select apbdindex from indexnya) "
                    + "group by kode, nama "
                    + "), "
                    + "jumlahskpd52 as( "
                    + "select kode, nama, 0::double precision nilai4, 0::double precision nilai51, sum(nilaianggaran) nilai52, 0::double precision nilai61, 0::double precision nilai62  from skpd "
                    + "left join kegiatanapbd keg on skpd.kode=(kodeurusanpelaksana||'.'||kodeskpd) "
                    + "left join koderekapbd rek on keg.kegiatanindex=rek.kegiatanindex "
                    + "where kodeakunutama='5' and kodeakunkelompok='2' and apbdindex in (select apbdindex from indexnya) "
                    + "group by kode, nama "
                    + "), "
                    + "jumlahurusan61 as( "
                    + "select kode, nama, 0::double precision nilai4, 0::double precision nilai51, 0::double precision nilai52, sum(nilaianggaran) nilai61, 0::double precision nilai62 from koderekapbd rek "
                    + "inner join kegiatanapbd keg on rek.kegiatanindex=keg.kegiatanindex "
                    + "inner join urusan on substring(keg.kodeurusanpelaksana from 1 for 1)=urusan.kode "
                    + "where kodeakunutama='6' and kodeakunkelompok='1' and apbdindex in (select apbdindex from indexnya) "
                    + "group by kode, nama "
                    + "), "
                    + "jumlahpelaksana61 as( "
                    + "select kode, nama, 0::double precision nilai4, 0::double precision nilai51, 0::double precision nilai52, sum(nilaianggaran) nilai61, 0::double precision nilai62 from pelaksana "
                    + "left join kegiatanapbd keg on pelaksana.kode=keg.kodeurusanpelaksana "
                    + "left join koderekapbd rek on keg.kegiatanindex=rek.kegiatanindex "
                    + "where kodeakunutama='6' and kodeakunkelompok='1' and apbdindex in (select apbdindex from indexnya) "
                    + "group by kode, nama "
                    + "), "
                    + "jumlahskpd61 as( "
                    + "select kode, nama, 0::double precision nilai4, 0::double precision nilai51, 0::double precision nilai52, sum(nilaianggaran) nilai61, 0::double precision nilai62 from skpd "
                    + "left join kegiatanapbd keg on skpd.kode=(kodeurusanpelaksana||'.'||kodeskpd) "
                    + "left join koderekapbd rek on keg.kegiatanindex=rek.kegiatanindex "
                    + "where kodeakunutama='6' and kodeakunkelompok='1' and apbdindex in (select apbdindex from indexnya) "
                    + "group by kode, nama "
                    + "), "
                    + "jumlahurusan62 as( "
                    + "select kode, nama, 0::double precision nilai4, 0::double precision nilai51, 0::double precision nilai52, 0::double precision nilai61, sum(nilaianggaran) nilai62 from koderekapbd rek "
                    + "inner join kegiatanapbd keg on rek.kegiatanindex=keg.kegiatanindex "
                    + "inner join urusan on substring(keg.kodeurusanpelaksana from 1 for 1)=urusan.kode "
                    + "where kodeakunutama='6' and kodeakunkelompok='2' and apbdindex in (select apbdindex from indexnya) "
                    + "group by kode, nama "
                    + "), "
                    + "jumlahpelaksana62 as( "
                    + "select kode, nama, 0::double precision nilai4, 0::double precision nilai51, 0::double precision nilai52, 0::double precision nilai61, sum(nilaianggaran) nilai62 from pelaksana "
                    + "left join kegiatanapbd keg on pelaksana.kode=keg.kodeurusanpelaksana "
                    + "left join koderekapbd rek on keg.kegiatanindex=rek.kegiatanindex "
                    + "where kodeakunutama='6' and kodeakunkelompok='2' and apbdindex in (select apbdindex from indexnya) "
                    + "group by kode, nama "
                    + "), "
                    + "jumlahskpd62 as( "
                    + "select kode, nama, 0::double precision nilai4, 0::double precision nilai51, 0::double precision nilai52, 0::double precision nilai61, sum(nilaianggaran) nilai62 from skpd "
                    + "left join kegiatanapbd keg on skpd.kode=(kodeurusanpelaksana||'.'||kodeskpd) "
                    + "left join koderekapbd rek on keg.kegiatanindex=rek.kegiatanindex "
                    + "where kodeakunutama='6' and kodeakunkelompok='2' and apbdindex in (select apbdindex from indexnya) "
                    + "group by kode, nama "
                    + "), "
                    + "gabung as( "
                    + "select * from jumlahurusan4 "
                    + "union "
                    + "select * from jumlahurusan51 "
                    + "union "
                    + "select * from jumlahurusan52 "
                    + "union "
                    + "select * from jumlahurusan61 "
                    + "union "
                    + "select * from jumlahurusan62 "
                    + "union "
                    + "select * from jumlahpelaksana4 "
                    + "union "
                    + "select * from jumlahpelaksana51 "
                    + "union "
                    + "select * from jumlahpelaksana52 "
                    + "union "
                    + "select * from jumlahpelaksana61 "
                    + "union "
                    + "select * from jumlahpelaksana62 "
                    + "union "
                    + "select * from jumlahskpd4 "
                    + "union "
                    + "select * from jumlahskpd51 "
                    + "union "
                    + "select * from jumlahskpd52 "
                    + "union "
                    + "select * from jumlahskpd61 "
                    + "union "
                    + "select * from jumlahskpd62 "
                    + ") "
                    + "select kode, nama, sum(nilai4) nilai4, sum(nilai51) nilai51, sum(nilai52) nilai52, (sum(nilai51) + sum(nilai52)) nilai5, sum(nilai61) nilai61, sum(nilai62) nilai62, (sum(nilai61)-sum(nilai62)) nilai63, "
                    + "(sum(nilai4) - (sum(nilai51) + sum(nilai52)) + (sum(nilai61)-sum(nilai62))) silpa "
                    + "from gabung "
                    + "group by kode, nama "
                    + "order by kode";
            
            stm = conn.createStatement();
            rs = stm.executeQuery(sql);
            while (rs.next()) {
                result.add(new LampiranIIPerdaAPBD(rs.getString(1), rs.getString(2), rs.getDouble(3), rs.getDouble(4), rs.getDouble(5), rs.getDouble(7), rs.getDouble(8), rs.getDouble(10)));
            }
            /*
            String sql = "with xx as( "
                    + "  SELECT distinct keg.kodeurusanpelaksana, keg.namaurusanpelaksana,  keg.kodeskpd, keg.namaskpd, "
                    + "  (kodeakunutama || '.' || kodeakunkelompok || '.' || kodeakunjenis )akun, sum(nilaianggaran) jumlah"
                    + "  from kegiatanapbd keg"
                    + "  inner join apbd on keg.apbdindex=apbd.apbdindex "
                    + "  inner join koderekapbd rek on keg.kegiatanindex=rek.kegiatanindex"
                    + "  where kodesatker='" + kodeSatker.trim() + "' and tahunanggaran=" + year + " and kodedata=" + kodeData + " and jeniscoa=" + jenisCOA + " "
                    + "  group by keg.kodeurusanpelaksana, keg.namaurusanpelaksana, keg.kodeskpd, keg.namaskpd, akun"
                    + "  order by keg.kodeurusanpelaksana, kodeskpd, akun ) "
                    + "  select distinct xx.kodeurusanpelaksana, xx.namaurusanpelaksana, xx.kodeskpd, xx.namaskpd, sum(ss.jumlah) pendapatan, sum(bl.jumlah) bl, sum(btl.jumlah) btl, sum(tr.jumlah) tr, sum(kl.jumlah) kl, sum(net.jumlah) net from xx "
                    + "  left join (select * from xx where akun like '4%') ss on ss.kodeurusanpelaksana=xx.kodeurusanpelaksana and ss.kodeskpd=xx.kodeskpd"
                    + "  left join (select * from xx where akun like '5.1%') bl on bl.kodeurusanpelaksana=xx.kodeurusanpelaksana and bl.kodeskpd=xx.kodeskpd"
                    + "  left join (select * from xx where akun like '5.2%') btl on btl.kodeurusanpelaksana=xx.kodeurusanpelaksana and btl.kodeskpd=xx.kodeskpd"
                    + "  left join (select * from xx where akun like '6.1%') tr on tr.kodeurusanpelaksana=xx.kodeurusanpelaksana and tr.kodeskpd=xx.kodeskpd"
                    + "  left join (select * from xx where akun like '6.2%') kl on kl.kodeurusanpelaksana=xx.kodeurusanpelaksana and kl.kodeskpd=xx.kodeskpd"
                    + "  left join (select * from xx where akun like '6.3%') net on net.kodeurusanpelaksana=xx.kodeurusanpelaksana and net.kodeskpd=xx.kodeskpd"
                    + "  group by xx.kodeurusanpelaksana, xx.namaurusanpelaksana, xx.kodeskpd, xx.namaskpd"
                    + "  order by xx.kodeurusanpelaksana, xx.kodeskpd";

            stm = conn.createStatement();
            rs = stm.executeQuery(sql);
            String kodePelaksana = "";
            double pd = 0, bl = 0, btl = 0, biaya = 0, neto = 0, silpa = 0;
            int id = 0;
            while (rs.next()) {
                if (kodePelaksana.trim().equals("")) {
                    kodePelaksana = rs.getString(1).trim();
                    result.add(new LampiranIIPerdaAPBD(rs.getString(1), rs.getString(2), rs.getDouble(5), rs.getDouble(6), rs.getDouble(7), rs.getDouble(8), rs.getDouble(9), rs.getDouble(10)));
                    id = result.size() - 1;
                    result.add(new LampiranIIPerdaAPBD(rs.getString(1).trim() + "." + rs.getString(3).trim(), rs.getString(4), rs.getDouble(5), rs.getDouble(6), rs.getDouble(7), rs.getDouble(8), rs.getDouble(10), rs.getDouble(9)));
                    pd = rs.getDouble(5);
                    bl = rs.getDouble(6);
                    btl = rs.getDouble(7);
                    biaya = rs.getDouble(8);
                    neto = rs.getDouble(9);
                    silpa = rs.getDouble(10);
                } else {
                    String k = rs.getString(1).trim();
                    if (k.trim().length() > 4) {
                        k = k.trim().substring(0, 4);
                    }
                    if (kodePelaksana.trim().equals(k.trim())) {
                        result.add(new LampiranIIPerdaAPBD(rs.getString(1).trim() + "." + rs.getString(3).trim(), rs.getString(4), rs.getDouble(5), rs.getDouble(6), rs.getDouble(7), rs.getDouble(8), rs.getDouble(9), rs.getDouble(10)));
                        pd += rs.getDouble(5);
                        bl += rs.getDouble(6);
                        btl += rs.getDouble(7);
                        biaya += rs.getDouble(8);
                        neto += rs.getDouble(9);
                        silpa += rs.getDouble(10);
                        result.get(id).setBelanjaLangsung(bl);
                        result.get(id).setBelanjaTidakLangsung(btl);
                        result.get(id).setPembiayaanNeto(neto);
                        result.get(id).setPendapatan(pd);
                        result.get(id).setPenerimaan(biaya);
                        result.get(id).setSilpa(silpa);
                    } else {
                        kodePelaksana = k.trim();
                        result.add(new LampiranIIPerdaAPBD("", "", 0, 0, 0, 0, 0, 0));
                        result.add(new LampiranIIPerdaAPBD(rs.getString(1).trim(), rs.getString(2), rs.getDouble(5), rs.getDouble(6), rs.getDouble(7), rs.getDouble(8), rs.getDouble(9), rs.getDouble(10)));
                        id = result.size() - 1;
                        result.add(new LampiranIIPerdaAPBD(rs.getString(1).trim() + "." + rs.getString(3).trim(), rs.getString(4), rs.getDouble(5), rs.getDouble(6), rs.getDouble(7), rs.getDouble(8), rs.getDouble(9), rs.getDouble(10)));
                        pd = rs.getDouble(5);
                        bl = rs.getDouble(6);
                        btl = rs.getDouble(7);
                        biaya = rs.getDouble(8);
                        neto = rs.getDouble(9);
                        silpa = rs.getDouble(10);
                    }
                }

            }*/
            return result;
        } catch (SQLException ex) {
            throw new SQLException("Gagal mengambil Lampiran II Perda APBD " + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    /**
     *
     * @param year
     * @param kodePemdas
     * @param kodeData
     * @param jenisCOA
     * @param conn
     * @return
     * @throws SQLException
     */
    @Override
    public List<LampiranIIPerdaAPBD> getKompilasiLampiranIIPerdaAPBD(short year, List<String> kodePemdas, short kodeData, short jenisCOA, Connection conn) throws SQLException {
        List<LampiranIIPerdaAPBD> result = new ArrayList();
        Statement stm = null;
        ResultSet rs = null;
        try {
            String sql = "with xx as( "
                    + "  SELECT distinct keg.kodeurusanpelaksana, keg.namaurusanpelaksana,  keg.kodeskpd, keg.namaskpd, "
                    + "  (kodeakunutama || '.' || kodeakunkelompok || '.' || kodeakunjenis )akun, sum(nilaianggaran) jumlah"
                    + "  from kegiatanapbd keg"
                    + "  inner join apbd on keg.apbdindex=apbd.apbdindex "
                    + "  inner join koderekapbd rek on keg.kegiatanindex=rek.kegiatanindex";
            String qPemdas = "";
            if(kodePemdas!=null && kodePemdas.size() >0 ){
                for (String kodePemda : kodePemdas) {
                    if(!qPemdas.trim().equals("")) qPemdas = qPemdas + " or ";
                    qPemdas = qPemdas + " kodepemda = '" + kodePemda + "' ";
                }
                qPemdas= "(" + qPemdas + ") ";
            }
            if( !qPemdas.trim().equals("") ) sql = sql + " and " + qPemdas;
            sql = sql + " and tahunanggaran=" + year + " and kodedata=" + kodeData + " and jeniscoa=" + jenisCOA + " "
                    + "  group by keg.kodeurusanpelaksana, keg.namaurusanpelaksana, keg.kodeskpd, keg.namaskpd, akun"
                    + "  order by keg.kodeurusanpelaksana, kodeskpd, akun ) "
                    + "  select distinct xx.kodeurusanpelaksana, xx.namaurusanpelaksana, xx.kodeskpd, xx.namaskpd, sum(ss.jumlah) pendapatan, sum(bl.jumlah) bl, sum(btl.jumlah) btl, sum(tr.jumlah) tr, sum(kl.jumlah) kl, sum(net.jumlah) net from xx "
                    + "  left join (select * from xx where akun like '4%') ss on ss.kodeurusanpelaksana=xx.kodeurusanpelaksana and ss.kodeskpd=xx.kodeskpd"
                    + "  left join (select * from xx where akun like '5.1%') bl on bl.kodeurusanpelaksana=xx.kodeurusanpelaksana and bl.kodeskpd=xx.kodeskpd"
                    + "  left join (select * from xx where akun like '5.2%') btl on btl.kodeurusanpelaksana=xx.kodeurusanpelaksana and btl.kodeskpd=xx.kodeskpd"
                    + "  left join (select * from xx where akun like '6.1%') tr on tr.kodeurusanpelaksana=xx.kodeurusanpelaksana and tr.kodeskpd=xx.kodeskpd"
                    + "  left join (select * from xx where akun like '6.2%') kl on kl.kodeurusanpelaksana=xx.kodeurusanpelaksana and kl.kodeskpd=xx.kodeskpd"
                    + "  left join (select * from xx where akun like '6.3%') net on net.kodeurusanpelaksana=xx.kodeurusanpelaksana and net.kodeskpd=xx.kodeskpd"
                    + "  group by xx.kodeurusanpelaksana, xx.namaurusanpelaksana, xx.kodeskpd, xx.namaskpd"
                    + "  order by xx.kodeurusanpelaksana, xx.kodeskpd";

            stm = conn.createStatement();
            rs = stm.executeQuery(sql);
            String kodePelaksana = "";
            double pd = 0, bl = 0, btl = 0, biaya = 0, neto = 0, silpa = 0;
            int id = 0;
            while (rs.next()) {
                if (kodePelaksana.trim().equals("")) {
                    kodePelaksana = rs.getString(1).trim();
                    result.add(new LampiranIIPerdaAPBD(rs.getString(1), rs.getString(2), rs.getDouble(5), rs.getDouble(6), rs.getDouble(7), rs.getDouble(8), rs.getDouble(9), rs.getDouble(10)));
                    id = result.size() - 1;
                    result.add(new LampiranIIPerdaAPBD(rs.getString(1).trim() + "." + rs.getString(3).trim(), rs.getString(4), rs.getDouble(5), rs.getDouble(6), rs.getDouble(7), rs.getDouble(8), rs.getDouble(10), rs.getDouble(9)));
                    pd = rs.getDouble(5);
                    bl = rs.getDouble(6);
                    btl = rs.getDouble(7);
                    biaya = rs.getDouble(8);
                    neto = rs.getDouble(9);
                    silpa = rs.getDouble(10);
                } else {
                    String k = rs.getString(1).trim();
                    if (k.trim().length() > 4) {
                        k = k.trim().substring(0, 4);
                    }
                    if (kodePelaksana.trim().equals(k.trim())) {
                        result.add(new LampiranIIPerdaAPBD(rs.getString(1).trim() + "." + rs.getString(3).trim(), rs.getString(4), rs.getDouble(5), rs.getDouble(6), rs.getDouble(7), rs.getDouble(8), rs.getDouble(9), rs.getDouble(10)));
                        pd += rs.getDouble(5);
                        bl += rs.getDouble(6);
                        btl += rs.getDouble(7);
                        biaya += rs.getDouble(8);
                        neto += rs.getDouble(9);
                        silpa += rs.getDouble(10);
                        result.get(id).setBelanjaLangsung(bl);
                        result.get(id).setBelanjaTidakLangsung(btl);
                        result.get(id).setPembiayaanNeto(neto);
                        result.get(id).setPendapatan(pd);
                        result.get(id).setPenerimaan(biaya);
                        result.get(id).setSilpa(silpa);
                    } else {
                        kodePelaksana = k.trim();
                        result.add(new LampiranIIPerdaAPBD("", "", 0, 0, 0, 0, 0, 0));
                        result.add(new LampiranIIPerdaAPBD(rs.getString(1).trim(), rs.getString(2), rs.getDouble(5), rs.getDouble(6), rs.getDouble(7), rs.getDouble(8), rs.getDouble(9), rs.getDouble(10)));
                        id = result.size() - 1;
                        result.add(new LampiranIIPerdaAPBD(rs.getString(1).trim() + "." + rs.getString(3).trim(), rs.getString(4), rs.getDouble(5), rs.getDouble(6), rs.getDouble(7), rs.getDouble(8), rs.getDouble(9), rs.getDouble(10)));
                        pd = rs.getDouble(5);
                        bl = rs.getDouble(6);
                        btl = rs.getDouble(7);
                        biaya = rs.getDouble(8);
                        neto = rs.getDouble(9);
                        silpa = rs.getDouble(10);
                    }
                }

            }
            return result;
        } catch (SQLException ex) {
            throw new SQLException("Gagal mengambil Lampiran II Perda APBD " + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }

    @Override
    public List<String> getDinasKirimAPBD(short year, String kodeSatker, short kodeData, short jenisCOA, Connection conn) throws SQLException {
        Statement stm = null;
        ResultSet rs = null;
        try {
            stm = conn.createStatement();
            rs = stm.executeQuery("select distinct keg.kodeurusanpelaksana, keg.kodeskpd, keg.namaskpd from kegiatanapbd keg "
                    + "inner join apbd on keg.apbdindex=apbd.apbdindex where tahunanggaran="
                    + year + " and kodesatker='" + kodeSatker + "' and kodeData=" + kodeData + " and jeniscoa=" + jenisCOA + " "
                    + " order by keg.kodeurusanpelaksana, keg.kodeskpd");
            List<String> result = new ArrayList();

            while (rs.next()) {
                result.add(rs.getString("kodeurusanpelaksana").trim() + "." + rs.getString("kodeskpd") + " " + rs.getString("namaskpd"));
            }

            return result;
        } catch (SQLException ex) {
            throw new SQLException("Gagal mengambil data SKPD");
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    @Override
    public Timestamp getTanggalDinasKirimAPBD(short year, String kodeSatker, short kodeData, short jenisCOA, Connection conn) throws SQLException {
        Statement stm = null;
        ResultSet rs = null;
        try {
            stm = conn.createStatement();
            rs = stm.executeQuery("select tglpengiriman from apbd "
                    + " where tahunanggaran=" + year 
                    + " and kodesatker='" + kodeSatker + "' and kodeData=" + kodeData + " and jeniscoa=" + jenisCOA );
            Timestamp result = null;

            while (rs.next()) {
                result = rs.getTimestamp("tglpengiriman");
            }

            return result;
        } catch (SQLException ex) {
            throw new SQLException("Gagal mengambil data Tanggal SKPD Kirim APBD");
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }

    @Override
    public String getUrusan(String kodeUrusan, Connection conn) throws SQLException {
        Statement stm = null;
        ResultSet rs = null;
        try {
            stm = conn.createStatement();
            if (kodeUrusan == null || kodeUrusan.trim().equals("")) {
                kodeUrusan = "";
            }
            rs = stm.executeQuery("select keg.namaurusanprogram nama from kegiatanapbd keg "
                    + "where kodeurusanprogram='" + kodeUrusan.trim() + "'");
            if (rs.next()) {
                return rs.getString("nama");
            }

            return "";
        } catch (SQLException ex) {
            throw new SQLException("Gagal mengambil data Urusan");
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }

    @Override
    public List<String> getDinasKirimRealisasiAPBD(short year, String kodeSatker, short kodeData, short jenisCOA, Connection conn) throws SQLException {
        Statement stm = null;
        ResultSet rs = null;
        try {
            stm = conn.createStatement();
            rs = stm.executeQuery("select distinct keg.kodeurusanpelaksana, keg.kodeskpd, keg.namaskpd  from realisasikegiatanapbd keg "
                    + "inner join realisasiapbd apbd on keg.apbdindex=apbd.apbdindex where tahunanggaran="
                    + year + " and kodesatker='" + kodeSatker + "' and kodeData=" + kodeData + " and jeniscoa=" + jenisCOA + " "
                    + " order by keg.kodeurusanpelaksana, keg.kodeskpd");
            List<String> result = new ArrayList();
            while (rs.next()) {
                result.add(rs.getString("kodeurusanpelaksana").trim() + "." + rs.getString("kodeskpd") + " " + rs.getString("namaskpd"));
            }
            return result;
        } catch (SQLException ex) {
            throw new SQLException("Gagal mengambil data SKPD");
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    @Override
    public Timestamp getTanggalDinasKirimRealisasiAPBD(short year, String kodeSatker, short kodeData, short jenisCOA, Connection conn) throws SQLException {
        Statement stm = null;
        ResultSet rs = null;
        try {
            stm = conn.createStatement();
            rs = stm.executeQuery("select tglpengiriman from realisasiapbd apbd "
                    + "where tahunanggaran=" + year 
                    + " and kodesatker='" + kodeSatker + "' and kodeData=" + kodeData + " and jeniscoa=" + jenisCOA );
            Timestamp result = null;
            if (rs.next()) {
                result = rs.getTimestamp("tglpengiriman");
            }
            return result;
        } catch (SQLException ex) {
            throw new SQLException("Gagal mengambil data Tanggal Pengiriman Realisasi APBD");
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }
    

    @Override
    public List<RekapitulasiGajiPegawai> getRekapitulasiGajiPegawai(short year, String kodeSatker, short month, Connection conn) throws SQLException {
        List<RekapitulasiGajiPegawai> result = new ArrayList();
        Statement stm = null;
        ResultSet rs = null;
        String sql = "select kodegolongan, count(nip) pegawai, sum(jumlahpasangan) pasangan, sum(jumlahanak) anak, "
                    + "sum(gajipokok) gaji, sum(tunjanganpasangan) tpas, sum(tunjangananak) tanak, sum(tunjanganperbaikan) tpp, "
                    + "sum(tunjanganstruktural) tstruktur,  sum(tunjanganumum) tumum,  sum(tunjanganfungsional) tfungsi,  sum(tunjangankhusus) tkhusus, "
                    + "sum(tunjanganterpencil) tpencil, sum(tunjangankemahalan) tkd, sum(tunjanganpendidikan) tpendidikan, sum(tunjanganberas) tberas, "
                    + "sum(tunjanganpajak) tpajak,  sum(tunjanganaskes) taskes,  sum(tunjanganpembulatan) tbulat,  sum(jumlahkotor) kotor, "
                    + "sum(potonganpajak) ppajak,  sum(potonganaskes) paskes,  sum(iwp) iwp,  sum(potonganperum) taperum, "
                    + "sum(potonganutang) utang,  sum(potonganbulog) pbulog,  sum(potongansewarumah) psewa,  sum(jumlahpotongan) jumlahpot, "
                    + "sum(jumlahbersih) bersih "
                    + " from rincianpegawai rp "
                    + " inner join skpdpegawai sp on rp.skpdindex=sp.skpdindex "
                    + " inner join daftarpegawai dp on sp.pegawaiindex=dp.pegawaiindex ";
        String where = "";
        if( year > 0 ) where = "dp.tahunanggaran=" + year;
        if( kodeSatker!=null && !kodeSatker.trim().equals("") ){
            if( !where.trim().equals("") ) where = where + " and ";
            where = where + " dp.kodesatker='" +  kodeSatker + "'";
        }
        if( month > 0 ){
            if( !where.trim().equals("") ) where = where + " and ";
            where = where + "dp.bulangaji=" + month; 
        }
        
        if( !where.trim().equals("") ){
            sql = sql + " where " + where;
        }
        sql = sql + " group by kodegolongan  order by kodegolongan desc";

        try {
            stm = conn.createStatement();
            rs = stm.executeQuery(sql);
            while (rs.next()) {
                result.add(new RekapitulasiGajiPegawai(rs.getString("kodegolongan"), 
                        rs.getInt("pegawai"), rs.getInt("pasangan"), rs.getInt("anak"),
                        rs.getDouble("gaji"), rs.getDouble("tpas"), rs.getDouble("tanak"),  rs.getDouble("tpp"), 
                        rs.getDouble("tstruktur"), rs.getDouble("tumum"), rs.getDouble("tfungsi"),  rs.getDouble("tkhusus"), 
                        rs.getDouble("tpencil"), rs.getDouble("tkd"), rs.getDouble("tpendidikan"),  rs.getDouble("tberas"), 
                        rs.getDouble("tpajak"), rs.getDouble("taskes"), rs.getDouble("tbulat"),  rs.getDouble("kotor"), 
                        rs.getDouble("ppajak"), rs.getDouble("paskes"), rs.getDouble("iwp"),  rs.getDouble("taperum"), 
                        rs.getDouble("utang"), rs.getDouble("pbulog"), rs.getDouble("psewa"),  rs.getDouble("jumlahpot"), 
                        rs.getDouble("bersih")));
            }

            return result;
        } catch (SQLException ex) {
            Logger.getLogger(OfficeSQL.class.getName()).log(Level.SEVERE, null, ex);
            throw new SQLException("SQL: Gagal mengambil data Rekapitulasi Gaji Pegawai \n" + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }

    @Override
    public PerhitunganFihakKetiga getPFK(short year, String kodeSatker, Connection conn) throws SQLException {
        Statement stm = null;
        ResultSet rs = null;
        try {
            String sql = "select * from pfk where kodesatker='" + kodeSatker + "' and tahunanggaran=" + year;

            stm = conn.createStatement();
            rs = stm.executeQuery(sql);
            if (rs.next()) {
                return new PerhitunganFihakKetiga(rs.getLong("pfkindex"), rs.getString("kodesatker"), rs.getString("kodepemda"), rs.getString("namapemda"), rs.getShort("tahunanggaran"), rs.getShort("statusdata"), rs.getString("namaaplikasi"), rs.getString("pengembangaplikasi"));
            }
            return null;
        } catch (SQLException ex) {
            throw new SQLException("Gagal mengambil Perhitungan Fihak Ketiga " + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }

    @Override
    public long getPFKIndex(short year, String kodeSatker, Connection conn) throws SQLException {
        Statement stm = null;
        ResultSet rs = null;
        try {
            String sql = "select * from pfk where kodesatker='" + kodeSatker + "' and tahunanggaran=" + year;

            stm = conn.createStatement();
            rs = stm.executeQuery(sql);
            if (rs.next()) {
                return rs.getLong("pfkindex");
            }
            return 0;
        } catch (SQLException ex) {
            throw new SQLException("Gagal mengambil Perhitungan Fihak Ketiga " + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }

    @Override
    public List<RincianPerhitunganFihakKetiga> getRincianPFK(long indexPFK, Connection conn) throws SQLException {
        List<RincianPerhitunganFihakKetiga> result = new ArrayList();
        Statement stm = null;
        ResultSet rs = null;
        try {
            String sql = "select * from Rincianpfk rpfk where pfkindex=" + indexPFK;

            stm = conn.createStatement();
            rs = stm.executeQuery(sql);
            while (rs.next()) {
                result.add(new RincianPerhitunganFihakKetiga(rs.getString("uraian"), rs.getDouble("pungutan"), rs.getDouble("setoran")));
            }
            return result;
        } catch (SQLException ex) {
            throw new SQLException("Gagal mengambil Rincian Perhitungan Fihak Ketiga " + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }

    @Override
    public List<RincianPerhitunganFihakKetiga> getRincianPFK(short year, String kodeSatker, Connection conn) throws SQLException {
        List<RincianPerhitunganFihakKetiga> result = new ArrayList();
        Statement stm = null;
        ResultSet rs = null;
        try {
            String sql = "select rpfk.* from Rincianpfk rpfk inner join pfk on rpfk.pfkindex=pfk.pfkindex where "
                    + "pfk.kodesatker='" + kodeSatker.trim() + "' and "
                    + "pfk.tahunanggaran=" + year + " order by uraian";

            stm = conn.createStatement();
            rs = stm.executeQuery(sql);
            while (rs.next()) {
                result.add(new RincianPerhitunganFihakKetiga(rs.getString("uraian"), rs.getDouble("pungutan"), rs.getDouble("setoran")));
            }
            return result;
        } catch (SQLException ex) {
            throw new SQLException("Gagal mengambil Rincian Perhitungan Fihak Ketiga " + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }

    @Override
    public long createPFK(PerhitunganFihakKetiga pfk, short iotype, Connection conn) throws SQLException {
        long id = 0;
        PreparedStatement stm = null;
        Statement stm2 = null;
        ResultSet rs = null;
        try {
            Date d = new Date();
            String sql = "insert into pfk (kodesatker, kodepemda, namapemda, tahunanggaran, tanggalpengiriman, statusdata, namaaplikasi, pengembangaplikasi, iotype) "
                    + "values (?, ?, ?, ?, ?, ?, ?, ?, ?)";

            stm = conn.prepareStatement(sql);
            stm.setString(1, pfk.getKodeSatker());
            stm.setString(2, pfk.getKodePemda());
            stm.setString(3, pfk.getNamaPemda());
            stm.setShort(4, pfk.getTahunAnggaran());
            stm.setDate(5, new java.sql.Date(d.getTime()));
            stm.setShort(6, pfk.getStatusData());
            stm.setString(7, pfk.getNamaAplikasi());
            stm.setString(8, pfk.getPengembangAplikasi());
            stm.setShort(9, iotype);
            stm.executeUpdate();
            stm2 = conn.createStatement();
            rs = stm2.executeQuery("select max(pfkindex) id from pfk");
            if (rs.next()) {
                id = rs.getLong("id");
            }
            return id;
        } catch (SQLException ex) {
            throw new SQLException("Gagal input data Perhitungan Fihak Ketiga " + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm2 != null) {
                stm2.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }

    @Override
    public void createRincianPFK(long indexPFK, RincianPerhitunganFihakKetiga rpfk, Connection conn) throws SQLException {
        Statement stm = null;
        try {
            String sql = "insert into rincianpfk values ("
                    + indexPFK + ", '"
                    + rpfk.getUraian() + "', "
                    + rpfk.getPungutan()+ ", "
                    + rpfk.getSetoran()+ ")";

            stm = conn.createStatement();
            stm.executeUpdate(sql);
        } catch (SQLException ex) {
            throw new SQLException("Gagal input data Rincian Perhitungan Fihak Ketiga " + ex.getMessage());
        } finally {
            if (stm != null) {
                stm.close();
            }
        }
    }

    @Override
    public void deletePFK(PerhitunganFihakKetiga pfk, Connection conn) throws SQLException {
        Statement stm = null;
        try {
            String sql = "delete from pfk where pfkindex=" + pfk.getId();

            stm = conn.createStatement();
            stm.executeUpdate(sql);
        } catch (SQLException ex) {
            throw new SQLException("Gagal menghapus data Perhitungan Fihak Ketiga " + ex.getMessage());
        } finally {
            if (stm != null) {
                stm.close();
            }
        }
    }

    @Override
    public void deleteRincianPFK(long indexPFK, RincianPerhitunganFihakKetiga rpfk, Connection conn) throws SQLException {
        Statement stm = null;
        try {
            String sql = "delete from rincianpfk where pfkindex=" + indexPFK + " and "                    
                    + " uraian='" + rpfk.getUraian().trim() + "'";
            System.out.println("sql " + sql);

            stm = conn.createStatement();
            stm.executeUpdate(sql);
        } catch (SQLException ex) {
            throw new SQLException("Gagal menghapus data Rincian Perhitungan Fihak Ketiga " + ex.getMessage());
        } finally {
            if (stm != null) {
                stm.close();
            }
        }
    }

    @Override
    public void deleteRincianPFK(long indexPFK, Connection conn) throws SQLException {
        Statement stm = null;
        try {
            String sql = "delete from rincianpfk where pfkindex=" + indexPFK;

            stm = conn.createStatement();
            stm.executeUpdate(sql);
        } catch (SQLException ex) {
            throw new SQLException("Gagal menghapus data Rincian Perhitungan Fihak Ketiga " + ex.getMessage());
        } finally {
            if (stm != null) {
                stm.close();
            }
        }
    }

    @Override
    public List<LampiranIIIPerdaAPBD> getLampiranIIIPerdaAPBD(short year, String kodeSatker, short kodeData, String kodeSKPD, short jenisCOA, Connection conn) throws SQLException {
        if (kodeSKPD == null || kodeSKPD.trim().equals("")) {
            kodeSKPD = "9.99.9999";
        }
        StringTokenizer tok = new StringTokenizer(kodeSKPD.trim(), ".");
        String urusan = "9.99";
        String skpd = "9999";
        if(tok.countTokens()==3){
            urusan = tok.nextToken();
            urusan= urusan +"." + tok.nextToken();
            skpd= tok.nextToken();
        }
        String sql="with u as( " +
"select (kodeakunutama) kode, namaakunutama nama, sum(nilaianggaran) nilai, 1::smallint jenis from koderekapbd r  " +
"inner join kegiatanapbd k on r.kegiatanindex=k.kegiatanindex  " +
"inner join apbd on k.apbdindex=apbd.apbdindex   " +
"                  where   " +
"                  kodeurusanpelaksana='" + urusan + "' and   " +
"                  kodeskpd='" + skpd + "' and   " +
"                  apbd.tahunanggaran=" + year + " and   " +
"                  jeniscoa=" + jenisCOA + " and   " +
"                  kodedata=" + kodeData + " and   " +
"                  kodesatker='" + kodeSatker + "' and   " +
"                  (kodekegiatan = '000000' or namakegiatan <> '')  " +
"                  group by kode, nama  " +
"                  order by kode  " +
"),  " +
"k as(  " +
"select (kodeakunutama||'.'||kodeakunkelompok) kode, namaakunkelompok nama, sum(nilaianggaran) nilai, 2::smallint jenis from koderekapbd r  " +
"inner join kegiatanapbd k on r.kegiatanindex=k.kegiatanindex  " +
"inner join apbd on k.apbdindex=apbd.apbdindex   " +
"                  where   " +
"                  kodeurusanpelaksana='" + urusan + "' and   " +
"                  kodeskpd='" + skpd + "' and   " +
"                  apbd.tahunanggaran=" + year + " and   " +
"                  jeniscoa=" + jenisCOA + " and   " +
"                  kodedata=" + kodeData + " and   " +
"                  kodesatker='" + kodeSatker + "' and   " +
"                  (kodekegiatan = '000000' or namakegiatan <> '')  " +
"                  group by kode, nama  " +
"                  order by kode  " +
"),  " +
"program as(  " +
"select (kodeakunutama||'.'||kodeakunkelompok||'.'||kodeurusanprogram||'.'||kodeurusanpelaksana||'.'||kodeskpd||'.'||kodeprogram) kode, namaprogram nama, sum(nilaianggaran) nilai, 3::smallint jenis from koderekapbd r  " +
"inner join kegiatanapbd k on r.kegiatanindex=k.kegiatanindex  " +
"inner join apbd on k.apbdindex=apbd.apbdindex   " +
"                  where   " +
"                  kodeurusanpelaksana='" + urusan + "' and   " +
"                  kodeskpd='" + skpd + "' and   " +
"                  apbd.tahunanggaran=" + year + " and   " +
"                  jeniscoa=" + jenisCOA + " and   " +
"                  kodedata=" + kodeData + " and   " +
"                  kodesatker='" + kodeSatker + "' and   " +
"                  (kodekegiatan = '000000' or namakegiatan <> '')  " +
"                  group by kode, nama  " +
"                  order by kode  " +
"),  " +
"kegiatan as(  " +
"select (kodeakunutama||'.'||kodeakunkelompok||'.'||kodeurusanprogram||'.'||kodeurusanpelaksana||'.'||kodeskpd||'.'||kodeprogram||'.'||kodekegiatan) kode, namakegiatan nama, sum(nilaianggaran) nilai, 4::smallint jenis from koderekapbd r  " +
"inner join kegiatanapbd k on r.kegiatanindex=k.kegiatanindex " +
"inner join apbd on k.apbdindex=apbd.apbdindex " +
"                  where " +
"                  kodeurusanpelaksana='" + urusan + "' and " +
"                  kodeskpd='" + skpd + "' and " +
"                  apbd.tahunanggaran=" + year + " and " +
"                  jeniscoa=" + jenisCOA + " and " +
"                  kodedata=" + kodeData + " and " +
"                  kodesatker='" + kodeSatker + "' and " +
"                  (kodekegiatan = '000000' or namakegiatan <> '') " +
"                  group by kode, nama " +
"                  order by kode" +
"), " +
"j as( " +
"select (kodeakunutama||'.'||kodeakunkelompok||'.'||kodeurusanprogram||'.'||kodeurusanpelaksana||'.'||kodeskpd||'.'||kodeprogram||'.'||kodekegiatan||'.'||kodeakunutama||'.'||kodeakunkelompok||'.'||kodeakunjenis) kode, namaakunjenis nama, sum(nilaianggaran) nilai, 5::smallint jenis from koderekapbd r " +
"inner join kegiatanapbd k on r.kegiatanindex=k.kegiatanindex " +
"inner join apbd on k.apbdindex=apbd.apbdindex " +
"                  where " +
"                  kodeurusanpelaksana='" + urusan + "' and " +
"                  kodeskpd='" + skpd + "' and " +
"                  apbd.tahunanggaran=" + year + " and " +
"                  jeniscoa=" + jenisCOA + " and " +
"                  kodedata=" + kodeData + " and " +
"                  kodesatker='" + kodeSatker + "' and " +
"                  (kodekegiatan = '000000' or namakegiatan <> '') " +
"                  group by kode, nama " +
"                  order by kode " +
") " +
"select * from u " +
"union select * from k " +
"union select * from program where nama <> null or trim(nama) <> '' " +
"union select * from kegiatan where nama <> null or trim(nama) <> '' " +
"union select * from j " +
"order by kode, jenis";
        /*String sql = "select (kodeurusanprogram || '.' || kodeurusanpelaksana || '.' || kodeskpd ||'.'|| kodeprogram) codeprogram, " +
                     "(kodeurusanprogram || '.' || kodeurusanpelaksana || '.' || kodeskpd ||'.'|| kodeprogram || '.' || kodekegiatan) codekegiatan, " +
                    "kodeakunutama, kodeakunkelompok, kodeakunjenis, "
                + " namaprogram, namakegiatan, namaakunutama, namaakunkelompok, namaakunjenis, sum(nilaianggaran) nilai from kegiatanapbd k "
                + " inner join koderekapbd r on k.kegiatanindex=r.kegiatanindex "
                + " inner join apbd on k.apbdindex=apbd.apbdindex "
                + " where "
                + " kodeurusanprogram='" + urusan + "' and "
                + " kodeskpd='" + skpd + "' and "
                + " apbd.tahunanggaran=" + year + " and "
                + " jeniscoa=" + jenisCOA + " and "
                + " kodedata=" + kodeData + " and "
                + " kodesatker='" + kodeSatker + "' and "
                + " (kodekegiatan = '000000' or namakegiatan <> '') "
                + " group by codeprogram, codekegiatan, kodeakunutama, kodeakunkelompok, kodeakunjenis, namaprogram, namakegiatan, namaakunutama, namaakunkelompok, namaakunjenis "
                + " order by codeprogram, codekegiatan, kodeakunutama, kodeakunkelompok, kodeakunjenis";*/
        List<LampiranIIIPerdaAPBD> result = new ArrayList();
        Statement stm = null;
        ResultSet rs = null;
        try {
            stm = conn.createStatement();
            rs = stm.executeQuery(sql);
            String kode;
            String lastS = "";
            double jumlah = 0;
            while(rs.next()){
                kode = rs.getString("kode");
                if(rs.getShort("jenis")==1) {
                    kode = urusan + "."+skpd+".000.000000."+kode.trim();
                    result.add(new LampiranIIIPerdaAPBD("", "", 0));
                    result.add(new LampiranIIIPerdaAPBD(kode, rs.getString("nama"), rs.getDouble("nilai")));
                }
                if(rs.getShort("jenis")==2) {
                    kode = "";
                    result.add(new LampiranIIIPerdaAPBD("", "", 0));
                    result.add(new LampiranIIIPerdaAPBD(kode, rs.getString("nama"), rs.getDouble("nilai")));
                }
                if(rs.getShort("jenis")==3) {
                    kode = kode.substring(4);
                    result.add(new LampiranIIIPerdaAPBD("", "", 0));
                    result.add(new LampiranIIIPerdaAPBD(kode, rs.getString("nama"), rs.getDouble("nilai")));
                }
                if(rs.getShort("jenis")==4) {
                    kode = kode.substring(4);
//                    result.add(new LampiranIIIPerdaAPBD("", "", 0));
                    result.add(new LampiranIIIPerdaAPBD(kode, rs.getString("nama"), rs.getDouble("nilai")));
                }
                if(rs.getShort("jenis")==5) {
                    kode = kode.substring(4);
                    result.add(new LampiranIIIPerdaAPBD(kode, rs.getString("nama"), rs.getDouble("nilai")));
                }
            }
            /*String au = "", ak = "", p = "", k = "", np;
            int idP = -1, idK = -1, idU = -1, idAK = -1;
            double jP = 0, jK = 0, jU = 0, jAK = 0;
            while (rs.next()) {
                String codeProgram = rs.getString("codeprogram");
                String codeKegiatan = rs.getString("codekegiatan");
                String codeAkunUtama = rs.getString("kodeakunutama");
                String codeAkunKelompok = rs.getString("kodeakunkelompok");
                String codeAkunJenis = rs.getString("kodeakunjenis");
                String fullCode = codeKegiatan.trim() + "." + codeAkunUtama.trim() + "." + codeAkunKelompok.trim() + "." + codeAkunJenis;
                
                np = rs.getString("namaprogram");
                if (au.trim().equals("")) {
                    au = rs.getString("namaakunutama");
                    idU = result.size();
                    result.add(new LampiranIIIPerdaAPBD(kodeSKPD + ".000.000000." + codeAkunUtama.trim(), au.toUpperCase(), 0));
                }
                if (ak.trim().equals("")) {
                    ak = rs.getString("namaakunkelompok");
                    result.add(new LampiranIIIPerdaAPBD("", "", 0));
                    idAK = result.size();
                    result.add(new LampiranIIIPerdaAPBD("", ak.toUpperCase(), 0));
                }
                if (p.trim().equals("")) {
                    jP = 0;
                    if (np != null && !np.trim().equals("")) {
                        idP = result.size();
                        p = codeProgram.trim();
                        result.add(new LampiranIIIPerdaAPBD(p, np, 0));
                    }
                }
                if (k.trim().equals("")) {
                    jK = 0;
                    if (np != null && !np.trim().equals("")) {
                        idK = result.size();
                        k = codeKegiatan.trim();
                        result.add(new LampiranIIIPerdaAPBD(k, rs.getString("namakegiatan"), 0));
                    }
                }

                if (!au.trim().equals(rs.getString("namaakunutama").trim())) {
                    result.add(new LampiranIIIPerdaAPBD("", "", 0));
                    result.get(idU).setJumlah(jU);
                    idU = result.size();
                    jU = 0;
                    au = rs.getString("namaakunutama");
                    result.add(new LampiranIIIPerdaAPBD(kodeSKPD + ".000.000000." + codeAkunUtama.trim(), au.toUpperCase(), 0));
                }
                if (!ak.trim().equals(rs.getString("namaakunkelompok").trim())) {
                    ak = rs.getString("namaakunkelompok");
                    result.get(idAK).setJumlah(jAK);
                    result.add(new LampiranIIIPerdaAPBD("", "", 0));
                    idAK = result.size();
                    jAK = 0;
                    result.add(new LampiranIIIPerdaAPBD("", ak.toUpperCase(), 0));
                }
                if (!p.trim().equals(codeProgram.trim())) {
                    p = codeProgram.trim();
                    if (np != null && !np.trim().equals("")) {
                        if (idP > -1) {
                            result.get(idP).setJumlah(jP);
                        }
                        jP = 0;
                        result.add(new LampiranIIIPerdaAPBD("", "", 0));
                        idP = result.size();

                        result.add(new LampiranIIIPerdaAPBD(p, np, 0));
                    }
                }
                if (!k.trim().equals(codeKegiatan.trim())) {
                    k = codeKegiatan.trim();
                    if (np != null && !np.trim().equals("")) {
                        if (idK > -1) {
                            result.get(idK).setJumlah(jK);
                        }
                        jK = 0;
                        idK = result.size();

                        result.add(new LampiranIIIPerdaAPBD(k, rs.getString("namakegiatan"), 0));
                    }
                }
                result.add(new LampiranIIIPerdaAPBD(fullCode.trim(), rs.getString("namaakunjenis"), rs.getDouble("nilai")));
                jP += rs.getDouble("nilai");
                jK += rs.getDouble("nilai");
                jU += rs.getDouble("nilai");
                jAK += rs.getDouble("nilai");
            }
            if (idP != -1) {
                result.get(idP).setJumlah(jP);
            }
            if (idK != -1) {
                result.get(idK).setJumlah(jK);
            }
            if (idU != -1) {
                result.get(idU).setJumlah(jU);
            }
            if (idAK != -1) {
                result.get(idAK).setJumlah(jAK);
            }*/
            return result;
        } catch (SQLException ex) {
            throw new SQLException("Gagal mengambil Lampiran III Perda APBD " + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }

    public List<LampiranIIIPerdaAPBD> getLampiranIIIAPerdaAPBD(short year, String kodeSatker, short kodeData, short jenisCOA, Connection conn) throws SQLException {
        String sql = "select (kodeurusanprogram || '.' || kodeurusanpelaksana "
                + "|| '.' || kodeskpd || '.' || kodekegiatan || '.' || kodeakunutama || '.' || kodeakunkelompok || '.' || kodeakunjenis || '.' || kodeakunobjek) kode, "
                + " namaprogram, namakegiatan, namaakunjenis, namaakunobjek, sum(nilaianggaran) from kegiatanapbd k "
                + "inner join koderekapbd r on k.kegiatanindex=r.kegiatanindex group by kode, namaprogram, namakegiatan, namaakunjenis, namaakunobjek";
        List<LampiranIIIPerdaAPBD> result = new ArrayList();
        Statement stm = null;
        ResultSet rs = null;
        try {
            stm = conn.createStatement();
            rs = stm.executeQuery(sql);

            while (rs.next()) {
                result.add(new LampiranIIIPerdaAPBD(rs.getString("kode"), rs.getString("namaakunobjek"), rs.getDouble("nilai")));
            }
            return result;
        } catch (SQLException ex) {
            throw new SQLException("Gagal mengambil Lampiran IIIA Perda APBD " + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }

    @Override
    public List<LampiranIVPerdaAPBD> getLampiranIVPerdaAPBD(short year, String kodeSatker, short kodeData, short jenisCOA, Connection conn) throws SQLException {
        /*String sql = "with xx as( "
                + "select (kodeurusanprogram || '.' || kodeurusanpelaksana "
                + "|| '.' || kodeskpd || '.' || kodeprogram || '.' || kodekegiatan ) kode, "
                + "(kodeurusanpelaksana || '.' || kodeskpd) codeskpd, "
                + "(kodeurusanprogram || '.' || kodeurusanpelaksana || '.' || kodeskpd || '.' || kodeprogram) codeprogram, "
                + " namaurusanprogram, namaurusanpelaksana, namaskpd, namaprogram, namakegiatan, sum(nilaianggaran) total "
                + " from kegiatanapbd k "
                + "inner join koderekapbd r on k.kegiatanindex=r.kegiatanindex "
                + "inner join apbd on k.apbdindex=apbd.apbdindex "
                + "where namakegiatan notnull and trim(namakegiatan) <> '' "
                + "and apbd.kodesatker='" + kodeSatker + "' and apbd.tahunanggaran=" + year + " and kodedata=" + kodeData + " and jeniscoa=" + jenisCOA + " "
                + "group by kode, codeskpd, codeprogram, namaurusanprogram, namaurusanpelaksana, namaskpd, namaprogram, namakegiatan "
                + "order by kode) "
                + "select xx.kode, xx.codeskpd, xx.codeprogram, xx.namaurusanprogram, xx.namaurusanpelaksana, xx.namaskpd, xx.namaprogram, xx.namakegiatan, "
                + "xx.total, b.barangjasa, m.modal, p.pegawai from xx "
                + "left join "
                + "(select (kodeurusanprogram || '.' || kodeurusanpelaksana "
                + "|| '.' || kodeskpd || '.' || kodeprogram || '.' || kodekegiatan ) kode, "
                + " sum(nilaianggaran) pegawai "
                + " from kegiatanapbd k "
                + "inner join koderekapbd r on k.kegiatanindex=r.kegiatanindex "
                + "inner join apbd on k.apbdindex=apbd.apbdindex "
                + "where namakegiatan notnull and trim(namakegiatan) <> '' "
                + "and apbd.kodesatker='" + kodeSatker + "' and apbd.tahunanggaran=" + year + " and kodedata=" + kodeData + " and jeniscoa="+ jenisCOA + " "
                + "and r.kodeakunutama='5'  and (r.kodeakunkelompok='2' OR r.kodeakunkelompok='1') and kodeakunjenis='1' "
                + "group by kode, namaurusanprogram, namaskpd, namaprogram, namakegiatan "
                + "order by kode) p "
                + "on xx.kode = p.kode "
                + "left join "
                + "(select (kodeurusanprogram || '.' || kodeurusanpelaksana "
                + "|| '.' || kodeskpd || '.' || kodeprogram || '.' || kodekegiatan ) kode, "
                + " sum(nilaianggaran) barangjasa "
                + " from kegiatanapbd k "
                + "inner join koderekapbd r on k.kegiatanindex=r.kegiatanindex "
                + "inner join apbd on k.apbdindex=apbd.apbdindex "
                + "where namakegiatan notnull and trim(namakegiatan) <> '' "
                + "and apbd.kodesatker='" + kodeSatker + "' and apbd.tahunanggaran=" + year + " and kodedata=" + kodeData + " and jeniscoa=" + jenisCOA + " "
                + "and r.kodeakunutama='5'  and r.kodeakunkelompok='2' and kodeakunjenis='2' "
                + "group by kode, namaurusanprogram, namaskpd, namaprogram, namakegiatan "
                + "order by kode) b "
                + "on xx.kode=b.kode "
                + "left join "
                + "(select (kodeurusanprogram || '.' || kodeurusanpelaksana "
                + "|| '.' || kodeskpd || '.' || kodeprogram || '.' || kodekegiatan ) kode, "
                + " sum(nilaianggaran) modal "
                + " from kegiatanapbd k "
                + "inner join koderekapbd r on k.kegiatanindex=r.kegiatanindex "
                + "inner join apbd on k.apbdindex=apbd.apbdindex "
                + "where namakegiatan notnull and trim(namakegiatan) <> '' "
                + "and apbd.kodesatker='" + kodeSatker + "' and apbd.tahunanggaran=" + year + " and kodedata=" + kodeData + " and jeniscoa=" + jenisCOA + " "
                + "and r.kodeakunutama='5'  and r.kodeakunkelompok='2' and kodeakunjenis='3' "
                + "group by kode, namaurusanprogram, namaskpd, namaprogram, namakegiatan "
                + "order by kode) m "
                + "on xx.kode = m.kode "
                + "order by kode";*/
        String sql = "with iapbd as(  \n" +
"select apbdindex from apbd where apbd.kodesatker='" + kodeSatker + "' and apbd.tahunanggaran=" + year + " and kodedata=" + kodeData + " and jeniscoa=" + jenisCOA +
"), \n" +
"wp as( \n" +
" select '1'::character(5) kode, 'URUSAN WAJIB'::character(255) nama, 0::double precision pegawai, 0::double precision barang, 0::double precision modal, 1::smallint jenis \n" +
" union select '2'::character(5) kode, 'URUSAN PILIHAN'::character(255) nama, 0::double precision pegawai, 0::double precision barang, 0::double precision modal, 1::smallint jenis \n" +
"), \n" +
"urusan as( \n" +
" select distinct kodeurusanpelaksana kode, namaurusanpelaksana nama, 0::double precision pegawai, 0::double precision barang, 0::double precision modal, 2::smallint jenis from kegiatanapbd k \n" +
" where namakegiatan notnull and trim(namakegiatan) <> ''  \n" +
" and apbdindex in(select apbdindex from iapbd) \n" +
"), \n" +
"skpd as( \n" +
" select distinct kodeurusanpelaksana||'.'||kodeskpd kode, namaskpd nama, 0::double precision pegawai, 0::double precision barang, 0::double precision modal, 3::smallint jenis from kegiatanapbd k \n" +
" where namakegiatan notnull and trim(namakegiatan) <> ''  \n" +
" and apbdindex in(select apbdindex from iapbd) \n" +
"), \n" +
"program as( \n" +
" select distinct kodeurusanpelaksana||'.'||kodeskpd||'.'||kodeurusanprogram||'.'||kodeurusanpelaksana||'.'||kodeskpd||'.'||kodeprogram kode, namaprogram nama,  \n" +
" p.peg pegawai, b.barang, m.modal, 4::smallint jenis \n" +
" from kegiatanapbd keg  \n" +
" left join  \n" +
"(  \n" +
"   select kodeurusanpelaksana||'.'||kodeskpd||'.'||kodeurusanprogram||'.'||kodeurusanpelaksana||'.'||kodeskpd||'.'||kodeprogram kode, sum(nilaianggaran) peg from koderekapbd r \n" +
"   inner join kegiatanapbd k on r.kegiatanindex=k.kegiatanindex \n" +
"   where namakegiatan notnull and trim(namakegiatan) <> ''  \n" +
"   and apbdindex in(select apbdindex from iapbd) \n" +
"   and r.kodeakunutama='5'  and (r.kodeakunkelompok='2' OR r.kodeakunkelompok='1') and kodeakunjenis='1' \n" +
"   group by kode \n" +
" ) p on (keg.kodeurusanpelaksana||'.'||keg.kodeskpd||'.'||keg.kodeurusanprogram||'.'||keg.kodeurusanpelaksana||'.'||keg.kodeskpd||'.'||keg.kodeprogram)=p.kode \n" +
" left join  \n" +
"(  \n" +
"   select kodeurusanpelaksana||'.'||kodeskpd||'.'||kodeurusanprogram||'.'||kodeurusanpelaksana||'.'||kodeskpd||'.'||kodeprogram kode, sum(nilaianggaran) barang from koderekapbd r \n" +
"   inner join kegiatanapbd k on r.kegiatanindex=k.kegiatanindex \n" +
"   where namakegiatan notnull and trim(namakegiatan) <> ''  \n" +
"   and apbdindex in(select apbdindex from iapbd) \n" +
"   and r.kodeakunutama='5' and r.kodeakunkelompok='2' and kodeakunjenis='2' \n" +
"   group by kode \n" +
" ) b on (keg.kodeurusanpelaksana||'.'||keg.kodeskpd||'.'||keg.kodeurusanprogram||'.'||keg.kodeurusanpelaksana||'.'||keg.kodeskpd||'.'||keg.kodeprogram)=b.kode \n" +
"left join \n" +
"(  \n" +
"   select kodeurusanpelaksana||'.'||kodeskpd||'.'||kodeurusanprogram||'.'||kodeurusanpelaksana||'.'||kodeskpd||'.'||kodeprogram kode, sum(nilaianggaran) modal from koderekapbd r \n" +
"   inner join kegiatanapbd k on r.kegiatanindex=k.kegiatanindex \n" +
"   where namakegiatan notnull and trim(namakegiatan) <> ''  \n" +
"   and apbdindex in(select apbdindex from iapbd) \n" +
"   and r.kodeakunutama='5'  and r.kodeakunkelompok='2' and kodeakunjenis='3' \n" +
"   group by kode \n" +
" ) m on (keg.kodeurusanpelaksana||'.'||keg.kodeskpd||'.'||keg.kodeurusanprogram||'.'||keg.kodeurusanpelaksana||'.'||keg.kodeskpd||'.'||keg.kodeprogram)=m.kode \n" +
" where namakegiatan notnull and trim(namakegiatan) <> ''  \n" +
" and apbdindex in(select apbdindex from iapbd) \n" +
"), \n" +
"kegiatan as( \n" +
" select distinct kodeurusanpelaksana||'.'||kodeskpd||'.'||kodeurusanprogram||'.'||kodeurusanpelaksana||'.'||kodeskpd||'.'||kodeprogram||'.'||kodekegiatan kode, namakegiatan nama,  \n" +
"  p.peg pegawai, b.barang, m.modal, 5::smallint jenis \n" +
" from kegiatanapbd keg  \n" +
" left join  \n" +
"(  \n" +
"   select kodeurusanpelaksana||'.'||kodeskpd||'.'||kodeurusanprogram||'.'||kodeurusanpelaksana||'.'||kodeskpd||'.'||kodeprogram||'.'||kodekegiatan kode, sum(nilaianggaran) peg from koderekapbd r \n" +
"   inner join kegiatanapbd k on r.kegiatanindex=k.kegiatanindex \n" +
"   where namakegiatan notnull and trim(namakegiatan) <> '' \n" +
"   and apbdindex in(select apbdindex from iapbd) \n" +
"   and r.kodeakunutama='5'  and (r.kodeakunkelompok='2' OR r.kodeakunkelompok='1') and kodeakunjenis='1' \n" +
"   group by kode \n" +
" ) p on (keg.kodeurusanpelaksana||'.'||keg.kodeskpd||'.'||keg.kodeurusanprogram||'.'||keg.kodeurusanpelaksana||'.'||keg.kodeskpd||'.'||keg.kodeprogram||'.'||keg.kodekegiatan)=p.kode \n" +
" left join \n" +
"(  \n" +
"   select kodeurusanpelaksana||'.'||kodeskpd||'.'||kodeurusanprogram||'.'||kodeurusanpelaksana||'.'||kodeskpd||'.'||kodeprogram||'.'||kodekegiatan kode, sum(nilaianggaran) barang from koderekapbd r \n" +
"   inner join kegiatanapbd k on r.kegiatanindex=k.kegiatanindex \n" +
"   where namakegiatan notnull and trim(namakegiatan) <> ''  \n" +
"   and apbdindex in(select apbdindex from iapbd) \n" +
"   and r.kodeakunutama='5' and r.kodeakunkelompok='2' and kodeakunjenis='2' \n" +
"   group by kode \n" +
" ) b on (keg.kodeurusanpelaksana||'.'||keg.kodeskpd||'.'||keg.kodeurusanprogram||'.'||keg.kodeurusanpelaksana||'.'||keg.kodeskpd||'.'||keg.kodeprogram||'.'||keg.kodekegiatan)=b.kode \n" +
"left join \n" +
"( \n" +
"   select kodeurusanpelaksana||'.'||kodeskpd||'.'||kodeurusanprogram||'.'||kodeurusanpelaksana||'.'||kodeskpd||'.'||kodeprogram||'.'||kodekegiatan kode, sum(nilaianggaran) modal from koderekapbd r \n" +
"   inner join kegiatanapbd k on r.kegiatanindex=k.kegiatanindex \n" +
"   where namakegiatan notnull and trim(namakegiatan) <> '' \n" +
"   and apbdindex in(select apbdindex from iapbd) \n" +
"   and r.kodeakunutama='5'  and r.kodeakunkelompok='2' and kodeakunjenis='3' \n" +
"   group by kode \n" +
" ) m on (keg.kodeurusanpelaksana||'.'||keg.kodeskpd||'.'||keg.kodeurusanprogram||'.'||keg.kodeurusanpelaksana||'.'||keg.kodeskpd||'.'||keg.kodeprogram||'.'||keg.kodekegiatan)=m.kode \n" +
" where namakegiatan notnull and trim(namakegiatan) <> '' \n" +
" and apbdindex in(select apbdindex from iapbd) \n" +
")\n" +
"select * from wp union \n" +
"select * from urusan union \n" +
"select * from skpd union \n" +
"select * from program union \n" +
"select * from kegiatan \n" +
"order by kode ";
        List<LampiranIVPerdaAPBD> result = new ArrayList();
        Statement stm = null;
        ResultSet rs = null;
        try {
            stm = conn.createStatement();
            rs = stm.executeQuery(sql);
            while (rs.next()) {
                String kode = rs.getString("kode");
                if(rs.getShort("jenis")==1 || rs.getShort("jenis")==2 || rs.getShort("jenis")==3)
                    result.add(new LampiranIVPerdaAPBD("", "", 0, 0, 0));
                if(rs.getShort("jenis") > 3)
                    kode = kode.trim().substring(10);
                result.add(new LampiranIVPerdaAPBD(kode, rs.getString("nama"), rs.getDouble("pegawai"), rs.getDouble("barang"), rs.getDouble("modal")));
            }
            /*String k = "", u = "", skpd = "", program = "";
            double jumlahProgramP = 0, jumlahProgramB = 0, jumlahProgramM = 0;
            int idprogram = -1;
            while (rs.next()) {
                String kode = rs.getString("kode");
                String codeskpd = rs.getString("codeskpd");
                String codeprogram = rs.getString("codeprogram");                
                if (k.trim().equals("")) {
                    result.add(new LampiranIVPerdaAPBD("1", "URUSAN WAJIB", 0, 0, 0));
                    k = "1";
                }
                if (u.trim().equals("")) {
                    result.add(new LampiranIVPerdaAPBD(kode.trim().substring(0, 4), rs.getString("namaurusanprogram"), 0, 0, 0));
                    u = kode.trim().substring(0, 4);
                }
                if (skpd.trim().equals("")) {
                    result.add(new LampiranIVPerdaAPBD(codeskpd.trim(), rs.getString("namaskpd"), 0, 0, 0));
                    skpd = codeskpd.trim();
                }
                if (program.trim().equals("")) {
                    idprogram = result.size();
                    result.add(new LampiranIVPerdaAPBD(codeprogram.trim(), rs.getString("namaprogram"), 0, 0, 0));
                    program = codeprogram.trim();
                    jumlahProgramP = 0;
                    jumlahProgramB = 0;
                    jumlahProgramM = 0;
                }
                if (!kode.trim().startsWith(k.trim())) {
                    result.add(new LampiranIVPerdaAPBD("", "", 0, 0, 0));
                    result.add(new LampiranIVPerdaAPBD("2", "URUSAN PILIHAN", 0, 0, 0));
                    k = "2";
                }
                if (!kode.trim().substring(0, 4).equals(u.trim())) {
                    result.add(new LampiranIVPerdaAPBD("", "", 0, 0, 0));
                    result.add(new LampiranIVPerdaAPBD(kode.trim().substring(0, 4), rs.getString("namaurusanprogram"), 0, 0, 0));
                    u = kode.trim().substring(0, 4);
                }

                if (!codeskpd.trim().equals(skpd.trim()) && kode.trim().substring(5, 9).equals(u.trim())) {

                    if (result.get(result.size() - 1).getKode().trim().length() != 4) {
                        result.add(new LampiranIVPerdaAPBD("", "", 0, 0, 0));
                    }
                    result.add(new LampiranIVPerdaAPBD(codeskpd.trim(), rs.getString("namaskpd"), 0, 0, 0));
                    skpd = codeskpd.trim();
                }

                if (!codeprogram.trim().equals(program.trim())) {
                    result.get(idprogram).setPegawai(jumlahProgramP);
                    result.get(idprogram).setBarangJasa(jumlahProgramB);
                    result.get(idprogram).setModal(jumlahProgramM);
//                    if (result.get(result.size() - 1).getKode().trim().length() != 7) {
//                        result.add(new LampiranIVPerdaAPBD("", "", 0, 0, 0));
//                    }
                    idprogram = result.size();
                    jumlahProgramP = 0;
                    jumlahProgramB = 0;
                    jumlahProgramM = 0;
                    result.add(new LampiranIVPerdaAPBD(codeprogram.trim(), rs.getString("namaprogram"), 0, 0, 0));
                    program = codeprogram.trim();
                }

                result.add(new LampiranIVPerdaAPBD(rs.getString("kode"), rs.getString("namakegiatan"), rs.getDouble("pegawai"), rs.getDouble("barangjasa"), rs.getDouble("modal")));
                jumlahProgramP += rs.getDouble("pegawai");
                jumlahProgramB += rs.getDouble("barangjasa");
                jumlahProgramM += rs.getDouble("modal");
            }*/
            return result;
        } catch (SQLException ex) {
            throw new SQLException("Gagal mengambil Lampiran IV Perda APBD " + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }

    @Override
    public List<LampiranVPerdaAPBD> getLampiranVPerdaAPBD(short year, String kodeSatker, short kodeData, short jenisCOA, Connection conn) throws SQLException {
        String sql = "with poin1 as("
                + "  select distinct kodefungsi, namafungsi, kodeurusanprogram, namaurusanprogram, sum(nilaianggaran) nilai "
                + "  from kegiatanapbd k "
                + "  inner join koderekapbd r on k.kegiatanindex=r.kegiatanindex "
                + "  inner join apbd on k.apbdindex=apbd.apbdindex "
                + "  where r.kodeakunutama='5' and apbd.kodesatker='" + kodeSatker + "' and apbd.tahunanggaran=" + year + " and kodedata=" + kodeData + " and jeniscoa=" + jenisCOA + " "
                + "  group by kodefungsi, namafungsi, kodeurusanprogram, namaurusanprogram order by kodefungsi, kodeurusanprogram) "
                + "  select poin1.kodefungsi, upper(poin1.namafungsi) namafungsi, poin1.kodeurusanprogram, poin1.namaurusanprogram, sum(poin1.nilai) total, "
                + "  sum(ptl.pegawaitidaklangsung) pegtdklangsung, sum(p.pegawai) peg, sum(b.barang) brgjasa, sum(m.modal) modal "
                + "  from poin1 "
                + "  left join "
                + "  ( "
                + "  select distinct kodefungsi, namafungsi, kodeurusanprogram, namaurusanprogram, sum(nilaianggaran) pegawaiTidakLangsung "
                + "  from kegiatanapbd k "
                + "  inner join koderekapbd r on k.kegiatanindex=r.kegiatanindex "
                + "  inner join apbd on k.apbdindex=apbd.apbdindex "
                + "  where r.kodeakunutama='5'  and r.kodeakunkelompok='1' and kodeakunjenis='1' and apbd.kodesatker='" + kodeSatker + "' and apbd.tahunanggaran=" + year + " and kodedata=" + kodeData + " and jeniscoa=" + jenisCOA + " "
                + "  group by kodefungsi, namafungsi, kodeurusanprogram, namaurusanprogram order by kodefungsi, kodeurusanprogram) ptl "
                + "  on poin1.kodefungsi=ptl.kodefungsi and poin1.kodeurusanprogram=ptl.kodeurusanprogram "
                + "  left join "
                + "  ( "
                + "  select distinct kodefungsi, namafungsi, kodeurusanprogram, namaurusanprogram, sum(nilaianggaran) pegawai "
                + "  from kegiatanapbd k "
                + "  inner join koderekapbd r on k.kegiatanindex=r.kegiatanindex "
                + "  inner join apbd on k.apbdindex=apbd.apbdindex "
                + "  where r.kodeakunutama='5'  and r.kodeakunkelompok='2' and kodeakunjenis='1' and apbd.kodesatker='" + kodeSatker + "' and apbd.tahunanggaran=" + year + " and kodedata=" + kodeData + " and jeniscoa=" + jenisCOA + " "
                + "  group by kodefungsi, namafungsi, kodeurusanprogram, namaurusanprogram order by kodefungsi, kodeurusanprogram) p "
                + "  on poin1.kodefungsi=p.kodefungsi and poin1.kodeurusanprogram=p.kodeurusanprogram "
                + "  left join "
                + "  ( "
                + "  select distinct kodefungsi, namafungsi, kodeurusanprogram, namaurusanprogram, sum(nilaianggaran) barang "
                + "  from kegiatanapbd k "
                + "  inner join koderekapbd r on k.kegiatanindex=r.kegiatanindex "
                + "  inner join apbd on k.apbdindex=apbd.apbdindex "
                + "  where r.kodeakunutama='5'  and r.kodeakunkelompok='2' and kodeakunjenis='2' and apbd.kodesatker='" + kodeSatker + "' and apbd.tahunanggaran=" + year + " and kodedata=" + kodeData + " and jeniscoa=" + jenisCOA + " "
                + "  group by kodefungsi, namafungsi, kodeurusanprogram, namaurusanprogram order by kodefungsi, kodeurusanprogram) b "
                + "  on poin1.kodefungsi=b.kodefungsi and poin1.kodeurusanprogram=b.kodeurusanprogram "
                + "  left join "
                + "  ( "
                + "  select distinct kodefungsi, namafungsi, kodeurusanprogram, namaurusanprogram, sum(nilaianggaran) modal "
                + "  from kegiatanapbd k "
                + "  inner join koderekapbd r on k.kegiatanindex=r.kegiatanindex\n"
                + "  inner join apbd on k.apbdindex=apbd.apbdindex "
                + "  where r.kodeakunutama='5'  and r.kodeakunkelompok='2' and kodeakunjenis='3' and apbd.kodesatker='" + kodeSatker + "' and apbd.tahunanggaran=" + year + " and kodedata=" + kodeData + " and jeniscoa=" + jenisCOA + " "
                + "  group by kodefungsi, namafungsi, kodeurusanprogram, namaurusanprogram order by kodefungsi, kodeurusanprogram) m "
                + "  on poin1.kodefungsi=m.kodefungsi and poin1.kodeurusanprogram=m.kodeurusanprogram "
                + "  group by poin1.kodefungsi, poin1.namafungsi, poin1.kodeurusanprogram, poin1.namaurusanprogram "
                + "  order by poin1.kodefungsi, poin1.kodeurusanprogram";
        List<LampiranVPerdaAPBD> result = new ArrayList();
        Statement stm = null;
        ResultSet rs = null;
        try {
            stm = conn.createStatement();
            rs = stm.executeQuery(sql);
            String kodeF = "";
            double ptl = 0, b = 0, p = 0, m = 0, la = 0;
            int id = 0;
            while (rs.next()) {
                if (!rs.getString("kodefungsi").trim().equals(kodeF.trim())) {
                    kodeF = rs.getString("kodefungsi").trim();
                    if (result.size() > 0) {
                        result.get(id).setBarangJasa(b);
                        result.get(id).setBelanjaLainnya(la);
                        result.get(id).setModal(m);
                        result.get(id).setPegawai(p);
                        result.get(id).setPegawaiTidakLangsung(ptl);
                        result.add(new LampiranVPerdaAPBD(0, 0, " ", " ", 0, 0, 0));
                    }
                    id = result.size();
                    ptl = rs.getDouble("pegtdklangsung");
                    p = rs.getDouble("peg");
                    b = rs.getDouble("brgjasa");
                    m = rs.getDouble("modal");
                    la = rs.getDouble("total");
                    result.add(new LampiranVPerdaAPBD(rs.getDouble("pegtdklangsung"), rs.getDouble("total"), rs.getString("kodefungsi"), rs.getString("namafungsi"), rs.getDouble("peg"), rs.getDouble("brgjasa"), rs.getDouble("modal")));
                    result.add(new LampiranVPerdaAPBD(rs.getDouble("pegtdklangsung"), rs.getDouble("total"),
                            kodeF + "." + rs.getString("kodeurusanprogram"), rs.getString("namaurusanprogram"),
                            rs.getDouble("peg"), rs.getDouble("brgjasa"), rs.getDouble("modal")));
                } else {
                    ptl += rs.getDouble("pegtdklangsung");
                    p += rs.getDouble("peg");
                    b += rs.getDouble("brgjasa");
                    m += rs.getDouble("modal");
                    la += rs.getDouble("total");
                    result.add(new LampiranVPerdaAPBD(rs.getDouble("pegtdklangsung"),
                            rs.getDouble("total"),
                            kodeF + "." + rs.getString("kodeurusanprogram"), rs.getString("namaurusanprogram"),
                            rs.getDouble("peg"), rs.getDouble("brgjasa"), rs.getDouble("modal")));
                }
            }

            if (result.size() > 0) {
                result.get(id).setBarangJasa(b);
                result.get(id).setBelanjaLainnya(la);
                result.get(id).setModal(m);
                result.get(id).setPegawai(p);
                result.get(id).setPegawaiTidakLangsung(ptl);
            }

            return result;
        } catch (SQLException ex) {
            throw new SQLException("Gagal mengambil Lampiran V Perda APBD " + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }

    @Override
    public List<LampiranILra> getRealisasiAPBD(short year, String kodeSatker, short kodeData, short month, short jenisCOA, Connection conn) throws SQLException {
        List<LampiranILra> result = new ArrayList();
        Statement stm = null;
        ResultSet rs = null;
        String sql = "with utamaapbd as  "
                + " (select rek.kodeakunutama kode,  "
                + " sum(rek.nilaianggaran) jumlah  "
                + " from koderekapbd rek  "
                + " inner join kegiatanapbd keg on rek.kegiatanindex=keg.kegiatanindex  "
                + " inner join apbd on keg.apbdindex=apbd.apbdindex  "
                + " where apbd.tahunanggaran= " + year
                + " and apbd.kodedata= " + kodeData + " and apbd.jeniscoa= " + jenisCOA + " and kodesatker='" + kodeSatker + "' "
                + " group by kode "
                + " order by kode),  "
                + " kelompokapbd as  "
                + " (select (rek.kodeakunutama || '.' || rek.kodeakunkelompok) kode,  "
                + " sum(rek.nilaianggaran) jumlah   "
                + " from koderekapbd rek  "
                + " inner join kegiatanapbd keg on rek.kegiatanindex=keg.kegiatanindex  "
                + " inner join apbd on keg.apbdindex=apbd.apbdindex  "
                + " where apbd.tahunanggaran= " + year
                + " and apbd.kodedata= " + kodeData + " and apbd.jeniscoa= " + jenisCOA + " and kodesatker='" + kodeSatker + "' "
                + " group by kode "
                + " order by kode),  "
                + " jenisapbd as  "
                + " (select (rek.kodeakunutama || '.' || rek.kodeakunkelompok || '.' || rek.kodeakunjenis) kode,  "
                + " sum(rek.nilaianggaran) jumlah  "
                + "  from koderekapbd rek  "
                + "  inner join kegiatanapbd keg on rek.kegiatanindex=keg.kegiatanindex  "
                + "  inner join apbd on keg.apbdindex=apbd.apbdindex  "
                + " where apbd.tahunanggaran= " + year
                + " and apbd.kodedata= " + kodeData + " and apbd.jeniscoa= " + jenisCOA + " and kodesatker='" + kodeSatker + "' "
                + " group by kode "
                + " order by kode) , "
                + " apbdtemp as( "
                + " select * from utamaapbd  "
                + " union select * from kelompokapbd  "
                + " union select * from jenisapbd  "
                + " order by kode ), "
                + " apbd1 as( "
                + " select apbdtemp.*, apbdtemp.kode kodelra, 0 jumlahlra  "
                + " from apbdtemp "
                + " ), "
                + "  "
                + " utama as( "
                + " select rek.kodeakunutama kode,  "
                + " sum(rek.nilaianggaran) jumlah  "
                + " from realisasikoderekapbd rek  "
                + " inner join realisasikegiatanapbd keg on rek.kegiatanindex=keg.kegiatanindex  "
                + " inner join realisasiapbd apbd on keg.apbdindex=apbd.apbdindex  "
                + " where apbd.tahunanggaran= " + year;
        if (month < 13) {
            sql = sql + " and bulan<=" + month;
        } else {
            sql = sql + " and bulan = " + month;
        }
        sql = sql + " and apbd.kodedata= " + kodeData + " and apbd.jeniscoa= " + jenisCOA + " and kodesatker='" + kodeSatker + "' "
                + " group by kode "
                + " order by kode "
                + " ), "
                + " kelompok as  "
                + " (select (rek.kodeakunutama || '.' || rek.kodeakunkelompok) kode,  "
                + " sum(rek.nilaianggaran) jumlah   "
                + " from realisasikoderekapbd rek  "
                + " inner join realisasikegiatanapbd keg on rek.kegiatanindex=keg.kegiatanindex  "
                + " inner join realisasiapbd apbd on keg.apbdindex=apbd.apbdindex  "
                + " where apbd.tahunanggaran= " + year;
        if (month < 13) {
            sql = sql + " and bulan<=" + month;
        } else {
            sql = sql + " and bulan = " + month;
        }
        sql = sql + " and apbd.kodedata= " + kodeData + " and apbd.jeniscoa= " + jenisCOA + " and kodesatker='" + kodeSatker + "' "
                + " group by kode "
                + " order by kode),  "
                + " jenis as  "
                + " (select (rek.kodeakunutama || '.' || rek.kodeakunkelompok || '.' || rek.kodeakunjenis) kode,  "
                + " sum(rek.nilaianggaran) jumlah  "
                + "  from realisasikoderekapbd rek  "
                + "  inner join realisasikegiatanapbd keg on rek.kegiatanindex=keg.kegiatanindex  "
                + "  inner join realisasiapbd apbd on keg.apbdindex=apbd.apbdindex  "
                + " where apbd.tahunanggaran= " + year;
        if (month < 13) {
            sql = sql + " and bulan<=" + month;
        } else {
            sql = sql + " and bulan = " + month;
        }
        sql = sql + " and apbd.kodedata= " + kodeData + " and apbd.jeniscoa= " + jenisCOA + " and kodesatker='" + kodeSatker + "' "
                + " group by kode "
                + " order by kode), "
                + " jumlahkode as(  "
                + " select * from utama  "
                + " union select * from kelompok  "
                + " union select * from jenis  "
                + " order by kode ), "
                + " lra1 as( "
                + " select jk.kode as kode, 0 jumlah, jk.kode kodelra, jk.jumlah jumlahlra "
                + " from jumlahkode jk "
                + " ), "
                + " apbdlra as( "
                + " select * from apbd1 "
                + " union select * from lra1 "
                + " order by kode, kodelra "
                + " ), "
                + " gabung as( "
                + " select kode, sum(jumlah) nilai, sum(jumlahlra) nilailra from apbdlra "
                + " group by kode "
                + " order by kode), "
                + " result as( "
                + " select kode, "
                + " case "
                + "  when length(trim(kode))=1 then "
                + "   (select upper(namaakunutama) "
                + "   from koderekapbd rek "
                + "   inner join kegiatanapbd keg on rek.kegiatanindex=keg.kegiatanindex "
                + "   inner join apbd on keg.apbdindex=apbd.apbdindex "
                + " where apbd.tahunanggaran= " + year
                + " and apbd.kodedata= " + kodeData + " and apbd.jeniscoa= " + jenisCOA + " and kodesatker='" + kodeSatker + "' "
                + "  and kodeakunutama= kode limit 1) "
                + " when length(trim(kode))=3 then "
                + "   (select upper(namaakunkelompok) "
                + "   from koderekapbd rek "
                + "   inner join kegiatanapbd keg on rek.kegiatanindex=keg.kegiatanindex "
                + "   inner join apbd on keg.apbdindex=apbd.apbdindex "
                + " where apbd.tahunanggaran= " + year
                + " and apbd.kodedata= " + kodeData + " and apbd.jeniscoa= " + jenisCOA + " and kodesatker='" + kodeSatker + "' "
                + " and (kodeakunutama||'.'||kodeakunkelompok)=kode limit 1) "
                + " when length(trim(kode))=5 then "
                + "   (select namaakunjenis "
                + "   from koderekapbd rek  "
                + "   inner join kegiatanapbd keg on rek.kegiatanindex=keg.kegiatanindex "
                + "   inner join apbd on keg.apbdindex=apbd.apbdindex "
                + " where apbd.tahunanggaran= " + year
                + " and apbd.kodedata= " + kodeData + " and apbd.jeniscoa= " + jenisCOA + " and kodesatker='" + kodeSatker + "' "
                + " and (kodeakunutama||'.'||kodeakunkelompok||'.'||kodeakunjenis)=kode limit 1) "
                + " else '' "
                + " end as nama, nilai, nilailra "
                + " from gabung) "
                + " select r.kode, "
                + " case "
                + "  when r.nama is null then "
                + "    case "
                + "      when length(trim(kode))=1 then "
                + "      (select upper(namaakunutama) "
                + "      from realisasikoderekapbd rek "
                + "      inner join realisasikegiatanapbd keg on rek.kegiatanindex=keg.kegiatanindex "
                + "      inner join realisasiapbd apbd on keg.apbdindex=apbd.apbdindex "
                + " where apbd.tahunanggaran= " + year;
        if (month < 13) {
            sql = sql + " and bulan<=" + month;
        } else {
            sql = sql + " and bulan = " + month;
        }
        sql = sql + " and apbd.kodedata= " + kodeData + " and apbd.jeniscoa= " + jenisCOA + " and kodesatker='" + kodeSatker + "' "
                + "      and kodeakunutama= kode limit 1) "
                + "      when length(trim(kode))=3 then "
                + "      (select upper(namaakunkelompok) "
                + "      from realisasikoderekapbd rek "
                + "      inner join realisasikegiatanapbd keg on rek.kegiatanindex=keg.kegiatanindex "
                + "      inner join realisasiapbd apbd on keg.apbdindex=apbd.apbdindex "
                + " where apbd.tahunanggaran= " + year;
        if (month < 13) {
            sql = sql + " and bulan<=" + month;
        } else {
            sql = sql + " and bulan = " + month;
        }
        sql = sql + " and apbd.kodedata= " + kodeData + " and apbd.jeniscoa= " + jenisCOA + " and kodesatker='" + kodeSatker + "' "
                + " and (kodeakunutama||'.'||kodeakunkelompok)=kode limit 1) "
                + " when length(trim(kode))=5 then "
                + "   (select namaakunjenis "
                + "   from realisasikoderekapbd rek "
                + "   inner join realisasikegiatanapbd keg on rek.kegiatanindex=keg.kegiatanindex "
                + "   inner join realisasiapbd apbd on keg.apbdindex=apbd.apbdindex "
                + " where apbd.tahunanggaran= " + year;
        if (month < 13) {
            sql = sql + " and bulan<=" + month;
        } else {
            sql = sql + " and bulan = " + month;
        }
        sql = sql + " and apbd.kodedata= " + kodeData + " and apbd.jeniscoa= " + jenisCOA + " and kodesatker='" + kodeSatker + "' "
                + " and (kodeakunutama||'.'||kodeakunkelompok||'.'||kodeakunjenis)=kode limit 1) "
                + " else ''   "
                + " end  "
                + "  else r.nama "
                + "  end as nama, "
                + " r.nilai, r.nilailra from result r";
//        System.out.println(sql);
        
/*        String sql = "select rek.kodeakunutama, rek.namaakunutama, "
                    + "rek.kodeakunkelompok, rek.namaakunkelompok, "
                    + "rek.kodeakunjenis, rek.namaakunjenis, "
                    + "sum(rek.nilaianggaran) jumlah from realisasikoderekapbd rek "
                    + "inner join realisasikegiatanapbd keg on rek.kegiatanindex=keg.kegiatanindex "
                    + "inner join realisasiapbd apbd on keg.apbdindex=apbd.apbdindex "
                    + "where apbd.tahunanggaran= " + year + " ";
                    
        
        if(!kodeSatker.trim().equals("000000"))
            sql = sql + "and apbd.kodesatker = '" + kodeSatker + "' ";
        sql = sql + " and apbd.kodedata= " + kodeData + " and jeniscoa=" + jenisCOA;
        if( month <= 12 ) sql = sql + " and apbd.bulan <= " + month + " ";
        else if(month == 13 ) sql = sql + " and apbd.bulan = " + month + " ";
        
        sql = sql +" group by rek.kodeakunutama, rek.namaakunutama, "
                    + " rek.kodeakunkelompok, rek.namaakunkelompok, "
                    + " rek.kodeakunjenis, rek.namaakunjenis "
                    + " order by rek.kodeakunutama, rek.namaakunutama, "
                    + " rek.kodeakunkelompok, rek.namaakunkelompok, "
                    + " rek.kodeakunjenis, rek.namaakunjenis";
        */
        try {
            stm = conn.createStatement();
            rs = stm.executeQuery(sql);
            while(rs.next()){
                result.add(new LampiranILra(rs.getString("kode"), rs.getString("nama"), rs.getDouble("nilai"), rs.getDouble("nilailra")));
                if(result.get(result.size()-1).getKodeAkun().trim().length()==1 || result.get(result.size()-1).getKodeAkun().trim().length()==3)
                    result.get(result.size()-1).setFontStyle("font-weight: bold; ");
            }
          /*  String utama = "", kelompok = "", jenis;
            double jumlahKelompok = 0;
            List<String> indexKelompok = new ArrayList();
            while (rs.next()) {
                if (utama.trim().equals("")) {
                    utama = rs.getString("kodeakunutama").trim();
                    kelompok = utama.trim() + "." + rs.getString("kodeakunkelompok").trim();
                    jenis = kelompok.trim() + "." + rs.getString("kodeakunjenis").trim();
                    double jumlah = jumlahKelompok = rs.getDouble("jumlah");
                    result.add(new LampiranIPerdaAPBD(utama, rs.getString("namaakunutama"), 0));
                    result.add(new LampiranIPerdaAPBD(kelompok, rs.getString("namaakunkelompok"), jumlah));
                    indexKelompok.add(String.valueOf(result.size() - 1));
                    result.add(new LampiranIPerdaAPBD(jenis, rs.getString("namaakunjenis"), jumlah));
                } else {
                    if (!utama.trim().equals(rs.getString("kodeakunutama").trim())) {
                        utama = rs.getString("kodeakunutama").trim();
                        kelompok = utama.trim() + "." + rs.getString("kodeakunkelompok").trim();
                        jenis = kelompok.trim() + "." + rs.getString("kodeakunjenis").trim();
                        double jumlah = jumlahKelompok = rs.getDouble("jumlah");
                        result.add(new LampiranIPerdaAPBD(utama, rs.getString("namaakunutama"), 0));
                        result.add(new LampiranIPerdaAPBD(kelompok, rs.getString("namaakunkelompok"), jumlahKelompok));
                        indexKelompok.add(String.valueOf(result.size() - 1));
                        result.add(new LampiranIPerdaAPBD(jenis, rs.getString("namaakunjenis"), jumlah));
                    } else {
                        if (kelompok.trim().equals(utama.trim() + "." + rs.getString("kodeakunkelompok").trim())) {
                            jenis = kelompok.trim() + "." + rs.getString("kodeakunjenis");
                            double jumlah = rs.getDouble("jumlah");
                            jumlahKelompok = jumlahKelompok + jumlah;
                            result.add(new LampiranIPerdaAPBD(jenis, rs.getString("namaakunjenis"), jumlah));
                            int ii = 0;
                            boolean find = false;
                            while (ii < indexKelompok.size() && find == false) {
                                if (result.get(Integer.parseInt(indexKelompok.get(ii).trim())).getKode().trim().equals(kelompok.trim())) {
                                    result.get(Integer.parseInt(indexKelompok.get(ii).trim())).setJumlah(jumlahKelompok);
                                    find = true;
                                }
                                ii++;
                            }
                        } else {
                            //Kelompok Beda
                            kelompok = utama.trim() + "." + rs.getString("kodeakunkelompok").trim();
                            jenis = kelompok.trim() + "." + rs.getString("kodeakunjenis").trim();
                            double jumlah = jumlahKelompok = rs.getDouble("jumlah");
                            result.add(new LampiranIPerdaAPBD(kelompok, rs.getString("namaakunkelompok"), jumlahKelompok));
                            indexKelompok.add(String.valueOf(result.size() - 1));
                            result.add(new LampiranIPerdaAPBD(jenis, rs.getString("namaakunjenis"), jumlah));
                        }
                    }
                }

            }
            */
            return result;
        } catch (SQLException ex) {
            Logger.getLogger(OfficeSQL.class.getName()).log(Level.SEVERE, null, ex);
            throw new SQLException("SQL: Gagal mengambil data Realisasi apbd \n" + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }

    @Override
    public List<LampiranILra> getRealisasiAPBD(short year, String kodeSatker, short month, short jenisCOA, Connection conn) throws SQLException {
        List<LampiranILra> result = new ArrayList();
        Statement stm = null;
        ResultSet rs = null;
        
        String sql = "with lraid as( "
                + " select apbdindex from realisasiapbd where tahunanggaran=" + year + " and kodesatker='" + kodeSatker + "' and jeniscoa=" + jenisCOA ;
        if(month < 13 ) sql = sql + " and bulan <= " + month;
        else sql = sql + " and bulan = " + month;
        sql = sql + " ), "
                + " lrakegid as( "
                + "  select kegiatanindex from realisasikegiatanapbd where apbdindex in(select * from lraid) "
                + " ), "
                + " kdata as( "
                + "  select kodedata, bulan from realisasiapbd where tahunanggaran=" + year + " and kodesatker='" + kodeSatker + "' and jeniscoa=" + jenisCOA + " and bulan<= " + month
                + "  order by bulan desc "
                + "  limit 1 "
                + " ),  "
                + " lra as(  "
                + "  SELECT distinct kodeakunutama kode, namaakunutama nama, 0::double precision nilaiap, sum(nilaianggaran) nilailra "
                + "  FROM realisasikoderekapbd where kegiatanindex in(select * from lrakegid) "
                + "  group by kode, nama "
                + "  union "
                + "  SELECT (kodeakunutama||'.'||kodeakunkelompok) kode, namaakunkelompok nama, 0::double precision nilaiap, sum(nilaianggaran) nilailra  "
                + "  FROM realisasikoderekapbd where kegiatanindex in(select * from lrakegid)  "
                + "  group by kode, nama    "
                + "	union  "
                + "  SELECT (kodeakunutama||'.'||kodeakunkelompok||'.'||kodeakunjenis) kode, namaakunjenis nama, 0::double precision nilaiap, sum(nilaianggaran) nilailra  "
                + "  FROM realisasikoderekapbd where kegiatanindex in(select * from lrakegid)  "
                + "  group by kode, nama    "
                + "  order by kode  "
                + " ),  "
                + " apid as(  "
                + "  select apbdindex from apbd where tahunanggaran=" + year + " and kodesatker='" + kodeSatker + "' and jeniscoa=" + jenisCOA + " and kodedata=(select kodedata from kdata)  "
                + " ),  "
                + " apkegid as(  "
                + "  select kegiatanindex from kegiatanapbd where apbdindex in(select * from apid)  "
                + " ),  "
                + " ap as(  "
                + "  SELECT distinct kodeakunutama kode, namaakunutama nama, sum(nilaianggaran) nilaiap, 0::double precision nilailra  "
                + "  FROM koderekapbd where kegiatanindex in(select * from apkegid)    "
                + "  group by kode, nama    "
                + "  union  "
                + "  SELECT (kodeakunutama||'.'||kodeakunkelompok) kode, namaakunkelompok nama, sum(nilaianggaran) nilaiap, 0::double precision nilailra  "
                + "  FROM koderekapbd where kegiatanindex in(select * from apkegid)  "
                + "  group by kode, nama    "
                + "	union  "
                + "  SELECT (kodeakunutama||'.'||kodeakunkelompok||'.'||kodeakunjenis) kode, namaakunjenis nama, sum(nilaianggaran) nilaiap, 0::double precision nilailra  "
                + "  FROM koderekapbd where kegiatanindex in(select * from apkegid)  "
                + "  group by kode, nama    "
                + "  order by kode  "
                + " ),  "
                + " apbdlra as(  "
                + "  select * from ap  "
                + "  union select * from lra  "
                + " )  "
                + " select kode, nama, sum(nilaiap) nilaiap, sum(nilailra) nilailra "
                + " from apbdlra   "
                + " group by kode, nama  "
                + " order by kode  ";
        try {
            System.out.println("Mulai ambil lra sql");
            stm = conn.createStatement();
            rs = stm.executeQuery(sql);
            int id6=-1, id7=-1;
            double j6a=0, j6lra=0, j7a=0, j7lra=0;
            
            
            while (rs.next()){
                String kode = rs.getString("kode");                
                result.add(new LampiranILra(kode, rs.getString("nama"), rs.getDouble("nilaiap"), 
                        rs.getDouble("nilailra")));
                if(result.get(result.size()-1).getKodeAkun().trim().length()==1){
                    result.get(result.size()-1).setFontStyle("font-weight: bold; ");
                    result.add(result.size()-1, new LampiranILra("", "", 0, 0));
                    if(result.get(result.size() - 1).getKodeAkun().trim().equals("6")) id6=result.size()-1;
                    if(result.get(result.size() - 1).getKodeAkun().trim().equals("7")) id7=result.size()-1;
                }
                if(result.get(result.size()-1).getKodeAkun().trim().length()==3){
                    result.get(result.size()-1).setFontStyle("font-weight: bold; ");
                    if(jenisCOA==1){
                        if( result.get(result.size()-1).getKodeAkun().trim().equals("6.1")) {
                            j6a+=result.get(result.size()-1).getNilaiAkunApbd();
                            j6lra+=result.get(result.size()-1).getNilaiAkunLra();
                        }
                        else if( result.get(result.size()-1).getKodeAkun().trim().equals("6.2")) {
                            j6a=j6a-result.get(result.size()-1).getNilaiAkunApbd();
                            j6lra=j6lra-result.get(result.size()-1).getNilaiAkunLra();
                        }
                        else if( result.get(result.size()-1).getKodeAkun().trim().equals("6.3")) {
                            j6a=j6a+result.get(result.size()-1).getNilaiAkunApbd();
                            j6lra=j6lra+result.get(result.size()-1).getNilaiAkunLra();
                        }                        
                    }
                    if(jenisCOA==2){
                        if( result.get(result.size()-1).getKodeAkun().trim().equals("7.1")) {
                            j7a+=result.get(result.size()-1).getNilaiAkunApbd();
                            j7lra+=result.get(result.size()-1).getNilaiAkunLra();
                        }
                        else if( result.get(result.size()-1).getKodeAkun().trim().equals("7.2")) {
                            j7a=j7a-result.get(result.size()-1).getNilaiAkunApbd();
                            j7lra=j7lra-result.get(result.size()-1).getNilaiAkunLra();
                        }
                    }
                    
                }
                
                
            }
            if(!result.isEmpty()) {
                if (jenisCOA == 1 && id6>-1) {
                    result.get(id6).setNilaiAkunApbd(j6a);
                    result.get(id6).setNilaiAkunLra(j6lra);
                }
                else if (jenisCOA == 2 && id7>-1) {
                    result.get(id7).setNilaiAkunApbd(j7a);
                    result.get(id7).setNilaiAkunLra(j7lra);
                }                
            }
            System.out.println("selesai ambil lra sql");
            return result;
        } catch (SQLException ex) {
            Logger.getLogger(OfficeSQL.class.getName()).log(Level.SEVERE, null, ex);
            throw new SQLException("SQL: Gagal mengambil data Realisasi apbd \n" + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }

    @Override
    public List<LampiranIPerdaAPBD> getKompilasiRealisasiAPBD(short year, List<String> kodePemdas, short kodeData, short month, short jenisCOA, Connection conn) throws SQLException {
        List<LampiranIPerdaAPBD> result = new ArrayList();
        Statement stm = null;
        ResultSet rs = null;
        String sql = "select rek.kodeakunutama, rek.namaakunutama, "
                    + "rek.kodeakunkelompok, rek.namaakunkelompok, "
                    + "rek.kodeakunjenis, rek.namaakunjenis, "
                    + "sum(rek.nilaianggaran) jumlah from realisasikoderekapbd rek "
                    + "inner join realisasikegiatanapbd keg on rek.kegiatanindex=keg.kegiatanindex "
                    + "inner join realisasiapbd apbd on keg.apbdindex=apbd.apbdindex "
                    + "where apbd.tahunanggaran= " + year + " ";
        String pemdas = "";
        if(kodePemdas!=null && !kodePemdas.isEmpty()){
            for (String kodePemda : kodePemdas) {
                if( !pemdas.trim().equals("") ) pemdas = pemdas + " or ";
                pemdas = pemdas + " kodepemda='" + kodePemda + "'";
            }
        }
        if(!pemdas.trim().equals("")) {
            pemdas = "(" + pemdas + ")";
            sql = sql + " and " + pemdas;
        }
        sql = sql + " and apbd.kodedata= " + kodeData + " and jeniscoa=" + jenisCOA + " ";
        if( month <= 12 ) sql = sql + " and apbd.bulan <= " + month + " ";
        else if( month == 13 ) sql = sql + " and apbd.bulan = " + month + " ";
        
        sql =sql +"group by rek.kodeakunutama, rek.namaakunutama, "
                    + "rek.kodeakunkelompok, rek.namaakunkelompok, "
                    + "rek.kodeakunjenis, rek.namaakunjenis "
                    + "order by rek.kodeakunutama, rek.namaakunutama, "
                    + "rek.kodeakunkelompok, rek.namaakunkelompok, "
                    + "rek.kodeakunjenis, rek.namaakunjenis";    

        try {
            stm = conn.createStatement();
            rs = stm.executeQuery(sql);
            String utama = "", kelompok = "", jenis;
            double jumlahKelompok = 0;
            List<String> indexKelompok = new ArrayList();
            while (rs.next()) {
                if (utama.trim().equals("")) {
                    utama = rs.getString("kodeakunutama").trim();
                    kelompok = utama.trim() + "." + rs.getString("kodeakunkelompok").trim();
                    jenis = kelompok.trim() + "." + rs.getString("kodeakunjenis").trim();
                    double jumlah = jumlahKelompok = rs.getDouble("jumlah");
                    result.add(new LampiranIPerdaAPBD(utama, rs.getString("namaakunutama"), 0));
                    result.add(new LampiranIPerdaAPBD(kelompok, rs.getString("namaakunkelompok"), jumlah));
                    indexKelompok.add(String.valueOf(result.size() - 1));
                    result.add(new LampiranIPerdaAPBD(jenis, rs.getString("namaakunjenis"), jumlah));
                } else {
                    if (!utama.trim().equals(rs.getString("kodeakunutama").trim())) {
                        utama = rs.getString("kodeakunutama").trim();
                        kelompok = utama.trim() + "." + rs.getString("kodeakunkelompok").trim();
                        jenis = kelompok.trim() + "." + rs.getString("kodeakunjenis").trim();
                        double jumlah = jumlahKelompok = rs.getDouble("jumlah");
                        result.add(new LampiranIPerdaAPBD(utama, rs.getString("namaakunutama"), 0));
                        result.add(new LampiranIPerdaAPBD(kelompok, rs.getString("namaakunkelompok"), jumlahKelompok));
                        indexKelompok.add(String.valueOf(result.size() - 1));
                        result.add(new LampiranIPerdaAPBD(jenis, rs.getString("namaakunjenis"), jumlah));
                    } else {
                        if (kelompok.trim().equals(utama.trim() + "." + rs.getString("kodeakunkelompok").trim())) {
                            jenis = kelompok.trim() + "." + rs.getString("kodeakunjenis");
                            double jumlah = rs.getDouble("jumlah");
                            jumlahKelompok = jumlahKelompok + jumlah;
                            result.add(new LampiranIPerdaAPBD(jenis, rs.getString("namaakunjenis"), jumlah));
                            int ii = 0;
                            boolean find = false;
                            while (ii < indexKelompok.size() && find == false) {
                                if (result.get(Integer.parseInt(indexKelompok.get(ii).trim())).getKode().trim().equals(kelompok.trim())) {
                                    result.get(Integer.parseInt(indexKelompok.get(ii).trim())).setJumlah(jumlahKelompok);
                                    find = true;
                                }
                                ii++;
                            }
                        } else {
                            //Kelompok Beda
                            kelompok = utama.trim() + "." + rs.getString("kodeakunkelompok").trim();
                            jenis = kelompok.trim() + "." + rs.getString("kodeakunjenis").trim();
                            double jumlah = jumlahKelompok = rs.getDouble("jumlah");
                            result.add(new LampiranIPerdaAPBD(kelompok, rs.getString("namaakunkelompok"), jumlahKelompok));
                            indexKelompok.add(String.valueOf(result.size() - 1));
                            result.add(new LampiranIPerdaAPBD(jenis, rs.getString("namaakunjenis"), jumlah));
                        }
                    }
                }

            }

            return result;
        } catch (SQLException ex) {
            Logger.getLogger(OfficeSQL.class.getName()).log(Level.SEVERE, null, ex);
            throw new SQLException("SQL: Gagal mengambil data Realisasi apbd \n" + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }

    @Override
    public List<LampiranIPerdaAPBD> getRingkasanPembiayaan(short year, String kodeSatker, short kodeData, short jenisCOA, Connection conn) throws SQLException {
        List<LampiranIPerdaAPBD> result = new ArrayList();
        Statement stm = null;
        ResultSet rs = null;
        String sql ="select rek.kodeakunutama, rek.namaakunutama, "
                    + "rek.kodeakunkelompok, rek.namaakunkelompok, "
                    + "rek.kodeakunjenis, rek.namaakunjenis, "
                    + "sum(rek.nilaianggaran) jumlah from koderekapbd rek "
                    + "inner join kegiatanapbd keg on rek.kegiatanindex=keg.kegiatanindex "
                    + "inner join apbd on keg.apbdindex=apbd.apbdindex "
                    + "where apbd.tahunanggaran= " + year + " ";
        if( !kodeSatker.trim().equals("000000") )
            sql = sql + " and apbd.kodesatker = '" + kodeSatker + "' ";
        sql = sql + " and apbd.kodedata= " + kodeData + " and jeniscoa=" + jenisCOA + " "
                    + " and rek.kodeakunutama='6' "
                    + "group by rek.kodeakunutama, rek.namaakunutama, "
                    + "rek.kodeakunkelompok, rek.namaakunkelompok, "
                    + "rek.kodeakunjenis, rek.namaakunjenis "
                    + "order by rek.kodeakunutama, rek.namaakunutama, "
                    + "rek.kodeakunkelompok, rek.namaakunkelompok, "
                    + "rek.kodeakunjenis, rek.namaakunjenis";
    
        try {
            stm = conn.createStatement();
            rs = stm.executeQuery(sql);
            String utama = "", kelompok = "", jenis;
            double jumlahKelompok = 0;
            List<String> indexKelompok = new ArrayList();
            while (rs.next()) {
                if (utama.trim().equals("")) {
                    utama = rs.getString("kodeakunutama").trim();
                    kelompok = utama.trim() + "." + rs.getString("kodeakunkelompok").trim();
                    jenis = kelompok.trim() + "." + rs.getString("kodeakunjenis").trim();
                    double jumlah = jumlahKelompok = rs.getDouble("jumlah");
                    result.add(new LampiranIPerdaAPBD(utama, rs.getString("namaakunutama"), 0));
                    result.add(new LampiranIPerdaAPBD(kelompok, rs.getString("namaakunkelompok"), jumlah));
                    indexKelompok.add(String.valueOf(result.size() - 1));
                    result.add(new LampiranIPerdaAPBD(jenis, rs.getString("namaakunjenis"), jumlah));
                } else {
                    if (!utama.trim().equals(rs.getString("kodeakunutama").trim())) {
                        utama = rs.getString("kodeakunutama").trim();
                        kelompok = utama.trim() + "." + rs.getString("kodeakunkelompok").trim();
                        jenis = kelompok.trim() + "." + rs.getString("kodeakunjenis").trim();
                        double jumlah = jumlahKelompok = rs.getDouble("jumlah");
                        result.add(new LampiranIPerdaAPBD(utama, rs.getString("namaakunutama"), 0));
                        result.add(new LampiranIPerdaAPBD(kelompok, rs.getString("namaakunkelompok"), jumlahKelompok));
                        indexKelompok.add(String.valueOf(result.size() - 1));
                        result.add(new LampiranIPerdaAPBD(jenis, rs.getString("namaakunjenis"), jumlah));
                    } else {
                        if (kelompok.trim().equals(utama.trim() + "." + rs.getString("kodeakunkelompok").trim())) {
                            jenis = kelompok.trim() + "." + rs.getString("kodeakunjenis");
                            double jumlah = rs.getDouble("jumlah");
                            jumlahKelompok = jumlahKelompok + jumlah;
                            result.add(new LampiranIPerdaAPBD(jenis, rs.getString("namaakunjenis"), jumlah));
                            int ii = 0;
                            boolean find = false;
                            while (ii < indexKelompok.size() && find == false) {
                                if (result.get(Integer.parseInt(indexKelompok.get(ii).trim())).getKode().trim().equals(kelompok.trim())) {
                                    result.get(Integer.parseInt(indexKelompok.get(ii).trim())).setJumlah(jumlahKelompok);
                                    find = true;
                                }
                                ii++;
                            }
                        } else {
                            //Kelompok Beda
                            kelompok = utama.trim() + "." + rs.getString("kodeakunkelompok").trim();
                            jenis = kelompok.trim() + "." + rs.getString("kodeakunjenis").trim();
                            double jumlah = jumlahKelompok = rs.getDouble("jumlah");
                            result.add(new LampiranIPerdaAPBD(kelompok, rs.getString("namaakunkelompok"), jumlahKelompok));
                            indexKelompok.add(String.valueOf(result.size() - 1));
                            result.add(new LampiranIPerdaAPBD(jenis, rs.getString("namaakunjenis"), jumlah));
                        }
                    }
                }

            }

            return result;
        } catch (SQLException ex) {
            Logger.getLogger(OfficeSQL.class.getName()).log(Level.SEVERE, null, ex);
            throw new SQLException("SQL: Gagal mengambil data Ringkasan Pembiayaan \n" + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    @Override
    public List<LampiranIPerdaAPBD> getRingkasanPembiayaan(short year, List<String> kodepemdas, short kodeData, short jenisCOA, Connection conn) throws SQLException {
        List<LampiranIPerdaAPBD> result = new ArrayList();
        Statement stm = null;
        ResultSet rs = null;
        String sql = "select rek.kodeakunutama, rek.namaakunutama, "
                    + "rek.kodeakunkelompok, rek.namaakunkelompok, "
                    + "rek.kodeakunjenis, rek.namaakunjenis, "
                    + "sum(rek.nilaianggaran) jumlah from koderekapbd rek "
                    + "inner join kegiatanapbd keg on rek.kegiatanindex=keg.kegiatanindex "
                    + "inner join apbd on keg.apbdindex=apbd.apbdindex "
                    + "where apbd.tahunanggaran= " + year + " ";
        String qpemdas = "";
        if(kodepemdas!=null&&kodepemdas.size()>0){            
            for (String kodepemda : kodepemdas) {
                if( !qpemdas.trim().equals("") ) qpemdas = qpemdas + " or ";
                qpemdas = qpemdas + "apbd.kodepemda = '" + kodepemda + "'";
            }
        }
        if( !qpemdas.trim().equals("") ) sql = sql + " and (" + qpemdas + ") ";
                    sql = sql + "and apbd.kodedata= " + kodeData + " and jeniscoa=" + jenisCOA + " "
                    + " and rek.kodeakunutama='6' "
                    + "group by rek.kodeakunutama, rek.namaakunutama, "
                    + "rek.kodeakunkelompok, rek.namaakunkelompok, "
                    + "rek.kodeakunjenis, rek.namaakunjenis "
                    + "order by rek.kodeakunutama, rek.namaakunutama, "
                    + "rek.kodeakunkelompok, rek.namaakunkelompok, "
                    + "rek.kodeakunjenis, rek.namaakunjenis";
        try {
            stm = conn.createStatement();
            rs = stm.executeQuery(sql);
            String utama = "", kelompok = "", jenis;
            double jumlahKelompok = 0;
            List<String> indexKelompok = new ArrayList();
            while (rs.next()) {
                if (utama.trim().equals("")) {
                    utama = rs.getString("kodeakunutama").trim();
                    kelompok = utama.trim() + "." + rs.getString("kodeakunkelompok").trim();
                    jenis = kelompok.trim() + "." + rs.getString("kodeakunjenis").trim();
                    double jumlah = jumlahKelompok = rs.getDouble("jumlah");
                    result.add(new LampiranIPerdaAPBD(utama, rs.getString("namaakunutama"), 0));
                    result.add(new LampiranIPerdaAPBD(kelompok, rs.getString("namaakunkelompok"), jumlah));
                    indexKelompok.add(String.valueOf(result.size() - 1));
                    result.add(new LampiranIPerdaAPBD(jenis, rs.getString("namaakunjenis"), jumlah));
                } else {
                    if (!utama.trim().equals(rs.getString("kodeakunutama").trim())) {
                        utama = rs.getString("kodeakunutama").trim();
                        kelompok = utama.trim() + "." + rs.getString("kodeakunkelompok").trim();
                        jenis = kelompok.trim() + "." + rs.getString("kodeakunjenis").trim();
                        double jumlah = jumlahKelompok = rs.getDouble("jumlah");
                        result.add(new LampiranIPerdaAPBD(utama, rs.getString("namaakunutama"), 0));
                        result.add(new LampiranIPerdaAPBD(kelompok, rs.getString("namaakunkelompok"), jumlahKelompok));
                        indexKelompok.add(String.valueOf(result.size() - 1));
                        result.add(new LampiranIPerdaAPBD(jenis, rs.getString("namaakunjenis"), jumlah));
                    } else {
                        if (kelompok.trim().equals(utama.trim() + "." + rs.getString("kodeakunkelompok").trim())) {
                            jenis = kelompok.trim() + "." + rs.getString("kodeakunjenis");
                            double jumlah = rs.getDouble("jumlah");
                            jumlahKelompok = jumlahKelompok + jumlah;
                            result.add(new LampiranIPerdaAPBD(jenis, rs.getString("namaakunjenis"), jumlah));
                            int ii = 0;
                            boolean find = false;
                            while (ii < indexKelompok.size() && find == false) {
                                if (result.get(Integer.parseInt(indexKelompok.get(ii).trim())).getKode().trim().equals(kelompok.trim())) {
                                    result.get(Integer.parseInt(indexKelompok.get(ii).trim())).setJumlah(jumlahKelompok);
                                    find = true;
                                }
                                ii++;
                            }
                        } else {
                            //Kelompok Beda
                            kelompok = utama.trim() + "." + rs.getString("kodeakunkelompok").trim();
                            jenis = kelompok.trim() + "." + rs.getString("kodeakunjenis").trim();
                            double jumlah = jumlahKelompok = rs.getDouble("jumlah");
                            result.add(new LampiranIPerdaAPBD(kelompok, rs.getString("namaakunkelompok"), jumlahKelompok));
                            indexKelompok.add(String.valueOf(result.size() - 1));
                            result.add(new LampiranIPerdaAPBD(jenis, rs.getString("namaakunjenis"), jumlah));
                        }
                    }
                }

            }

            return result;
        } catch (SQLException ex) {
            Logger.getLogger(OfficeSQL.class.getName()).log(Level.SEVERE, null, ex);
            throw new SQLException("SQL: Gagal mengambil data Ringkasan Pembiayaan \n" + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }

    @Override
    public DTH getDTHReport(short year, String kodeSatker, short periode, String kodeSKPD, Connection conn) throws SQLException {
        DTH result = new DTH();
        ResultSet rs = null;
        Statement stm = null;
        try {
            stm = conn.createStatement();
            String urusan = "", skpd ="";
            if(kodeSKPD!=null && !kodeSKPD.trim().equals("")){
                StringTokenizer tok = new StringTokenizer(kodeSKPD, ".");
                if( tok.countTokens() == 3 ){
                    urusan = tok.nextToken();
                    urusan = urusan + "." + tok.nextToken();
                    skpd=tok.nextToken();
                }
            }
            String sql = "with sp2d as "
                    + "(SELECT distinct r.rinciandthindex, spmnumber, sp2dnumber, " 
                    + "nilaisp2d, npwpbud, npwpskpd, npwppenerima, namapenerima, " 
                    + "sumberdana, subsumberdana, keterangan from rinciandthskpd r " 
                    + "inner join dth d on r.dthindex=d.dthindex " 
                    + "inner join sp2dkegiatan s on r.rinciandthindex=s.rinciandthindex "
                    + "where kodesatker='" + kodeSatker.trim() + "' and tahunanggaran= " + year + "  "
                    + "and periode=" + periode + " and kodeurusanpelaksana='" + urusan + "' and kodeskpd='" 
                    + skpd + "'), "
                    + "akun as( " 
                    + "select  sp2d.rinciandthindex, (kodeakunutama || '.' || kodeakunkelompok ||'.'|| kodeakunjenis ||'.'|| kodeakunobjek || '.' || kodeakunrincian || ' ' || namaakunrincian) akunbelanja, sum(nilairekening) nilai from akundthskpd a " 
                    + "inner join sp2dkegiatan k on a.kegiatanindex=k.kegiatanindex " 
                    + "inner join sp2d on sp2d.rinciandthindex=k.rinciandthindex " 
                    + "group by sp2d.rinciandthindex, akunbelanja order by akunbelanja " 
                    + "), " 
                    + "pajak as( " 
                    + "select rinciandthskpd, kodeakunpajak, namaakunpajak, jenispajak, sum(nilaipotongan) nilPajak from pajakdthskpd p " 
                    + "inner join sp2d on sp2d.rinciandthindex=p.rinciandthskpd " 
                    + "where jenispajak>0 and jenispajak<6 " 
                    + "group by rinciandthskpd, kodeakunpajak, namaakunpajak, jenispajak " 
                    + "), " 
                    + "potongan as( " 
                    + "select rinciandthskpd, kodeakunpajak kodeakunpotongan, namaakunpajak namaakunpotongan, jenispajak jenispotongan, sum(nilaipotongan) nilPotongan from pajakdthskpd p " 
                    + "inner join sp2d on sp2d.rinciandthindex=p.rinciandthskpd " 
                    + "where jenispajak>1000 and jenispajak<1006 " 
                    + "group by rinciandthskpd, kodeakunpajak, namaakunpajak, jenispajak " 
                    + ") " 
                    + "select sp2d.*, akun.akunbelanja, akun.nilai, " 
                    + "pajak.kodeakunpajak, pajak.namaakunpajak, pajak.jenispajak, pajak.nilpajak nilpajak, " 
                    + "potongan.kodeakunpotongan, potongan.namaakunpotongan, potongan.jenispotongan, potongan.nilPotongan nilpotongan " 
                    + "from sp2d left join akun on sp2d.rinciandthindex=akun.rinciandthindex " 
                    + "left join pajak on sp2d.rinciandthindex=pajak.rinciandthskpd " 
                    + "left join potongan on sp2d.rinciandthindex=potongan.rinciandthskpd " 
                    + "order by sp2d.rinciandthindex, sp2d.spmnumber, akun.akunbelanja, pajak.kodeakunpajak, potongan.kodeakunpotongan";
            
            rs = stm.executeQuery(sql);
            result.setKodeSatker(kodeSatker);
            if (periode > 0 && periode < 12) {
                result.setShortPeriode(periode);
            }
            result.setTahunAnggaran(year);

            List<DTHSKPD> dthskpds = new ArrayList();
            DTHSKPD dthskpd = new DTHSKPD();
            dthskpd.setKodeUrusanPelaksana(kodeSKPD.trim().substring(0, 4));
            dthskpd.setKodeSKPD(kodeSKPD.trim().substring(kodeSKPD.trim().length() - 2, kodeSKPD.trim().length()));

            long indexdth = 0;
            String akun = "", pajak = "", potongan="";
            RincianDTHSKPD rincian = new RincianDTHSKPD();
            List<RincianDTHSKPD> rincians = new ArrayList();
            List<AkunDTHSKPD> akuns = new ArrayList();
            List<PajakDTHSKPD> pajaks = new ArrayList();
            List<PotonganDTHSKPD> potongans = new ArrayList();
            while (rs.next()) {
                if (rs.getLong("rinciandthindex") != indexdth) {
                    if (indexdth > 0) {
                        rincian.setAkuns(akuns);
                        rincian.setPajaks(pajaks);
                        rincian.setPotongans(potongans);
                        rincians.add(rincian);
                    }

                    rincian = new RincianDTHSKPD();
                    rincian.setIndexRincian(rs.getLong("rinciandthindex"));
                    rincian.setNomorSP2D(rs.getString("sp2dnumber"));
                    rincian.setNomorSPM(rs.getString("spmnumber"));
                    rincian.setNilaiSP2D(rs.getDouble("nilaisp2d"));
                    rincian.setNpwpBUD(rs.getString("npwpbud"));
                    rincian.setNpwpSKPD(rs.getString("npwpskpd"));
                    rincian.setNpwpVendor(rs.getString("npwppenerima"));
                    rincian.setNamaVendor(rs.getString("namapenerima"));
                    rincian.setSumberDanaSP2D(rs.getShort("sumberdana"));
                    rincian.setSubSumberDanaSP2D(rs.getString("subsumberdana"));
                    rincian.setKeterangan(rs.getString("keterangan"));

                    indexdth = rs.getLong("rinciandthindex");

                    akun = rs.getString("akunbelanja");
                    akuns = new ArrayList();

                    if (akun != null && !akun.trim().equals("")) {
                        akuns.add(new AkunDTHSKPD(rs.getString("akunBelanja"), rs.getDouble("nilai")));
                    }
                    pajak = rs.getString("kodeakunpajak");
                    pajaks = new ArrayList();
                    if (pajak != null && !pajak.trim().equals("")) {
                        pajaks.add(new PajakDTHSKPD(rs.getString("kodeakunpajak"), rs.getString("namaakunpajak"), rs.getShort("jenispajak"), rs.getDouble("nilpajak")));
                    }
                    
                    potongan = rs.getString("kodeakunpotongan");
                    potongans = new ArrayList();
                    if (potongan != null && !potongan.trim().equals("")) {
                        potongans.add(new PotonganDTHSKPD(rs.getString("kodeakunpotongan"), rs.getString("namaakunpotongan"), rs.getShort("jenispotongan"), rs.getDouble("nilpotongan")));
                    }
                    
                } else {
                    String ak = rs.getString("akunbelanja");
                    if (ak != null && !ak.trim().equals("") && akuns.size()>0) {
                        boolean find = false;
                        int ii = 0;
                        while(find==false && ii<akuns.size() ){
                            if (ak.trim().equals(akuns.get(ii).getKodeAkunUtama().trim())) {
                                find =true;
                            }
                            ii++;
                        }
                        if(find==false){
                            akuns.add(new AkunDTHSKPD(rs.getString("akunBelanja"), rs.getDouble("nilai")));
                        }
//                        if (akun != null && !ak.trim().equals(akun.trim())) {
//                            akuns.add(new AkunDTHSKPD(rs.getString("akunBelanja"), rs.getDouble("nilai")));
//                            akun = ak;
//                        }
                    }
                    String paj = rs.getString("kodeakunpajak");
                    if (paj != null && pajaks.size()>0) {
                        boolean find = false;
                        int ii = 0;
                        while (find == false && ii < pajaks.size()) {
                            if (paj.trim().equals(pajaks.get(ii).getKodeAkunPajak().trim())) {
                                find = true;
                            }
                            ii++;
                        }
                        if(find==false){
                            pajaks.add(new PajakDTHSKPD(rs.getString("kodeakunpajak"), rs.getString("namaakunpajak"), rs.getShort("jenispajak"), rs.getDouble("nilpajak")));
                        }
                        
                    }
                    String pot = rs.getString("kodeakunpotongan");
                    if (pot != null && potongans.size()>0) {
                        boolean find =false;
                        int ii = 0;
                        while (find == false && ii < potongans.size()) {
                            if (pot.trim().equals(potongans.get(ii).getKodeAkunPotongan().trim())) {
                                find = true;
                            }
                            ii++;
                        }
                        if(find==false){
                            potongans.add(new PotonganDTHSKPD(rs.getString("kodeakunpotongan"), rs.getString("namaakunpotongan"), rs.getShort("jenispotongan"), rs.getDouble("nilpotongan")));
                        }
                        
                    }
                }
            }
//            if(rincian != null) {
            if (akuns.size() > 0) {
                rincian.setAkuns(akuns);
            }
            if (pajaks.size() > 0) {
                rincian.setPajaks(pajaks);
            }
            if( potongans.size()>0){
                rincian.setPotongans(potongans);
            }
            rincians.add(rincian);
//            }
            if (rincians.size() > 0) {
                dthskpd.setRincians(rincians);
            }
            dthskpds.add(dthskpd);

            result.setDthSkpd(dthskpds);
            return result;
        } catch (SQLException | SIKDServiceException ex) {
            Logger.getLogger(OfficeSQL.class.getName()).log(Level.SEVERE, null, ex);
            throw new SQLException("SQL: Gagal mengambil data DTH (Data Transaksi Harian) \n" + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }

    @Override
    public List<RTH> getRTHReport(short year, String kodeSatker, short periode, Connection conn) throws SQLException {
        List<RTH> result = new ArrayList();
        ResultSet rs = null;
        Statement stm = null;
        try {
            stm = conn.createStatement();
            String sql = "with org as " 
                    + "( SELECT distinct (kodeurusanpelaksana||'.'||kodeskpd||' '||namaskpd) skpd, spmnumber, sp2dnumber from sp2dkegiatan s " 
                    + "inner join rinciandthskpd r on s.rinciandthindex=r.rinciandthindex " 
                    + "inner join dth d on r.dthindex=d.dthindex " 
                    + "where kodesatker='" + kodeSatker + "' " 
                    + "and tahunanggaran= " + year + " " 
                    + "and periode= " + periode + "), " 
                    
                    + "akun as " 
                    + "(SELECT distinct (kodeurusanpelaksana||'.'||kodeskpd||' '||namaskpd) skpd, " 
                    + "sum(nilairekening) nilai from rinciandthskpd r " 
                    + "inner join dth d on r.dthindex=d.dthindex " 
                    + "inner join sp2dkegiatan s on r.rinciandthindex=s.rinciandthindex " 
                    + "left join akundthskpd a on s.kegiatanindex=a.kegiatanindex " 
                    + "where kodesatker='" + kodeSatker + "' " 
                    + "and tahunanggaran= " + year + " " 
                    + "and periode= " + periode + " " 
                    + "group by skpd), " 
                    
                    + "pajak as " 
                    + "(SELECT distinct(kodeurusanpelaksana||'.'||kodeskpd||' '||namaskpd) skpd, sum(p.nilaipotongan) pajak from rinciandthskpd r " 
                    + "inner join dth d on r.dthindex=d.dthindex " 
                    + "inner join sp2dkegiatan s on r.rinciandthindex=s.rinciandthindex " 
                    + "left join pajakdthskpd p on r.rinciandthindex=p.rinciandthskpd " 
                    + "where kodesatker='" + kodeSatker + "' " 
                    + "and tahunanggaran= " + year + " " 
                    + "and periode= " + periode + " " 
                    + "and p.jenispajak>0 and p.jenispajak<6 " 
                    + "group by skpd ), " 
                    
                    + "potongan as " 
                    + "(SELECT distinct (kodeurusanpelaksana||'.'||kodeskpd||' '||namaskpd) skpd, sum(p.nilaipotongan) potong from rinciandthskpd r " 
                    + "inner join dth d on r.dthindex=d.dthindex " 
                    + "inner join sp2dkegiatan s on r.rinciandthindex=s.rinciandthindex " 
                    + "left join pajakdthskpd p on r.rinciandthindex=p.rinciandthskpd " 
                    + "where kodesatker='" + kodeSatker + "' " 
                    + "and tahunanggaran= " + year + " " 
                    + "and periode= " + periode + " " 
                    + "and p.jenispajak>1000 and p.jenispajak<1006 " 
                    + "group by skpd ) " 
                    
                    + "SELECT distinct(org.skpd), count(spmnumber) jumlahspm, count(sp2dnumber) jumlahsp2d, " 
                    + "sum(nilai) nilai, sum(pajak) pajak, sum(potong) potong  from org " 
                    + "left join akun on org.skpd=akun.skpd " 
                    + "left join pajak on org.skpd=pajak.skpd " 
                    + "left join potongan on org.skpd=potongan.skpd " 
                    + "group by org.skpd";
            rs = stm.executeQuery(sql);
            int i = 1;
            int jumlahSPM = 0, jumlahSP2D = 0;
            double nilai = 0, potongan = 0, pajak=0;
            while (rs.next()) {
                result.add(new RTH(String.valueOf(i), rs.getString("skpd"), rs.getInt("jumlahspm"), rs.getDouble("nilai"), rs.getInt("jumlahsp2d"), rs.getDouble("nilai"), rs.getDouble("pajak"), rs.getDouble("potong"), ""));
                jumlahSPM += result.get(result.size() - 1).getJumlahSPM();
                jumlahSP2D += result.get(result.size() - 1).getJumlahSP2D();
                nilai += result.get(result.size() - 1).getJumlahBelanjaSP2D();
                pajak += result.get(result.size() - 1).getJumlahPajak();
                potongan += result.get(result.size() - 1).getJumlahPotongan();
                i++;
            }
            if (result.size() > 0) {
                result.add(new RTH());
                result.add(new RTH("", "JUMLAH", jumlahSPM, nilai, jumlahSP2D, nilai, pajak, potongan, "", "font-weight: bold;"));
            }
            return result;
        } catch (SQLException ex) {
            Logger.getLogger(OfficeSQL.class.getName()).log(Level.SEVERE, null, ex);
            throw new SQLException("SQL: Gagal mengambil data RTH (Rekapitulasi Data Transaksi Harian) \n" + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }

    @Override
    public List<String> getDinasKirimDTH(short year, String kodeSatker, Connection conn) throws SQLException {
        List<String> result = new ArrayList();
        ResultSet rs = null;
        Statement stm = null;
        try {
            stm = conn.createStatement();

            rs = stm.executeQuery(" select distinct(kodeurusanpelaksana||'.'|| kodeskpd || ' ' || namaskpd) skpd from sp2dkegiatan "
                    + "where rinciandthindex in( "
                    + "select rinciandthindex from rinciandthskpd where dthindex in( "
                    + "select dthindex from dth where "
                    + "kodesatker='" + kodeSatker.trim() + "' and "
                    + "tahunanggaran=" + year + ")) order by skpd");
            while (rs.next()) {
                result.add(rs.getString("skpd"));
            }

            return result;
        } catch (SQLException ex) {
            Logger.getLogger(OfficeSQL.class.getName()).log(Level.SEVERE, null, ex);
            throw new SQLException("SQL: Gagal mengambil data SKPD kirim DTH (Data Transaksi Harian) \n" + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }

    @Override
    public long createPinjamanDaerah(PinjamanDaerah pinjaman, short iotype, Connection conn) throws SQLException {
        PreparedStatement pstm = null;
        Statement stm = null;
        ResultSet rs = null;
        Date dd = new Date();
        try {
            String sql = "insert into pinjamandaerah("
                    + IAPBDConstants.ATTR_KODE_SATKER + ", "
                    + IAPBDConstants.ATTR_KODE_PEMDA + ", "
                    + IAPBDConstants.ATTR_NAMA_PEMDA + ", "
                    + IAPBDConstants.ATTR_TAHUN_ANGGARAN + ", tanggalpengiriman, statusdata, namaaplikasi, pengembangaplikasi, iotype) "
                    + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
            pstm = conn.prepareStatement(sql);
            pstm.setString(1, pinjaman.getKodeSatker().trim());
            pstm.setString(2, pinjaman.getKodePemda().trim());
            pstm.setString(3, pinjaman.getNamaPemda().trim());
            pstm.setShort(4, pinjaman.getTahunAnggaran());
            pstm.setDate(5, new java.sql.Date(dd.getTime()));
            pstm.setShort(6, pinjaman.getStatusData());
            pstm.setString(7, pinjaman.getNamaAplikasi());
            pstm.setString(8, pinjaman.getPengembangAplikasi());
            pstm.setShort(9, iotype);
            pstm.executeUpdate();
            stm = conn.createStatement();
            rs = stm.executeQuery("select max(pinjamanindex) id from pinjamandaerah");
            if (rs.next()) {
                return rs.getLong("id");
            }
            return 0;
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (pstm != null) {
                pstm.close();
            }
        }
    }

    @Override
    public void updatePinjamanDaerah(PinjamanDaerah pinjaman, long pinjamanIndex, Connection conn) throws SQLException {
        PreparedStatement stm = null;
        try {
            String sql = "update pinjamandaerah set "
                    + IAPBDConstants.ATTR_KODE_SATKER + "=?, "
                    + IAPBDConstants.ATTR_KODE_PEMDA + "=?, "
                    + IAPBDConstants.ATTR_NAMA_PEMDA + "=?, "
                    + IAPBDConstants.ATTR_TAHUN_ANGGARAN + "= ? WHERE "
                    + " pinjamanindex=?";
            stm = conn.prepareStatement(sql);
            stm.setString(1, pinjaman.getKodeSatker().trim());
            stm.setString(2, pinjaman.getKodePemda().trim());
            stm.setString(3, pinjaman.getNamaPemda().trim());
            stm.setShort(4, pinjaman.getTahunAnggaran());
            stm.executeUpdate();
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if (stm != null) {
                stm.close();
            }
        }
    }

    @Override
    public void deletePinjamanDaerah(String kodeSatker, short thn, Connection conn) throws SQLException {
        PreparedStatement stm = null;
        try {
            String sql = "delete from pinjamandaerah where "
                    + IAPBDConstants.ATTR_KODE_SATKER + "=? and "
                    + IAPBDConstants.ATTR_TAHUN_ANGGARAN + "=? ";
            stm = conn.prepareStatement(sql);
            stm.setString(1, kodeSatker.trim());
            stm.setShort(2, thn);
            stm.executeUpdate();
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if (stm != null) {
                stm.close();
            }
        }
    }

    @Override
    public long getPinjamanDaerahIndex(String kodeSatker, short thn, Connection conn) throws SQLException {
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            String sql = "select pinjamanindex from pinjamandaerah where "
                    + IAPBDConstants.ATTR_KODE_SATKER + "=? and "
                    + IAPBDConstants.ATTR_TAHUN_ANGGARAN + "=?";
            stm = conn.prepareStatement(sql);
            stm.setString(1, kodeSatker.trim());
            stm.setShort(2, thn);
            rs = stm.executeQuery();
            long id = 0;
            if (rs.next()) {
                id = rs.getLong("pinjamanindex");
            }
            return id;
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }

    @Override
    public String createRincianPinjamanDaerah(RincianPinjamanDaerah rincian, long indexPinjaman, Connection conn) throws SQLException {
        PreparedStatement stm = null;
        try {
            String sql = "insert into rincianpinjamandaerah VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
            stm = conn.prepareStatement(sql);
            stm.setLong(1, indexPinjaman);
            stm.setString(2, rincian.getSumber().trim());
            stm.setString(3, rincian.getDasarHukum().trim());
            if (rincian.getTanggalPerjanjian() != null) {
                stm.setDate(4, new java.sql.Date(rincian.getTanggalPerjanjian().getTime()));
            } else {
                stm.setNull(4, Types.DATE);
            }
            stm.setDouble(5, rincian.getJumlahPinjaman());
            stm.setDouble(6, rincian.getJangkaWaktu());
            stm.setDouble(7, rincian.getBunga());
            stm.setString(8, rincian.getTujuan().trim());
            stm.setDouble(9, rincian.getBayarPokok());
            stm.setDouble(10, rincian.getBayarBunga());
            stm.setDouble(11, rincian.getSisaPokok());
            stm.setDouble(12, rincian.getSisaBunga());
            stm.executeUpdate();
            return "Sukses Create Rincian Pinjaman Daerah";
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if (stm != null) {
                stm.close();
            }
        }
    }

    @Override
    public List<RincianPinjamanDaerah> getRincianPinjamanDaerahs(String kodeSatker, short thn, Connection conn) throws SQLException {
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            String sql = "select * from rincianpinjamandaerah "
                    + "where pinjamanindex in("
                    + "select pinjamanindex from pinjamandaerah where "
                    + IAPBDConstants.ATTR_KODE_SATKER + "=? and "
                    + IAPBDConstants.ATTR_TAHUN_ANGGARAN + "=?)";
            stm = conn.prepareStatement(sql);
            stm.setString(1, kodeSatker.trim());
            stm.setShort(2, thn);
            rs = stm.executeQuery();

            List<RincianPinjamanDaerah> result = new ArrayList();
            while (rs.next()) {
                result.add(new RincianPinjamanDaerah(rs.getString("sumber"), rs.getString("dasarhukum"), rs.getDate("tglperjanjian"), rs.getDouble("jumlahpinjaman"), rs.getDouble("jangkawaktu"), rs.getDouble("bunga"), rs.getString("tujuan"), rs.getDouble("bayarpokok"), rs.getDouble("bayarbunga"), rs.getDouble("sisapokok"), rs.getDouble("sisabunga")));
            }

            return result;
        } catch (SQLException | SIKDServiceException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }

    @Override
    public void deleteRincianPinjamanDaerah(String kodeSatker, short thn, RincianPinjamanDaerah rincian, Connection conn) throws SQLException {
        PreparedStatement stm = null;
        try {
            String sql = "delete from rincianpinjamandaerah "
                    + "where sumber = ? and dasarhukum=? and tglperjanjian=? "
                    + "and jumlahpinjaman=? and jangkawaktu=? and bunga=? and tujuan=? "
                    + "and bayarpokok=? and bayarbunga=? and sisapokok=? and sisabunga=? "
                    + "and pinjamanindex in("
                    + "select pinjamanindex from pinjamandaerah where "
                    + IAPBDConstants.ATTR_KODE_SATKER + "=? and "
                    + IAPBDConstants.ATTR_TAHUN_ANGGARAN + "=?)";
            stm = conn.prepareStatement(sql);
            stm.setString(1, rincian.getSumber().trim());
            stm.setString(2, rincian.getDasarHukum().trim());
            if (rincian.getTanggalPerjanjian() != null) {
                stm.setDate(3, new java.sql.Date(rincian.getTanggalPerjanjian().getTime()));
            } else {
                stm.setNull(3, Types.DATE);
            }
            stm.setDouble(4, rincian.getJumlahPinjaman());
            stm.setDouble(5, rincian.getJangkaWaktu());
            stm.setDouble(6, rincian.getBunga());
            stm.setString(7, rincian.getTujuan());
            stm.setDouble(8, rincian.getBayarPokok());
            stm.setDouble(9, rincian.getBayarBunga());
            stm.setDouble(10, rincian.getSisaPokok());
            stm.setDouble(11, rincian.getSisaBunga());
            stm.setString(12, kodeSatker.trim());
            stm.setShort(13, thn);
            stm.executeUpdate();
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if (stm != null) {
                stm.close();
            }
        }
    }

    @Override
    public void updateRincianPinjamanDaerah(String kodeSatker, short thn, RincianPinjamanDaerah oldRincian, RincianPinjamanDaerah newRincian, Connection conn) throws SQLException {
        PreparedStatement stm = null;
        try {
            String sql = "update rincianpinjamandaerah "
                    + "set sumber = ?, dasarhukum=?, tglperjanjian=?, "
                    + "jumlahpinjaman=?, jangkawaktu=?, bunga=?, tujuan=?, "
                    + "bayarpokok=?, bayarbunga=?, sisapokok=?, sisabunga=? "
                    + "where sumber = ? and dasarhukum=? and tglperjanjian=? "
                    + "and jumlahpinjaman=? and jangkawaktu=? and bunga=? and tujuan=? "
                    + "and bayarpokok=? and bayarbunga=? and sisapokok=? and sisabunga=? "
                    + "and pinjamanindex in("
                    + "select pinjamanindex from pinjamandaerah where "
                    + IAPBDConstants.ATTR_KODE_SATKER + "=? and "
                    + IAPBDConstants.ATTR_TAHUN_ANGGARAN + "=?)";
            stm = conn.prepareStatement(sql);
            stm.setString(1, newRincian.getSumber().trim());
            stm.setString(2, newRincian.getDasarHukum().trim());
            if (newRincian.getTanggalPerjanjian() != null) {
                stm.setDate(3, new java.sql.Date(newRincian.getTanggalPerjanjian().getTime()));
            } else {
                stm.setNull(3, Types.DATE);
            }
            stm.setDouble(4, newRincian.getJumlahPinjaman());
            stm.setDouble(5, newRincian.getJangkaWaktu());
            stm.setDouble(6, newRincian.getBunga());
            stm.setString(7, newRincian.getTujuan());
            stm.setDouble(8, newRincian.getBayarPokok());
            stm.setDouble(9, newRincian.getBayarBunga());
            stm.setDouble(10, newRincian.getSisaPokok());
            stm.setDouble(11, newRincian.getSisaBunga());
            stm.setString(12, oldRincian.getSumber().trim());
            stm.setString(13, oldRincian.getDasarHukum().trim());
            if (oldRincian.getTanggalPerjanjian() != null) {
                stm.setDate(14, new java.sql.Date(oldRincian.getTanggalPerjanjian().getTime()));
            } else {
                stm.setNull(14, Types.DATE);
            }
            stm.setDouble(15, oldRincian.getJumlahPinjaman());
            stm.setDouble(16, oldRincian.getJangkaWaktu());
            stm.setDouble(17, oldRincian.getBunga());
            stm.setString(18, oldRincian.getTujuan());
            stm.setDouble(19, oldRincian.getBayarPokok());
            stm.setDouble(20, oldRincian.getBayarBunga());
            stm.setDouble(21, oldRincian.getSisaPokok());
            stm.setDouble(22, oldRincian.getSisaBunga());
            stm.setString(23, kodeSatker.trim());
            stm.setShort(24, thn);
            stm.executeUpdate();
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if (stm != null) {
                stm.close();
            }
        }
    }

    @Override
    public long createDataDaerah(DataDaerah obj, Connection conn) throws SQLException {
        PreparedStatement pstm = null;
        Statement stm = null;
        ResultSet rs = null;
        long id = 0;
        try {
            java.sql.Date d = new java.sql.Date(new java.util.Date().getTime());
            String sql = "insert into datadaerah "
                    + "(kodesatker, kodepemda, namapemda, tahunanggaran, tglpengiriman, statusdata, namaaplikasi, pengembangaplikasi ) "
                    + "values (?, ?, ?, ?, ?, ?, ?, ? )";
            pstm = conn.prepareStatement(sql);
            pstm.setString(1, obj.getKodeSatker());
            pstm.setString(2, obj.getKodePemda());
            pstm.setString(3, obj.getNamaPemda());
            pstm.setShort(4, obj.getTahunAnggaran());
            pstm.setDate(5, d);
            pstm.setShort(6, (short)0);
            pstm.setString(7, "Input Data");
            pstm.setString(8, "Input Data");
            pstm.executeUpdate();
            stm=conn.createStatement();
            rs = stm.executeQuery("select max(datadaerahindex) id from datadaerah");
            if (rs.next()) {
                return rs.getLong("id");
            }
            return id;
        } catch (SQLException e) {
            Logger.getLogger(OfficeSQL.class.getName()).log(Level.SEVERE, null, e);
            throw new SQLException("SQL: Gagal menginput Data Daerah \n" + e.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (pstm != null) {
                pstm.close();
            }
        }
    }

    @Override
    public long getDataDaerahIndex(String kodeSatker, short tahun, Connection conn) throws SQLException {
        Statement stm = null;
        ResultSet rs = null;

        try {
            stm = conn.createStatement();
            rs = stm.executeQuery("select datadaerahindex from datadaerah where "
                    + "kodesatker='" + kodeSatker + "' and "
                    + "tahunanggaran=" + tahun);
            while (rs.next()) {
                return rs.getLong("datadaerahindex");
            }

            return 0;
        } catch (SQLException ex) {
            Logger.getLogger(OfficeSQL.class.getName()).log(Level.SEVERE, null, ex);
            throw new SQLException("SQL: Gagal mengambil index Data Daerah \n" + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }

    @Override
    public long createDataDaerahDetail(DataDaerahDetail obj, long dataDaerahIndex, Connection conn) throws SQLException {
        PreparedStatement stm = null;
        Statement stm2 = null;
        ResultSet rs = null;
        String sql = "insert into datadaerahdetail "
                + "(datadaerahindex, jumlahpenduduk, persenmiskin, garismiskin, ipm, "
                + "pengangguran, tingkatpengangguran) VALUES (?,?,?,?,?,?,?)";

        try {
            stm = conn.prepareStatement(sql);
            stm.setLong(1, dataDaerahIndex);
            stm.setInt(2, obj.getJumlahPenduduk());
            stm.setDouble(3, obj.getPersenMiskin());
            stm.setInt(4, obj.getGarisMiskin());
            stm.setDouble(5, obj.getIpm());
            stm.setInt(6, obj.getPengangguran());
            stm.setDouble(7, obj.getTingkatPengangguran());
            stm.executeUpdate();
            long id = 0;
            stm2 = conn.createStatement();
            rs = stm2.executeQuery("select max(detailindex) id from datadaerahdetail");
            if (rs.next()) {
                return rs.getLong("id");
            }
            return id;

        } catch (SQLException ex) {
            Logger.getLogger(OfficeSQL.class.getName()).log(Level.SEVERE, null, ex);
            throw new SQLException("SQL: Gagal input Detail Data Daerah \n" + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm2 != null) {
                stm2.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }

    @Override
    public void updateDataDaerahDetail(DataDaerahDetail obj, long dataDaerahIndex, Connection conn) throws SQLException {
        Statement stm = null;
        String sql = "update datadaerahdetail set jumlahpenduduk = " + obj.getJumlahPenduduk() + ", "
                + "persenmiskin=" + obj.getPersenMiskin() + ", "
                + "garismiskin=" + obj.getGarisMiskin() + ", "
                + "ipm=" + obj.getIpm() + ", "
                + "pengangguran=" + obj.getPengangguran() + ", "
                + "tingkatpengangguran=" + obj.getTingkatPengangguran() + " "
                + "where datadaerahindex = " + dataDaerahIndex;
        try {
            stm = conn.createStatement();
            stm.executeUpdate(sql);
        } catch (SQLException ex) {
            Logger.getLogger(OfficeSQL.class.getName()).log(Level.SEVERE, null, ex);
            throw new SQLException("SQL: Gagal merubah data detail daerah \n" + ex.getMessage());
        } finally {
            if (stm != null) {
                stm.close();
            }
        }
    }

    @Override
    public void deleteDataDaerah(String kodeSatker, short tahun, Connection conn) throws SQLException {
        Statement stm = null;
        String sql = "delete from datadaerah where kodesatker ='" + kodeSatker + "' and tahunanggaran=" + tahun;
        try {
            stm = conn.createStatement();
            stm.executeUpdate(sql);
        } catch (SQLException ex) {
            Logger.getLogger(OfficeSQL.class.getName()).log(Level.SEVERE, null, ex);
            throw new SQLException("SQL: Gagal menghapus data daerah\n" + ex.getMessage());
        } finally {
            if (stm != null) {
                stm.close();
            }
        }
    }

    @Override
    public DataDaerahDetail getDataDaerahDetail(long dataDaerahIndex, Connection conn) throws SQLException {
        Statement stm = null;
        ResultSet rs = null;

        try {
            stm = conn.createStatement();
            rs = stm.executeQuery("select * from datadaerahdetail where datadaerahindex=" + dataDaerahIndex);
            DataDaerahDetail result = new DataDaerahDetail();
            if (rs.next()) {
                result.setIndex(rs.getLong("detailindex"));
                result.setJumlahPenduduk(rs.getInt("jumlahpenduduk"));
                result.setPersenMiskin(rs.getDouble("persenmiskin"));
                result.setGarisMiskin(rs.getInt("garismiskin"));
                result.setIpm(rs.getDouble("ipm"));
                result.setPengangguran(rs.getInt("pengangguran"));
                result.setTingkatPengangguran(rs.getDouble("tingkatpengangguran"));
            }
            return result;
        } catch (SQLException ex) {
            Logger.getLogger(OfficeSQL.class.getName()).log(Level.SEVERE, null, ex);
            throw new SQLException("SQL: Gagal mengambil data Detail Daerah \n" + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }

    @Override
    public long getDataDaerahDetailIndex(long dataDaerahIndex, Connection conn) throws SQLException {
        Statement stm = null;
        ResultSet rs = null;

        try {
            stm = conn.createStatement();
            rs = stm.executeQuery("select detailindex from datadaerahdetail where datadaerahindex=" + dataDaerahIndex);

            if (rs.next()) {
                return rs.getLong("detailindex");
            }
            return 0;
        } catch (SQLException ex) {
            Logger.getLogger(OfficeSQL.class.getName()).log(Level.SEVERE, null, ex);
            throw new SQLException("SQL: Gagal mengambil data Detail Daerah \n" + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }

    @Override
    public long createPDRB(PDRB obj, Connection conn) throws SQLException {
        PreparedStatement pstm = null;
        Statement stm = null;
        ResultSet rs = null;
        long id = 0;
        java.sql.Date d = new java.sql.Date(new Date().getTime());
        String sql = "insert into pdrb "
                + " (kodesatker, kodepemda, namapemda, tahunanggaran, tanggalpengiriman, "
                + " statusdata, namaaplikasi, pengembangaplikasi ) "
                + " values ( ?, ?, ?, ?, ?, ?, ?, ? )";
        try {
            pstm = conn.prepareStatement(sql);
            pstm.setString(1, obj.getKodeSatker());
            pstm.setString(2, obj.getKodePemda());
            pstm.setString(3, obj.getNamaPemda());
            pstm.setShort(4, obj.getTahunAnggaran());
            pstm.setDate(5, d);
            pstm.setShort(6, (short)0);
            pstm.setString(7, "Input Data");
            pstm.setString(8, "Input Data");            
            pstm.executeUpdate();
            
            stm=conn.createStatement();
            rs = stm.executeQuery("select max(pdrbindex) id from pdrb");
            if (rs.next()) {
                return rs.getLong("id");
            }
            return id;
        } catch (SQLException e) {
            Logger.getLogger(OfficeSQL.class.getName()).log(Level.SEVERE, null, e);
            throw new SQLException("SQL: Gagal menginput Data PDRB \n" + e.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (pstm != null) {
                pstm.close();
            }
        }
    }

    @Override
    public long getPDRBIndex(String kodeSatker, short tahun, Connection conn) throws SQLException {
        Statement stm = null;
        ResultSet rs = null;

        try {
            stm = conn.createStatement();
            rs = stm.executeQuery("select pdrbindex from pdrb where "
                    + "kodesatker='" + kodeSatker + "' and "
                    + "tahunanggaran=" + tahun);
            while (rs.next()) {
                return rs.getLong("pdrbindex");
            }

            return 0;
        } catch (SQLException ex) {
            Logger.getLogger(OfficeSQL.class.getName()).log(Level.SEVERE, null, ex);
            throw new SQLException("SQL: Gagal mengambil index Data PDRB \n" + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }

    @Override
    public void deletePDRB(String kodeSatker, short tahun, Connection conn) throws SQLException {
        Statement stm = null;
        String sql = "delete from pdrb where kodesatker ='" + kodeSatker + "' and tahunanggaran=" + tahun;
        try {
            stm = conn.createStatement();
            stm.executeUpdate(sql);
        } catch (SQLException ex) {
            Logger.getLogger(OfficeSQL.class.getName()).log(Level.SEVERE, null, ex);
            throw new SQLException("SQL: Gagal menghapus data PDRB\n" + ex.getMessage());
        } finally {
            if (stm != null) {
                stm.close();
            }
        }
    }

    @Override
    public long createPDRBHargaBerlaku(PDRBHarga obj, long pdrbIndex, Connection conn) throws SQLException {
        PreparedStatement stm = null;
        Statement stm2 = null;
        ResultSet rs = null;
        long id = 0;
        try {
            stm = conn.prepareStatement("insert into pdrbhargaberlaku ( pdrbindex, pertanian, tambanggali, tambangmigas, "
                    + "pengolahan, migas, listrikgasair , bangunan , resto , komunikasi , keuangan , "
                    + "jasa, pdrbmigas, pdrbsubmigas, pdrbnonmigas, pdrbnonsub  ) "
                    + "values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
            int i = 1;
            stm.setLong(i++, pdrbIndex);
            stm.setDouble(i++, obj.getPertanian());
            stm.setDouble(i++, obj.getTambanggali());
            stm.setDouble(i++, obj.getTambangmigas());
            stm.setDouble(i++, obj.getPengolahan());
            stm.setDouble(i++, obj.getMigas());
            stm.setDouble(i++, obj.getListrikgasair());
            stm.setDouble(i++, obj.getBangunan());
            stm.setDouble(i++, obj.getResto());
            stm.setDouble(i++, obj.getKomunikasi());
            stm.setDouble(i++, obj.getKeuangan());
            stm.setDouble(i++, obj.getJasa());
            stm.setDouble(i++, obj.getPdrbmigas());
            stm.setDouble(i++, obj.getPdrbsubmigas());
            stm.setDouble(i++, obj.getPdrbnonmigas());
            stm.setDouble(i++, obj.getPdrbnonsub());
            stm.executeUpdate();
            
            stm2 = conn.createStatement();
            rs = stm2.executeQuery("select max(hargaberlakuindex) id from pdrbhargaberlaku");
            if (rs.next()) {
                return rs.getLong("id");
            }
            return id;
        } catch (SQLException e) {
            Logger.getLogger(OfficeSQL.class.getName()).log(Level.SEVERE, null, e);
            throw new SQLException("SQL: Gagal menginput Data PDRB Harga Berlaku\n" + e.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if(stm2!=null) stm2.close();
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    @Override
    public long createPDRBHargaKonstant(PDRBHarga obj, long pdrbIndex, Connection conn) throws SQLException {
        PreparedStatement stm = null;
        Statement stm2 = null;
        ResultSet rs = null;
        long id = 0;
        try {
            stm = conn.prepareStatement("insert into pdrbhargakonstan ( pdrbindex, pertanian, tambanggali, tambangmigas, "
                    + "pengolahan, migas, listrikgasair , bangunan , resto , komunikasi , keuangan , "
                    + "jasa, pdrbmigas, pdrbsubmigas, pdrbnonmigas, pdrbnonsub  ) "
                    + "values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
            int i = 1;
            stm.setLong(i++, pdrbIndex);
            stm.setDouble(i++, obj.getPertanian());
            stm.setDouble(i++, obj.getTambanggali());
            stm.setDouble(i++, obj.getTambangmigas());
            stm.setDouble(i++, obj.getPengolahan());
            stm.setDouble(i++, obj.getMigas());
            stm.setDouble(i++, obj.getListrikgasair());
            stm.setDouble(i++, obj.getBangunan());
            stm.setDouble(i++, obj.getResto());
            stm.setDouble(i++, obj.getKomunikasi());
            stm.setDouble(i++, obj.getKeuangan());
            stm.setDouble(i++, obj.getJasa());
            stm.setDouble(i++, obj.getPdrbmigas());
            stm.setDouble(i++, obj.getPdrbsubmigas());
            stm.setDouble(i++, obj.getPdrbnonmigas());
            stm.setDouble(i++, obj.getPdrbnonsub());
            stm.executeUpdate();
            
            stm2 = conn.createStatement();
            rs = stm2.executeQuery("select max(hargakonstanindex) id from pdrbhargakonstan");
            if (rs.next()) {
                return rs.getLong("id");
            }
            return id;
        } catch (SQLException e) {
            Logger.getLogger(OfficeSQL.class.getName()).log(Level.SEVERE, null, e);
            throw new SQLException("SQL: Gagal menginput Data PDRB Harga Konstan\n" + e.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm2 != null) stm2.close();
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    @Override
    public PDRBHarga getPDRBHargaKonstan(String kodeSatker, short tahun, Connection conn) throws SQLException {
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            stm = conn.prepareStatement("select k.* from pdrbhargakonstan k inner join pdrb on "
                    + "k.pdrbindex = pdrb.pdrbindex where kodesatker = ? and tahunanggaran=?");
            int i = 1;
            stm.setString(i++, kodeSatker);
            stm.setShort(i++, tahun);             
            rs =stm.executeQuery();            
            PDRBHarga result = new PDRBHarga();
            if (rs.next()) {
                result.setIndex(rs.getLong("hargakonstanindex"));
                result.setPertanian(rs.getDouble("pertanian"));
                result.setTambanggali(rs.getDouble("tambanggali"));
                result.setTambangmigas(rs.getDouble("tambangmigas"));
                result.setPengolahan(rs.getDouble("pengolahan"));
                result.setMigas(rs.getDouble("migas"));
                result.setListrikgasair(rs.getDouble("listrikgasair"));
                result.setBangunan(rs.getDouble("bangunan"));
                result.setResto(rs.getDouble("resto"));
                result.setKomunikasi(rs.getDouble("komunikasi"));
                result.setKeuangan(rs.getDouble("keuangan"));
                result.setJasa(rs.getDouble("jasa"));
                result.setPdrbmigas(rs.getDouble("pdrbmigas"));
                result.setPdrbsubmigas(rs.getDouble("pdrbsubmigas"));
                result.setPdrbnonmigas(rs.getDouble("pdrbnonmigas"));
                result.setPdrbnonsub(rs.getDouble("pdrbnonsub"));
            }
            return result;
        } catch (SQLException e) {
            Logger.getLogger(OfficeSQL.class.getName()).log(Level.SEVERE, null, e);
            throw new SQLException("SQL: Gagal mengambil Data PDRB Harga Konstan\n" + e.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    @Override
    public PDRBHarga getPDRBHargaBerlaku(String kodeSatker, short tahun, Connection conn) throws SQLException {
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            stm = conn.prepareStatement("select k.* from pdrbhargaberlaku k inner join pdrb on "
                    + "k.pdrbindex = pdrb.pdrbindex where kodesatker = ? and tahunanggaran=?");
            int i = 1;
            stm.setString(i++, kodeSatker);
            stm.setShort(i++, tahun);             
            rs =stm.executeQuery();            
            PDRBHarga result = new PDRBHarga();
            if (rs.next()) {
                result.setIndex(rs.getLong("hargaberlakuindex"));
                result.setPertanian(rs.getDouble("pertanian"));
                result.setTambanggali(rs.getDouble("tambanggali"));
                result.setTambangmigas(rs.getDouble("tambangmigas"));
                result.setPengolahan(rs.getDouble("pengolahan"));
                result.setMigas(rs.getDouble("migas"));
                result.setListrikgasair(rs.getDouble("listrikgasair"));
                result.setBangunan(rs.getDouble("bangunan"));
                result.setResto(rs.getDouble("resto"));
                result.setKomunikasi(rs.getDouble("komunikasi"));
                result.setKeuangan(rs.getDouble("keuangan"));
                result.setJasa(rs.getDouble("jasa"));
                result.setPdrbmigas(rs.getDouble("pdrbmigas"));
                result.setPdrbsubmigas(rs.getDouble("pdrbsubmigas"));
                result.setPdrbnonmigas(rs.getDouble("pdrbnonmigas"));
                result.setPdrbnonsub(rs.getDouble("pdrbnonsub"));
            }
            return result;
        } catch (SQLException e) {
            Logger.getLogger(OfficeSQL.class.getName()).log(Level.SEVERE, null, e);
            throw new SQLException("SQL: Gagal mengambil Data PDRB Harga Berlaku\n" + e.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    @Override
    public List<RincianKendaraan> getRincianKendaraan(String kodeSatker, short tahun, Connection conn) throws SQLException {
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            stm = conn.prepareStatement("select r.* from rinciankendaraan r inner join kendaraan p on "
                    + "r.kendaraanindex = p.kendaraanindex where kodesatker = ? and tahunanggaran=?");
            int i = 1;
            stm.setString(i++, kodeSatker);
            stm.setShort(i++, tahun);             
            rs =stm.executeQuery();            
            List<RincianKendaraan> result = new ArrayList();
            while (rs.next()) {     
                result.add(new RincianKendaraan(rs.getString("nomorpolisi"), rs.getString("pemilik"), 
                        rs.getString("alamat"), rs.getString("npwp"), rs.getString("kpp"), 
                        rs.getString("cabangnpwp"), rs.getShort("tahunpembuatan"), 
                        rs.getDouble("njkb"), rs.getString("jenis"), rs.getString("merk"), 
                        rs.getString("tipe"), rs.getString("cc"), rs.getString("bahanbakar")));
            }
            return result;
        } catch (SQLException e) {
            Logger.getLogger(OfficeSQL.class.getName()).log(Level.SEVERE, null, e);
            throw new SQLException("SQL: Gagal mengambil Data Rincian Kepemilikan Kendaraan\n" + e.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    @Override
    public List<RincianBPHTB> getRincianBPHTB(String kodeSatker, short tahun, Connection conn) throws SQLException {
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            stm = conn.prepareStatement("select r.* from rincianbphtb r inner join bphtb p on "
                    + "r.bphtbindex = p.bphtbindex where kodesatker = ? and tahunanggaran=?");
            int i = 1;
            stm.setString(i++, kodeSatker);
            stm.setShort(i++, tahun);             
            rs =stm.executeQuery();
            List<RincianBPHTB> result = new ArrayList();
            while (rs.next()) {                
                result.add(new RincianBPHTB(rs.getString("namapenerima"), rs.getString("alamatpenerima"), 
                        rs.getString("npwppenerima"), rs.getString("kpppenerima"), rs.getString("cabangnpwppenerima"), 
                        rs.getString("alamatobjek"), rs.getDouble("nilaiperolehan"), 
                        rs.getDouble("luastanah"), rs.getDouble("luasbangunan"), rs.getDate("tanggaltransaksi"), 
                        rs.getDouble("nilaibphtb")));
            }
            return result;
        } catch (SQLException e) {
            Logger.getLogger(OfficeSQL.class.getName()).log(Level.SEVERE, null, e);
            throw new SQLException("SQL: Gagal mengambil Data Rincian BPHTB\n" + e.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    @Override
    public List<RincianHiburan> getRincianHiburan(String kodeSatker, short tahun, Connection conn) throws SQLException {
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            stm = conn.prepareStatement("select r.* from rincianhiburan r inner join hiburan p on "
                    + "r.hiburanindex = p.hiburanindex where kodesatker = ? and tahunanggaran=?");
            int i = 1;
            stm.setString(i++, kodeSatker);
            stm.setShort(i++, tahun);             
            rs =stm.executeQuery();
            List<RincianHiburan> result = new ArrayList();
            while (rs.next()) { 
                result.add(new RincianHiburan(
                        rs.getString("namahiburan"), rs.getString("alamat"), 
                        rs.getString("namapemilik"), rs.getString("alamatpemilik"), 
                        rs.getString("npwppemilik"), 
                        rs.getString("kpppemilik"), rs.getString("cabangnpwppemilik"), 
                        rs.getString("namapengelola"), rs.getString("alamatpengelola"), 
                        rs.getString("npwppengelola"), rs.getString("kpppengelola"), 
                        rs.getString("cabangnpwppengelola"), 
                        rs.getDouble("jumlahpajak")));
            }
            return result;
        } catch (SQLException e) {
            Logger.getLogger(OfficeSQL.class.getName()).log(Level.SEVERE, null, e);
            throw new SQLException("SQL: Gagal mengambil Data Rincian Kepemilikan Tempat Hiburan\n" + e.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    @Override
    public List<RincianHotel> getRincianHotel(String kodeSatker, short tahun, Connection conn) throws SQLException {
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            stm = conn.prepareStatement("select r.* from rincianhotel r inner join hotel p on "
                    + "r.hotelindex = p.hotelindex where kodesatker = ? and tahunanggaran=?");
            int i = 1;
            stm.setString(i++, kodeSatker);
            stm.setShort(i++, tahun);             
            rs =stm.executeQuery();
            List<RincianHotel> result = new ArrayList();
            while (rs.next()) {                 
                result.add(new RincianHotel(
                        rs.getString("namahotel"), rs.getString("alamat"), rs.getInt("jumlahkamar"), 
                        rs.getString("namapemilik"), rs.getString("alamatpemilik"), 
                        rs.getString("npwppemilik"), 
                        rs.getString("kpppemilik"), rs.getString("cabangnpwppemilik"), 
                        rs.getString("namapengelola"), rs.getString("alamatpengelola"), 
                        rs.getString("npwppengelola"), rs.getString("kpppengelola"), 
                        rs.getString("cabangnpwppengelola"), 
                        rs.getDouble("jumlahpajak")));
            }
            return result;
        } catch (SQLException e) {
            Logger.getLogger(OfficeSQL.class.getName()).log(Level.SEVERE, null, e);
            throw new SQLException("SQL: Gagal mengambil Data Rincian Kepemilikan Tempat Hiburan\n" + e.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    
    @Override
    public List<RincianIMB> getRincianIMB(String kodeSatker, short tahun, Connection conn) throws SQLException {
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            stm = conn.prepareStatement("select r.* from rincianimb r inner join imb p on "
                    + "r.imbindex = p.imbindex where kodesatker = ? and tahunanggaran=?");
            int i = 1;
            stm.setString(i++, kodeSatker);
            stm.setShort(i++, tahun);             
            rs =stm.executeQuery();
            List<RincianIMB> result = new ArrayList();
            while (rs.next()) {             
                result.add(new RincianIMB(
                        rs.getString("nomorizin"), rs.getDate("tanggalizin"), 
                        rs.getString("namapemohon"), rs.getString("alamatpemohon"), 
                        rs.getString("npwppemohon"), rs.getString("kpppemohon"), 
                        rs.getString("cabangnpwppemohon"), rs.getString("lokasi"), 
                        rs.getDouble("luasbangunan"), rs.getDouble("luastanah"), 
                        rs.getInt("jumlahlantai"), rs.getString("fungsi"), 
                        rs.getString("statustanah")));
            }
            return result;
        } catch (SQLException e) {
            Logger.getLogger(OfficeSQL.class.getName()).log(Level.SEVERE, null, e);
            throw new SQLException("SQL: Gagal mengambil Data Rincian IMB\n" + e.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    @Override
    public List<RincianRestoran> getRincianRestoran(String kodeSatker, short tahun, Connection conn) throws SQLException {
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            stm = conn.prepareStatement("select r.* from rincianrestoran r inner join restoran p on "
                    + "r.restoranindex = p.restoranindex where kodesatker = ? and tahunanggaran=?");
            int i = 1;
            stm.setString(i++, kodeSatker);
            stm.setShort(i++, tahun);             
            rs =stm.executeQuery();
            List<RincianRestoran> result = new ArrayList();
            while (rs.next()) {                    
                result.add(new RincianRestoran(
                        rs.getString("namarestoran"), rs.getString("alamat"), rs.getInt("kapasitas"), 
                        rs.getString("namapemilik"), rs.getString("alamatpemilik"), rs.getString("npwppemilik"), rs.getString("kpppemilik"), rs.getString("cabangnpwppemilik"), 
                        rs.getString("namapengelola"), rs.getString("alamatpengelola"), rs.getString("npwppengelola"), rs.getString("kpppengelola"), rs.getString("cabangnpwppengelola"), 
                        rs.getDouble("jumlahpajak"), rs.getInt("jumlahkaryawan")));
            }
            return result;
        } catch (SQLException e) {
            Logger.getLogger(OfficeSQL.class.getName()).log(Level.SEVERE, null, e);
            throw new SQLException("SQL: Gagal mengambil Data Rincian Kepemilikan Restoran\n" + e.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    @Override
    public List<RincianIzinUsaha> getRincianIzinUsaha(String kodeSatker, short tahun, Connection conn) throws SQLException {
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            stm = conn.prepareStatement("select r.* from rincianusaha r inner join usaha p on "
                    + "r.usahaindex = p.usahaindex where kodesatker = ? and tahunanggaran=?");
            int i = 1;
            stm.setString(i++, kodeSatker);
            stm.setShort(i++, tahun);             
            rs =stm.executeQuery();
            List<RincianIzinUsaha> result = new ArrayList();
            while (rs.next()) {                
                result.add(new RincianIzinUsaha(
                        rs.getString("nomorizin"), rs.getDate("tanggalizin"), rs.getString("namaperusahaan"), 
                        rs.getString("alamatperusahaan"), rs.getString("npwpperusahaan"), rs.getString("kppperusahaan"), rs.getString("cabangnpwpperusahaan"),
                        rs.getString("jenisusaha"), 
                        rs.getString("namapemilik"), rs.getString("alamatpemilik"), rs.getString("npwppemilik"), rs.getString("kpppemilik"), rs.getString("cabangnpwppemilik"), 
                        rs.getString("klasifikasi"), 
                        rs.getDouble("modal"), rs.getInt("jumlahkaryawan"), rs.getShort("masaberlaku")));
            }
            return result;
        } catch (SQLException e) {
            Logger.getLogger(OfficeSQL.class.getName()).log(Level.SEVERE, null, e);
            throw new SQLException("SQL: Gagal mengambil Data Rincian Izin Usahan\n" + e.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    @Override
    public List<RincianPajakDanRetribusi> getRincianPajakDanRetribusi(String kodeSatker, short tahun, Connection conn) throws SQLException {
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            stm = conn.prepareStatement("select r.* from rincianpajakdanretribusidaerah r inner join pajakdanretribusidaerah p on "
                    + "r.pajakindex = p.pajakindex where kodesatker = ? and tahunanggaran=?");
            int i = 1;
            stm.setString(i++, kodeSatker);
            stm.setShort(i++, tahun);             
            rs =stm.executeQuery();
            List<RincianPajakDanRetribusi> result = new ArrayList();
            while (rs.next()) { 
                result.add(new RincianPajakDanRetribusi(
                        rs.getString("namapungutan"), rs.getShort("jenispungutan"), 
                        rs.getString("dasarhukum"), rs.getDouble("jumlahpungutan")));
            }
            return result;
        } catch (SQLException e) {
            Logger.getLogger(OfficeSQL.class.getName()).log(Level.SEVERE, null, e);
            throw new SQLException("SQL: Gagal mengambil Data Rincian Pajak Dan Retribusi Daerah\n" + e.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    /**
     *
     * @param tahun
     * @param bulan
     * @param kodeSatker
     * @param kodeData
     * @param kodeFungsi
     * @param conn
     * @return
     * @throws SQLException
     */
    @Override
    public List<NilaiFungsiPemda> getRealisasiKegiatanBerdasarkanFungsi(short tahun, short bulan, String kodeSatker, short kodeData, String kodeFungsi, Connection conn) throws SQLException {
        Statement stm = null;
        ResultSet rs = null;
        List<NilaiFungsiPemda> result= new ArrayList(); 
        String sql = "with zz as ("
                + "select "
                    + " (kodeurusanprogram || '.' || kodeurusanpelaksana || '.' || kodeskpd || '.' || kodeprogram) kodeprog, namaprogram, "
                    + "  (kodeurusanprogram || '.' || kodeurusanpelaksana || '.' || kodeskpd || '.' || kodeprogram || '.' || kodekegiatan) kodekeg, namakegiatan, "
//                    + "  (kodeakunutama||'.'|| kodeakunkelompok||'.'||kodeakunjenis) kodeakun, namaakunjenis namaakun, "
                    + "  sum(nilaianggaran) nilai "
                    + "  from  realisasikoderekapbd rek "
                    + "  inner join realisasikegiatanapbd keg on rek.kegiatanindex=keg.kegiatanindex "
                    + "  inner join realisasiapbd apbd on keg.apbdindex=apbd.apbdindex "
                    + "  where tahunanggaran=" + tahun + " and bulan<= " + bulan
                    + "  and kodesatker='" + kodeSatker + "' and kodefungsi='" + kodeFungsi + "' "
                    + "  and kodedata=" + kodeData
                    //+ "  and namakegiatan is not null or namakegiatan<>'' "
                    + "  group by kodeprog, namaprogram, kodekeg, namakegiatan "//, kodeakun, namaakun "
                    + "  order by kodeprog, kodekeg ) "
                    + " select (kodeprog || ' ' || namaprogram) prog, (kodekeg || ' ' || namakegiatan) keg, nilai " 
                    + " from zz " 
                    + " order by prog, keg ";
        try {
            stm = conn.createStatement();
            rs =stm.executeQuery(sql);
            while (rs.next()) { 
                result.add(new NilaiFungsiPemda(
                        rs.getString("prog").trim(), 
                        rs.getString("keg"), rs.getDouble("nilai")));
            }
            return result;
        } catch (SQLException e) {
            Logger.getLogger(OfficeSQL.class.getName()).log(Level.SEVERE, null, e);
            throw new SQLException("SQL: Gagal mengambil Data Realisasi Belanja Berdasarkan Fungsi \n" + e.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    /**
     *
     * @param tahun
     * @param bulan
     * @param kodeSatker
     * @param sumberDana
     * @param conn
     * @return
     * @throws SQLException
     */
    @Override
    public List<NilaiFungsiPemda> getRealisasiBelanjaBerdasarkanSumberDana(short tahun, short bulan, String kodeSatker, short sumberDana, Connection conn) throws SQLException {
        Statement stm = null;
        ResultSet rs = null;
        List<NilaiFungsiPemda> result= new ArrayList(); 
        try {
            stm = conn.createStatement();
            rs =stm.executeQuery("select "
                    + " (kodeurusanprogram || '.' || kodeurusanpelaksana || '.' || kodeskpd || '.' || kodeprogram || '.' || kodekegiatan) kodekeg, namakegiatan namakeg, "
                    + " (kodeakunutama||'.'|| kodeakunkelompok||'.'||kodeakunjenis) kodeakun, namaakunjenis namaakun, "
                    + " sum(nilairekening) nilai from akundthskpd akun "
                    + " inner join sp2dkegiatan keg on akun.kegiatanindex=keg.kegiatanindex "
                    + " inner join rinciandthskpd rinci on keg.rinciandthindex=rinci.rinciandthindex "
                    + " inner join dth on rinci.dthindex=dth.dthindex "
                    + " where tahunanggaran=" + tahun + " and periode <=" + bulan + " and kodesatker='"+kodeSatker+"'  and sumberdana= " + sumberDana
                    + " group by kodekeg, namakeg, kodeakun, namaakun "
                    + " order by kodekeg");
            while (rs.next()) { 
                result.add(new NilaiFungsiPemda(
                        rs.getString("kodekeg").trim() + " " + rs.getString("namakeg").trim(), 
                        rs.getString("namaakun"), rs.getDouble("nilai")));
            }
            return result;
        } catch (SQLException e) {
            Logger.getLogger(OfficeSQL.class.getName()).log(Level.SEVERE, null, e);
            throw new SQLException("SQL: Gagal mengambil Data Realisasi Belanja Berdasarkan Sumber Dana \n" + e.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    /**
     *
     * @param year
     * @param kodeSatker
     * @param kodeData
     * @param jenisCOA
     * @param conn
     * @return
     * @throws SQLException
     */
    @Override
    public List<LampiranIPerdaAPBD> getLampiranIPenjabaranAPBDbyYear(short year, String kodeSatker, short kodeData, short jenisCOA, Connection conn) throws SQLException {
        List<LampiranIPerdaAPBD> result = new ArrayList();
        Statement stm = null;
        ResultSet rs = null;
        String sql = "with utama as " 
                + " (select rek.kodeakunutama kode, rek.namaakunutama nama, " 
                + " sum(rek.nilaianggaran) jumlah " 
                + " from koderekapbd rek " 
                + " inner join kegiatanapbd keg on rek.kegiatanindex=keg.kegiatanindex " 
                + " inner join apbd on keg.apbdindex=apbd.apbdindex " 
                + " where apbd.tahunanggaran= " + year
                + " and apbd.kodedata= " + kodeData + " and apbd.jeniscoa= " + jenisCOA
                + " and apbd.kodesatker='" + kodeSatker + "' " 
                + " group by kode, nama " 
                + " order by kode " 
                + " ), " 
                + " kelompok as " 
                + " ( " 
                + " select (rek.kodeakunutama||'.'||rek.kodeakunkelompok) kode, " 
                + " rek.namaakunkelompok nama, " 
                + " sum(rek.nilaianggaran) jumlah " 
                + " from koderekapbd rek " 
                + " inner join kegiatanapbd keg on rek.kegiatanindex=keg.kegiatanindex " 
                + " inner join apbd on keg.apbdindex=apbd.apbdindex " 
                + " where apbd.tahunanggaran= " + year
                + " and apbd.kodedata= " + kodeData + " and apbd.jeniscoa= " + jenisCOA 
                + " and apbd.kodesatker='" + kodeSatker + "' " 
                + " group by kode, nama " 
                + " order by kode " 
                + " ), " 
                + " jenis as " 
                + " ( " 
                + " select (rek.kodeakunutama || '.' || rek.kodeakunkelompok || '.' || rek.kodeakunjenis) kode, rek.namaakunjenis nama, " 
                + " sum(rek.nilaianggaran) jumlah " 
                + " from koderekapbd rek " 
                + " inner join kegiatanapbd keg on rek.kegiatanindex=keg.kegiatanindex  " 
                + " inner join apbd on keg.apbdindex=apbd.apbdindex " 
                + " where apbd.tahunanggaran= " + year
                + " and apbd.kodedata= " + kodeData + " and apbd.jeniscoa= " + jenisCOA
                + " and apbd.kodesatker='" + kodeSatker + "' " 
                + " group by kode, nama " 
                + " order by kode " 
                + " ), " 
                + " objek as " 
                + " ( " 
                + " select (rek.kodeakunutama || '.' || rek.kodeakunkelompok || '.' || rek.kodeakunjenis || '.' || rek.kodeakunobjek) kode, rek.namaakunobjek nama, " 
                + " sum(rek.nilaianggaran) jumlah " 
                + " from koderekapbd rek " 
                + " inner join kegiatanapbd keg on rek.kegiatanindex=keg.kegiatanindex " 
                + " inner join apbd on keg.apbdindex=apbd.apbdindex " 
                + " where apbd.tahunanggaran= " + year
                + " and apbd.kodedata= " + kodeData + " and apbd.jeniscoa= " + jenisCOA
                + " and apbd.kodesatker='" + kodeSatker + "' " 
                + " group by kode, nama " 
                + " order by kode " 
                + " ), " 
                + " rinci as "
                + " ( " 
                + " select (rek.kodeakunutama || '.' || rek.kodeakunkelompok || '.' || rek.kodeakunjenis || '.' || rek.kodeakunobjek || '.' || rek.kodeakunrincian) kode, rek.namaakunrincian nama, " 
                + " sum(rek.nilaianggaran) jumlah " 
                + " from koderekapbd rek " 
                + " inner join kegiatanapbd keg on rek.kegiatanindex=keg.kegiatanindex " 
                + " inner join apbd on keg.apbdindex=apbd.apbdindex " 
                + " where apbd.tahunanggaran= " + year
                + " and apbd.kodedata= " + kodeData + " and apbd.jeniscoa= " + jenisCOA
                + " and apbd.kodesatker='" + kodeSatker + "' " 
                + " group by kode, nama " 
                + " order by kode " 
                + " ) " 
                + " select * from utama " 
                + " union select * from kelompok " 
                + " union select * from jenis " 
                + " union select * from objek " 
                + " union select * from rinci " 
                + " order by kode ";
        try {
            stm = conn.createStatement();
            rs = stm.executeQuery(sql);
            while (rs.next()) {
                result.add(new LampiranIPerdaAPBD(rs.getString("kode"), rs.getString("nama"), rs.getDouble("jumlah")));
            }

            return result;
        } catch (SQLException ex) {
            Logger.getLogger(OfficeSQL.class.getName()).log(Level.SEVERE, null, ex);
            throw new SQLException("SQL: Gagal mengambil data Lampiran I Penjabaran apbd \n" + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    /**
     *
     * @param level
     * @param conn
     * @return
     * @throws SQLException
     */
    @Override
    public List<MappingAccount> getMappingAccounts(short level, Connection conn) throws SQLException {
        List<MappingAccount> result = new ArrayList();
        Statement stm = null;
        ResultSet rs = null;
        String sql = "select ca.sequenceindex cashIndex, ca.accountcode cashCode, ca.accountname cashName, " 
                + " aa.autoindex accrualIndex, aa.accountcode accrualCode, aa.accountname accrualName " 
                + " from cashaccount ca " 
                + " left join mappingaccount ma on ca.sequenceindex=ma.cashaccount " 
                + " left join accrualaccount aa on ma.accrualaccount=aa.autoindex " 
                + " where ca.accounttype= " + level + " and (ca.accountcode like '4%' or ca.accountcode like '5%' or ca.accountcode like '6%') "
                + " order by cashCode";
        try {
            stm = conn.createStatement();
            rs = stm.executeQuery(sql);
            while (rs.next()) {
                result.add(new MappingAccount(rs.getLong("cashIndex"), rs.getString("cashCode"), rs.getString("cashName"), new SimpleAccount(rs.getLong("accrualIndex"), rs.getString("accrualCode"), rs.getString("accrualName"))));
            }

            return result;
        } catch (SQLException ex) {
            Logger.getLogger(OfficeSQL.class.getName()).log(Level.SEVERE, null, ex);
            throw new SQLException("SQL: Gagal mengambil data Mapping Kode Rekening \n" + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    /**
     *
     * @param level
     * @param conn
     * @return
     * @throws SQLException
     */
    @Override
    public List<SimpleAccount> getAccrualAccounts(short[] level, Connection conn) throws SQLException {
        List<SimpleAccount> result = new ArrayList();
        Statement stm = null;
        ResultSet rs = null;
        String sql = "select aa.autoindex accrualIndex, aa.accountcode accrualCode, aa.accountname accrualName " 
                + " from accrualaccount aa";
        String where = "";
        if(level!= null && level.length>0){
            for (int i = 0; i < level.length; i++) {
                short s = level[i];
                if( !where.trim().equals("") )
                    where = where + " or ";
                where = where + " aa.accounttype=" + s;
            }            
        }
        if( !where.trim().equals("") )
            sql = sql + " where " + where;
        sql = sql + " order by accrualCode";
        try {
            stm = conn.createStatement();
            rs = stm.executeQuery(sql);
            while (rs.next()) {
                result.add(new SimpleAccount(rs.getLong("accrualIndex"), rs.getString("accrualCode"), rs.getString("accrualName")));
            }

            return result;
        } catch (SQLException ex) {
            Logger.getLogger(OfficeSQL.class.getName()).log(Level.SEVERE, null, ex);
            throw new SQLException("SQL: Gagal mengambil data Kode Rekening Akrual \n" + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    /**
     *
     * @param ma
     * @param conn
     * @throws SQLException
     */
    @Override
    public void createMappingAccounts(MappingAccount ma, Connection conn) throws SQLException {
        Statement stm = null;
        String sqlD = " delete from mappingaccount where cashaccount=" + ma.getIndex();
        String sql = " insert into mappingaccount values(" + ma.getIndex() + "," + ma.getMapAccount().getIndex() + ")";
        try {
            stm = conn.createStatement();
            stm.executeUpdate(sqlD);
            stm.executeUpdate(sql);
        } catch (SQLException ex) {
            Logger.getLogger(OfficeSQL.class.getName()).log(Level.SEVERE, null, ex);
            throw new SQLException("SQL: Gagal Input data Mapping Kode Rekening \n" + ex.getMessage());
        } finally {
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    /**
     *
     * @param ma
     * @param conn
     * @throws SQLException
     */
    @Override
    public void deleteMappingAccounts(MappingAccount ma, Connection conn) throws SQLException {
        Statement stm = null;
        String sqlD = " delete from mappingaccount where cashaccount=" + ma.getIndex() + " and accrualaccount = " + ma.getMapAccount().getIndex();
        try {
            stm = conn.createStatement();
            stm.executeUpdate(sqlD);
        } catch (SQLException ex) {
            Logger.getLogger(OfficeSQL.class.getName()).log(Level.SEVERE, null, ex);
            throw new SQLException("SQL: Gagal Hapus data Mapping Kode Rekening \n" + ex.getMessage());
        } finally {
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    
    public void deleteGfsMappingAccounts(MappingAccount ma, Connection conn) throws SQLException {
        Statement stm = null;
        String sqlD = " delete from mappingaccrualgfs where accrualaccount=" + ma.getIndex() + " and "
                + " gfsaccount = " + ma.getMapAccount().getIndex();
        try {
            stm = conn.createStatement();
            stm.executeUpdate(sqlD);
        } catch (SQLException ex) {
            Logger.getLogger(OfficeSQL.class.getName()).log(Level.SEVERE, null, ex);
            throw new SQLException("SQL: Gagal Hapus data Mapping Kode Rekening GFS \n" + ex.getMessage());
        } finally {
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    public void createGfsMappingAccounts(MappingAccount ma, Connection conn) throws SQLException {
        Statement stm = null;
        String sqlD = " delete from mappingaccrualgfs where accrualaccount=" + ma.getIndex();
        String sql = " insert into mappingaccrualgfs values(" + ma.getIndex() + "," + ma.getMapAccount().getIndex() + ")";
        try {
            stm = conn.createStatement();
            stm.executeUpdate(sqlD);
            stm.executeUpdate(sql);
        } catch (SQLException ex) {
            Logger.getLogger(OfficeSQL.class.getName()).log(Level.SEVERE, null, ex);
            throw new SQLException("SQL: Gagal Input data Mapping Kode Rekening GFS \n" + ex.getMessage());
        } finally {
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    public List<MappingAccount> getGfsMappingAccounts(short[] levels, Connection conn) throws SQLException{
        List<MappingAccount> result = new ArrayList();
        Statement stm = null;
        ResultSet rs = null;
        String sql = "select aa.autoindex accrualIndex, aa.accountcode accrualCode, aa.accountname accrualName, " 
                + " g.autoindex gfsIndex, g.accountcode gfsCode, g.accountname gfsName " 
                + " from accrualaccount aa "
                + " left join mappingaccrualgfs ma on aa.autoindex=ma.accrualaccount "
                + " left join gfsaccount g on ma.gfsaccount=g.autoindex ";
        String where = "";
        if(levels!=null && levels.length>0){
            for (int i = 0; i < levels.length; i++) {
                short s = levels[i];
                if( !where.trim().equals("") ) where = where + " or ";
                where = where + " aa.accounttype="+s;                
            }            
        }
        if( !where.trim().equals("") )
            sql = sql + " where " + where;
        
        sql = sql + " order by accrualCode";
        try {
            stm = conn.createStatement();
            rs = stm.executeQuery(sql);
            while (rs.next()) {
                result.add(new MappingAccount(rs.getLong("accrualIndex"), rs.getString("accrualCode"), rs.getString("accrualName"), new SimpleAccount(rs.getLong("gfsIndex"), rs.getString("gfsCode"), rs.getString("gfsName"))));
            }

            return result;
        } catch (SQLException ex) {
            Logger.getLogger(OfficeSQL.class.getName()).log(Level.SEVERE, null, ex);
            throw new SQLException("SQL: Gagal mengambil data Mapping Kode Rekening GFS \n" + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    
    public List<SimpleAccount> getGfsAccounts(short[] level, Connection conn) throws SQLException{
        List<SimpleAccount> result = new ArrayList();
        Statement stm = null;
        ResultSet rs = null;
        String sql = "select aa.autoindex gfsIndex, aa.accountcode gfsCode, aa.accountname gfsName " 
                + " from gfsaccount aa";
        String where = "";
        if(level!= null && level.length>0){
            for (int i = 0; i < level.length; i++) {
                short s = level[i];
                if( !where.trim().equals("") )
                    where = where + " or ";
                where = where + " aa.accounttype=" + s;
            }            
        }
        if( !where.trim().equals("") )
            sql = sql + " where " + where;
        sql = sql + " order by gfsCode";
        try {
            stm = conn.createStatement();
            rs = stm.executeQuery(sql);
            while (rs.next()) {
                result.add(new SimpleAccount(rs.getLong("gfsIndex"), rs.getString("gfsCode"), rs.getString("gfsName")));
            }

            return result;
        } catch (SQLException ex) {
            Logger.getLogger(OfficeSQL.class.getName()).log(Level.SEVERE, null, ex);
            throw new SQLException("SQL: Gagal mengambil data Kode Rekening GFS \n" + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    /**
     *
     * @param level
     * @param tahun
     * @param kodeSatker
     * @param conn
     * @return
     * @throws SQLException
     */
    public List<MappingNonStandar> getMappingNonStandarAccountApbds(short level, short tahun, String kodeSatker, Connection conn) throws SQLException {
        List<MappingNonStandar> result = new ArrayList();
        Statement stm = null;
        ResultSet rs = null;
        String sql = "with kodeAcc as(";
        if(level==5){
                sql = sql + "  select kegiatanindex, (kodeakunutama||'.'||kodeakunkelompok || '.' ||kodeakunjenis||'.'||kodeakunobjek||'.'||kodeakunrincian) kode, trim(namaakunrincian) nama ";
        }
        else if(level==4){
                sql = sql + "  select kegiatanindex, (kodeakunutama||'.'||kodeakunkelompok || '.' ||kodeakunjenis||'.'||kodeakunobjek) kode, trim(namaakunobjek) nama ";
        }
        else if(level==3){
            sql = sql + "  select kegiatanindex, (kodeakunutama||'.'||kodeakunkelompok || '.' ||kodeakunjenis) kode, trim(namaakunjenis) nama ";
        }
        
        sql = sql + "  from koderekapbd ";
        if(level==5)
            sql = sql + "  where (kodeakunutama||'.'||kodeakunkelompok || '.' ||kodeakunjenis||'.'||kodeakunobjek||'.'||kodeakunrincian) not in(select accountcode from cashaccount) ";
        else if(level==4)
            sql = sql + "  where (kodeakunutama||'.'||kodeakunkelompok || '.' ||kodeakunjenis||'.'||kodeakunobjek) not in(select accountcode from cashaccount) ";
        else if(level==3)
                sql = sql + "  where (kodeakunutama||'.'||kodeakunkelompok || '.' ||kodeakunjenis) not in(select accountcode from cashaccount) ";
        
        sql = sql + "  order by kode) "
                + "  select distinct a.kodesatker, a.kodepemda, trim(a.namapemda) namapemda, acc.kode, acc.nama, "
                + " acr.autoindex acrindex, acr.accountcode acrcode, acr.accountname acrname, map.sequenceindex mapindex "
                + "  from kodeAcc acc "
                + " inner join kegiatanapbd k on acc.kegiatanindex=k.kegiatanindex "
                + "                inner join apbd a on k.apbdindex=a.apbdindex "
                + "                left join mappingaccountnon map on a.kodesatker=map.kodesatker and acc.kode=map.accountcode "
                + "                left join accrualaccount acr on map.accrualaccount=acr.autoindex "
                + " where tahunanggaran=" + tahun + " and jeniscoa=1 ";
        if (kodeSatker != null && !kodeSatker.trim().equals(""))
                sql = sql + " and a.kodesatker='" + kodeSatker + "' ";
        sql = sql + " order by kodepemda, kode";
        try {
            stm = conn.createStatement();
            rs = stm.executeQuery(sql);
            while (rs.next()) {
                MappingNonStandar mm = new MappingNonStandar(rs.getLong("mapindex"), rs.getString("kodesatker"), rs.getString("kodepemda"), rs.getString("namapemda"), rs.getString("kode"), rs.getString("nama"));                
                mm.setAccrualAccount(new SimpleAccount(rs.getLong("acrindex"), rs.getString("acrcode"), rs.getString("acrname")));
                result.add(mm);
            }

            return result;
        } catch (SQLException ex) {
            Logger.getLogger(OfficeSQL.class.getName()).log(Level.SEVERE, null, ex);
            throw new SQLException("SQL: Gagal mengambil data Mapping Kode Rekening Non Standar Apbd\n" + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    
    /**
     *
     * @param level
     * @param tahun
     * @param kodeSatker
     * @param conn
     * @return
     * @throws SQLException
     */
    public List<MappingNonStandar> getMappingNonStandarAccountLras(short level, short tahun, String kodeSatker, Connection conn) throws SQLException {
        List<MappingNonStandar> result = new ArrayList();
        Statement stm = null;
        ResultSet rs = null;
        String sql = "with kodeAcc as(";
        if(level==5)
                sql = sql + "  select kegiatanindex, (kodeakunutama||'.'||kodeakunkelompok || '.' ||kodeakunjenis||'.'||kodeakunobjek||'.'||kodeakunrincian) kode, trim(namaakunrincian) nama ";
        else if(level==4)
                sql = sql + "  select kegiatanindex, (kodeakunutama||'.'||kodeakunkelompok || '.' ||kodeakunjenis||'.'||kodeakunobjek) kode, trim(namaakunobjek) nama ";
        else if(level==3)
            sql = sql + "  select kegiatanindex, (kodeakunutama||'.'||kodeakunkelompok || '.' ||kodeakunjenis) kode, trim(namaakunjenis) nama ";
        
        sql = sql + "  from realisasikoderekapbd ";
        if(level==5)
            sql = sql + "  where (kodeakunutama||'.'||kodeakunkelompok || '.' ||kodeakunjenis||'.'||kodeakunobjek||'.'||kodeakunrincian) not in(select accountcode from cashaccount) ";
        else if(level==4)
            sql = sql + "  where (kodeakunutama||'.'||kodeakunkelompok || '.' ||kodeakunjenis||'.'||kodeakunobjek) not in(select accountcode from cashaccount) ";
        else if(level==3)
                sql = sql + "  where (kodeakunutama||'.'||kodeakunkelompok || '.' ||kodeakunjenis) not in(select accountcode from cashaccount) ";
        
        sql = sql + "  order by kode) "
                + "  select distinct a.kodesatker, a.kodepemda, trim(a.namapemda) namapemda, acc.kode, acc.nama, "
                + " acr.autoindex acrindex, acr.accountcode acrcode, acr.accountname acrname, map.sequenceindex mapindex "
                + "  from kodeAcc acc "
                + " inner join realisasikegiatanapbd k on acc.kegiatanindex=k.kegiatanindex "
                + "                inner join realisasiapbd a on k.apbdindex=a.apbdindex "
                + "                left join mappingaccountnon map on a.kodesatker=map.kodesatker and acc.kode=map.accountcode "
                + "                left join accrualaccount acr on map.accrualaccount=acr.autoindex "
                + " where tahunanggaran=" + tahun + " and jeniscoa=1 ";
        if (kodeSatker != null && !kodeSatker.trim().equals(""))
                sql = sql + " and a.kodesatker='" + kodeSatker + "' ";
        sql = sql + " order by kodepemda, kode";
        try {
            stm = conn.createStatement();
            rs = stm.executeQuery(sql);
            while (rs.next()) {
                MappingNonStandar mm = new MappingNonStandar(rs.getLong("mapindex"), rs.getString("kodesatker"), rs.getString("kodepemda"), rs.getString("namapemda"), rs.getString("kode"), rs.getString("nama"));                
                mm.setAccrualAccount(new SimpleAccount(rs.getLong("acrindex"), rs.getString("acrcode"), rs.getString("acrname")));
                result.add(mm);
            }

            return result;
        } catch (SQLException ex) {
            Logger.getLogger(OfficeSQL.class.getName()).log(Level.SEVERE, null, ex);
            throw new SQLException("SQL: Gagal mengambil data Mapping Kode Rekening Non Standar Lra\n" + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    
    /**
     *
     * @param level
     * @param tahun
     * @param kodeSatker
     * @param conn
     * @return
     * @throws SQLException
     */
    public List<MappingNonStandar> getMappingNonStandarAccountDths(short level, short tahun, String kodeSatker, Connection conn) throws SQLException {
        List<MappingNonStandar> result = new ArrayList();
        Statement stm = null;
        ResultSet rs = null;
        String sql = "with kodeAcc as(";
        if(level==5)
                sql = sql + "  select kegiatanindex, (kodeakunutama||'.'||kodeakunkelompok || '.' ||kodeakunjenis||'.'||kodeakunobjek||'.'||kodeakunrincian) kode, trim(namaakunrincian) nama ";
        if(level==4)
                sql = sql + "  select kegiatanindex, (kodeakunutama||'.'||kodeakunkelompok || '.' ||kodeakunjenis||'.'||kodeakunobjek) kode, trim(namaakunobjek) nama ";
        else if(level==3)
            sql = sql + "  select kegiatanindex, (kodeakunutama||'.'||kodeakunkelompok || '.' ||kodeakunjenis) kode, trim(namaakunjenis) nama ";
        
        sql = sql + "  from akundthskpd ";
        if(level==5)
            sql = sql + "  where (kodeakunutama||'.'||kodeakunkelompok || '.' ||kodeakunjenis||'.'||kodeakunobjek||'.'||kodeakunrincian) not in(select accountcode from cashaccount) ";
        else if(level==4)
            sql = sql + "  where (kodeakunutama||'.'||kodeakunkelompok || '.' ||kodeakunjenis||'.'||kodeakunobjek) not in(select accountcode from cashaccount) ";
        else if(level==3)
                sql = sql + "  where (kodeakunutama||'.'||kodeakunkelompok || '.' ||kodeakunjenis) not in(select accountcode from cashaccount) ";
        
        sql = sql + "  order by kode) "
                + "  select distinct a.kodesatker, a.kodepemda, trim(a.namapemda) namapemda, acc.kode, acc.nama, "
                + " acr.autoindex acrindex, acr.accountcode acrcode, acr.accountname acrname, map.sequenceindex mapindex "
                + "  from kodeAcc acc "
                + " inner join sp2dkegiatan k on acc.kegiatanindex=k.kegiatanindex "
                + " inner join rinciandthskpd rinci on k.rinciandthindex=rinci.rinciandthindex "                
                + "                inner join dth a on rinci.dthindex=a.dthindex "
                + "                left join mappingaccountnon map on a.kodesatker=map.kodesatker and acc.kode=map.accountcode "
                + "                left join accrualaccount acr on map.accrualaccount=acr.autoindex "
                + " where tahunanggaran=" + tahun;
        if (kodeSatker != null && !kodeSatker.trim().equals(""))
                sql = sql + " and a.kodesatker='" + kodeSatker + "' ";
        sql = sql + " order by kodepemda, kode";
        try {
            stm = conn.createStatement();
            rs = stm.executeQuery(sql);
            while (rs.next()) {
                MappingNonStandar mm = new MappingNonStandar(rs.getLong("mapindex"), rs.getString("kodesatker"), rs.getString("kodepemda"), rs.getString("namapemda"), rs.getString("kode"), rs.getString("nama"));                
                mm.setAccrualAccount(new SimpleAccount(rs.getLong("acrindex"), rs.getString("acrcode"), rs.getString("acrname")));
                result.add(mm);
            }

            return result;
        } catch (SQLException ex) {
            Logger.getLogger(OfficeSQL.class.getName()).log(Level.SEVERE, null, ex);
            throw new SQLException("SQL: Gagal mengambil data Mapping Kode Rekening Non Standar Dth\n" + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    /**
     *
     * @param level
     * @param tahun
     * @param kodeSatker
     * @param conn
     * @return
     * @throws SQLException
     */
    public List<MappingNonStandar> getMappingNonStandarAccountNeracas(short level, short tahun, String kodeSatker, Connection conn) throws SQLException {
        List<MappingNonStandar> result = new ArrayList();
        Statement stm = null;
        ResultSet rs = null;
        String sql = "with kodeAcc as( ";
        if(level==3)
            sql = sql + " select u.neracaindex, (u.kodeakunutama||'.'||k.kodeakunkelompok||'.'||j.kodeakunjenis) kode, j.namaakunjenis nama ";
        else 
            sql = sql + "  select u.neracaindex, (u.kodeakunutama||'.'||k.kodeakunkelompok||'.'||j.kodeakunjenis||'.'||o.kodeakunobjek) kode, trim(namaakunobjek) nama ";
        
        sql = sql + " from neracakodrekutama u  "
                + " inner join neracakodrekkelompok k on u.akunutamaindex=k.kodrekutamaindex "
                + " inner join neracakodrekjenis j on k.akunkelompokindex=j.kodrekkelompokindex ";
        if(level==3)
            sql = sql + " where (u.kodeakunutama||'.'||k.kodeakunkelompok||'.'||j.kodeakunjenis) not in (select accountcode from cashaccount) ";
        else
            sql = sql + "inner join neracakodrekobjek o on j.akunjenisindex=o.kodrekjenisindex "
                    + " where (u.kodeakunutama||'.'||k.kodeakunkelompok||'.'||j.kodeakunjenis||'.'||o.kodeakunobjek) not in (select accountcode from cashaccount) ";
        
        sql = sql + " order by kode "
                + ") "
                + "select distinct n.kodesatker, n.kodepemda, trim(n.namapemda) namapemda, acc.kode, acc.nama, "
                + "acr.autoindex acrindex, "
                + "  acr.accountcode acrcode, "
                + "  acr.accountname acrname, "
                + "  map.sequenceindex mapindex "
                + "  from kodeAcc acc "
                + "inner join neraca n on acc.neracaindex=n.neracaindex "
                + "left join mappingaccountnon map on n.kodesatker=map.kodesatker and acc.kode=map.accountcode "
                + "left join accrualaccount acr on map.accrualaccount=acr.autoindex "
                + "where "
                + "tahunanggaran=" + tahun;
        if (kodeSatker != null && !kodeSatker.trim().equals(""))
                sql = sql + " and n.kodesatker='" + kodeSatker + "' ";
        sql = sql + " order by kodepemda, kode";
        try {
            stm = conn.createStatement();
            rs = stm.executeQuery(sql);
            while (rs.next()) {
                MappingNonStandar mm = new MappingNonStandar(rs.getLong("mapindex"), rs.getString("kodesatker"), rs.getString("kodepemda"), rs.getString("namapemda"), rs.getString("kode"), rs.getString("nama"));                
                mm.setAccrualAccount(new SimpleAccount(rs.getLong("acrindex"), rs.getString("acrcode"), rs.getString("acrname")));
                result.add(mm);
            }

            return result;
        } catch (SQLException ex) {
            Logger.getLogger(OfficeSQL.class.getName()).log(Level.SEVERE, null, ex);
            throw new SQLException("SQL: Gagal mengambil data Mapping Kode Rekening Non Standar Neraca\n" + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    
    
    /**
     *
     * @param ma
     * @param conn
     * @return 
     * @throws SQLException
     */
    @Override
    public MappingNonStandar createMappingAccountNonStandars(MappingNonStandar ma, Connection conn) throws SQLException {
        PreparedStatement stm = null;
//        String sqlD = " delete from mappingaccountnon where sequenceindex=?";
        String sql = " with xx as ( "
                + "insert into mappingaccountnon (kodesatker, accountcode, accountname, accrualaccount) "
                + "values (?, ?, ?, ?) returning * ) select max(sequenceindex) as id from xx";
        String sqlE = " update mappingaccountnon  set accrualaccount= ?";
        
        try {
            if(ma.getIndex()>0){
                stm = conn.prepareStatement(sqlE);
                stm.setLong(1, ma.getAccrualAccount().getIndex());
                stm.executeUpdate();
            }
            else{
                stm = conn.prepareStatement(sql);
                stm.setString(1, ma.getKodeSatker());
                stm.setString(2, ma.getKodeAkun());
                stm.setString(3, ma.getNama());
                stm.setLong(4, ma.getAccrualAccount().getIndex());                
                ResultSet rs = stm.executeQuery();
                if(rs.next()) ma.setIndex(rs.getLong("id"));
            }
            stm = conn.prepareStatement("select * from accrualaccount where autoindex=?");
            stm.setLong(1, ma.getAccrualAccount().getIndex());
            ResultSet rs = stm.executeQuery();
            if( rs.next() ){
                SimpleAccount sacc= new SimpleAccount(rs.getLong("autoindex"), rs.getString("accountcode"), rs.getString("accountname"));
                ma.setAccrualAccount(sacc);
            }
            return ma;
        } catch (SQLException ex) {
            Logger.getLogger(OfficeSQL.class.getName()).log(Level.SEVERE, null, ex);
            throw new SQLException("SQL: Gagal Input data Mapping Kode Rekening Non Standar \n" + ex.getMessage());
        } finally {
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    /**
     *
     * @param ma
     * @param conn
     * @throws SQLException
     */
    @Override
    public void deleteMappingAccountNonStandars(MappingNonStandar ma, Connection conn) throws SQLException {
        Statement stm = null;
        String sqlD = "delete from mappingaccountnon where sequenceindex="+ma.getIndex();
        try {
            stm = conn.createStatement();
            stm.executeUpdate(sqlD);
        } catch (SQLException ex) {
            Logger.getLogger(OfficeSQL.class.getName()).log(Level.SEVERE, null, ex);
            throw new SQLException("SQL: Gagal Hapus data Mapping Kode Rekening Non Standar \n" + ex.getMessage());
        } finally {
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    
    
    /**
     *
     * @param kodeSatker
     * @param tahun
     * @param bulan
     * @param kodeData
     * @param jenisCOA
     * @param userName
     * @param password
     * @param conn
     * @return
     * @throws SQLException
     */
    @Override
    public RealisasiAPBD getLRA(String kodeSatker, short tahun , short bulan, short kodeData, short jenisCOA, String userName, String password, Connection conn) throws SQLException{
        PreparedStatement pstm = null;
        Statement stm = null;
        ResultSet rs = null;
        String sql = "select * from realisasiapbd where kodesatker=? and tahunanggaran=? and bulan=? and kodedata=? and jeniscoa=?";
        try {
            pstm = conn.prepareStatement(sql);
            pstm.setString(1, kodeSatker);
            pstm.setShort(2, tahun);
            pstm.setShort(3, bulan);
            pstm.setShort(4, kodeData);
            pstm.setShort(5, jenisCOA);
            rs = pstm.executeQuery();
            
            if (rs.next()) {
                return new RealisasiAPBD(rs.getLong("apbdindex"), kodeSatker, rs.getString("kodepemda"), rs.getString("namapemda"), tahun, bulan, kodeData, jenisCOA, rs.getShort("statusdata"), rs.getString("nomorPerda"), (rs.getDate("tanggalperda")==null?null:new java.util.Date(rs.getDate("tanggalper").getTime())), userName, password, rs.getString("namaAplikasi"), rs.getString("pengembangAplikasi"));
            }
            return null;
        } catch (SQLException e) {
            Logger.getLogger(OfficeSQL.class.getName()).log(Level.SEVERE, null, e);
            throw new SQLException("SQL: Gagal mengambil Data LRA \n" + e.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (pstm != null) {
                pstm.close();
            }
        }        
    }
    
    /**
     *
     * @param indexLRA
     * @param conn
     * @return
     * @throws SQLException
     */
    @Override
    public List<RealisasiKegiatanAPBD> getLRAKegiatan(long indexLRA, Connection conn) throws SQLException{
        PreparedStatement pstm = null;
        Statement stm = null;
        ResultSet rs = null;
        String sql = "select * from realisasikegiatanapbd where apbdindex=?";
        List<RealisasiKegiatanAPBD> result = new ArrayList();
        try {
            pstm = conn.prepareStatement(sql);
            pstm.setLong(1, indexLRA);
            rs = pstm.executeQuery();
            
            while (rs.next()) {
                result.add(new RealisasiKegiatanAPBD(rs.getLong("kegiatanindex"), rs.getString("kodeurusanprogram"), rs.getString("namaurusanprogram"), rs.getString("kodeurusanpelaksana"), rs.getString("namaurusanpelaksana"), rs.getString("kodeskpd"), rs.getString("namaskpd"), rs.getString("kodeprogram"), rs.getString("namaprogram"), rs.getString("kodekegiatan"), rs.getString("namakegiatan"), rs.getString("kodefungsi"), rs.getString("namafungsi")));
            }
            return result;
        } catch (SQLException e) {
            Logger.getLogger(OfficeSQL.class.getName()).log(Level.SEVERE, null, e);
            throw new SQLException("SQL: Gagal mengambil Data Kegiatan LRA \n" + e.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (pstm != null) {
                pstm.close();
            }
        }        
    }
    
    /**
     *
     * @param indexKegiatanLRA
     * @param conn
     * @return
     * @throws SQLException
     */
    @Override
    public List<RealisasiKodeRekeningAPBD> getLRAAkun(long indexKegiatanLRA, Connection conn) throws SQLException{
        PreparedStatement pstm = null;
        Statement stm = null;
        ResultSet rs = null;
        String sql = "select * from realisasikoderekapbd where kegiatanindex=?";
//        Vector v = new Vector();
        List<RealisasiKodeRekeningAPBD> result = new ArrayList();
        try {
            pstm = conn.prepareStatement(sql);
            pstm.setLong(1, indexKegiatanLRA);
            rs = pstm.executeQuery();
            
            while (rs.next()) {
               result.add(new RealisasiKodeRekeningAPBD(rs.getString("kodeakunutama"), rs.getString("namaakunutama"), rs.getString("kodeakunkelompok"), rs.getString("namaakunkelompok"), rs.getString("kodeAkunjenis"), rs.getString("namaakunjenis"), rs.getString("kodeakunobjek"), rs.getString("namaakunobjek") , rs.getString("kodeakunrincian"), rs.getString("namaakunrincian"), rs.getString("kodeAkunsub") , rs.getString("namaakunsub"), rs.getDouble("nilaianggaran")));            
            }
            return result;
        } catch (SQLException e) {
            Logger.getLogger(OfficeSQL.class.getName()).log(Level.SEVERE, null, e);
            throw new SQLException("SQL: Gagal mengambil Data Kode Rekening Kegiatan LRA \n" + e.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (pstm != null) {
                pstm.close();
            }
        }
        
    }
    

    
}
