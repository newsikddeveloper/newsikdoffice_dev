/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.office.ejb.dbapi;

import app.sikd.entity.backoffice.AkunKompilasi;
import app.sikd.entity.backoffice.AkunKompilasiGFS;
import app.sikd.entity.backoffice.KompilasiApbd1364;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author sora
 */
public interface IKompilasiSQL {
    public void deleteApbdKompilasi64(short tahun, short kodeData, Connection conn) throws SQLException;
    public List<KompilasiApbd1364> getApbd13Kompilasi64(short tahun, short kodeData, Connection conn) throws SQLException;
    public KompilasiApbd1364 createApbdKompilasi64(KompilasiApbd1364 apbd1364, Connection conn) throws SQLException;
    public List<AkunKompilasiGFS> getApbdKompilasi64(short tahun, short kodeData, String kodepemda, Connection conn) throws SQLException;
//    public KompilasiKegiatanApbd1364 createKegiatanApbdKompilasi64(long indexApbd, KompilasiKegiatanApbd1364 kegiatan, Connection conn) throws SQLException;
    
    public List<AkunKompilasi> getApbdKompilasiLangsungs(short tahun, List<String> pemdaCodes, Connection conn) throws SQLException;
    
}
