/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.office.ejb.dbapi;

import app.sikd.entity.utilitas.MappingAccount;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author sora
 */
public interface INotifikasiSQL {
    public short getApbdNotifikasi(short tahun, String kodeSatker, short kodeData, short jeniscoa, Connection conn) throws SQLException;
    public short getLraNotifikasi(short tahun, short periode, String kodeSatker, short kodeData, short jeniscoa, Connection conn) throws SQLException;
    public short getDthNotifikasi(short tahun, short periode, String kodeSatker, Connection conn) throws SQLException;
    
    public short getNeracaNotifikasi(short tahun, short semester, String kodeSatker, Connection conn) throws SQLException;
    public short getArusKasNotifikasi(short tahun, String kodeSatker, Connection conn) throws SQLException;
    public short getLONotifikasi(short tahun, short triwulan, String kodeSatker, Connection conn) throws SQLException;
    public short getEkuitasNotifikasi(short tahun, String kodeSatker, Connection conn) throws SQLException;
    public short getSalNotifikasi(short tahun, String kodeSatker, Connection conn) throws SQLException;
    public short getPFKNotifikasi(short tahun, String kodeSatker, Connection conn) throws SQLException;
    public short getPinjamanDaerahNotifikasi(short tahun, String kodeSatker, Connection conn) throws SQLException;
    
    public List<MappingAccount> getAllNotifikasi(short year, String kodesatker, short jenisCOA, Connection conn) throws SQLException;
    
}
