/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.sikd.office.ejb.newdbapi;

import app.sikd.entity.Waktu;
import app.sikd.entity.backoffice.setting.Account;
import app.sikd.entity.backoffice.setting.StatusData;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author sora
 */
public interface INewSettingSQL {

    /**
     *
     * @param conn
     * @return List<Account>
     * @throws SQLException
     */
    public List<Account> getParentCashAccount(Connection conn) throws SQLException;
    public List<Account> getSubCashAccount(long idParent, Connection conn) throws SQLException;
    
    public List<StatusData> getStatusDatas(Connection conn) throws SQLException;
    public StatusData getStatusDataByKode(short kode, Connection conn) throws SQLException;
    public void createStatusData(StatusData obj, Connection conn) throws SQLException;
    public void deleteStatusData(short kode, Connection conn) throws SQLException;
    public void updateStatusData(StatusData oldObj, StatusData newObj, Connection conn) throws SQLException;
    
    public List<StatusData> getStatusDatasByUserGroup(long idGroup, Connection conn) throws SQLException;
    
    public List<Waktu> getWaktus(Connection conn) throws SQLException;
    public Waktu getWaktuByIkd(String ikd, Connection conn) throws SQLException;
    public void updateWaktu(Waktu obj, Connection conn) throws SQLException;
    

}
