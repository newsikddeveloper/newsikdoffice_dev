package app.sikd.office.ejb.newdbapi;

import app.sikd.entity.Waktu;
import app.sikd.entity.backoffice.setting.Account;
import app.sikd.entity.backoffice.setting.StatusData;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author sora
 */
public class NewSettingSQL  implements INewSettingSQL{

    /**
     *
     * @param conn
     * @return List<Account>
     * @throws SQLException
     */
    public List<Account> getParentCashAccount(Connection conn) throws SQLException{
        List<Account> result = new ArrayList();
        Statement stm = null;
        ResultSet rs = null;

        try {
            stm = conn.createStatement();
            rs = stm.executeQuery("select * from cashaccount where sequenceindex not in(select subaccount from cashaccountstructure) order by accountcode");
            while (rs.next()) {
                result.add(new Account(rs.getLong("sequenceindex"), rs.getString("accountcode"), rs.getString("accountname"), rs.getShort("accounttype"), rs.getBoolean("groupaccount")));
            }

            return result;
        } catch (SQLException ex) {
            Logger.getLogger(NewSettingSQL.class.getName()).log(Level.SEVERE, null, ex);
            throw new SQLException("SQL: Gagal mengambil data  Kode Rekening PMDN 13 \n" + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }

    public List<Account> getSubCashAccount(long idParent, Connection conn) throws SQLException {
        List<Account> result = new ArrayList();
        Statement stm = null;
        ResultSet rs = null;

        try {
            stm = conn.createStatement();
            rs = stm.executeQuery("select * from cashaccount where sequenceindex in(select subaccount from cashaccountstructure where parentaccount=" + idParent + ") order by accountcode");
            while (rs.next()) {
                result.add(new Account(rs.getLong("sequenceindex"), rs.getString("accountcode"), rs.getString("accountname"), rs.getShort("accounttype"), rs.getBoolean("groupaccount")));
            }

            return result;
        } catch (SQLException ex) {
            Logger.getLogger(NewSettingSQL.class.getName()).log(Level.SEVERE, null, ex);
            throw new SQLException("SQL: Gagal mengambil data  Kode Rekening PMDN 13 \n" + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }

    public List<StatusData> getStatusDatas(Connection conn) throws SQLException {
        List<StatusData> result = new ArrayList();
        Statement stm = null;
        ResultSet rs = null;

        try {
            stm = conn.createStatement();
            rs = stm.executeQuery("select * from tstatusdata order by kode");
            while (rs.next()) {
                result.add(new StatusData(rs.getShort("kode"), rs.getString("nama")));
            }

            return result;
        } catch (SQLException ex) {
            Logger.getLogger(NewSettingSQL.class.getName()).log(Level.SEVERE, null, ex);
            throw new SQLException("SQL: Gagal mengambil data StatusData \n" + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    public List<StatusData> getStatusDatasByUserGroup(long idGroup, Connection conn) throws SQLException {
        List<StatusData> result = new ArrayList();
        Statement stm = null;
        ResultSet rs = null;

        try {
            stm = conn.createStatement();
            rs = stm.executeQuery("select ts.* from tstatusdata ts "
                    + " where kode in(select kode from tgroupstatusdata where usergroup=" + idGroup + ")"
                    + " order by kode");
            while (rs.next()) {
                result.add(new StatusData(rs.getShort("kode"), rs.getString("nama")));
            }

            return result;
        } catch (SQLException ex) {
            Logger.getLogger(NewSettingSQL.class.getName()).log(Level.SEVERE, null, ex);
            throw new SQLException("SQL: Gagal mengambil data StatusData \n" + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    
    public StatusData getStatusDataByKode(short kode, Connection conn) throws SQLException {
        StatusData result = null;
        Statement stm = null;
        ResultSet rs = null;

        try {
            stm = conn.createStatement();
            rs = stm.executeQuery("select * from tstatusdata where kode="+ kode);
            if (rs.next()) {
                result = new StatusData(rs.getShort("kode"), rs.getString("nama"));
            }

            return result;
        } catch (SQLException ex) {
            Logger.getLogger(NewSettingSQL.class.getName()).log(Level.SEVERE, null, ex);
            throw new SQLException("SQL: Gagal mengambil data StatusData \n" + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    public void createStatusData(StatusData obj, Connection conn) throws SQLException {
        Statement stm = null;
        String sql = "INSERT INTO tstatusdata VALUES("+obj.getKode()+", '"+obj.getNama()+"')";
        try {
            stm = conn.createStatement();
            stm.executeUpdate(sql);
        } catch (SQLException ex) {
            Logger.getLogger(NewSettingSQL.class.getName()).log(Level.SEVERE, null, ex);
            throw new SQLException("SQL: Gagal input data StatusData \n" + ex.getMessage());
        } finally {
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    public void deleteStatusData(short kode, Connection conn) throws SQLException {
        Statement stm = null;
        String sql = "DELETE from tstatusdata where kode="+kode;
        try {
            stm = conn.createStatement();
            stm.executeUpdate(sql);
        } catch (SQLException ex) {
            Logger.getLogger(NewSettingSQL.class.getName()).log(Level.SEVERE, null, ex);
            throw new SQLException("SQL: Gagal hapus data StatusData \n" + ex.getMessage());
        } finally {
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    public void updateStatusData(StatusData oldObj, StatusData newObj, Connection conn) throws SQLException {
        Statement stm = null;
        String sql = "UPDATE tstatusdata set kode="+newObj.getKode()+", nama= '"+newObj.getNama()+"' "
                + " where kode="+oldObj.getKode();
        try {
            stm = conn.createStatement();
            stm.executeUpdate(sql);
        } catch (SQLException ex) {
            Logger.getLogger(NewSettingSQL.class.getName()).log(Level.SEVERE, null, ex);
            throw new SQLException("SQL: Gagal merubah data StatusData \n" + ex.getMessage());
        } finally {
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    public List<Waktu> getWaktus(Connection conn) throws SQLException {
        List<Waktu> result = new ArrayList();
        Statement stm = null;
        ResultSet rs = null;

        try {
            stm = conn.createStatement();
            rs = stm.executeQuery("select * from twaktu order by id");
            while (rs.next()) {
                result.add(new Waktu(rs.getString("ikd"), rs.getShort("tanggal"), rs.getShort("bulan")));
            }

            return result;
        } catch (SQLException ex) {
            Logger.getLogger(NewSettingSQL.class.getName()).log(Level.SEVERE, null, ex);
            throw new SQLException("SQL: Gagal mengambil data Waktu \n" + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    public Waktu getWaktuByIkd(String ikd, Connection conn) throws SQLException {
        Statement stm = null;
        ResultSet rs = null;

        try {
            stm = conn.createStatement();
            rs = stm.executeQuery("select * from twaktu "
                    + " where ikd = '" + ikd + "' order by id");
            if (rs.next()) {
                return new Waktu(rs.getString("ikd"), rs.getShort("tanggal"), rs.getShort("bulan"));
            }

            return null;
        } catch (SQLException ex) {
            Logger.getLogger(NewSettingSQL.class.getName()).log(Level.SEVERE, null, ex);
            throw new SQLException("SQL: Gagal mengambil data Waktu \n" + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }

    public void updateWaktu(Waktu obj, Connection conn) throws SQLException {
        Statement stm = null;
        String sql = "UPDATE twaktu set tanggal="+obj.getTanggal()+", bulan= "+obj.getBulan()
                + " where ikd='" + obj.getIkd() + "'";
        try {
            stm = conn.createStatement();
            stm.executeUpdate(sql);
        } catch (SQLException ex) {
            Logger.getLogger(NewSettingSQL.class.getName()).log(Level.SEVERE, null, ex);
            throw new SQLException("SQL: Gagal merubah data Waktu \n" + ex.getMessage());
        } finally {
            if (stm != null) {
                stm.close();
            }
        }
    }

}
