package app.sikd.office.ejb.newdbapi;

import app.sikd.entity.Pemda;
import app.sikd.entity.backoffice.StatusPemda;
import app.sikd.entity.backoffice.setting.StatusData;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author sora
 */
public class NewStatusSQL implements NewStatusSQLInterface {

    public List<StatusPemda> getStatusDataLraByPemda(List<Pemda> pemdas, short tahun, short periode, Connection conn) throws SQLException {
        Statement stm = null;
        ResultSet rs = null;
        String skodedata = "";
        if(periode>0 && periode <13) skodedata = "Laporan Realisasi APBD Periode " + periode;
        else if(periode == 13) skodedata = "LRA Pertanggungjawaban ";
        
        String sql = "select * from realisasiapbd apbd "
                + " inner join tstatusdata on apbd.statusdata=tstatusdata.kode "
                + " where tahunanggaran=" + tahun + " and bulan= " + periode;
        String p = "";
        List<StatusPemda> result = new ArrayList();
        if (pemdas != null && !pemdas.isEmpty()) {
            for (Pemda pe : pemdas) {
                if (!p.trim().equals("")) {
                    p = p + " or ";
                }
                p = p + " kodesatker='" + pe.getKodeSatker().trim() + "'";
                result.add(new StatusPemda(pe, new StatusData((short)-1, ""), skodedata, "color:red;"));
            }
        }
        if (!p.trim().equals("")) {
            p = "(" + p + ")";
            sql = sql + " and " + p;
        }
        sql = sql + " order by kodepemda ";
        try {
            stm = conn.createStatement();
            rs = stm.executeQuery(sql);
            while (rs.next()) {
                String s = rs.getString("kodesatker");
                short stat = rs.getShort("statusdata");
                StatusPemda sp = new StatusPemda(new Pemda(s), skodedata, "");
                int ii = findpemda(result, s);
                if(ii>-1){
                    result.get(ii).setStatusData(new StatusData(rs.getShort("kode"), rs.getString("nama")));
                    result.get(ii).setStyle("color: black;");
                }
            }
            return result;
        } catch (SQLException ex) {
            Logger.getLogger(NewSettingSQL.class.getName()).log(Level.SEVERE, null, ex);
            throw new SQLException("SQL: Gagal mengambil data StatusData LRA \n" + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }

    public List<StatusPemda> getStatusDataNeracaByPemda(List<Pemda> pemdas, short tahun, short semester, Connection conn) throws SQLException {
        Statement stm = null;
        ResultSet rs = null;
        String skodedata = "Neraca Semester " + semester;
        
        String sql = "select * from neraca n "
                + " inner join tstatusdata on n.statusdata=tstatusdata.kode "
                + " where tahunanggaran=" + tahun + " and semester= " + semester;        
        String p = "";
        List<StatusPemda> result = new ArrayList();
        if (pemdas != null && !pemdas.isEmpty()) {
            for (Pemda pe : pemdas) {
                if (!p.trim().equals("")) {
                    p = p + " or ";
                }
                p = p + " kodesatker='" + pe.getKodeSatker().trim() + "'";
                result.add(new StatusPemda(pe, new StatusData((short)-1, ""), skodedata, "color:red;"));
            }
        }
        if (!p.trim().equals("")) {
            p = "(" + p + ")";
            sql = sql + " and " + p;
        }
        sql = sql + " order by kodepemda ";
        try {
            stm = conn.createStatement();
            rs = stm.executeQuery(sql);
            while (rs.next()) {
                String s = rs.getString("kodesatker");
                short stat = rs.getShort("statusdata");
                StatusPemda sp = new StatusPemda(new Pemda(s), skodedata, "");
                int ii = findpemda(result, s);
                if(ii>-1){
                    result.get(ii).setStatusData(new StatusData(rs.getShort("kode"), rs.getString("nama")));
                    result.get(ii).setStyle("color: black;");
                }
            }
            return result;
        } catch (SQLException ex) {
            Logger.getLogger(NewSettingSQL.class.getName()).log(Level.SEVERE, null, ex);
            throw new SQLException("SQL: Gagal mengambil data StatusData Neraca \n" + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }

    public List<StatusPemda> getStatusDataArusKasByPemda(List<Pemda> pemdas, short tahun, Connection conn) throws SQLException {
        Statement stm = null;
        ResultSet rs = null;
        String sql = "select * from aruskas n "
                + " inner join tstatusdata on n.statusdata=tstatusdata.kode "
                + " where tahunanggaran=" + tahun;
        String p = "";
        List<StatusPemda> result = new ArrayList();
        if (pemdas != null && !pemdas.isEmpty()) {
            for (Pemda pe : pemdas) {
                if (!p.trim().equals("")) {
                    p = p + " or ";
                }
                p = p + " kodesatker='" + pe.getKodeSatker().trim() + "'";
                result.add(new StatusPemda(pe, new StatusData((short)-1, ""), "Arus Kas", "color:red;"));
            }
        }
        if (!p.trim().equals("")) {
            p = "(" + p + ")";
            sql = sql + " and " + p;
        }
        sql = sql + " order by kodepemda ";
        try {
            stm = conn.createStatement();
            rs = stm.executeQuery(sql);
            while (rs.next()) {
                String s = rs.getString("kodesatker");
                short stat = rs.getShort("statusdata");
                StatusPemda sp = new StatusPemda(new Pemda(s), "Arus Kas", "");
                int ii = findpemda(result, s);
                if(ii>-1){
                    result.get(ii).setStatusData(new StatusData(rs.getShort("kode"), rs.getString("nama")));
                    result.get(ii).setStyle("color: black;");
                }
            }
            return result;
        } catch (SQLException ex) {
            Logger.getLogger(NewSettingSQL.class.getName()).log(Level.SEVERE, null, ex);
            throw new SQLException("SQL: Gagal mengambil data StatusData Arus Kas \n" + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }

    public List<StatusPemda> getStatusDataApbdByPemda(List<Pemda> pemdas, short tahun, short kodeData, Connection conn) throws SQLException {
        Statement stm = null;
        ResultSet rs = null;
        String skodedata = "";
        if( kodeData == 0 ) skodedata = "APBD Anggaran ";
        else if( kodeData == 1 ) skodedata = "APBD Perubahan ";
        String sql = "select * from apbd "
                + " inner join tstatusdata on apbd.statusdata=tstatusdata.kode "
                + " where tahunanggaran=" + tahun + " and kodedata= " + kodeData;
        String p = "";
        List<StatusPemda> result = new ArrayList();
        if (pemdas != null && !pemdas.isEmpty()) {
            for (Pemda pe : pemdas) {
                if (!p.trim().equals("")) {
                    p = p + " or ";
                }
                p = p + " kodesatker='" + pe.getKodeSatker().trim() + "'";
                result.add(new StatusPemda(pe, new StatusData((short)-1, ""), skodedata, "color: red;"));
            }
        }
        if (!p.trim().equals("")) {
            p = "(" + p + ")";
            sql = sql + " and " + p;
        }
        sql = sql + " order by kodepemda ";       
        try {
            stm = conn.createStatement();
            rs = stm.executeQuery(sql);
            while (rs.next()) {
                String s = rs.getString("kodesatker");
                short stat = rs.getShort("statusdata");
                String namastat = rs.getString("nama");
                
                StatusPemda sp = new StatusPemda(new Pemda(s), skodedata, "");
                int ii = findpemda(result, s);
                if(ii>-1){
                    result.get(ii).setStatusData(new StatusData(rs.getShort("kode"), namastat));
                    result.get(ii).setStyle("color: black;");
                }
            }
            return result;
        } catch (SQLException ex) {
            Logger.getLogger(NewSettingSQL.class.getName()).log(Level.SEVERE, null, ex);
            throw new SQLException("SQL: Gagal mengambil data StatusData APBD \n" + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    int findpemda(List<StatusPemda> list, String kodeSatker){
        int result=-1;
        boolean find= false;
        int ii = 0;
        while(ii<list.size() && find == false){
            if(list.get(ii).getPemda().getKodeSatker().trim().equals(kodeSatker.trim())){
                result = ii;
                find = true;
            }
            ii++;
        }
        return result;
    }

    public List<StatusPemda> getStatusDataLOByPemda(List<Pemda> pemdas, short tahun, short triwulan, Connection conn) throws SQLException {
        Statement stm = null;
        ResultSet rs = null;
        String skodedata= "Laporan Operasional Triwulan "+ triwulan;
        
        String sql = "select * from lo "
                + " inner join tstatusdata on lo.statusdata=tstatusdata.kode "
                + " where tahunanggaran=" + tahun + " and triwulan= " + triwulan;
        String p = "";
        List<StatusPemda> result = new ArrayList();
        if (pemdas != null && !pemdas.isEmpty()) {
            for (Pemda pe : pemdas) {
                if (!p.trim().equals("")) {
                    p = p + " or ";
                }
                p = p + " kodesatker='" + pe.getKodeSatker().trim() + "'";
                result.add(new StatusPemda(pe, new StatusData((short)-1, ""), skodedata, "color:red;"));
            }
        }
        if (!p.trim().equals("")) {
            p = "(" + p + ")";
            sql = sql + " and " + p;
        }
        sql = sql + " order by kodepemda ";
        try {
            stm = conn.createStatement();
            rs = stm.executeQuery(sql);
            while (rs.next()) {
                String s = rs.getString("kodesatker");
                short stat = rs.getShort("statusdata");
                StatusPemda sp = new StatusPemda(new Pemda(s), skodedata, "");
                int ii = findpemda(result, s);
                if(ii>-1){
                    result.get(ii).setStatusData(new StatusData(rs.getShort("kode"), rs.getString("nama")));
                    result.get(ii).setStyle("color: black;");
                }
            }
            return result;
        } catch (SQLException ex) {
            Logger.getLogger(NewSettingSQL.class.getName()).log(Level.SEVERE, null, ex);
            throw new SQLException("SQL: Gagal mengambil data StatusData LO \n" + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }

    public List<StatusPemda> getStatusDataDTHByPemda(List<Pemda> pemdas, short tahun, short periode, Connection conn) throws SQLException {
        Statement stm = null;
        ResultSet rs = null;
        String skodedata = "DTH Periode " + periode;
        String sql = "select * from dth "
                + " inner join tstatusdata on dth.statusdata=tstatusdata.kode "
                + " where tahunanggaran=" + tahun + " and periode= " + periode;
        String p = "";
        List<StatusPemda> result = new ArrayList();
        if (pemdas != null && !pemdas.isEmpty()) {
            for (Pemda pe : pemdas) {
                if (!p.trim().equals("")) {
                    p = p + " or ";
                }
                p = p + " kodesatker='" + pe.getKodeSatker().trim() + "'";
                result.add(new StatusPemda(pe, new StatusData((short)-1, ""), skodedata, "color:red;"));
            }
        }
        if (!p.trim().equals("")) {
            p = "(" + p + ")";
            sql = sql + " and " + p;
        }
        sql = sql + " order by kodepemda ";
        try {
            stm = conn.createStatement();
            rs = stm.executeQuery(sql);
            while (rs.next()) {
                String s = rs.getString("kodesatker");
                short stat = rs.getShort("statusdata");
                StatusPemda sp = new StatusPemda(new Pemda(s), skodedata,"");
                int ii = findpemda(result, s);
                if(ii>-1){
                    result.get(ii).setStatusData(new StatusData(rs.getShort("kode"), rs.getString("nama")));
                    result.get(ii).setStyle("color: black;");
                }
            }
            return result;
        } catch (SQLException ex) {
            Logger.getLogger(NewSettingSQL.class.getName()).log(Level.SEVERE, null, ex);
            throw new SQLException("SQL: Gagal mengambil data StatusData DTH \n" + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }

    public List<StatusPemda> getStatusDataPFKByPemda(List<Pemda> pemdas, short tahun, Connection conn) throws SQLException {
        Statement stm = null;
        ResultSet rs = null;
        String sql = "select * from pfk "
                + " inner join tstatusdata on pfk.statusdata=tstatusdata.kode "
                + " where tahunanggaran=" + tahun;
        String p = "";
        List<StatusPemda> result = new ArrayList();
        if (pemdas != null && !pemdas.isEmpty()) {
            for (Pemda pe : pemdas) {
                if (!p.trim().equals("")) {
                    p = p + " or ";
                }
                p = p + " kodesatker='" + pe.getKodeSatker().trim() + "'";
                result.add(new StatusPemda(pe, new StatusData((short)-1, ""), "Perhitungan Fihak Ketiga", "color:red;"));
            }
        }
        if (!p.trim().equals("")) {
            p = "(" + p + ")";
            sql = sql + " and " + p;
        }
        sql = sql + " order by kodepemda ";
        try {
            stm = conn.createStatement();
            rs = stm.executeQuery(sql);
            while (rs.next()) {
                String s = rs.getString("kodesatker");
                short stat = rs.getShort("statusdata");
                StatusPemda sp = new StatusPemda(new Pemda(s), "Perhitungan Fihak Ketiga","");
                int ii = findpemda(result, s);
                if(ii>-1){
                    result.get(ii).setStatusData(new StatusData(rs.getShort("kode"), rs.getString("nama")));
                    result.get(ii).setStyle("color: black;");
                }
            }
            return result;
        } catch (SQLException ex) {
            Logger.getLogger(NewSettingSQL.class.getName()).log(Level.SEVERE, null, ex);
            throw new SQLException("SQL: Gagal mengambil data StatusData PFK \n" + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }

    public List<StatusPemda> getStatusDataPinjamanDaerahByPemda(List<Pemda> pemdas, short tahun, Connection conn) throws SQLException {
        Statement stm = null;
        ResultSet rs = null;
        String sql = "select * from pinjamandaerah pd "
                + " inner join tstatusdata on pd.statusdata=tstatusdata.kode "
                + " where tahunanggaran=" + tahun;
        String p = "";
        List<StatusPemda> result = new ArrayList();
        if (pemdas != null && !pemdas.isEmpty()) {
            for (Pemda pe : pemdas) {
                if (!p.trim().equals("")) {
                    p = p + " or ";
                }
                p = p + " kodesatker='" + pe.getKodeSatker().trim() + "'";
                result.add(new StatusPemda(pe, new StatusData((short)-1, ""), "Pinjaman Daerah ", "color:red;"));
            }
        }
        if (!p.trim().equals("")) {
            p = "(" + p + ")";
            sql = sql + " and " + p;
        }
        sql = sql + " order by kodepemda ";
        try {
            stm = conn.createStatement();
            rs = stm.executeQuery(sql);
            while (rs.next()) {
                String s = rs.getString("kodesatker");
                short stat = rs.getShort("statusdata");
                StatusPemda sp = new StatusPemda(new Pemda(s), "Pinjaman Daerah", "");
                int ii = findpemda(result, s);
                if(ii>-1){
                    result.get(ii).setStatusData(new StatusData(rs.getShort("kode"), rs.getString("nama")));
                    result.get(ii).setStyle("color: black;");
                }
            }
            return result;
        } catch (SQLException ex) {
            Logger.getLogger(NewSettingSQL.class.getName()).log(Level.SEVERE, null, ex);
            throw new SQLException("SQL: Gagal mengambil data StatusData Pinjaman Daerah \n" + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    public List<StatusPemda> getStatusDataLPSalByPemda(List<Pemda> pemdas, short tahun, Connection conn) throws SQLException {
        Statement stm = null;
        ResultSet rs = null;
        String sql = "select * from perubahansal sal "
                + " inner join tstatusdata on sal.statusdata=tstatusdata.kode "
                + " where tahunanggaran=" + tahun;
        String p = "";
        List<StatusPemda> result = new ArrayList();
        if (pemdas != null && !pemdas.isEmpty()) {
            for (Pemda pe : pemdas) {
                if (!p.trim().equals("")) {
                    p = p + " or ";
                }
                p = p + " kodesatker='" + pe.getKodeSatker().trim() + "'";
                result.add(new StatusPemda(pe, new StatusData((short)-1, ""), "Laporan Perubahan Saldo", "color: red;"));
            }
        }
        if (!p.trim().equals("")) {
            p = "(" + p + ")";
            sql = sql + " and " + p;
        }
        sql = sql + " order by kodepemda ";       
        try {
            stm = conn.createStatement();
            rs = stm.executeQuery(sql);
            while (rs.next()) {
                String s = rs.getString("kodesatker");
                short stat = rs.getShort("statusdata");
                String namastat = rs.getString("nama");
                
                StatusPemda sp = new StatusPemda(new Pemda(s), "Laporan Perubahan Saldo", "");
                int ii = findpemda(result, s);
                if(ii>-1){
                    result.get(ii).setStatusData(new StatusData(rs.getShort("kode"), namastat));
                    result.get(ii).setStyle("color: black;");
                }
            }
            return result;
        } catch (SQLException ex) {
            Logger.getLogger(NewSettingSQL.class.getName()).log(Level.SEVERE, null, ex);
            throw new SQLException("SQL: Gagal mengambil data StatusData LPSaldo \n" + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    public List<StatusPemda> getStatusDataLpeByPemda(List<Pemda> pemdas, short tahun, Connection conn) throws SQLException {
        Statement stm = null;
        ResultSet rs = null;
        String sql = "select * from perubahanekuitas lpe "
                + " inner join tstatusdata on lpe.statusdata=tstatusdata.kode "
                + " where tahunanggaran=" + tahun;
        String p = "";
        List<StatusPemda> result = new ArrayList();
        if (pemdas != null && !pemdas.isEmpty()) {
            for (Pemda pe : pemdas) {
                if (!p.trim().equals("")) {
                    p = p + " or ";
                }
                p = p + " kodesatker='" + pe.getKodeSatker().trim() + "'";
                result.add(new StatusPemda(pe, new StatusData((short)-1, ""), "Laporan Perubahan Ekuitas", "color: red;"));
            }
        }
        if (!p.trim().equals("")) {
            p = "(" + p + ")";
            sql = sql + " and " + p;
        }
        sql = sql + " order by kodepemda ";       
        try {
            stm = conn.createStatement();
            rs = stm.executeQuery(sql);
            while (rs.next()) {
                String s = rs.getString("kodesatker");
                short stat = rs.getShort("statusdata");
                String namastat = rs.getString("nama");
                
                StatusPemda sp = new StatusPemda(new Pemda(s), "Laporan Perubahan Ekuitas", "");
                int ii = findpemda(result, s);
                if(ii>-1){
                    result.get(ii).setStatusData(new StatusData(rs.getShort("kode"), namastat));
                    result.get(ii).setStyle("color: black;");
                }
            }
            return result;
        } catch (SQLException ex) {
            Logger.getLogger(NewSettingSQL.class.getName()).log(Level.SEVERE, null, ex);
            throw new SQLException("SQL: Gagal mengambil data StatusData LPE \n" + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    public void updateStatusDataLraByPemda(StatusPemda statusPemda, short tahun, short periode, Connection conn) throws SQLException {
        Statement stm = null;
        String sql = "UPDATE realisasiapbd "
                + " set statusdata=" + statusPemda.getStatusData().getKode()
                + " where tahunanggaran=" + tahun + " and bulan= " + periode
                + " and kodesatker='" + statusPemda.getPemda().getKodeSatker().trim() + "'";
        try {
            stm = conn.createStatement();
            stm.executeUpdate(sql);
        } catch (SQLException ex) {
            Logger.getLogger(NewSettingSQL.class.getName()).log(Level.SEVERE, null, ex);
            throw new SQLException("SQL: Gagal merubah data StatusData LRA \n" + ex.getMessage());
        } finally {
            if (stm != null) {
                stm.close();
            }
        }
    }
//    public void updateStatusDataLraByPemda(StatusPemda statusPemda, short tahun, short periode, short kodeData, Connection conn) throws SQLException {
//        Statement stm = null;
//        String sql = "UPDATE realisasiapbd "
//                + " set statusdata=" + statusPemda.getStatusData().getKode()
//                + " where tahunanggaran=" + tahun + " and bulan= " + periode
//                + " and kodedata= " + kodeData + " and kodesatker='" + statusPemda.getPemda().getKodeSatker().trim() + "'";
//        try {
//            stm = conn.createStatement();
//            stm.executeUpdate(sql);
//        } catch (SQLException ex) {
//            Logger.getLogger(NewSettingSQL.class.getName()).log(Level.SEVERE, null, ex);
//            throw new SQLException("SQL: Gagal merubah data StatusData LRA \n" + ex.getMessage());
//        } finally {
//            if (stm != null) {
//                stm.close();
//            }
//        }
//    }

    public void updateStatusDataNeracaByPemda(StatusPemda statusPemda, short tahun, short semester, Connection conn) throws SQLException {
        Statement stm = null;
        
        String sql = "Update neraca set statusdata=" + statusPemda.getStatusData().getKode()
                + " where tahunanggaran=" + tahun + " and semester= " + semester
                +" kodesatker='" + statusPemda.getPemda().getKodeSatker().trim() + "'";
        try {
            stm = conn.createStatement();
            stm.executeUpdate(sql);
        } catch (SQLException ex) {
            Logger.getLogger(NewSettingSQL.class.getName()).log(Level.SEVERE, null, ex);
            throw new SQLException("SQL: Gagal merubah data StatusData Neraca \n" + ex.getMessage());
        } finally {
            if (stm != null) {
                stm.close();
            }
        }
    }

    public void updateStatusDataArusKasByPemda(StatusPemda statusPemda, short tahun, Connection conn) throws SQLException {
        Statement stm = null;
        ResultSet rs = null;
        String sql = "update aruskas set statusdata= " +statusPemda.getStatusData().getKode()
                + " where tahunanggaran=" + tahun 
                + " and kodesatker='" + statusPemda.getPemda().getKodeSatker().trim() + "'";
        try {
            stm = conn.createStatement();
            stm.executeUpdate(sql);
        } catch (SQLException ex) {
            Logger.getLogger(NewSettingSQL.class.getName()).log(Level.SEVERE, null, ex);
            throw new SQLException("SQL: Gagal merubah data StatusData Arus Kas \n" + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }

    public void updateStatusDataApbdByPemda(StatusPemda statusPemda, short tahun, short kodeData, Connection conn) throws SQLException {
        Statement stm = null;
        String skodedata = "";
        String sql = "update apbd set statusdata = " + statusPemda.getStatusData().getKode()
                + " where tahunanggaran=" + tahun + " and kodedata= " + kodeData
                + " and kodesatker='" + statusPemda.getPemda().getKodeSatker().trim() + "'";
        try {
            stm = conn.createStatement();
            stm.executeUpdate(sql);
        } catch (SQLException ex) {
            Logger.getLogger(NewSettingSQL.class.getName()).log(Level.SEVERE, null, ex);
            throw new SQLException("SQL: Gagal merubah data StatusData APBD \n" + ex.getMessage());
        } finally {
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    public void updateStatusDataLOByPemda(StatusPemda statusPemda, short tahun, short triwulan, Connection conn) throws SQLException {
        Statement stm = null;
        
        String sql = "update lo set statusdata = " + statusPemda.getStatusData().getKode()
                + " where tahunanggaran=" + tahun + " and triwulan= " + triwulan
                + " and kodesatker='" + statusPemda.getPemda().getKodeSatker().trim() + "'";
        try {
            stm = conn.createStatement();
            stm.executeUpdate(sql);
        } catch (SQLException ex) {
            Logger.getLogger(NewSettingSQL.class.getName()).log(Level.SEVERE, null, ex);
            throw new SQLException("SQL: Gagal merubah data StatusData LO \n" + ex.getMessage());
        } finally {
            if (stm != null) {
                stm.close();
            }
        }
    }

    public void updateStatusDataDTHByPemda(StatusPemda statusPemda, short tahun, short periode, Connection conn) throws SQLException {
        Statement stm = null;
        String sql = "update dth set statusdata=" + statusPemda.getStatusData().getKode()
                + " where tahunanggaran=" + tahun + " and periode= " + periode
                + " and kodesatker='" + statusPemda.getPemda().getKodeSatker().trim() + "'";
        try {
            stm = conn.createStatement();
            stm.executeUpdate(sql);
        } catch (SQLException ex) {
            Logger.getLogger(NewSettingSQL.class.getName()).log(Level.SEVERE, null, ex);
            throw new SQLException("SQL: Gagal merubah data StatusData DTH \n" + ex.getMessage());
        } finally {
            if (stm != null) {
                stm.close();
            }
        }
    }

    public void updateStatusDataPFKByPemda(StatusPemda statusPemda, short tahun, Connection conn) throws SQLException {
        Statement stm = null;
        String sql = "update pfk set statusdata = " + statusPemda.getStatusData().getKode()
                + " where tahunanggaran=" + tahun
                + " and kodesatker='" + statusPemda.getPemda().getKodeSatker().trim() + "'";
        try {
            stm = conn.createStatement();
            stm.executeUpdate(sql);
        } catch (SQLException ex) {
            Logger.getLogger(NewSettingSQL.class.getName()).log(Level.SEVERE, null, ex);
            throw new SQLException("SQL: Gagal merubah data StatusData PFK \n" + ex.getMessage());
        } finally {
            if (stm != null) {
                stm.close();
            }
        }
    }

    public void updateStatusDataPinjamanDaerahByPemda(StatusPemda statusPemda, short tahun, Connection conn) throws SQLException {
        Statement stm = null;
        String sql = "update pinjamandaerah set statusdata=" + statusPemda.getStatusData().getKode() 
                + " where tahunanggaran=" + tahun
                + " and kodesatker='" + statusPemda.getPemda().getKodeSatker().trim() + "'";
        try {
            stm = conn.createStatement();
            stm.executeUpdate(sql);
        } catch (SQLException ex) {
            Logger.getLogger(NewSettingSQL.class.getName()).log(Level.SEVERE, null, ex);
            throw new SQLException("SQL: Gagal merubah data StatusData Pinjaman Daerah \n" + ex.getMessage());
        } finally {
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    public void updateStatusDataLPSalByPemda(StatusPemda statusPemda, short tahun, Connection conn) throws SQLException {
        Statement stm = null;
        String sql = "update perubahansal set statusdata= "  + statusPemda.getStatusData().getKode()
                + " where tahunanggaran=" + tahun
                + " and kodesatker='" + statusPemda.getPemda().getKodeSatker().trim() + "'";
        try {
            stm = conn.createStatement();
            stm.executeUpdate(sql);
        } catch (SQLException ex) {
            Logger.getLogger(NewSettingSQL.class.getName()).log(Level.SEVERE, null, ex);
            throw new SQLException("SQL: Gagal merubah data StatusData LPSaldo \n" + ex.getMessage());
        } finally {
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    public void updateStatusDataLpeByPemda(StatusPemda statusPemda, short tahun, Connection conn) throws SQLException {
        Statement stm = null;
        String sql = "update perubahanekuitas set statusdata= "+ statusPemda.getStatusData().getKode()
                + " where tahunanggaran=" + tahun
                + " and kodesatker='" + statusPemda.getPemda().getKodeSatker().trim() + "'";
        try {
            stm = conn.createStatement();
            stm.executeUpdate(sql);
        } catch (SQLException ex) {
            Logger.getLogger(NewSettingSQL.class.getName()).log(Level.SEVERE, null, ex);
            throw new SQLException("SQL: Gagal merubah data StatusData LPE \n" + ex.getMessage());
        } finally {
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    public List<StatusPemda> getStatusDataByPemda(List<Pemda> pemdas, short tahun, Connection conn) throws SQLException {
        Statement stm = null;
        ResultSet rs = null;        
        List<StatusPemda> result = new ArrayList();
        try {
            List<StatusPemda> apbdM = getStatusDataApbdByPemda(pemdas, tahun, (short)0, conn); 
            if(apbdM!=null && !apbdM.isEmpty())
                result.add(apbdM.get(0));
            
            List<StatusPemda> apbdP = getStatusDataApbdByPemda(pemdas, tahun, (short)1, conn);
            if(apbdP!=null && !apbdP.isEmpty()) result.add(apbdP.get(0));
            for (int i = 1; i < 14; i++) {
                List<StatusPemda> lra1 = getStatusDataLraByPemda(pemdas, tahun, (short)i, conn);
//                List<StatusPemda> lra1 = getStatusDataLraByPemda(pemdas, tahun, (short)i, (short) 0, conn);
//                if (lra1 == null || lra1.isEmpty()) {
//                    lra1 = getStatusDataLraByPemda(pemdas, tahun, (short)i, (short) 1, conn);
//                }
                if(lra1!=null && !lra1.isEmpty())
                    result.add(lra1.get(0));
            }
            /*List<StatusPemda> lra1 = getStatusDataLraByPemda(pemdas, tahun, (short)1, (short)0, conn);
            if( lra1==null || lra1.isEmpty() ) 
                lra1 = getStatusDataLraByPemda(pemdas, tahun, (short)1, (short)1, conn);
            result.add(lra1.get(0));
            List<StatusPemda> lra2 = getStatusDataLraByPemda(pemdas, tahun, (short)2, (short)0, conn);
            if( lra2==null || lra2.isEmpty() ) 
                lra2 = getStatusDataLraByPemda(pemdas, tahun, (short)2, (short)1, conn);
            result.add(lra2.get(0));
            List<StatusPemda> lra3 = getStatusDataLraByPemda(pemdas, tahun, (short)3, (short)0, conn);
            if( lra3==null || lra3.isEmpty() ) 
                lra3 = getStatusDataLraByPemda(pemdas, tahun, (short)3, (short)1, conn);
            result.add(lra3.get(0));
            List<StatusPemda> lra4 = getStatusDataLraByPemda(pemdas, tahun, (short)4, (short)0, conn);
            if( lra4==null || lra4.isEmpty() ) 
                lra4 = getStatusDataLraByPemda(pemdas, tahun, (short)4, (short)1, conn);
            result.add(lra4.get(0));
            List<StatusPemda> lra5 = getStatusDataLraByPemda(pemdas, tahun, (short)5, (short)0, conn);
            if( lra5==null || lra5.isEmpty() ) 
                lra5 = getStatusDataLraByPemda(pemdas, tahun, (short)5, (short)1, conn);
            result.add(lra5.get(0));
            List<StatusPemda> lra6 = getStatusDataLraByPemda(pemdas, tahun, (short)6, (short)0, conn);
            if( lra6==null || lra6.isEmpty() ) 
                lra6 = getStatusDataLraByPemda(pemdas, tahun, (short)6, (short)1, conn);
            result.add(lra6.get(0));
            List<StatusPemda> lra7 = getStatusDataLraByPemda(pemdas, tahun, (short)7, (short)0, conn);
            if( lra7==null || lra7.isEmpty() ) 
                lra7 = getStatusDataLraByPemda(pemdas, tahun, (short)7, (short)1, conn);
            result.add(lra7.get(0));
            List<StatusPemda> lra8 = getStatusDataLraByPemda(pemdas, tahun, (short)8, (short)0, conn);
            if( lra8==null || lra8.isEmpty() ) 
                lra8 = getStatusDataLraByPemda(pemdas, tahun, (short)8, (short)1, conn);
            result.add(lra8.get(0));
            List<StatusPemda> lra9 = getStatusDataLraByPemda(pemdas, tahun, (short)9, (short)0, conn);
            if( lra9==null || lra9.isEmpty() ) 
                lra9 = getStatusDataLraByPemda(pemdas, tahun, (short)9, (short)1, conn);
            result.add(lra9.get(0));
            List<StatusPemda> lra10 = getStatusDataLraByPemda(pemdas, tahun, (short)10, (short)0, conn);
            if( lra10==null || lra10.isEmpty() ) 
                lra10 = getStatusDataLraByPemda(pemdas, tahun, (short)10, (short)1, conn);
            result.add(lra10.get(0));
            List<StatusPemda> lra11 = getStatusDataLraByPemda(pemdas, tahun, (short)11, (short)0, conn);
            if( lra11==null || lra11.isEmpty() ) 
                lra11 = getStatusDataLraByPemda(pemdas, tahun, (short)11, (short)1, conn);
            result.add(lra11.get(0));
            List<StatusPemda> lra12 = getStatusDataLraByPemda(pemdas, tahun, (short)12, (short)0, conn);
            if( lra12==null || lra12.isEmpty() ) 
                lra12 = getStatusDataLraByPemda(pemdas, tahun, (short)12, (short)1, conn);
            result.add(lra12.get(0));
            List<StatusPemda> lra13 = getStatusDataLraByPemda(pemdas, tahun, (short)13, (short)0, conn);
            if( lra13==null || lra13.isEmpty() ) 
                lra13 = getStatusDataLraByPemda(pemdas, tahun, (short)13, (short)1, conn);
            result.add(lra13.get(0));
            */
            for (int i = 1; i < 3; i++) {
                List<StatusPemda> ner1 = getStatusDataNeracaByPemda(pemdas, tahun, (short) i, conn);
                if(ner1!=null && !ner1.isEmpty()) result.add(ner1.get(0));
            }
//            List<StatusPemda> ner1 = getStatusDataNeracaByPemda(pemdas, tahun, (short)1, conn);
//            result.add(ner1.get(0));
//            List<StatusPemda> ner2 = getStatusDataNeracaByPemda(pemdas, tahun, (short)2, conn);
//            result.add(ner2.get(0));
            
            List<StatusPemda> ak = getStatusDataArusKasByPemda(pemdas, tahun, conn);
            if(ak!=null && !ak.isEmpty()) result.add(ak.get(0));
            
            for (int i = 1; i < 4; i++) {
                List<StatusPemda> lo1 = getStatusDataLOByPemda(pemdas, tahun, (short)i, conn);
                if(lo1 != null  && !lo1.isEmpty()) result.add(lo1.get(0));
            }
            /*List<StatusPemda> lo1 = getStatusDataLOByPemda(pemdas, tahun, (short)1, conn);
            result.add(lo1.get(0));
            List<StatusPemda> lo2 = getStatusDataLOByPemda(pemdas, tahun, (short)2, conn);
            result.add(lo2.get(0));
            List<StatusPemda> lo3 = getStatusDataLOByPemda(pemdas, tahun, (short)3, conn);
            result.add(lo3.get(0));*/
            
            List<StatusPemda> lps = getStatusDataLPSalByPemda(pemdas, tahun, conn);
            if(lps!=null && !lps.isEmpty()) result.add(lps.get(0));
            List<StatusPemda> lpe = getStatusDataLpeByPemda(pemdas, tahun, conn);
            if(lpe!=null && !lpe.isEmpty()) result.add(lpe.get(0));
            for (int i = 1; i < 13; i++) {
                List<StatusPemda> dth = getStatusDataDTHByPemda(pemdas, tahun, (short)i, conn);
                if(dth!=null && !dth.isEmpty()) result.add(dth.get(0));
            }
            List<StatusPemda> pd = getStatusDataPinjamanDaerahByPemda(pemdas, tahun, conn);
            if(pd!=null && !pd.isEmpty()) result.add(pd.get(0));
            List<StatusPemda> pfk = getStatusDataPFKByPemda(pemdas, tahun, conn);
            if(pfk!=null && !pfk.isEmpty()) result.add(pfk.get(0));
            
            
            
            return result;
        } catch (SQLException ex) {
            Logger.getLogger(NewSettingSQL.class.getName()).log(Level.SEVERE, null, ex);
            throw new SQLException("SQL: Gagal mengambil data StatusData\n" + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    

}
