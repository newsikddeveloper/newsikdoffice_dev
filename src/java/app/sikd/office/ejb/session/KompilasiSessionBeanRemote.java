/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.office.ejb.session;

import app.sikd.entity.backoffice.AkunKompilasi;
import app.sikd.entity.backoffice.AkunKompilasiGFS;
import app.sikd.entity.backoffice.KompilasiApbd1364;
import java.util.List;
import javax.ejb.Remote;

/**
 *
 * @author sora
 */
@Remote
public interface KompilasiSessionBeanRemote {
    public List<KompilasiApbd1364> getApbd13Kompilasi64(short tahun, short kodeData) throws Exception;
    public KompilasiApbd1364 createApbdKompilasi64(KompilasiApbd1364 apbd1364) throws Exception;
    public List<AkunKompilasiGFS> getApbdKompilasi64(short tahun, short kodeData, String kodepemda) throws Exception;

    public List<AkunKompilasi> getApbdKompilasiLangsungs(short tahun, List<String> pemdaCodes) throws Exception;

    public void deleteApbdKompilasi64(short tahun, short kodeData) throws Exception;
    
    
    
}
