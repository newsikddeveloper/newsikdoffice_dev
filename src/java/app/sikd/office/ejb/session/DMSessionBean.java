/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.sikd.office.ejb.session;

import app.sikd.dbapi.SIKDConnectionManager;
import app.sikd.entity.backoffice.APBD;
import app.sikd.entity.backoffice.ArusKas;
import app.sikd.entity.backoffice.ArusKasSaldo;
import app.sikd.entity.backoffice.KegiatanAPBD;
import app.sikd.entity.backoffice.KodeRekeningAPBD;
import app.sikd.entity.backoffice.Neraca;
import app.sikd.entity.backoffice.NeracaAkunJenis;
import app.sikd.entity.backoffice.NeracaAkunKelompok;
import app.sikd.entity.backoffice.NeracaAkunObjek;
import app.sikd.entity.backoffice.NeracaAkunUtama;
import app.sikd.entity.backoffice.ObjArusKasAkun;
import app.sikd.entity.backoffice.RealisasiAPBD;
import app.sikd.entity.backoffice.RealisasiKegiatanAPBD;
import app.sikd.entity.backoffice.RealisasiKodeRekeningAPBD;
import app.sikd.office.ejb.bl.DMBL;
import app.sikd.office.ejb.bl.IDMBL;
import java.sql.Connection;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.Remote;
import javax.ejb.Stateless;

/**
 *
 * @author detra
 */
@Stateless
@Remote({DMSessionBeanRemote.class})
public class DMSessionBean implements DMSessionBeanRemote {

    SIKDConnectionManager conMan;
    IDMBL bl;

    @PostConstruct
    public void init() {
        try {
            conMan = new SIKDConnectionManager();
            bl = new DMBL();
        } catch (Exception ex) {
            Logger.getLogger(DMSessionBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    Connection createConnection() throws Exception {
        return conMan.createConnection("sikd.properties");
    }

    /**
     *
     * @param kodeSatker
     * @param tahun
     * @param semester
     * @param judul
     * @return
     * @throws Exception
     */
    @SuppressWarnings("ConvertToTryWithResources")
    public Neraca getNeracaHeader(String kodeSatker, short tahun, short semester, String judul) throws Exception {
        Connection conn = null;
        try {
            conn = createConnection();
            return bl.getNeracaHeader(kodeSatker, tahun, semester, judul, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    
    /**
     *
     * @param indexNeraca
     * @return
     * @throws Exception
     */
    @SuppressWarnings("ConvertToTryWithResources")
    public List<NeracaAkunUtama> getNeracaAkunStruktur(long indexNeraca) throws Exception {
        Connection conn = null;
        try {
            conn = createConnection();
            return bl.getNeracaAkunStruktur(indexNeraca, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }

    /**
     *
     * @param neraca
     * @return
     * @throws Exception
     */
    @SuppressWarnings("ConvertToTryWithResources")
    public Neraca createNeracaHeader(Neraca neraca, short iotype) throws Exception {
        Connection conn = null;
        try {
            conn = createConnection();
            return bl.createNeracaHeader(neraca, iotype, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }

    /**
     *
     * @param indexNeraca
     * @param obj
     * @return
     * @throws Exception
     */
    @SuppressWarnings("ConvertToTryWithResources")
    public NeracaAkunUtama createNeracaAkunUtama(long indexNeraca, NeracaAkunUtama obj) throws Exception {
        Connection conn = null;
        try {
            conn = createConnection();
            return bl.createNeracaAkunUtama(indexNeraca, obj, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }

    /**
     *
     * @param indexAkunUtama
     * @param obj
     * @return
     * @throws Exception
     */
    @SuppressWarnings("ConvertToTryWithResources")
    public NeracaAkunKelompok createNeracaAkunKelompok(long indexAkunUtama, NeracaAkunKelompok obj) throws Exception {
        Connection conn = null;
        try {
            conn = createConnection();
            return bl.createNeracaAkunKelompok(indexAkunUtama, obj, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }

    /**
     *
     * @param indexAkunKelompok
     * @param obj
     * @return
     * @throws Exception
     */
    @SuppressWarnings("ConvertToTryWithResources")
    public NeracaAkunJenis createNeracaAkunJenis(long indexAkunKelompok, NeracaAkunJenis obj) throws Exception {
        Connection conn = null;
        try {
            conn = createConnection();
            return bl.createNeracaAkunJenis(indexAkunKelompok, obj, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }

    /**
     *
     * @param indexAkunJenis
     * @param obj
     * @return
     * @throws Exception
     */
    @SuppressWarnings("ConvertToTryWithResources")
    public NeracaAkunObjek createNeracaAkunObjek(long indexAkunJenis, NeracaAkunObjek obj) throws Exception {
        Connection conn = null;
        try {
            conn = createConnection();
            return bl.createNeracaAkunObjek(indexAkunJenis, obj, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }

    /**
     *
     * @param index
     * @throws Exception
     */
    @SuppressWarnings("ConvertToTryWithResources")
    public void deleteNeracaAkunObjek(long index) throws Exception {
        Connection conn = null;
        try {
            conn = createConnection();
            bl.deleteNeracaAkunObjek(index, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }

    /**
     *
     * @param index
     * @throws Exception
     */
    @SuppressWarnings("ConvertToTryWithResources")
    public void deleteNeracaAkunJenis(long index) throws Exception {
        Connection conn = null;
        try {
            conn = createConnection();
            bl.deleteNeracaAkunJenis(index, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }

    /**
     *
     * @param index
     * @throws Exception
     */
    @SuppressWarnings("ConvertToTryWithResources")
    public void deleteNeracaAkunKelompok(long index) throws Exception {
        Connection conn = null;
        try {
            conn = createConnection();
            bl.deleteNeracaAkunKelompok(index, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }

    /**
     *
     * @param index
     * @throws Exception
     */
    @SuppressWarnings("ConvertToTryWithResources")
    public void deleteNeracaAkunUtama(long index) throws Exception {
        Connection conn = null;
        try {
            conn = createConnection();
            bl.deleteNeracaAkunUtama(index, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    
    @SuppressWarnings("ConvertToTryWithResources")
    public void deleteArusKas(long index) throws Exception {
        Connection conn = null;
        try {
            conn = createConnection();
            bl.deleteArusKas(index, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    
    @SuppressWarnings("ConvertToTryWithResources")
    public void deleteArusKasRinci(long index, String table, String field) throws Exception {
        Connection conn = null;
        try {
            conn = createConnection();
            bl.deleteArusKasRinci(index, table, field, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    
    @SuppressWarnings("ConvertToTryWithResources")
    public ArusKas createArusKasHeader(ArusKas arusKas, short iotype) throws Exception {
        Connection conn = null;
        try {
            conn = createConnection();
            return bl.createArusKasHeader(arusKas, iotype, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    
    
    @SuppressWarnings("ConvertToTryWithResources")
    public ObjArusKasAkun createArusKasRinci(long indexKas, ObjArusKasAkun obj, String table, String field) throws Exception {
        Connection conn = null;
        try {
            conn = createConnection();
            return bl.createArusKasRinci(indexKas, obj, table, field, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    
    @SuppressWarnings("ConvertToTryWithResources")
    public ArusKas getArusKasHeader(String kodeSatker, short tahun, String judul) throws Exception {
        Connection conn = null;
        try {
            conn = createConnection();
            return bl.getArusKasHeader(kodeSatker, tahun, judul, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    @SuppressWarnings("ConvertToTryWithResources")
    public List<ObjArusKasAkun> getArusKasRinci(long indexArusKas, String table, String field) throws Exception {
        Connection conn = null;
        try {
            conn = createConnection();
            return bl.getArusKasRinci(indexArusKas, table, field, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    
    
    @SuppressWarnings("ConvertToTryWithResources")
    public ArusKasSaldo createArusKasSaldo(long indexKas, ArusKasSaldo obj) throws Exception {
        Connection conn = null;
        try {
            conn = createConnection();
            return bl.createArusKasSaldo(indexKas, obj, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    
    /*
    ==========================================
    APBD & Realisasi
    ===========================================
    */
    
    @SuppressWarnings("ConvertToTryWithResources")
    public APBD getAPBDHeader(short year, String kodeSatker, short kodeData, short jenisCOA) throws Exception{
        Connection conn = null;
        try {
            conn = createConnection();
            return bl.getAPBDHeader(year, kodeSatker, kodeData, jenisCOA, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
            
    @SuppressWarnings("ConvertToTryWithResources")
    public APBD getApbdStruktur(String kodeSatker, short tahun, short kodeData, short jenisCOA) throws Exception{
        Connection conn = null;
        try {
            conn = createConnection();
            return bl.getApbdStruktur(kodeSatker, tahun, kodeData, jenisCOA, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    
    @SuppressWarnings("ConvertToTryWithResources")
    public APBD createAPBDHeader(APBD apbd, short iotype) throws Exception{
        Connection conn = null;
        try {
            conn = createConnection();
            return bl.createAPBDHeader(apbd, iotype, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    
    @SuppressWarnings("ConvertToTryWithResources")
    public APBD updateAPBDHeader(APBD apbd) throws Exception{
        Connection conn = null;
        try {
            conn = createConnection();
            return bl.updateAPBDHeader(apbd, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    
    @SuppressWarnings("ConvertToTryWithResources")
    public void deleteAPBD(long id) throws Exception{
        Connection conn = null;
        try {
            conn = createConnection();
            bl.deleteAPBD(id, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    
    @SuppressWarnings("ConvertToTryWithResources")
    public KegiatanAPBD createKegiatanAPBD(long indexApbd, KegiatanAPBD kegiatan) throws Exception{
        Connection conn = null;
        try {
            conn = createConnection();
            return bl.createKegiatanAPBD(indexApbd, kegiatan, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    @SuppressWarnings("ConvertToTryWithResources")
    public KegiatanAPBD updateKegiatanAPBD(KegiatanAPBD kegiatan) throws Exception{
        Connection conn = null;
        try {
            conn = createConnection();
            return bl.updateKegiatanAPBD(kegiatan, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    @SuppressWarnings("ConvertToTryWithResources")
    public void deleteKegiatanAPBD(long id) throws Exception{
        Connection conn = null;
        try {
            conn = createConnection();
            bl.deleteKegiatanAPBD(id, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    @SuppressWarnings("ConvertToTryWithResources")
    public void createRekeningAPBD(long indexKegiatan, KodeRekeningAPBD rekening) throws Exception{
        Connection conn = null;
        try {
            conn = createConnection();
            bl.createRekeningAPBD(indexKegiatan, rekening, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    @SuppressWarnings("ConvertToTryWithResources")
    public void deleteRekeningAPBD(long indexKegiatan, KodeRekeningAPBD rekening) throws Exception{
        Connection conn = null;
        try {
            conn = createConnection();
            bl.deleteRekeningAPBD(indexKegiatan, rekening, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    
    
    @SuppressWarnings("ConvertToTryWithResources")
    public RealisasiAPBD getRealisasiApbdHeader(String kodeSatker, short tahun, short bulan, short jenisCOA) throws Exception{
        Connection conn = null;
        try {
            conn = createConnection();
            return bl.getRealisasiApbdHeader(kodeSatker, tahun, bulan, jenisCOA, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    
    @SuppressWarnings("ConvertToTryWithResources")
    public RealisasiAPBD getRealisasiApbdHeader(String kodeSatker, short tahun, short bulan, short kodeData, short jenisCOA) throws Exception{
        Connection conn = null;
        try {
            conn = createConnection();
            return bl.getRealisasiApbdHeader(kodeSatker, tahun, bulan, kodeData, jenisCOA, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    
    @SuppressWarnings("ConvertToTryWithResources")
    public RealisasiAPBD getRealisasiApbdStruktur(String kodeSatker, short tahun, short bulan, short kodeData, short jenisCOA) throws Exception{
        Connection conn = null;
        try {
            conn = createConnection();
            return bl.getRealisasiApbdStruktur(kodeSatker, tahun, bulan, kodeData, jenisCOA, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    @SuppressWarnings("ConvertToTryWithResources")
    public RealisasiAPBD createRealiasiAPBDHeader(RealisasiAPBD apbd, short iotype) throws Exception{
        Connection conn = null;
        try {
            conn = createConnection();
            return bl.createRealiasiAPBDHeader(apbd, iotype, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    @SuppressWarnings("ConvertToTryWithResources")
    public RealisasiAPBD updateRealisasiAPBDHeader(RealisasiAPBD apbd) throws Exception{
        Connection conn = null;
        try {
            conn = createConnection();
            return bl.updateRealisasiAPBDHeader(apbd, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    @SuppressWarnings("ConvertToTryWithResources")
    public void deleteRealisasiAPBD(long id) throws Exception{
        Connection conn = null;
        try {
            conn = createConnection();
            bl.deleteRealisasiAPBD(id, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    @SuppressWarnings("ConvertToTryWithResources")
    public RealisasiKegiatanAPBD createRealisasiKegiatanAPBD(long indexApbd, RealisasiKegiatanAPBD kegiatan) throws Exception{
        Connection conn = null;
        try {
            conn = createConnection();
            return bl.createRealisasiKegiatanAPBD(indexApbd, kegiatan, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    @SuppressWarnings("ConvertToTryWithResources")
    public RealisasiKegiatanAPBD updateRealisasiKegiatanAPBD(RealisasiKegiatanAPBD kegiatan) throws Exception{
        Connection conn = null;
        try {
            conn = createConnection();
            return bl.updateRealisasiKegiatanAPBD(kegiatan, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    @SuppressWarnings("ConvertToTryWithResources")
    public void deleteRealisasiKegiatanAPBD(long id) throws Exception{
        Connection conn = null;
        try {
            conn = createConnection();
            bl.deleteRealisasiKegiatanAPBD(id, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    @SuppressWarnings("ConvertToTryWithResources")
    public void createRealisasiRekeningAPBD(long indexKegiatan, RealisasiKodeRekeningAPBD rekening) throws Exception{
        Connection conn = null;
        try {
            conn = createConnection();
            bl.createRealisasiRekeningAPBD(indexKegiatan, rekening, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    @SuppressWarnings("ConvertToTryWithResources")
    public void deleteRealisasiRekeningAPBD(long indexKegiatan, RealisasiKodeRekeningAPBD rekening) throws Exception{
        Connection conn = null;
        try {
            conn = createConnection();
            bl.deleteRealisasiRekeningAPBD(indexKegiatan, rekening, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    

}
