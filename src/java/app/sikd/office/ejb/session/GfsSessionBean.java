/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.office.ejb.session;

import app.sikd.dbapi.SIKDConnectionManager;
import app.sikd.entity.utilitas.SimpleAccount;
import app.sikd.office.ejb.bl.GfsBL;
import app.sikd.office.ejb.bl.IGfsBL;
import java.sql.Connection;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.Remote;
import javax.ejb.Stateless;

/**
 *
 * @author sora
 */
@Stateless
@Remote({GfsSessionBeanRemote.class})
public class GfsSessionBean implements GfsSessionBeanRemote {

    IGfsBL bl;
    SIKDConnectionManager conMan;

    @PostConstruct
    public void init() {
        try {
            conMan = new SIKDConnectionManager();
            bl = new GfsBL();
        } catch (Exception ex) {
            Logger.getLogger(GfsSessionBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    Connection createConnection() throws Exception {
        return conMan.createConnection("sikd.properties");
    }
    
    /**
     *    
     * @return
     * @throws Exception
     */
    @Override
    @SuppressWarnings("ConvertToTryWithResources")
    public List<SimpleAccount> getGfsAkunStruktur() throws Exception {
        Connection conn = null;
        try {
            conn = createConnection();
            return bl.getGfsAkunStruktur(conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }

    }
}
