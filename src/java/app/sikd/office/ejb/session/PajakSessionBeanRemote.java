/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.office.ejb.session;

import app.sikd.entity.backoffice.DTH;
import app.sikd.entity.backoffice.RTH;
import app.sikd.entity.backoffice.RincianBPHTB;
import app.sikd.entity.backoffice.RincianHiburan;
import app.sikd.entity.backoffice.RincianHotel;
import app.sikd.entity.backoffice.RincianIMB;
import app.sikd.entity.backoffice.RincianIzinUsaha;
import app.sikd.entity.backoffice.RincianKendaraan;
import app.sikd.entity.backoffice.RincianPajakDanRetribusi;
import app.sikd.entity.backoffice.RincianRestoran;
import java.util.List;
import javax.ejb.Remote;

/**
 *
 * @author sora
 */
@Remote
public interface PajakSessionBeanRemote {

    public DTH getDTHReport(short tahun, String kodeSatker, short periode, String kodeSKPD) throws Exception;
    public List<String> getDinasKirimDTH(short year, String kodeSatker) throws Exception;
    
    public List<RTH> getRTHReport(short tahun, String kodeSatker, short periode) throws Exception;

    public List<RincianKendaraan> getRincianKendaraan(String kodeSatker, short tahun) throws Exception;

    public List<RincianBPHTB> getRincianBPHTB(String kodeSatker, short tahun) throws Exception;

    public List<RincianHiburan> getRincianHiburan(String kodeSatker, short tahun) throws Exception;

    public List<RincianHotel> getRincianHotel(String kodeSatker, short tahun) throws Exception;

    public List<RincianIMB> getRincianIMB(String kodeSatker, short tahun) throws Exception;

    public List<RincianRestoran> getRincianRestoran(String kodeSatker, short tahun) throws Exception;

    public List<RincianIzinUsaha> getRincianIzinUsaha(String kodeSatker, short tahun) throws Exception;
    
    public List<RincianPajakDanRetribusi> getRincianPajakDanRetribusi(String kodeSatker, short tahun) throws Exception;
    
}
