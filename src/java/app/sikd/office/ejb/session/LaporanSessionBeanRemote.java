/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.office.ejb.session;

import app.sikd.entity.backoffice.ArusKasReport;
import app.sikd.entity.backoffice.LOReport;
import app.sikd.entity.backoffice.Neraca;
import app.sikd.entity.backoffice.NeracaAkunJenis;
import app.sikd.entity.backoffice.NeracaAkunKelompok;
import app.sikd.entity.backoffice.NeracaAkunObjek;
import app.sikd.entity.backoffice.NeracaAkunUtama;
import app.sikd.entity.backoffice.NeracaReport;
import app.sikd.entity.backoffice.PerubahanEkuitas;
import app.sikd.entity.backoffice.PerubahanSAL;
import java.util.List;
import javax.ejb.Remote;

/**
 *
 * @author sora
 */
@Remote
public interface LaporanSessionBeanRemote {

    public List<NeracaReport> getNeracaReports(short tahun, short semester, String kodeSatker, String judulNeraca ) throws Exception;

    public List<String> getNeracaLabels(short tahun, short semester, String kodeSatker) throws Exception;

    public Neraca createNeracaStruktur(Neraca neraca, short iotype) throws Exception;

    public long createNeraca(Neraca neraca, short iotype) throws Exception;

    public void updateNeraca(Neraca neraca) throws Exception;

    public void deleteNeraca(Neraca neraca) throws Exception;

    public long createNeracaAkunUtama(long indexNeraca, NeracaAkunUtama obj) throws Exception;

    public void updateNeracaAkunUtama(NeracaAkunUtama obj) throws Exception;

    public void deleteNeracaAkunUtama(NeracaAkunUtama obj) throws Exception;

    public long createNeracaAkunKelompok(long indexNeracaAkunUtama, NeracaAkunKelompok obj) throws Exception;

    public void updateNeracaAkunKelompok(NeracaAkunKelompok obj) throws Exception;

    public void deleteNeracaAkunKelompok(NeracaAkunKelompok obj) throws Exception;

    public long createNeracaAkunJenis(long indexNeracaAkunKelompok, NeracaAkunJenis obj) throws Exception;

    public void updateNeracaAkunJenis(NeracaAkunJenis obj) throws Exception;

    public void deleteNeracaAkunJenis(NeracaAkunJenis obj) throws Exception;

    public long createNeracaAkunObjek(long indexNeracaAkunJenis, NeracaAkunObjek obj) throws Exception;

    public void updateNeracaAkunObjek(NeracaAkunObjek obj) throws Exception;

    public void deleteNeracaAkunObjek(NeracaAkunObjek obj) throws Exception;

    public Neraca getNeraca(short tahun, short semester, String kodeSatker, String judulNeraca) throws Exception;

    public List<NeracaAkunUtama> getNeracaAkunUtamas(long indexNeraca) throws Exception;

    public List<NeracaAkunKelompok> getNeracaAkunKelompoks(long indexNeracaAkunUtama) throws Exception;

    public List<NeracaAkunJenis> getNeracaAkunJeniss(long indexNeracaAkunKelompok) throws Exception;

    public List<NeracaAkunObjek> getNeracaAkunObjeks(long indexNeracaAkunJenis) throws Exception;

    public List<ArusKasReport> getArusKasReports(short tahun, String kodeSatker, String judulArusKas) throws Exception;

    public List<String> getArusKasLabels(short tahun, String kodeSatker) throws Exception;

    public PerubahanSAL getPerubahanSalReports(short year, String kodeSatker) throws Exception;
    public long createPerubahanSalReports(PerubahanSAL sal, short iotype) throws Exception;
    public void deletePerubahanSalReports(short year, String kodeSatker) throws Exception;

    public PerubahanEkuitas getPerubahanEkuitasReports(short year, String kodeSatker) throws Exception;
    public long createPerubahanEkuitasReports(PerubahanEkuitas lpe, short iotype) throws Exception;
    public void deletePerubahanEkuitasReports(short year, String kodeSatker) throws Exception;

    public List<LOReport> getLaporanOperasionalReports(short year, short triwulan, String kodeSatker) throws Exception;


    
}
