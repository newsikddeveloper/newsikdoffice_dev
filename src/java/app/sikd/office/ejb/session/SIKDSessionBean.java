/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.office.ejb.session;

import app.sikd.dbapi.SIKDConnectionManager;
import app.sikd.entity.utilitas.MappingAccount;
import app.sikd.office.ejb.bl.ISIKDBusinessLogic;
import app.sikd.office.ejb.bl.SIKDBusinessLogic;
import java.sql.Connection;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.Remote;
import javax.ejb.Stateless;

/**
 *
 * @author sora
 */
@Stateless
@Remote({SIKDSessionBeanRemote.class})
public class SIKDSessionBean implements SIKDSessionBeanRemote {
    ISIKDBusinessLogic sikdBL;
    SIKDConnectionManager conMan;

    @PostConstruct
    public void init() {
        try {
            conMan = new SIKDConnectionManager();
            sikdBL = new SIKDBusinessLogic();
        } catch (Exception ex) {
            Logger.getLogger(SIKDSessionBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    Connection createConnection() throws Exception {
        return conMan.createConnection("sikd.properties");
    }
    
    /**
     *
     * @param tahun
     * @param kodeSatker
     * @param kodeData
     * @param jeniscoa
     * @return
     * @throws Exception
     */
    @Override
    @SuppressWarnings("ConvertToTryWithResources")
    public short getApbdNotifikasi(short tahun, String kodeSatker, short kodeData, short jeniscoa) throws Exception{
        Connection conn = null;
        try {
            conn = createConnection();
            return sikdBL.getApbdNotifikasi(tahun, kodeSatker, kodeData, jeniscoa, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }

    /**
     *
     * @param tahun
     * @param periode
     * @param kodeSatker
     * @param kodeData
     * @param jeniscoa
     * @return
     * @throws Exception
     */
    @Override
    @SuppressWarnings("ConvertToTryWithResources")
    public short getLraNotifikasi(short tahun, short periode, String kodeSatker, short kodeData, short jeniscoa) throws Exception{
        Connection conn = null;
        try {
            conn = createConnection();
            return sikdBL.getLraNotifikasi(tahun, periode, kodeSatker, kodeData, jeniscoa, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }

    /**
     *
     * @param tahun
     * @param periode
     * @param kodeSatker
     * @return
     * @throws Exception
     */
    @Override
    @SuppressWarnings("ConvertToTryWithResources")
    public short getDthNotifikasi(short tahun, short periode, String kodeSatker) throws Exception{
        Connection conn = null;
        try {
            conn = createConnection();
            return sikdBL.getDthNotifikasi(tahun, periode, kodeSatker, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    
    /**
     *
     * @param tahun
     * @param semester
     * @param kodeSatker
     * @return
     * @throws Exception
     */
    @SuppressWarnings("ConvertToTryWithResources")
    @Override
    public short getNeracaNotifikasi(short tahun, short semester, String kodeSatker) throws Exception{
        Connection conn = null;
        try {
            conn = createConnection();
            return sikdBL.getNeracaNotifikasi(tahun, semester, kodeSatker, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    
    /**
     *
     * @param tahun
     * @param triwulan
     * @param kodeSatker
     * @return
     * @throws Exception
     */
    @SuppressWarnings("ConvertToTryWithResources")
    @Override
    public short getLONotifikasi(short tahun, short triwulan, String kodeSatker) throws Exception{
        Connection conn = null;
        try {
            conn = createConnection();
            return sikdBL.getLONotifikasi(tahun, triwulan, kodeSatker, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    
    /**
     *
     * @param tahun
     * @param kodeSatker
     * @return
     * @throws Exception
     */
    @SuppressWarnings("ConvertToTryWithResources")
    @Override
    public short getArusKasNotifikasi(short tahun, String kodeSatker) throws Exception{
        Connection conn = null;
        try {
            conn = createConnection();
            return sikdBL.getArusKasNotifikasi(tahun, kodeSatker, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    
    /**
     *
     * @param tahun
     * @param kodeSatker
     * @return
     * @throws Exception
     */
    @SuppressWarnings("ConvertToTryWithResources")
    @Override
    public short getEkuitasNotifikasi(short tahun, String kodeSatker) throws Exception{
        Connection conn = null;
        try {
            conn = createConnection();
            return sikdBL.getEkuitasNotifikasi(tahun, kodeSatker, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    
    /**
     *
     * @param tahun
     * @param kodeSatker
     * @return
     * @throws Exception
     */
    @SuppressWarnings("ConvertToTryWithResources")
    @Override
    public short getSalNotifikasi(short tahun, String kodeSatker) throws Exception{
        Connection conn = null;
        try {
            conn = createConnection();
            return sikdBL.getSalNotifikasi(tahun, kodeSatker, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    
    /**
     *
     * @param tahun
     * @param kodeSatker
     * @return
     * @throws Exception
     */
    @SuppressWarnings("ConvertToTryWithResources")
    @Override
    public short getPFKNotifikasi(short tahun, String kodeSatker) throws Exception{
        Connection conn = null;
        try {
            conn = createConnection();
            return sikdBL.getPFKNotifikasi(tahun, kodeSatker, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    
    /**
     *
     * @param tahun
     * @param kodeSatker
     * @return
     * @throws Exception
     */
    @SuppressWarnings("ConvertToTryWithResources")
    @Override
    public short getPinjamanDaerahNotifikasi(short tahun, String kodeSatker) throws Exception{
        Connection conn = null;
        try {
            conn = createConnection();
            return sikdBL.getPinjamanDaerahNotifikasi(tahun, kodeSatker, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    
    /**
     *
     * @param tahun
     * @param kodeSatker
     * @param jenisCOA
     * @return
     * @throws Exception
     */
    @SuppressWarnings("ConvertToTryWithResources")
    public List<MappingAccount> getAllNotifikasi(short tahun, String kodeSatker, short jenisCOA) throws Exception{
        Connection conn = null;
        try {
            conn = createConnection();
            return sikdBL.getAllNotifikasi(tahun, kodeSatker, jenisCOA, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    

}
