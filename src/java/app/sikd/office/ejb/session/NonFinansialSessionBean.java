/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.office.ejb.session;

import app.sikd.dbapi.SIKDConnectionManager;
import app.sikd.entity.backoffice.DataDaerah;
import app.sikd.entity.backoffice.DataDaerahDetail;
import app.sikd.entity.backoffice.PDRB;
import app.sikd.entity.backoffice.PDRBHarga;
import app.sikd.office.ejb.bl.INonFinansialBusinessLogic;
import app.sikd.office.ejb.bl.IUploadFileBusinessLogic;
import app.sikd.office.ejb.bl.NonFinansialBusinessLogic;
import app.sikd.office.ejb.bl.UploadFileBusinessLogic;
import java.sql.Connection;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.Remote;
import javax.ejb.Stateless;

/**
 *
 * @author detra
 */
@Stateless
@Remote({NonFinansialSessionBeanRemote.class})
public class NonFinansialSessionBean implements NonFinansialSessionBeanRemote {
    INonFinansialBusinessLogic bl;
    IUploadFileBusinessLogic uploadbl;
    String domain;
    SIKDConnectionManager conMan;
    
    @PostConstruct
    public void init() {
        try {
            conMan = new SIKDConnectionManager();
            domain = conMan.getDomain();
            bl = new NonFinansialBusinessLogic();
            uploadbl = new UploadFileBusinessLogic();

        } catch (Exception ex) {
            Logger.getLogger(APBDSessionBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    Connection createConnection() throws Exception {
        return conMan.createConnection("sikd.properties");
    }

    @Override
    @SuppressWarnings("ConvertToTryWithResources")
    public void createDataDaerah(DataDaerah obj) throws Exception {
        Connection conn = null;
        try {
            conn = createConnection();
            bl.createDataDaerah(obj, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }

    }

    @Override
    @SuppressWarnings("ConvertToTryWithResources")
    public void deleteDataDaerah(String kodeSatker, short tahun) throws Exception {
        Connection conn = null;
        try {
            conn = createConnection();
            bl.deleteDataDaerah(kodeSatker, tahun, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }

    }
    
    @Override
    @SuppressWarnings("ConvertToTryWithResources")
    public DataDaerahDetail getDataDaerahDetail(String kodeSatker, short tahun) throws Exception {
        Connection conn = null;
        try {
            conn = createConnection();
            return bl.getDataDaerahDetail(kodeSatker, tahun, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }

    }
    
    @Override
    @SuppressWarnings("ConvertToTryWithResources")
    public void createPDRB(PDRB obj) throws Exception {
        Connection conn = null;
        try {
            conn = createConnection();
            bl.createPDRB(obj, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }

    }
    
    @Override
    @SuppressWarnings("ConvertToTryWithResources")
    public void deletePDRB(String kodeSatker, short tahun) throws Exception {
        Connection conn = null;
        try {
            conn = createConnection();
            bl.deletePDRB(kodeSatker, tahun, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }

    }
    
    @Override
    @SuppressWarnings("ConvertToTryWithResources")
    public  PDRBHarga getPDRBHargaBerlaku(String kodeSatker, short tahun) throws Exception {
        Connection conn = null;
        try {
            conn = createConnection();
            return bl.getPDRBHargaBerlaku(kodeSatker, tahun, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }

    }
    
    @Override
    @SuppressWarnings("ConvertToTryWithResources")
    public  PDRBHarga getPDRBHargaKonstan(String kodeSatker, short tahun) throws Exception {
        Connection conn = null;
        try {
            conn = createConnection();
            return bl.getPDRBHargaKonstan(kodeSatker, tahun, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }

    }
    
     
}
