/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.sikd.office.ejb.session;

/**
 *
 * @author detra
 */
import app.sikd.dbapi.IWebServiceSQL;
import app.sikd.dbapi.SIKDConnectionManager;
import app.sikd.dbapi.WebServiceSQL;
import app.sikd.entity.apbd.LampiranIIIPerdaAPBD;
import app.sikd.entity.apbd.LampiranIIPerdaAPBD;
import app.sikd.entity.apbd.LampiranILra;
import app.sikd.entity.apbd.LampiranIPerdaAPBD;
import app.sikd.entity.apbd.LampiranIVPerdaAPBD;
import app.sikd.entity.apbd.LampiranVPerdaAPBD;
import app.sikd.entity.backoffice.APBDPerda;
import app.sikd.entity.backoffice.NilaiFungsiPemda;
import app.sikd.entity.backoffice.PerhitunganFihakKetiga;
import app.sikd.entity.backoffice.PinjamanDaerah;
import app.sikd.entity.backoffice.RealisasiAPBD;
import app.sikd.entity.backoffice.RealisasiKegiatanAPBD;
import app.sikd.entity.backoffice.RealisasiKodeRekeningAPBD;
import app.sikd.entity.backoffice.RekapitulasiGajiPegawai;
import app.sikd.entity.backoffice.RincianPerhitunganFihakKetiga;
import app.sikd.entity.backoffice.RincianPinjamanDaerah;
import app.sikd.entity.utilitas.MappingAccount;
import app.sikd.entity.utilitas.MappingNonStandar;
import app.sikd.entity.utilitas.SimpleAccount;
import app.sikd.entity.ws.RealisasiAPBD_WS;
import app.sikd.entity.ws.RealisasiKegiatanAPBD_WS;
import app.sikd.entity.ws.RealisasiKodeRekeningAPBD_WS;
import app.sikd.office.ejb.bl.APBDBusinessLogic;
import app.sikd.office.ejb.bl.IAPBDBusinessLogic;
import app.sikd.office.ejb.bl.IUploadFileBusinessLogic;
import app.sikd.office.ejb.bl.UploadFileBusinessLogic;
import app.sikd.util.SIKDUtil;
import java.sql.Connection;
import java.sql.Timestamp;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.Remote;
import javax.ejb.Stateless;

/**
 *
 * @author detra
 */
@Stateless
@Remote({APBDSessionBeanRemote.class})
public class APBDSessionBean implements APBDSessionBeanRemote {

    IAPBDBusinessLogic bl;
    IWebServiceSQL serviceSQL;
    IUploadFileBusinessLogic uploadbl;
    String domain;
    SIKDConnectionManager conMan;

    @PostConstruct
    public void init() {
        try {
            conMan = new SIKDConnectionManager();
            domain = conMan.getDomain();
            bl = new APBDBusinessLogic();
            uploadbl = new UploadFileBusinessLogic();
        } catch (Exception ex) {
            Logger.getLogger(APBDSessionBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    Connection createConnection() throws Exception {
        return conMan.createConnection("sikd.properties");
    }
    
    /**
     *
     * @param tahun
     * @param kodeSatker
     * @param jenisCOA
     * @return
     * @throws Exception
     */
    @Override
    @SuppressWarnings("ConvertToTryWithResources")
    public APBDPerda getAPBDMurniPerdaNumber(short tahun, String kodeSatker, short jenisCOA) throws Exception {
        Connection conn = null;
        try {
            conn = createConnection();
            return bl.getAPBDPerdaNumber(tahun, kodeSatker, SIKDUtil.APBD_MURNI_SHORT, jenisCOA, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }

    }
    
    /**
     *
     * @param tahun
     * @param kodeSatker
     * @param jenisCOA
     * @return
     * @throws Exception
     */
    @Override
    @SuppressWarnings("ConvertToTryWithResources")
    public APBDPerda getRealisasiAPBDMurniPerdaNumber(short tahun, String kodeSatker, short jenisCOA) throws Exception {
        Connection conn = null;
        try {
            conn = createConnection();
            return bl.getRealisasiAPBDPerdaNumber(tahun, kodeSatker, SIKDUtil.APBD_MURNI_SHORT, jenisCOA, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    
    /**
     *
     * @param tahun
     * @param kodeSatker
     * @param jenisCOA
     * @return
     * @throws Exception
     */
    @Override
    @SuppressWarnings("ConvertToTryWithResources")
    public APBDPerda getAPBDPerubahanPerdaNumber(short tahun, String kodeSatker, short jenisCOA) throws Exception {
        Connection conn = null;
        try {
            conn = createConnection();
            return bl.getAPBDPerdaNumber(tahun, kodeSatker, SIKDUtil.APBD_PERUBAHAN_SHORT, jenisCOA, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }

    }
    
    /**
     *
     * @param tahun
     * @param kodeSatker
     * @param jenisCOA
     * @return
     * @throws Exception
     */
    @SuppressWarnings("ConvertToTryWithResources")
    @Override
    public APBDPerda getRealisasiAPBDPerubahanPerdaNumber(short tahun, String kodeSatker, short jenisCOA) throws Exception {
        Connection conn = null;
        try {
            conn = createConnection();
            return bl.getRealisasiAPBDPerdaNumber(tahun, kodeSatker, SIKDUtil.APBD_PERUBAHAN_SHORT, jenisCOA, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }

    }

    @Override
    @SuppressWarnings("ConvertToTryWithResources")
    public List<LampiranIPerdaAPBD> getLampiranIPerdaAPBDMurni(short tahun, String kodeSatker, short jenisCOA) throws Exception {
        Connection conn = null;
        try {
            conn = createConnection();
            return bl.getLampiranIPerdaAPBD(tahun, kodeSatker, SIKDUtil.APBD_MURNI_SHORT, jenisCOA, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }

    }
    
    @Override
    @SuppressWarnings("ConvertToTryWithResources")
    public List<LampiranIPerdaAPBD> getKompilasiLampiranIPerdaAPBDMurni(short tahun, List<String> kodePemdas, short jenisCOA) throws Exception{
        Connection conn = null;
        try {
            conn = createConnection();
            return bl.getKompilasiLampiranIPerdaAPBD(tahun, kodePemdas, SIKDUtil.APBD_MURNI_SHORT, jenisCOA, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }

    @Override
    @SuppressWarnings("ConvertToTryWithResources")
    public List<LampiranIPerdaAPBD> getKompilasiLampiranIPerdaAPBDPerubahan(short tahun, List<String> kodePemdas, short jenisCOA) throws Exception{
        Connection conn = null;
        try {
            conn = createConnection();
            return bl.getKompilasiLampiranIPerdaAPBD(tahun, kodePemdas, SIKDUtil.APBD_PERUBAHAN_SHORT, jenisCOA, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }

    @Override
    @SuppressWarnings("ConvertToTryWithResources")
    public List<LampiranIPerdaAPBD> getRingkasanPembiayaanMurni(short tahun, String kodeSatker, short jenisCOA) throws Exception {
        Connection conn = null;
        try {
            conn = createConnection();
            return bl.getRingkasanPembiayaan(tahun, kodeSatker, SIKDUtil.APBD_MURNI_SHORT, jenisCOA, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    
    /**
     *
     * @param year
     * @param kodepemdas
     * @param jenisCOA
     * @return
     * @throws Exception
     */
    @Override
    @SuppressWarnings("ConvertToTryWithResources")
    public List<LampiranIPerdaAPBD> getRingkasanPembiayaanMurni(short year, List<String> kodepemdas, short jenisCOA) throws Exception{
        Connection conn = null;
        try {
            conn = createConnection();
            return bl.getRingkasanPembiayaan(year, kodepemdas, SIKDUtil.APBD_MURNI_SHORT, jenisCOA, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }

    @Override
    @SuppressWarnings("ConvertToTryWithResources")
    public List<LampiranIIPerdaAPBD> getLampiranIIPerdaAPBDMurni(short tahun, String kodeSatker, short jenisCOA) throws Exception {
        Connection conn = null;
        try {
            conn = createConnection();
            return bl.getLampiranIIPerdaAPBD(tahun, kodeSatker, SIKDUtil.APBD_MURNI_SHORT, jenisCOA, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }

    @Override
    @SuppressWarnings("ConvertToTryWithResources")
    public List<LampiranIPerdaAPBD> getLampiranIPerdaAPBDPerubahan(short tahun, String kodeSatker, short jenisCOA) throws Exception {
        Connection conn = null;
        try {
            conn = createConnection();
            return bl.getLampiranIPerdaAPBD(tahun, kodeSatker, SIKDUtil.APBD_PERUBAHAN_SHORT, jenisCOA, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }

    }

    @Override
    @SuppressWarnings("ConvertToTryWithResources")
    public List<LampiranIIPerdaAPBD> getLampiranIIPerdaAPBDPerubahan(short tahun, String kodeSatker, short jenisCOA) throws Exception {
        Connection conn = null;
        try {
            conn = createConnection();
            return bl.getLampiranIIPerdaAPBD(tahun, kodeSatker, SIKDUtil.APBD_PERUBAHAN_SHORT, jenisCOA, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    
    @Override
    @SuppressWarnings("ConvertToTryWithResources")
    public List<LampiranIIPerdaAPBD> getKompilasiLampiranIIPerdaAPBDMurni(short tahun, List<String> kodePemdas, short jenisCOA) throws Exception{
        Connection conn = null;
        try {
            conn = createConnection();
            return bl.getKompilasiLampiranIIPerdaAPBD(tahun, kodePemdas, SIKDUtil.APBD_MURNI_SHORT, jenisCOA, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    
    @Override
    @SuppressWarnings("ConvertToTryWithResources")
    public List<LampiranIIPerdaAPBD> getKompilasiLampiranIIPerdaAPBDPerubahan(short tahun, List<String> kodePemdas, short jenisCOA) throws Exception{
        Connection conn = null;
        try {
            conn = createConnection();
            return bl.getKompilasiLampiranIIPerdaAPBD(tahun, kodePemdas, SIKDUtil.APBD_PERUBAHAN_SHORT, jenisCOA, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }

    @Override
    @SuppressWarnings("ConvertToTryWithResources")
    public List<String> getDinasKirimAPBD(short year, String kodeSatker, short kodeData, short jenisCOA) throws Exception {
        Connection conn = null;
        try {
            conn = createConnection();
            return bl.getDinasKirimAPBD(year, kodeSatker, kodeData, jenisCOA, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }

    }
    
    @Override
    @SuppressWarnings("ConvertToTryWithResources")
    public Timestamp getTanggalDinasKirimAPBD(short year, String kodeSatker, short kodeData, short jenisCOA) throws Exception {
        Connection conn = null;
        try {
            conn = createConnection();
            return bl.getTanggalDinasKirimAPBD(year, kodeSatker, kodeData, jenisCOA, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }

    }

    @Override
    @SuppressWarnings("ConvertToTryWithResources")
    public String getUrusan(String kodeUrusan) throws Exception {
        Connection conn = null;
        try {
            conn = createConnection();
            return bl.getUrusan(kodeUrusan, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }

    }

    @Override
    @SuppressWarnings("ConvertToTryWithResources")
    public List<String> getDinasKirimRealisasiAPBD(short year, String kodeSatker, short kodeData, short jenisCOA) throws Exception {
        Connection conn = null;
        try {
            conn = createConnection();
            return bl.getDinasKirimRealisasiAPBD(year, kodeSatker, kodeData, jenisCOA, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    
    @Override
    @SuppressWarnings("ConvertToTryWithResources")
    public Timestamp getTanggalDinasKirimRealisasiAPBD(short year, String kodeSatker, short kodeData, short jenisCOA) throws Exception {
        Connection conn = null;
        try {
            conn = createConnection();
            return bl.getTanggalDinasKirimRealisasiAPBD(year, kodeSatker, kodeData, jenisCOA, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }

    @Override
    @SuppressWarnings("ConvertToTryWithResources")
    public List<LampiranIIIPerdaAPBD> getLampiranIIIPerdaAPBDMurni(short tahun, String kodeSatker, String kodeSKPD, short jenisCOA) throws Exception {
        Connection conn = null;
        try {
            conn = createConnection();
            return bl.getLampiranIIIPerdaAPBD(tahun, kodeSatker, SIKDUtil.APBD_MURNI_SHORT, kodeSKPD, jenisCOA, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }

    @Override
    @SuppressWarnings("ConvertToTryWithResources")
    public List<LampiranIVPerdaAPBD> getLampiranIVPerdaAPBDMurni(short tahun, String kodeSatker, short jenisCOA) throws Exception {
        Connection conn = null;
        try {
            conn = createConnection();
            return bl.getLampiranIVPerdaAPBD(tahun, kodeSatker, SIKDUtil.APBD_MURNI_SHORT, jenisCOA, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }

    @Override
    @SuppressWarnings("ConvertToTryWithResources")
    public List<LampiranVPerdaAPBD> getLampiranVPerdaAPBDMurni(short tahun, String kodeSatker, short jenisCOA) throws Exception {
        Connection conn = null;
        try {
            conn = createConnection();
            return bl.getLampiranVPerdaAPBD(tahun, kodeSatker, SIKDUtil.APBD_MURNI_SHORT, jenisCOA, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }

    @Override
    @SuppressWarnings("ConvertToTryWithResources")
    public List<LampiranILra> getRealisasiAPBDMurni(short tahun, String kodeSatker, short month, short jenisCOA) throws Exception {
        Connection conn = null;
        try {
            conn = createConnection();
            return bl.getRealisasiAPBD(tahun, kodeSatker, SIKDUtil.APBD_MURNI_SHORT, month, jenisCOA, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    
    @Override
    @SuppressWarnings("ConvertToTryWithResources")
    public List<LampiranILra> getRealisasiAPBD(short year, String kodeSatker, short month, short jenisCOA) throws Exception{
        Connection conn = null;
        try {
            conn = createConnection();
            return bl.getRealisasiAPBD(year, kodeSatker, month, jenisCOA, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }

    @Override
    @SuppressWarnings("ConvertToTryWithResources")
    public List<LampiranILra> getRealisasiAPBDPerubahan(short tahun, String kodeSatker, short month, short jenisCOA) throws Exception {
        Connection conn = null;
        try {
            conn = createConnection();
            return bl.getRealisasiAPBD(tahun, kodeSatker, SIKDUtil.APBD_PERUBAHAN_SHORT, month, jenisCOA, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }

//    @Override
//    @SuppressWarnings("ConvertToTryWithResources")
//    public List<LampiranIPerdaAPBD> getRealisasiAPBDMurni(short tahun, String kodeSatker, short month, short jenisCOA) throws Exception {
//        Connection conn = null;
//        try {
//            conn = createConnection();
//            return bl.getRealisasiAPBD(tahun, kodeSatker, SIKDUtil.APBD_MURNI_SHORT, month, jenisCOA, conn);
//        } catch (Exception ex) {
//            throw new Exception(ex.getMessage());
//        } finally {
//            if (conn != null) {
//                conn.close();
//            }
//        }
//    }

    @Override
    @SuppressWarnings("ConvertToTryWithResources")
    public List<LampiranIPerdaAPBD> getKompilasiRealisasiAPBDMurni(short tahun, List<String> kodePemdas, short month, short jenisCOA) throws Exception {
        Connection conn = null;
        try {
            conn = createConnection();
            return bl.getKompilasiRealisasiAPBD(tahun, kodePemdas, SIKDUtil.APBD_MURNI_SHORT, month, jenisCOA, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }

    }


    @Override
    @SuppressWarnings("ConvertToTryWithResources")
    public List<RekapitulasiGajiPegawai> getRekapitulasiGajiPegawai(short tahun, String kodeSatker, short month) throws Exception {
        Connection conn = null;
        try {
            conn = createConnection();
            return bl.getRekapitulasiGajiPegawai(tahun, kodeSatker, month, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }

    }

    @Override
    @SuppressWarnings("ConvertToTryWithResources")
    public List<RincianPerhitunganFihakKetiga> getRincianPerhitunganFihakKetiga(short tahun, String kodeSatker) throws Exception {
        Connection conn = null;
        try {
            conn = createConnection();
            return bl.getRincianPFK(tahun, kodeSatker, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }

    }

    @Override
    @SuppressWarnings("ConvertToTryWithResources")
    public void createPerhitunganFihakKetiga(PerhitunganFihakKetiga obj, short iotype) throws Exception {
        Connection conn = null;
        try {
            conn = createConnection();
            bl.createPerhitunganFihakKetiga(obj, iotype, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }

    }

    @Override
    @SuppressWarnings("ConvertToTryWithResources")
    public void updatePerhitunganFihakKetiga(PerhitunganFihakKetiga oldObj, PerhitunganFihakKetiga newObj, short iotype) throws Exception {
        Connection conn = null;
        try {
            conn = createConnection();
            bl.updatePerhitunganFihakKetiga(oldObj, newObj, iotype, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }

    }

    @Override
    @SuppressWarnings("ConvertToTryWithResources")
    public void deleteRincianPerhitunganFihakKetiga(PerhitunganFihakKetiga obj) throws Exception {
        Connection conn = null;
        try {
            conn = createConnection();
            bl.deletePerhitunganFihakKetiga(obj, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }

    }

    @Override
    @SuppressWarnings("ConvertToTryWithResources")
    public List<RincianPinjamanDaerah> getRincianPinjamanDaerah(short tahun, String kodeSatker) throws Exception {
        Connection conn = null;
        try {
            conn = createConnection();
            return bl.getRincianPinjamanDaerah(tahun, kodeSatker, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }

    }

    @Override
    @SuppressWarnings("ConvertToTryWithResources")
    public void createPinjamanDaerah(PinjamanDaerah obj, short iotype) throws Exception {
        Connection conn = null;
        try {
            conn = createConnection();
            bl.createPinjamanDaerah(obj, iotype, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }

    }

    @Override
    @SuppressWarnings("ConvertToTryWithResources")
    public void updatePinjamanDaerah(PinjamanDaerah oldObj, PinjamanDaerah newObj) throws Exception {
        Connection conn = null;
        try {
            conn = createConnection();
            bl.updatePinjamanDaerah(oldObj, newObj, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }

    }

    @Override
    @SuppressWarnings("ConvertToTryWithResources")
    public void deleteRincianPinjamanDaerah(PinjamanDaerah obj) throws Exception {
        Connection conn = null;
        try {
            conn = createConnection();
            bl.deletePinjamanDaerah(obj, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }

    }
    
    /**
     *
     * @param tahun
     * @param bulan
     * @param kodeSatker
     * @param kodeData
     * @param kodeFungsi
     * @return
     * @throws Exception
     */
    @Override
    @SuppressWarnings("ConvertToTryWithResources")
    public List<NilaiFungsiPemda> getRealisasiKegiatanBerdasarkanFungsi(short tahun, short bulan, String kodeSatker, short kodeData, String kodeFungsi) throws Exception{
        Connection conn = null;
        try {
            conn = createConnection();
            return bl.getRealisasiKegiatanBerdasarkanFungsi(tahun, bulan, kodeSatker, kodeData, kodeFungsi, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    
    /**
     *
     * @param tahun
     * @param bulan
     * @param kodeSatker
     * @param sumberDana
     * @return
     * @throws Exception
     */
    @Override
    @SuppressWarnings("ConvertToTryWithResources")
    public List<NilaiFungsiPemda> getRealisasiBelanjaBerdasarkanSumberDana(short tahun, short bulan, String kodeSatker, short sumberDana) throws Exception{
        Connection conn = null;
        try {
            conn = createConnection();
            return bl.getRealisasiBelanjaBerdasarkanSumberDana(tahun, bulan, kodeSatker, sumberDana, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    
    /**
     *
     * @param year
     * @param kodeSatker
     * @param kodeData
     * @param jenisCOA
     * @return
     * @throws Exception
     */
    @Override
    @SuppressWarnings("ConvertToTryWithResources")
    public List<LampiranIPerdaAPBD> getLampiranIPenjabaranAPBDbyYear(short year, String kodeSatker, short kodeData, short jenisCOA) throws Exception{
        Connection conn = null;
        try {
            conn = createConnection();
            return bl.getLampiranIPenjabaranAPBDbyYear(year, kodeSatker, kodeData, jenisCOA, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    
    /**
     *
     * @param ma
     * @throws Exception
     */
    @Override
    @SuppressWarnings("ConvertToTryWithResources")
    public void deleteMappingAccounts(MappingAccount ma) throws Exception{
        Connection conn = null;
        try {
            conn = createConnection();
            bl.deleteMappingAccounts(ma, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }

    /**
     *
     * @param ma
     * @throws Exception
     */
    @Override
    @SuppressWarnings("ConvertToTryWithResources")
    public void createMappingAccounts(MappingAccount ma) throws Exception{
        Connection conn = null;
        try {
            conn = createConnection();
            bl.createMappingAccounts(ma, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }

    /**
     *
     * @param level
     * @return
     * @throws Exception
     */
    @Override
    @SuppressWarnings("ConvertToTryWithResources")
    public List<MappingAccount> getMappingAccounts(short level) throws Exception{
        Connection conn = null;
        try {
            conn = createConnection();
            return bl.getMappingAccounts(level, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }

    /**
     *
     * @param level
     * @return
     * @throws Exception
     */
    @Override
    @SuppressWarnings("ConvertToTryWithResources")
    public List<SimpleAccount> getAccrualAccounts(short[] level) throws Exception{
        Connection conn = null;
        try {
            conn = createConnection();
            return bl.getAccrualAccounts(level, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    
    
    @SuppressWarnings("ConvertToTryWithResources")
    public void deleteGfsMappingAccounts(MappingAccount ma) throws Exception{
        Connection conn = null;
        try {
            conn = createConnection();
            bl.deleteGfsMappingAccounts(ma, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    
    @SuppressWarnings("ConvertToTryWithResources")
    public void createGfsMappingAccounts(MappingAccount ma) throws Exception{
        Connection conn = null;
        try {
            conn = createConnection();
            bl.createGfsMappingAccounts(ma, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    @SuppressWarnings("ConvertToTryWithResources")
    public List<MappingAccount> getGfsMappingAccounts(short[] levels) throws Exception{
        Connection conn = null;
        try {
            conn = createConnection();
            return bl.getGfsMappingAccounts(levels, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    @SuppressWarnings("ConvertToTryWithResources")
    public List<SimpleAccount> getGfsAccounts(short[] level) throws Exception{
        Connection conn = null;
        try {
            conn = createConnection();
            return bl.getGfsAccounts(level, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    
    /**
     *
     * @param kodeSatker
     * @param tahun
     * @param bulan
     * @param kodeData
     * @param jenisCOA
     * @param userName
     * @param password
     * @return
     * @throws Exception
     */
    @Override
    @SuppressWarnings("ConvertToTryWithResources")
    public RealisasiAPBD getLRA(String kodeSatker, short tahun , short bulan, short kodeData, short jenisCOA, String userName, String password) throws Exception{
        Connection conn = null;
        try {
            conn = createConnection();
            return bl.getLRA(kodeSatker, tahun, bulan, kodeData, jenisCOA, userName, password, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }

    /**
     *
     * @param indexLRA
     * @return
     * @throws Exception
     */
    @Override
    @SuppressWarnings("ConvertToTryWithResources")
    public List<RealisasiKegiatanAPBD> getLRAKegiatan(long indexLRA) throws Exception{
        Connection conn = null;
        try {
            conn = createConnection();
            return bl.getLRAKegiatan(indexLRA, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }

    /**
     *
     * @param indexKegiatanLRA
     * @return
     * @throws Exception
     */
    @Override
    @SuppressWarnings("ConvertToTryWithResources")
    public List<RealisasiKodeRekeningAPBD> getLRAAkun(long indexKegiatanLRA) throws Exception{
        Connection conn = null;
        try {
            conn = createConnection();
            return bl.getLRAAkun(indexKegiatanLRA, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    
    /**
     *
     * @param obj
     * @return
     * @throws Exception
     */
    @Override
    public long createLRA(RealisasiAPBD_WS obj, short iotype) throws Exception{
        Connection conn = null;
        if(serviceSQL == null) serviceSQL = new WebServiceSQL();
        try {
            conn = createConnection();
            return serviceSQL.createRealisasiAPBD(obj, iotype, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    
    /**
     *
     * @param indexLRA
     * @param obj
     * @throws Exception
     */
    public void updateLRA(long indexLRA, RealisasiAPBD_WS obj) throws Exception{
        Connection conn = null;
        if(serviceSQL == null) serviceSQL = new WebServiceSQL();
        try {
            conn = createConnection();
            serviceSQL.updateRealisasiAPBD(indexLRA, obj, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }

    /**
     *
     * @param indexLRA
     * @param obj
     * @return
     * @throws Exception
     */
    @Override
    public long createLRAKegiatan(long indexLRA, RealisasiKegiatanAPBD_WS obj) throws Exception{
        Connection conn = null;
        if(serviceSQL == null) serviceSQL = new WebServiceSQL();
        try {
            conn = createConnection();
            return serviceSQL.createRealisasiKegiatanAPBD(indexLRA, obj, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }

    /**
     *
     * @param indexKegiatan
     * @param obj
     * @throws Exception
     */
    @Override
    public void createLRAAkun(long indexKegiatan, RealisasiKodeRekeningAPBD_WS obj) throws Exception{
        Connection conn = null;
        if(serviceSQL == null) serviceSQL = new WebServiceSQL();
        try {
            conn = createConnection();
            serviceSQL.createRealisasiRekeningAPBD(indexKegiatan, obj, conn);            
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    
    public List<MappingNonStandar> getMappingNonStandarAccountApbds(short level, short tahun, String kodeSatker) throws Exception{
        Connection conn = null;
        if(serviceSQL == null) serviceSQL = new WebServiceSQL();
        try {
            conn = createConnection();
            return bl.getMappingNonStandarAccountApbds(level, tahun, kodeSatker, conn);            
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    
    
    public List<MappingNonStandar> getMappingNonStandarAccountLras(short level, short tahun, String kodeSatker) throws Exception{
        Connection conn = null;
        if(serviceSQL == null) serviceSQL = new WebServiceSQL();
        try {
            conn = createConnection();
            return bl.getMappingNonStandarAccountLras(level, tahun, kodeSatker, conn);            
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    
    public List<MappingNonStandar> getMappingNonStandarAccountDths(short level, short tahun, String kodeSatker) throws Exception{
        Connection conn = null;
        if(serviceSQL == null) serviceSQL = new WebServiceSQL();
        try {
            conn = createConnection();
            return bl.getMappingNonStandarAccountDths(level, tahun, kodeSatker, conn);            
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    
    public List<MappingNonStandar> getMappingNonStandarAccountNeracas(short level, short tahun, String kodeSatker) throws Exception{
        Connection conn = null;
        if(serviceSQL == null) serviceSQL = new WebServiceSQL();
        try {
            conn = createConnection();
            return bl.getMappingNonStandarAccountNeracas(level, tahun, kodeSatker, conn);            
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    
    
    @SuppressWarnings("ConvertToTryWithResources")
    public MappingNonStandar createMappingAccountNonStandars(MappingNonStandar ma) throws Exception{
        Connection conn = null;
        try {
            conn = createConnection();
            return bl.createMappingAccountNonStandars(ma, conn);            
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    
    /**
     *
     * @param ma
     * @throws Exception
     */
    @SuppressWarnings("ConvertToTryWithResources")
    public void deleteMappingAccountNonStandars(MappingNonStandar ma) throws Exception{
        Connection conn = null;        
        try {
            conn = createConnection();
            bl.deleteMappingAccountNonStandars(ma, conn);            
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    
}
