/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.office.ejb.session;

import app.sikd.entity.backoffice.APBD;
import app.sikd.entity.backoffice.ArusKas;
import app.sikd.entity.backoffice.ArusKasSaldo;
import app.sikd.entity.backoffice.KegiatanAPBD;
import app.sikd.entity.backoffice.KodeRekeningAPBD;
import app.sikd.entity.backoffice.Neraca;
import app.sikd.entity.backoffice.NeracaAkunJenis;
import app.sikd.entity.backoffice.NeracaAkunKelompok;
import app.sikd.entity.backoffice.NeracaAkunObjek;
import app.sikd.entity.backoffice.NeracaAkunUtama;
import app.sikd.entity.backoffice.ObjArusKasAkun;
import app.sikd.entity.backoffice.RealisasiAPBD;
import app.sikd.entity.backoffice.RealisasiKegiatanAPBD;
import app.sikd.entity.backoffice.RealisasiKodeRekeningAPBD;
import java.util.List;
import javax.ejb.Remote;

/**
 *
 * @author sora
 */
@Remote
public interface DMSessionBeanRemote {

    public void deleteNeracaAkunUtama(long index) throws Exception;
    public void deleteNeracaAkunKelompok(long index) throws Exception;
    public void deleteNeracaAkunJenis(long index) throws Exception;
    public void deleteNeracaAkunObjek(long index) throws Exception;
    public NeracaAkunObjek createNeracaAkunObjek(long indexAkunJenis, NeracaAkunObjek obj) throws Exception;
    public NeracaAkunJenis createNeracaAkunJenis(long indexAkunKelompok, NeracaAkunJenis obj) throws Exception;
    public NeracaAkunKelompok createNeracaAkunKelompok(long indexAkunUtama, NeracaAkunKelompok obj) throws Exception;
    public NeracaAkunUtama createNeracaAkunUtama(long indexNeraca, NeracaAkunUtama obj) throws Exception;
    public Neraca createNeracaHeader(Neraca neraca, short iotype) throws Exception;
    public Neraca getNeracaHeader(String kodeSatker, short tahun, short semester, String judul) throws Exception;
    public List<NeracaAkunUtama> getNeracaAkunStruktur(long indexNeraca) throws Exception;
    
    public void deleteArusKas(long index) throws Exception;
    public void deleteArusKasRinci(long index, String table, String field) throws Exception;
    public ArusKas createArusKasHeader(ArusKas arusKas, short iotype) throws Exception;
    public ObjArusKasAkun createArusKasRinci(long indexKas, ObjArusKasAkun obj, String table, String field) throws Exception;
    
    public ArusKas getArusKasHeader(String kodeSatker, short tahun, String judul) throws Exception;
    public List<ObjArusKasAkun> getArusKasRinci(long indexArusKas, String table, String field) throws Exception;    
    public ArusKasSaldo createArusKasSaldo(long indexKas, ArusKasSaldo obj) throws Exception;
    
    public APBD getAPBDHeader(short year, String kodeSatker, short kodeData, short jenisCOA) throws Exception;
    public APBD getApbdStruktur(String kodeSatker, short tahun, short kodeData, short jenisCOA) throws Exception;
    public APBD createAPBDHeader(APBD apbd, short iotype) throws Exception;
    public APBD updateAPBDHeader(APBD apbd) throws Exception;
    public void deleteAPBD(long id) throws Exception;    
    public KegiatanAPBD createKegiatanAPBD(long indexApbd, KegiatanAPBD kegiatan) throws Exception;
    public KegiatanAPBD updateKegiatanAPBD(KegiatanAPBD kegiatan) throws Exception;
    public void deleteKegiatanAPBD(long id) throws Exception;
    public void createRekeningAPBD(long indexKegiatan, KodeRekeningAPBD rekening) throws Exception;
    public void deleteRekeningAPBD(long indexKegiatan, KodeRekeningAPBD rekening) throws Exception;
    
    public RealisasiAPBD getRealisasiApbdHeader(String kodeSatker, short tahun, short bulan, short jenisCOA) throws Exception;
    public RealisasiAPBD getRealisasiApbdHeader(String kodeSatker, short tahun, short bulan, short kodeData, short jenisCOA) throws Exception;
    public RealisasiAPBD getRealisasiApbdStruktur(String kodeSatker, short tahun, short bulan, short kodeData, short jenisCOA) throws Exception;
    public RealisasiAPBD createRealiasiAPBDHeader(RealisasiAPBD apbd, short iotype) throws Exception;
    public RealisasiAPBD updateRealisasiAPBDHeader(RealisasiAPBD apbd) throws Exception;
    public void deleteRealisasiAPBD(long id) throws Exception;
    public RealisasiKegiatanAPBD createRealisasiKegiatanAPBD(long indexApbd, RealisasiKegiatanAPBD kegiatan) throws Exception;
    public RealisasiKegiatanAPBD updateRealisasiKegiatanAPBD(RealisasiKegiatanAPBD kegiatan) throws Exception;
    public void deleteRealisasiKegiatanAPBD(long id) throws Exception;
    public void createRealisasiRekeningAPBD(long indexKegiatan, RealisasiKodeRekeningAPBD rekening) throws Exception;
    public void deleteRealisasiRekeningAPBD(long indexKegiatan, RealisasiKodeRekeningAPBD rekening) throws Exception;

    
    
}
