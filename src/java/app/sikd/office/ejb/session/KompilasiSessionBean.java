/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.office.ejb.session;

import app.sikd.dbapi.SIKDConnectionManager;
import app.sikd.entity.backoffice.AkunKompilasi;
import app.sikd.entity.backoffice.AkunKompilasiGFS;
import app.sikd.entity.backoffice.KompilasiApbd1364;
import app.sikd.office.ejb.bl.IKompilasiBL;
import app.sikd.office.ejb.bl.KompilasiBL;
import java.sql.Connection;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.Remote;
import javax.ejb.Stateless;

/**
 *
 * @author sora
 */
@Stateless
@Remote({KompilasiSessionBeanRemote.class})
public class KompilasiSessionBean implements KompilasiSessionBeanRemote {

    SIKDConnectionManager conMan;
    IKompilasiBL bl;

    @PostConstruct
    public void init() {
        try {
            conMan = new SIKDConnectionManager();
            bl = new KompilasiBL();
        } catch (Exception ex) {
            Logger.getLogger(KompilasiSessionBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    Connection createConnection() throws Exception {
        return conMan.createConnection("sikd.properties");
    }
    
    @SuppressWarnings("ConvertToTryWithResources")
    public List<KompilasiApbd1364> getApbd13Kompilasi64(short tahun, short kodeData) throws Exception{
        Connection conn = null;
        try {
            conn = createConnection();
            return bl.getApbd13Kompilasi64(tahun, kodeData, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    
    @SuppressWarnings("ConvertToTryWithResources")
    public void deleteApbdKompilasi64(short tahun, short kodeData) throws Exception{
        Connection conn = null;
        try {
            conn = createConnection();
            bl.deleteApbdKompilasi64(tahun, kodeData, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    
    @SuppressWarnings("ConvertToTryWithResources")
    public KompilasiApbd1364 createApbdKompilasi64(KompilasiApbd1364 apbd1364) throws Exception{
        Connection conn = null;
        try {
            conn = createConnection();
            return bl.createApbdKompilasi64(apbd1364, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    
    @SuppressWarnings("ConvertToTryWithResources")
    public List<AkunKompilasiGFS> getApbdKompilasi64(short tahun, short kodeData, String kodepemda) throws Exception{
        Connection conn = null;
        try {
            conn = createConnection();
            return bl.getApbdKompilasi64(tahun, kodeData, kodepemda, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }

    /**
     *
     * @param tahun 
     * @param pemdaCodes 
     * @return
     * @throws Exception
     */
    @SuppressWarnings("ConvertToTryWithResources")
    public List<AkunKompilasi> getApbdKompilasiLangsungs(short tahun, List<String> pemdaCodes) throws Exception {
        Connection conn = null;
        try {
            conn = createConnection();
            return bl.getApbdKompilasiLangsungs(tahun, pemdaCodes, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
}
