/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.office.ejb.session;

import app.sikd.entity.apbd.LampiranIIIPerdaAPBD;
import app.sikd.entity.apbd.LampiranIIPerdaAPBD;
import app.sikd.entity.apbd.LampiranILra;
import app.sikd.entity.apbd.LampiranIPerdaAPBD;
import app.sikd.entity.apbd.LampiranIVPerdaAPBD;
import app.sikd.entity.apbd.LampiranVPerdaAPBD;
import app.sikd.entity.backoffice.APBDPerda;
import app.sikd.entity.backoffice.NilaiFungsiPemda;
import app.sikd.entity.backoffice.PerhitunganFihakKetiga;
import app.sikd.entity.backoffice.PinjamanDaerah;
import app.sikd.entity.backoffice.RealisasiAPBD;
import app.sikd.entity.backoffice.RealisasiKegiatanAPBD;
import app.sikd.entity.backoffice.RealisasiKodeRekeningAPBD;
import app.sikd.entity.backoffice.RekapitulasiGajiPegawai;
import app.sikd.entity.backoffice.RincianPerhitunganFihakKetiga;
import app.sikd.entity.backoffice.RincianPinjamanDaerah;
import app.sikd.entity.utilitas.MappingAccount;
import app.sikd.entity.utilitas.MappingNonStandar;
import app.sikd.entity.utilitas.SimpleAccount;
import app.sikd.entity.ws.RealisasiAPBD_WS;
import app.sikd.entity.ws.RealisasiKegiatanAPBD_WS;
import app.sikd.entity.ws.RealisasiKodeRekeningAPBD_WS;
import java.sql.Timestamp;
import java.util.List;
import javax.ejb.Remote;

/**
 *
 * @author detra
 */
@Remote
public interface APBDSessionBeanRemote {
   
    public List<LampiranIPerdaAPBD> getLampiranIPerdaAPBDMurni(short tahun, String kodeSatker, short jenisCOA) throws Exception;
    public List<LampiranIIPerdaAPBD> getLampiranIIPerdaAPBDMurni(short tahun, String kodeSatker, short jenisCOA) throws Exception;
    public List<LampiranIIIPerdaAPBD> getLampiranIIIPerdaAPBDMurni(short tahun, String kodeSatker, String kodeSKPD, short jenisCOA) throws Exception;
    public List<LampiranIVPerdaAPBD> getLampiranIVPerdaAPBDMurni(short tahun, String kodeSatker, short jenisCOA) throws Exception;
    public List<LampiranVPerdaAPBD> getLampiranVPerdaAPBDMurni(short tahun, String kodeSatker, short jenisCOA) throws Exception;
    
    public List<LampiranIPerdaAPBD> getKompilasiLampiranIPerdaAPBDMurni(short year, List<String> kodePemdas, short jenisCOA) throws Exception;
    public List<LampiranIPerdaAPBD> getKompilasiLampiranIPerdaAPBDPerubahan(short year, List<String> kodePemdas, short jenisCOA) throws Exception;
    public List<LampiranIIPerdaAPBD> getKompilasiLampiranIIPerdaAPBDMurni(short year, List<String> kodePemdas, short jenisCOA) throws Exception;    
    public List<LampiranIIPerdaAPBD> getKompilasiLampiranIIPerdaAPBDPerubahan(short year, List<String> kodePemdas, short jenisCOA) throws Exception;
    
    public List<LampiranIPerdaAPBD> getLampiranIPerdaAPBDPerubahan(short tahun, String kodeSatker, short jenisCOA) throws Exception ;
    public List<LampiranIIPerdaAPBD> getLampiranIIPerdaAPBDPerubahan(short tahun, String kodeSatker, short jenisCOA) throws Exception;
    
    public List<LampiranILra> getRealisasiAPBD(short year, String kodeSatker, short month, short jenisCOA) throws Exception;
    public List<LampiranILra> getRealisasiAPBDMurni(short tahun, String kodeSatker, short month, short jenisCOA) throws Exception;
    public List<LampiranILra> getRealisasiAPBDPerubahan(short tahun, String kodeSatker, short month, short jenisCOA) throws Exception;
    
    public List<LampiranIPerdaAPBD> getKompilasiRealisasiAPBDMurni(short tahun, List<String> kodePemdas, short month, short jenisCOA) throws Exception;
//    public List<LampiranIPerdaAPBD> getKompilasiRealisasiAPBDPerubahan(short tahun, List<String> kodePemdas, short month, short jenisCOA) throws Exception;
//    public List<LampiranIPerdaAPBD> getRealisasiAPBDPerubahan(short tahun, String kodeSatker, short month, short jenisCOA) throws Exception;

    public List<String> getDinasKirimRealisasiAPBD(short year, String kodeSatker, short kodeData, short jenisCOA) throws Exception;
    public Timestamp getTanggalDinasKirimRealisasiAPBD(short year, String kodeSatker, short kodeData, short jenisCOA) throws Exception;

    public List<String> getDinasKirimAPBD(short year, String kodeSatker, short kodeData, short jenisCOA) throws Exception;
    public Timestamp getTanggalDinasKirimAPBD(short year, String kodeSatker, short kodeData, short jenisCOA) throws Exception;
    public String getUrusan(String kodeUrusan) throws Exception;
    
    public List<LampiranIPerdaAPBD> getRingkasanPembiayaanMurni(short tahun, String kodeSatker, short jenisCOA) throws Exception;
    public List<LampiranIPerdaAPBD> getRingkasanPembiayaanMurni(short year, List<String> kodepemdas, short jenisCOA) throws Exception;

    public List<RekapitulasiGajiPegawai> getRekapitulasiGajiPegawai(short tahun, String kodeSatker, short month) throws Exception;
    
    public List<RincianPerhitunganFihakKetiga> getRincianPerhitunganFihakKetiga(short tahun, String kodeSatker) throws Exception;
    public void createPerhitunganFihakKetiga(PerhitunganFihakKetiga obj, short iotype) throws Exception;
    public void updatePerhitunganFihakKetiga(PerhitunganFihakKetiga oldObj, PerhitunganFihakKetiga newObj, short iotype) throws Exception;
    public void deleteRincianPerhitunganFihakKetiga(PerhitunganFihakKetiga obj) throws Exception;
    
    public List<RincianPinjamanDaerah> getRincianPinjamanDaerah(short tahun, String kodeSatker) throws Exception;
    public void createPinjamanDaerah(PinjamanDaerah obj, short iotype) throws Exception;
    public void updatePinjamanDaerah(PinjamanDaerah oldObj, PinjamanDaerah newObj) throws Exception;
    public void deleteRincianPinjamanDaerah(PinjamanDaerah obj) throws Exception;

    public APBDPerda getAPBDMurniPerdaNumber(short tahun, String kodeSatker, short jenisCOA) throws Exception;

    public APBDPerda getRealisasiAPBDMurniPerdaNumber(short tahun, String kodeSatker, short jenisCOA) throws Exception;

    public APBDPerda getAPBDPerubahanPerdaNumber(short tahun, String kodeSatker, short jenisCOA) throws Exception;

    public APBDPerda getRealisasiAPBDPerubahanPerdaNumber(short tahun, String kodeSatker, short jenisCOA) throws Exception;
    
    public List<NilaiFungsiPemda> getRealisasiKegiatanBerdasarkanFungsi(short tahun, short bulan, String kodeSatker, short kodeData, String kodeFungsi) throws Exception;
    public List<NilaiFungsiPemda> getRealisasiBelanjaBerdasarkanSumberDana(short tahun, short bulan, String kodeSatker, short sumberDana) throws Exception;
    
    public List<LampiranIPerdaAPBD> getLampiranIPenjabaranAPBDbyYear(short year, String kodeSatker, short kodeData, short jenisCOA) throws Exception;
    
    public void deleteMappingAccounts(MappingAccount ma) throws Exception;
    public void createMappingAccounts(MappingAccount ma) throws Exception;
    public List<MappingAccount> getMappingAccounts(short level) throws Exception;
    public List<SimpleAccount> getAccrualAccounts(short[] level) throws Exception;
    
    public void deleteGfsMappingAccounts(MappingAccount ma) throws Exception;
    public void createGfsMappingAccounts(MappingAccount ma) throws Exception;
    public List<MappingAccount> getGfsMappingAccounts(short[] levels) throws Exception;
    public List<SimpleAccount> getGfsAccounts(short[] level) throws Exception;
    
    public long createLRA(RealisasiAPBD_WS obj, short iotype) throws Exception;
    public void updateLRA(long indexLRA, RealisasiAPBD_WS obj) throws Exception;
    public long createLRAKegiatan(long indexLRA, RealisasiKegiatanAPBD_WS obj) throws Exception;
    public void createLRAAkun(long indexKegiatan, RealisasiKodeRekeningAPBD_WS obj) throws Exception;
    public RealisasiAPBD getLRA(String kodeSatker, short tahun , short bulan, short kodeData, short jenisCOA, String userName, String password) throws Exception;
    public List<RealisasiKegiatanAPBD> getLRAKegiatan(long indexLRA) throws Exception;
    public List<RealisasiKodeRekeningAPBD> getLRAAkun(long indexKegiatanLRA) throws Exception;
    
    public List<MappingNonStandar> getMappingNonStandarAccountApbds(short level, short tahun, String kodeSatker) throws Exception;
    public List<MappingNonStandar> getMappingNonStandarAccountLras(short level, short tahun, String kodeSatker) throws Exception;
    public List<MappingNonStandar> getMappingNonStandarAccountDths(short level, short tahun, String kodeSatker) throws Exception;
    public List<MappingNonStandar> getMappingNonStandarAccountNeracas(short level, short tahun, String kodeSatker) throws Exception;
    public MappingNonStandar createMappingAccountNonStandars(MappingNonStandar ma) throws Exception;
    public void deleteMappingAccountNonStandars(MappingNonStandar ma) throws Exception;
    
}
