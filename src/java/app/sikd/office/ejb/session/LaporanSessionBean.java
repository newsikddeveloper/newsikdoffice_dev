/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.office.ejb.session;

import app.sikd.dbapi.SIKDConnectionManager;
import app.sikd.entity.backoffice.ArusKasReport;
import app.sikd.entity.backoffice.LOReport;
import app.sikd.entity.backoffice.Neraca;
import app.sikd.entity.backoffice.NeracaAkunJenis;
import app.sikd.entity.backoffice.NeracaAkunKelompok;
import app.sikd.entity.backoffice.NeracaAkunObjek;
import app.sikd.entity.backoffice.NeracaAkunUtama;
import app.sikd.entity.backoffice.NeracaReport;
import app.sikd.entity.backoffice.PerubahanEkuitas;
import app.sikd.entity.backoffice.PerubahanSAL;
import app.sikd.office.ejb.bl.ILaporanBusinessLogic;
import app.sikd.office.ejb.bl.LaporanBusinessLogic;
import java.sql.Connection;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.Stateless;

/**
 *
 * @author sora
 */
@Stateless
public class LaporanSessionBean implements LaporanSessionBeanRemote {

    ILaporanBusinessLogic bl;
    SIKDConnectionManager conMan;

    @PostConstruct
    public void init() {
        try {
            conMan = new SIKDConnectionManager();
            bl = new LaporanBusinessLogic();
        } catch (Exception ex) {
            Logger.getLogger(LaporanSessionBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    Connection createConnection() throws Exception {
        return conMan.createConnection("sikd.properties");
    }

    @Override
    @SuppressWarnings("ConvertToTryWithResources")
    public List<NeracaReport> getNeracaReports(short tahun, short semester, String kodeSatker, String judulNeraca ) throws Exception {
        Connection conn = null;
        try {
            conn = createConnection();
            return bl.getNeracaReports(tahun, semester, kodeSatker, judulNeraca, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }

    }
    
    /**
     *
     * @param tahun
     * @param semester
     * @param kodeSatker
     * @return
     * @throws Exception
     */
    @Override
    @SuppressWarnings("ConvertToTryWithResources")
    public List<String> getNeracaLabels(short tahun, short semester, String kodeSatker) throws Exception {
        Connection conn = null;
        try {
            conn = createConnection();
            return bl.getNeracaLabels(tahun, semester, kodeSatker, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }

    }
    
    /**
     *
     * @param tahun
     * @param semester
     * @param kodeSatker
     * @param judulNeraca
     * @return
     * @throws Exception
     */
    @Override
    @SuppressWarnings("ConvertToTryWithResources")
    public Neraca getNeraca(short tahun, short semester, String kodeSatker, String judulNeraca) throws Exception {
        Connection conn = null;
        try {
            conn = createConnection();
            return bl.getNeraca(tahun, semester, kodeSatker, judulNeraca, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }

    }
    
    /**
     *
     * @param neraca
     * @return
     * @throws Exception
     */
    @Override
    @SuppressWarnings("ConvertToTryWithResources")
    public Neraca createNeracaStruktur(Neraca neraca, short iotype) throws Exception {
        Connection conn = null;
        try {
            conn = createConnection();
            return bl.createNeracaStruktur(neraca, iotype, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }

    }
    
    /**
     *
     * @param neraca
     * @return
     * @throws Exception
     */
    @Override
    @SuppressWarnings("ConvertToTryWithResources")
    public long createNeraca(Neraca neraca, short iotype) throws Exception {
        Connection conn = null;
        try {
            conn = createConnection();
            return bl.createNeraca(neraca, iotype, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }

    }

    /**
     *
     * @param neraca
     * @throws Exception
     */
    @Override
    @SuppressWarnings("ConvertToTryWithResources")
    public void updateNeraca(Neraca neraca) throws Exception {
        Connection conn = null;
        try {
            conn = createConnection();
            bl.updateNeraca(neraca, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }

    }

    /**
     *
     * @param neraca
     * @throws Exception
     */
    @Override
    @SuppressWarnings("ConvertToTryWithResources")
    public void deleteNeraca(Neraca neraca) throws Exception {
        Connection conn = null;
        try {
            conn = createConnection();
            bl.deleteNeraca(neraca, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }

    }
    
    /**
     *
     * @param indexNeraca
     * @param obj
     * @return
     * @throws Exception
     */
    @Override
    @SuppressWarnings("ConvertToTryWithResources")
    public long createNeracaAkunUtama(long indexNeraca, NeracaAkunUtama obj) throws Exception {
        Connection conn = null;
        try {
            conn = createConnection();
            return bl.createNeracaAkunUtama(indexNeraca, obj, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }

    }
    
    /**
     *
     * @param indexNeraca
     * @return
     * @throws Exception
     */
    @Override
    @SuppressWarnings("ConvertToTryWithResources")
    public List<NeracaAkunUtama> getNeracaAkunUtamas(long indexNeraca) throws Exception {
        Connection conn = null;
        try {
            conn = createConnection();
            return bl.getNeracaAkunUtamas(indexNeraca, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }

    }

    /**
     *
     * @param obj
     * @throws Exception
     */
    @Override
    @SuppressWarnings("ConvertToTryWithResources")
    public void updateNeracaAkunUtama(NeracaAkunUtama obj) throws Exception {
        Connection conn = null;
        try {
            conn = createConnection();
             bl.updateNeracaAkunUtama(obj, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }

    }

    /**
     *
     * @param obj
     * @throws Exception
     */
    @Override
    @SuppressWarnings("ConvertToTryWithResources")
    public void deleteNeracaAkunUtama(NeracaAkunUtama obj) throws Exception {
        Connection conn = null;
        try {
            conn = createConnection();
             bl.deleteNeracaAkunUtama(obj, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }

    }
    
    /**
     *
     * @param indexNeracaAkunUtama
     * @param obj
     * @return
     * @throws Exception
     */
    @Override
    @SuppressWarnings("ConvertToTryWithResources")
    public long createNeracaAkunKelompok(long indexNeracaAkunUtama, NeracaAkunKelompok obj) throws Exception {
        Connection conn = null;
        try {
            conn = createConnection();
            return bl.createNeracaAkunKelompok(indexNeracaAkunUtama, obj, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }

    }
    
    /**
     *
     * @param indexNeracaAkunUtama
     * @return
     * @throws Exception
     */
    @Override
    @SuppressWarnings("ConvertToTryWithResources")
    public List<NeracaAkunKelompok> getNeracaAkunKelompoks(long indexNeracaAkunUtama) throws Exception {
        Connection conn = null;
        try {
            conn = createConnection();
            return bl.getNeracaAkunKelompoks(indexNeracaAkunUtama, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }

    }

    /**
     *
     * @param obj
     * @throws Exception
     */
    @Override
    @SuppressWarnings("ConvertToTryWithResources")
    public void updateNeracaAkunKelompok(NeracaAkunKelompok obj) throws Exception {
        Connection conn = null;
        try {
            conn = createConnection();
             bl.updateNeracaAkunKelompok(obj, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }

    }

    /**
     *
     * @param obj
     * @throws Exception
     */
    @Override
    @SuppressWarnings("ConvertToTryWithResources")
    public void deleteNeracaAkunKelompok(NeracaAkunKelompok obj) throws Exception {
        Connection conn = null;
        try {
            conn = createConnection();
            bl.deleteNeracaAkunKelompok(obj, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }

    }
    
    /**
     *
     * @param indexNeracaAkunKelompok
     * @param obj
     * @return
     * @throws Exception
     */
    @Override
    @SuppressWarnings("ConvertToTryWithResources")
    public long createNeracaAkunJenis(long indexNeracaAkunKelompok, NeracaAkunJenis obj) throws Exception {
        Connection conn = null;
        try {
            conn = createConnection();
            return bl.createNeracaAkunJenis(indexNeracaAkunKelompok, obj, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }

    }
    
    /**
     *
     * @param indexNeracaAkunKelompok
     * @return
     * @throws Exception
     */
    @Override
    @SuppressWarnings("ConvertToTryWithResources")
    public List<NeracaAkunJenis> getNeracaAkunJeniss(long indexNeracaAkunKelompok) throws Exception {
        Connection conn = null;
        try {
            conn = createConnection();
            return bl.getNeracaAkunJeniss(indexNeracaAkunKelompok, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }

    }

    /**
     *
     * @param obj
     * @throws Exception
     */
    @Override
    @SuppressWarnings("ConvertToTryWithResources")
    public void updateNeracaAkunJenis(NeracaAkunJenis obj) throws Exception {
        Connection conn = null;
        try {
            conn = createConnection();
            bl.updateNeracaAkunJenis(obj, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }

    }

    /**
     *
     * @param obj
     * @throws Exception
     */
    @Override
    @SuppressWarnings("ConvertToTryWithResources")
    public void deleteNeracaAkunJenis(NeracaAkunJenis obj) throws Exception {
        Connection conn = null;
        try {
            conn = createConnection();
             bl.deleteNeracaAkunJenis(obj, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }

    }
    
    /**
     *
     * @param indexNeracaAkunJenis
     * @param obj
     * @return
     * @throws Exception
     */
    @Override
    @SuppressWarnings("ConvertToTryWithResources")
    public long createNeracaAkunObjek(long indexNeracaAkunJenis, NeracaAkunObjek obj) throws Exception {
        Connection conn = null;
        try {
            conn = createConnection();
            return bl.createNeracaAkunObjek(indexNeracaAkunJenis, obj, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }

    }
    
    /**
     *
     * @param indexNeracaAkunJenis
     * @return
     * @throws Exception
     */
    @Override
    @SuppressWarnings("ConvertToTryWithResources")
    public List<NeracaAkunObjek> getNeracaAkunObjeks(long indexNeracaAkunJenis) throws Exception {
        Connection conn = null;
        try {
            conn = createConnection();
            return bl.getNeracaAkunObjeks(indexNeracaAkunJenis, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }

    }
    /**
     *
     * @param obj
     * @throws Exception
     */
    @Override
    @SuppressWarnings("ConvertToTryWithResources")
    public void updateNeracaAkunObjek(NeracaAkunObjek obj) throws Exception {
        Connection conn = null;
        try {
            conn = createConnection();
             bl.updateNeracaAkunObjek(obj, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }

    }

    /**
     *
     * @param obj
     * @throws Exception
     */
    @Override
    @SuppressWarnings("ConvertToTryWithResources")
    public void deleteNeracaAkunObjek(NeracaAkunObjek obj) throws Exception {
        Connection conn = null;
        try {
            conn = createConnection();
             bl.deleteNeracaAkunObjek(obj, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }

    }
    
    /**
     *
     * @param tahun
     * @param kodeSatker
     * @param judulArusKas
     * @return
     * @throws Exception
     */
    @Override
    @SuppressWarnings("ConvertToTryWithResources")
    public List<ArusKasReport> getArusKasReports(short tahun, String kodeSatker, String judulArusKas ) throws Exception {
        Connection conn = null;
        try {
            conn = createConnection();
            return bl.getArusKasReports(tahun, kodeSatker, judulArusKas, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }

    }
    
    /**
     *
     * @param tahun
     * @param kodeSatker
     * @return
     * @throws Exception
     */
    @Override
    @SuppressWarnings("ConvertToTryWithResources")
    public List<String> getArusKasLabels(short tahun, String kodeSatker) throws Exception {
        Connection conn = null;
        try {
            conn = createConnection();
            return bl.getArusKasLabels(tahun, kodeSatker, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }

    }
    
    /**
     *
     * @param year
     * @param kodeSatker
     * @return
     * @throws Exception
     */
    @Override
    @SuppressWarnings("ConvertToTryWithResources")
    public PerubahanSAL getPerubahanSalReports(short year, String kodeSatker) throws Exception{
        Connection conn = null;
        try {
            conn = createConnection();
            return bl.getPerubahanSalReports(year, kodeSatker, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    
    /**
     *
     * @param sal
     * @return
     * @throws Exception
     */
    @Override
    @SuppressWarnings("ConvertToTryWithResources")
    public long createPerubahanSalReports(PerubahanSAL sal, short iotype) throws Exception{
        Connection conn = null;
        try {
            conn = createConnection();
            return bl.createPerubahanSalReports(sal, iotype, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }

    /**
     *
     * @param year
     * @param kodeSatker
     * @throws Exception
     */
    @Override
    @SuppressWarnings("ConvertToTryWithResources")
    public void deletePerubahanSalReports(short year, String kodeSatker) throws Exception{
        Connection conn = null;
        try {
            conn = createConnection();
            bl.deletePerubahanSalReports(year, kodeSatker, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }

    /**
     *
     * @param year
     * @param kodeSatker
     * @return
     * @throws Exception
     */
    @Override
    @SuppressWarnings("ConvertToTryWithResources")
    public PerubahanEkuitas getPerubahanEkuitasReports(short year, String kodeSatker) throws Exception{
        Connection conn = null;
        try {
            conn = createConnection();
            return bl.getPerubahanEkuitasReports(year, kodeSatker, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    
    /**
     *
     * @param lpe
     * @return
     * @throws Exception
     */
    @Override
    @SuppressWarnings("ConvertToTryWithResources")
    public long createPerubahanEkuitasReports(PerubahanEkuitas lpe, short iotype) throws Exception{
        Connection conn = null;
        try {
            conn = createConnection();
            return bl.createPerubahanEkuitasReports(lpe, iotype, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }

    /**
     *
     * @param year
     * @param kodeSatker
     * @throws Exception
     */
    @Override
    @SuppressWarnings("ConvertToTryWithResources")
    public void deletePerubahanEkuitasReports(short year, String kodeSatker) throws Exception{
        Connection conn = null;
        try {
            conn = createConnection();
            bl.deletePerubahanEkuitasReports(year, kodeSatker, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }

    /**
     *
     * @param year
     * @param triwulan
     * @param kodeSatker
     * @return
     * @throws Exception
     */
    @Override
    @SuppressWarnings("ConvertToTryWithResources")
    public List<LOReport> getLaporanOperasionalReports(short year, short triwulan, String kodeSatker) throws Exception{
        Connection conn = null;
        try {
            conn = createConnection();
            return bl.getLaporanOperasionalReports(year, triwulan, kodeSatker, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    
}
