/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.office.ejb.session;

import javax.ejb.Local;

/**
 *
 * @author detra
 */
@Local
public interface UtilSessionBeanLocal {
    public String clearAll();
    public String getSelectedYear();
    public void setSelectedYear(String selectedYear);
    
    public String getSelectedSatker();
    public void setSelectedSatker(String selectedSatker);

//    public UserPemda getPemda();
//    public void setPemda(UserPemda pemda);

    public String getSelectedPemda();

    public void setSelectedPemda(String selectedPemda);

    public String getUploadPath();

    public void setUploadPath(String uploadPath);

    public String getSelectedSKPD();

    public void setSelectedSKPD(String selectedSKPD);

//    public List<AppMenu> getAppMenus();
//
//    public void setAppMenus(List<AppMenu> appMenus);
    
}
