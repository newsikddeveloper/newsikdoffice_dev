/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.office.ejb.session;

import app.sikd.entity.backoffice.NilaiFungsiPemda;
import app.sikd.entity.graph.GraphGlobal;
import java.util.List;
import javax.ejb.Remote;

/**
 *
 * @author sora
 */
@Remote
public interface GraphicSessionBeanRemote {

    public List<GraphGlobal> getPadGraph(short year, short kodeData, short jenisCOA, List<String> kodePemdas) throws Exception;

    public List<GraphGlobal> getFiskalGraph(short year, short kodeData, short jenisCOA, List<String> kodePemdas) throws Exception;


    public List<GraphGlobal> getDanaBelanjaPengeluaranGraph(short year, short kodeData, short jenisCOA, List<String> kodePemdas) throws Exception;

    public List<GraphGlobal> getTingkatBelanjaModalGraph(short year, short kodeData, short jenisCOA, List<String> kodePemdas) throws Exception;

    public List<GraphGlobal> getTingkatBelanjaPegawaiTdkLangsungGraph(short year, short kodeData, short jenisCOA, List<String> kodePemdas) throws Exception;
    
    public List<GraphGlobal> getTop10Fungsi(short year, short kodeData, short jenisCOA, String kodeFungsi) throws Exception;
    
    public List<NilaiFungsiPemda> getKompilasiNilaiFungsiPemda(short year, List<String> kodePemdas, short kodeData, short jenisCOA, String kodeFungsi) throws Exception;
    public List<NilaiFungsiPemda> getNilaiFungsiPemda(short year, String kodeSatker, short kodeData, short jenisCOA, String kodeFungsi) throws Exception;
    public int getJumlahDaerahKirimRealisasi(short year, short kodeData, short jenisCOA) throws Exception;
}
