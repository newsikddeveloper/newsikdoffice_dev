/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.office.ejb.session;

import app.sikd.dbapi.SIKDConnectionManager;
import app.sikd.entity.backoffice.DTH;
import app.sikd.entity.backoffice.RTH;
import app.sikd.entity.backoffice.RincianBPHTB;
import app.sikd.entity.backoffice.RincianHiburan;
import app.sikd.entity.backoffice.RincianHotel;
import app.sikd.entity.backoffice.RincianIMB;
import app.sikd.entity.backoffice.RincianIzinUsaha;
import app.sikd.entity.backoffice.RincianKendaraan;
import app.sikd.entity.backoffice.RincianPajakDanRetribusi;
import app.sikd.entity.backoffice.RincianRestoran;
import app.sikd.office.ejb.bl.IPajakBusinessLogic;
import app.sikd.office.ejb.bl.IUploadFileBusinessLogic;
import app.sikd.office.ejb.bl.PajakBusinessLogic;
import app.sikd.office.ejb.bl.UploadFileBusinessLogic;
import java.sql.Connection;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.Remote;
import javax.ejb.Stateless;

/**
 *
 * @author sora
 */
@Stateless
@Remote({PajakSessionBeanRemote.class})
public class PajakSessionBean implements PajakSessionBeanRemote {

    IPajakBusinessLogic bl;
    IUploadFileBusinessLogic uploadbl;
    String domain;
    SIKDConnectionManager conMan;

    @PostConstruct
    public void init() {
        try {
            conMan = new SIKDConnectionManager();
            domain = conMan.getDomain();
            bl = new PajakBusinessLogic();
            uploadbl = new UploadFileBusinessLogic();
        } catch (Exception ex) {
            Logger.getLogger(PajakSessionBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    Connection createConnection() throws Exception {
        return conMan.createConnection("sikd.properties");
    }

    @Override
    @SuppressWarnings("ConvertToTryWithResources")
    public DTH getDTHReport(short tahun, String kodeSatker, short periode, String kodeSKPD) throws Exception {
        Connection conn = null;
        try {
            conn = createConnection();
            return bl.getDTHReport(tahun, kodeSatker, periode, kodeSKPD, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }

    }
    
    
    @Override
    @SuppressWarnings("ConvertToTryWithResources")
    public List<RTH> getRTHReport(short tahun, String kodeSatker, short periode) throws Exception {
        Connection conn = null;
        try {
            conn = createConnection();
            return bl.getRTHReport(tahun, kodeSatker, periode, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }

    }
    
    @Override
    @SuppressWarnings("ConvertToTryWithResources")
    public List<String> getDinasKirimDTH(short year, String kodeSatker) throws Exception {
        Connection conn = null;
        try {
            conn = createConnection();
            return bl.getDinasKirimDTH(year, kodeSatker, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    
    @Override
    @SuppressWarnings("ConvertToTryWithResources")
    public List<RincianKendaraan> getRincianKendaraan(String kodeSatker, short tahun) throws Exception {
        Connection conn = null;
        try {
            conn = createConnection();
            return bl.getRincianKendaraan(kodeSatker, tahun, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    @Override
    @SuppressWarnings("ConvertToTryWithResources")
    public List<RincianBPHTB> getRincianBPHTB(String kodeSatker, short tahun) throws Exception {
        Connection conn = null;
        try {
            conn = createConnection();
            return bl.getRincianBPHTB(kodeSatker, tahun, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    @Override
    @SuppressWarnings("ConvertToTryWithResources")
    public List<RincianHiburan> getRincianHiburan(String kodeSatker, short tahun) throws Exception {
        Connection conn = null;
        try {
            conn = createConnection();
            return bl.getRincianHiburan(kodeSatker, tahun, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    @Override
    @SuppressWarnings("ConvertToTryWithResources")
    public List<RincianHotel> getRincianHotel(String kodeSatker, short tahun) throws Exception {
        Connection conn = null;
        try {
            conn = createConnection();
            return bl.getRincianHotel(kodeSatker, tahun, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    @Override
    @SuppressWarnings("ConvertToTryWithResources")
    public List<RincianIMB> getRincianIMB(String kodeSatker, short tahun) throws Exception {
        Connection conn = null;
        try {
            conn = createConnection();
            return bl.getRincianIMB(kodeSatker, tahun, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    @Override
    @SuppressWarnings("ConvertToTryWithResources")
    public List<RincianRestoran> getRincianRestoran(String kodeSatker, short tahun) throws Exception {
        Connection conn = null;
        try {
            conn = createConnection();
            return bl.getRincianRestoran( kodeSatker, tahun, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    @Override
    @SuppressWarnings("ConvertToTryWithResources")
    public List<RincianIzinUsaha> getRincianIzinUsaha(String kodeSatker, short tahun) throws Exception {
        Connection conn = null;
        try {
            conn = createConnection();
            return bl.getRincianIzinUsaha(kodeSatker, tahun, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    
    @Override
    @SuppressWarnings("ConvertToTryWithResources")
    public List<RincianPajakDanRetribusi> getRincianPajakDanRetribusi(String kodeSatker, short tahun) throws Exception {
        Connection conn = null;
        try {
            conn = createConnection();
            return bl.getRincianPajakDanRetribusiDaerah(kodeSatker, tahun, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
}
