/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.office.ejb.session;

import app.sikd.dbapi.SIKDConnectionManager;
import app.sikd.entity.backoffice.NilaiFungsiPemda;
import app.sikd.entity.graph.GraphGlobal;
import app.sikd.office.ejb.bl.GraphicBL;
import app.sikd.office.ejb.bl.IGraphicBL;
import java.sql.Connection;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.Remote;
import javax.ejb.Stateless;

/**
 *
 * @author sora
 */
@Stateless
@Remote({GraphicSessionBeanRemote.class})
public class GraphicSessionBean implements GraphicSessionBeanRemote {
    String domain;
    SIKDConnectionManager conMan;
    IGraphicBL bl;

    @PostConstruct
    public void init() {
        try {
            conMan = new SIKDConnectionManager();
            domain = conMan.getDomain();
            bl = new GraphicBL();
        } catch (Exception ex) {
            Logger.getLogger(APBDSessionBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    Connection createConnection() throws Exception {
        return conMan.createConnection("sikd.properties");
    }
    
    /**
     *
     * @param year
     * @param kodeData
     * @param jenisCOA
     * @param kodePemdas
     * @return
     * @throws Exception
     */
    @Override
    @SuppressWarnings("ConvertToTryWithResources")
    public List<GraphGlobal> getPadGraph(short year, short kodeData, short jenisCOA, List<String> kodePemdas) throws Exception {
        Connection conn = null;
        try {
            conn = createConnection();
            return bl.getPadGraph(year, kodeData, jenisCOA, kodePemdas, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    
    /**
     *
     * @param year
     * @param kodeData
     * @param jenisCOA
     * @param kodePemdas
     * @return
     * @throws Exception
     */
    @Override
    @SuppressWarnings("ConvertToTryWithResources")
    public List<GraphGlobal> getFiskalGraph(short year, short kodeData, short jenisCOA, List<String> kodePemdas) throws Exception{
        Connection conn = null;
        try {
            conn = createConnection();
            return bl.getFiskalGraph(year, kodeData, jenisCOA, kodePemdas, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    
    /**
     *
     * @param year
     * @param kodeData
     * @param jenisCOA
     * @param kodePemdas
     * @return
     * @throws Exception
     */
    @Override
    @SuppressWarnings("ConvertToTryWithResources")
    public List<GraphGlobal> getDanaBelanjaPengeluaranGraph(short year, short kodeData, short jenisCOA, List<String> kodePemdas) throws Exception{
        Connection conn = null;
        try {
            conn = createConnection();
            return bl.getDanaBelanjaPengeluaranGraph(year, kodeData, jenisCOA, kodePemdas, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    
    /**
     *
     * @param year
     * @param kodeData
     * @param jenisCOA
     * @param kodePemdas
     * @return
     * @throws Exception
     */
    @Override
    @SuppressWarnings("ConvertToTryWithResources")
    public List<GraphGlobal> getTingkatBelanjaModalGraph(short year, short kodeData, short jenisCOA, List<String> kodePemdas) throws Exception{
        Connection conn = null;
        try {
            conn = createConnection();
            return bl.getTingkatBelanjaModalGraph(year, kodeData, jenisCOA, kodePemdas, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    
    /**
     *
     * @param year
     * @param kodeData
     * @param jenisCOA
     * @param kodePemdas
     * @return
     * @throws Exception
     */
    @Override
    @SuppressWarnings("ConvertToTryWithResources")
    public List<GraphGlobal> getTingkatBelanjaPegawaiTdkLangsungGraph(short year, short kodeData, short jenisCOA, List<String> kodePemdas) throws Exception{
        Connection conn = null;
        try {
            conn = createConnection();
            return bl.getTingkatBelanjaPegawaiTdkLangsungGraph(year, kodeData, jenisCOA, kodePemdas, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    
    
    
    /**
     *
     * @param year
     * @param kodeData
     * @param jenisCOA
     * @param kodeFungsi
     * @return
     * @throws Exception
     */
    @Override
    @SuppressWarnings("ConvertToTryWithResources")
    public List<GraphGlobal> getTop10Fungsi(short year, short kodeData, short jenisCOA, String kodeFungsi) throws Exception{
        Connection conn = null;
        try {
            conn = createConnection();
            return bl.getTop10Fungsi(year, kodeData, jenisCOA, kodeFungsi, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    
    /**
     *
     * @param year
     * @param kodePemdas
     * @param kodeData
     * @param jenisCOA
     * @param kodeFungsi
     * @return
     * @throws Exception
     */
    @Override
    @SuppressWarnings("ConvertToTryWithResources")
    public List<NilaiFungsiPemda> getKompilasiNilaiFungsiPemda(short year, List<String> kodePemdas, short kodeData, short jenisCOA, String kodeFungsi) throws Exception{
        Connection conn = null;
        try {
            conn = createConnection();
            return bl.getKompilasiNilaiFungsiPemda(year, kodePemdas, kodeData, jenisCOA, kodeFungsi, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }

    /**
     *
     * @param year
     * @param kodeSatker
     * @param kodeData
     * @param jenisCOA
     * @param kodeFungsi
     * @return
     * @throws Exception
     */
    @Override
    @SuppressWarnings("ConvertToTryWithResources")
    public List<NilaiFungsiPemda> getNilaiFungsiPemda(short year, String kodeSatker, short kodeData, short jenisCOA, String kodeFungsi) throws Exception{
        Connection conn = null;
        try {
            conn = createConnection();
            return bl.getNilaiFungsiPemda(year, kodeSatker, kodeData, jenisCOA, kodeFungsi, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    
    /**
     *
     * @param year
     * @param kodeData
     * @param jenisCOA
     * @return
     * @throws Exception
     */
    @Override
    @SuppressWarnings("ConvertToTryWithResources")
    public int getJumlahDaerahKirimRealisasi(short year, short kodeData, short jenisCOA) throws Exception{
        Connection conn = null;
        try {
            conn = createConnection();
            return bl.getJumlahDaerahKirimRealisasi(year, kodeData, jenisCOA, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    
}
