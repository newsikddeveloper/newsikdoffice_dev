/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.office.ejb.session;

import app.sikd.entity.utilitas.UploadFile;
import java.util.List;
import javax.ejb.Remote;

/**
 *
 * @author sora
 */
@Remote
public interface UploadSessionBeanRemote {

    public void addUploadFile(UploadFile uploadFile) throws Exception;

    public void updateUploadFile(UploadFile uploadFile) throws Exception;

    public List<UploadFile> getAllUploadFiles(String kodeSatker, short tahun) throws Exception;

    public void deleteUploadFile(long id) throws Exception;

}
