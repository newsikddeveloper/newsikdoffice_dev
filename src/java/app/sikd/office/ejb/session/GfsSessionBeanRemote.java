/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.office.ejb.session;

import app.sikd.entity.utilitas.SimpleAccount;
import java.util.List;
import javax.ejb.Remote;

/**
 *
 * @author sora
 */
@Remote
public interface GfsSessionBeanRemote {

    public List<SimpleAccount> getGfsAkunStruktur() throws Exception;
    
}
