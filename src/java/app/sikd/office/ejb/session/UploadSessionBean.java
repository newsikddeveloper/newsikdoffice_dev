/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.sikd.office.ejb.session;

import app.sikd.dbapi.SIKDConnectionManager;
import app.sikd.entity.utilitas.UploadFile;
import app.sikd.office.ejb.bl.IUploadFileBusinessLogic;
import app.sikd.office.ejb.bl.UploadFileBusinessLogic;
import java.sql.Connection;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.Remote;
import javax.ejb.Stateless;

/**
 *
 * @author sora
 */
@Stateless
@Remote({UploadSessionBeanRemote.class})
public class UploadSessionBean implements UploadSessionBeanRemote {

    IUploadFileBusinessLogic uploadbl;
    String domain;
    SIKDConnectionManager conMan;

    @PostConstruct
    public void init() {
        try {
            conMan = new SIKDConnectionManager();
            domain = conMan.getDomain();
            uploadbl = new UploadFileBusinessLogic();
        } catch (Exception ex) {
            Logger.getLogger(APBDSessionBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    Connection createConnection() throws Exception {
        return conMan.createConnection("sikd.properties");
    }

    @Override
    @SuppressWarnings("ConvertToTryWithResources")
    public List<UploadFile> getAllUploadFiles(String kodeSatker, short tahun) throws Exception {
        Connection conn = null;
        try {
            conn = createConnection();
            return uploadbl.getAllUploadFiles("uploadpdf", kodeSatker, tahun, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }


    @Override
    @SuppressWarnings("ConvertToTryWithResources")
    public void addUploadFile(UploadFile uploadFile) throws Exception {
        Connection conn = null;
        try {
            conn = createConnection();
            uploadbl.addUploadFile(uploadFile, "uploadpdf", conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }

    @Override
    @SuppressWarnings("ConvertToTryWithResources")
    public void deleteUploadFile(long id) throws Exception {
        Connection conn = null;
        try {
            conn = createConnection();
            uploadbl.deleteUploadFile(id, "uploadpdf", conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        }
        finally {
            if (conn != null) {
                conn.close();
            }
        }
    }

    @Override
    @SuppressWarnings("ConvertToTryWithResources")
    public void updateUploadFile(UploadFile uploadFile) throws Exception {
        Connection conn = null;
        try {
            conn = createConnection();
            uploadbl.updateUploadFile(uploadFile, "uploadpdf", conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }

}
