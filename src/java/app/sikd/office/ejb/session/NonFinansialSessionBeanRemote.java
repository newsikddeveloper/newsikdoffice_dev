/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.office.ejb.session;

import app.sikd.entity.backoffice.DataDaerah;
import app.sikd.entity.backoffice.DataDaerahDetail;
import app.sikd.entity.backoffice.PDRB;
import app.sikd.entity.backoffice.PDRBHarga;
import javax.ejb.Remote;

/**
 *
 * @author detra
 */
@Remote
public interface NonFinansialSessionBeanRemote {
    public void createDataDaerah(DataDaerah obj) throws Exception;
    public void deleteDataDaerah(String kodeSatker, short tahun) throws Exception;
    public DataDaerahDetail getDataDaerahDetail(String kodeSatker, short tahun) throws Exception;

    public void createPDRB(PDRB obj) throws Exception;

    public void deletePDRB(String kodeSatker, short tahun) throws Exception;

    public PDRBHarga getPDRBHargaBerlaku(String kodeSatker, short tahun) throws Exception;

    public PDRBHarga getPDRBHargaKonstan(String kodeSatker, short tahun) throws Exception;

    
}
