/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.office.ejb.session;

import javax.ejb.Stateless;

/**
 *
 * @author detra
 */
@Stateless
public class UtilSessionBean implements UtilSessionBeanLocal {
    String selectedYear="";
    String selectedSatker="";
    String selectedPemda="";
    String selectedSKPD="";
    String uploadPath ="";
    
//    UserPemda pemda=new UserPemda();
//    private List<AppMenu> appMenus = new ArrayList();
    
    
    @Override
    public String clearAll(){        
        selectedYear = "";
        selectedSatker="";
        selectedPemda = "";
        
        return "";
    }

    @Override
    public String getSelectedYear() {
        return selectedYear;
    }
    @Override
    public void setSelectedYear(String selectedYear) {
        this.selectedYear = selectedYear;
    }
    @Override
    public String getSelectedSatker() {
        return selectedSatker;
    }
    @Override
    public void setSelectedSatker(String selectedSatker) {        
        this.selectedSatker = selectedSatker;
    }
    
    
//    @Override
//    public UserPemda getPemda() {
//        return pemda;
//    }
//    @Override
//    public void setPemda(UserPemda pemda) {
//        this.pemda = pemda;
//    }
@Override
    public String getSelectedPemda() {
        return selectedPemda;
    }
@Override
    public void setSelectedPemda(String selectedPemda) {
        this.selectedPemda = selectedPemda;
    }

    @Override
    public String getUploadPath() {
        return uploadPath;
    }

    @Override
    public void setUploadPath(String uploadPath) {
        this.uploadPath = uploadPath;
    }

    @Override
    public String getSelectedSKPD() {
        return selectedSKPD;
    }

    @Override
    public void setSelectedSKPD(String selectedSKPD) {
        this.selectedSKPD = selectedSKPD;
    }

//    @Override
//    public List<AppMenu> getAppMenus() {
//        return appMenus;
//    }
//
//    @Override
//    public void setAppMenus(List<AppMenu> appMenus) {
//        this.appMenus = appMenus;
//    }
    
}
