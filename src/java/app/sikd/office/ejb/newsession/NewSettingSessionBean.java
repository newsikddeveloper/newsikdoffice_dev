/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.sikd.office.ejb.newsession;

import app.sikd.dbapi.SIKDConnectionManager;
import app.sikd.entity.Waktu;
import app.sikd.entity.backoffice.setting.Account;
import app.sikd.entity.backoffice.setting.StatusData;
import app.sikd.office.ejb.newbl.NewSettingBL;
import app.sikd.office.ejb.newbl.NewSettingBLInterface;
import java.sql.Connection;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.Remote;
import javax.ejb.Stateless;

/**
 *
 * @author sora
 */
@Stateless
@Remote({NewSettingSessionBeanRemote.class})
public class NewSettingSessionBean implements NewSettingSessionBeanRemote {
    SIKDConnectionManager conMan;
    NewSettingBLInterface bl;

    @PostConstruct
    public void init() {
        try {
            conMan = new SIKDConnectionManager();
            bl = new NewSettingBL();
        } catch (Exception ex) {
            Logger.getLogger(NewSettingSessionBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    Connection createConnection() throws Exception {
        return conMan.createConnection("sikd.properties");
    }
    
    /**
     *
     * @param year
     * @param kodeData
     * @param jenisCOA
     * @param kodePemdas
     * @return List<Account>
     * @throws Exception
     */
    @Override
    public List<Account> getSuperCashAccount() throws Exception {
        Connection conn = null;
        try {
            conn = createConnection();
            return bl.getParentCashAccount(conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    
    public List<Account> getSubCashAccount(long idParent) throws Exception {
        Connection conn = null;
        try {
            conn = createConnection();
            return bl.getSubCashAccount(idParent, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    
    public List<StatusData> getStatusDatas() throws Exception {
        Connection conn = null;
        try {
            conn = createConnection();
            return bl.getStatusDatas(conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    public StatusData getStatusDataByKode(short kode) throws Exception {
        Connection conn = null;
        try {
            conn = createConnection();
            return bl.getStatusDataByKode(kode, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    
    public List<StatusData> getStatusDatasByUserGroup(long idGroup) throws Exception {
        Connection conn = null;
        try {
            conn = createConnection();
            return bl.getStatusDatasByUserGroup(idGroup, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    
    public void createStatusData(StatusData obj) throws Exception {
        Connection conn = null;
        try {
            conn = createConnection();
            bl.createStatusData(obj, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    public void deleteStatusData(short kode) throws Exception {
        Connection conn = null;
        try {
            conn = createConnection();
            bl.deleteStatusData(kode, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    public void updateStatusData(StatusData oldObj, StatusData newObj) throws Exception {
        Connection conn = null;
        try {
            conn = createConnection();
            bl.updateStatusData(oldObj, newObj, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    
    public List<Waktu> getWaktus() throws Exception{
        Connection conn = null;
        try {
            conn = createConnection();
            return bl.getWaktus(conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    
    public Waktu getWaktuByIkd(String ikd) throws Exception{
        Connection conn = null;
        try {
            conn = createConnection();
            return bl.getWaktuByIkd(ikd, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    
    public void updateWaktu(Waktu obj) throws Exception{
        Connection conn = null;
        try {
            conn = createConnection();
            bl.updateWaktu(obj, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    
    
}
