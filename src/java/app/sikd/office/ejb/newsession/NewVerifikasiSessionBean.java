package app.sikd.office.ejb.newsession;

import app.sikd.dbapi.SIKDConnectionManager;
import app.sikd.entity.Pemda;
import app.sikd.entity.backoffice.StatusPemda;
import app.sikd.office.ejb.newbl.NewStatusBL;
import app.sikd.office.ejb.newbl.NewStatusBLInterface;
import java.sql.Connection;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.Remote;
import javax.ejb.Stateless;

/**
 *
 * @author sora
 */
@Stateless
@Remote({NewVerifikasiSessionBeanRemote.class})
public class NewVerifikasiSessionBean implements NewVerifikasiSessionBeanRemote {
    SIKDConnectionManager conMan;
    NewStatusBLInterface bl;

    @PostConstruct
    public void init() {
        try {
            conMan = new SIKDConnectionManager();
            bl = new NewStatusBL();
        } catch (Exception ex) {
            Logger.getLogger(NewVerifikasiSessionBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    Connection createConnection() throws Exception {
        return conMan.createConnection("sikd.properties");
    }
    
    /**
     * method getStatusDataLraByPemda
     * @param pemdas
     * @param tahun
     * @param periode
     * @param kodeData
     * @return List
     * @throws Exception
     * berfungsi untuk menampilkan semua status data LRA dari pemda
     */
    @SuppressWarnings("ConvertToTryWithResources")
    public List<StatusPemda> getStatusDataLraByPemda(List<Pemda> pemdas, short tahun, short periode) throws Exception {
        Connection conn = null;
        try {
            conn = createConnection();
            return bl.getStatusDataLraByPemda(pemdas, tahun, periode, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
//    @SuppressWarnings("ConvertToTryWithResources")
//    public List<StatusPemda> getStatusDataLraByPemda(List<Pemda> pemdas, short tahun, short periode, short kodeData) throws Exception {
//        Connection conn = null;
//        try {
//            conn = createConnection();
//            return bl.getStatusDataLraByPemda(pemdas, tahun, periode, kodeData, conn);
//        } catch (Exception ex) {
//            throw new Exception(ex.getMessage());
//        } finally {
//            if (conn != null) {
//                conn.close();
//            }
//        }
//    }
    
    /**
     *
     * @param pemdas
     * @param tahun
     * @param semester
     * @return List
     * @throws Exception
     * berfungsi untuk menampilkan semua status data Neraca dari pemda
     */
    public List<StatusPemda> getStatusDataNeracaByPemda(List<Pemda> pemdas, short tahun, short semester) throws Exception{
        Connection conn = null;
        try {
            conn = createConnection();
            return bl.getStatusDataNeracaByPemda(pemdas, tahun, semester, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    
    /**
     *
     * @param pemdas
     * @param tahun
     * @return List
     * @throws Exception
     * berfungsi untuk menampilkan semua status data Arus Kas dari pemda
     */
    public List<StatusPemda> getStatusDataArusKasByPemda(List<Pemda> pemdas, short tahun) throws Exception{
        Connection conn = null;
        try {
            conn = createConnection();
            return bl.getStatusDataArusKasByPemda(pemdas, tahun, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    
    /**
     *
     * @param pemdas
     * @param tahun
     * @param kodeData
     * @return List
     * @throws Exception
     * berfungsi untuk menampilkan semua status data APBD dari pemda
     */
    public List<StatusPemda> getStatusDataApbdByPemda(List<Pemda> pemdas, short tahun, short kodeData) throws Exception{
        Connection conn = null;
        try {
            conn = createConnection();
            return bl.getStatusDataApbdByPemda(pemdas, tahun, kodeData, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    
    /**
     *
     * @param pemdas
     * @param tahun
     * @param triwulan
     * @return List
     * @throws Exception
     * berfungsi untuk menampilkan semua status data LO dari pemda
     */
    public List<StatusPemda> getStatusDataLOByPemda(List<Pemda> pemdas, short tahun, short triwulan) throws Exception{
        Connection conn = null;
        try {
            conn = createConnection();
            return bl.getStatusDataLOByPemda(pemdas, tahun, triwulan, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    
    /**
     *
     * @param pemdas
     * @param tahun
     * @param periode
     * @return List
     * @throws Exception
     * berfungsi untuk menampilkan semua status data DTH dari pemda
     */
    public List<StatusPemda> getStatusDataDTHByPemda(List<Pemda> pemdas, short tahun, short periode) throws Exception{
        Connection conn = null;
        try {
            conn = createConnection();
            return bl.getStatusDataDTHByPemda(pemdas, tahun, periode, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    
    /**
     *
     * @param pemdas
     * @param tahun
     * @return List
     * @throws Exception
     * berfungsi untuk menampilkan semua status data PFK dari pemda
     */
    public List<StatusPemda> getStatusDataPFKByPemda(List<Pemda> pemdas, short tahun) throws Exception{
        Connection conn = null;
        try {
            conn = createConnection();
            return bl.getStatusDataPFKByPemda(pemdas, tahun, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    
    /**
     *
     * @param pemdas
     * @param tahun
     * @return List
     * @throws Exception
     * berfungsi untuk menampilkan semua status data Pinjaman Daerah dari pemda
     */
    public List<StatusPemda> getStatusDataPinjamanDaerahByPemda(List<Pemda> pemdas, short tahun) throws Exception{
        Connection conn = null;
        try {
            conn = createConnection();
            return bl.getStatusDataPinjamanDaerahByPemda(pemdas, tahun, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    
    
    /**
     *
     * @param pemdas
     * @param tahun
     * @return List
     * @throws Exception
     * berfungsi untuk menampilkan semua status data Laporan Perubahan Saldo dari pemda
     */
    public List<StatusPemda> getStatusDataLPSalByPemda(List<Pemda> pemdas, short tahun) throws Exception{
        Connection conn = null;
        try {
            conn = createConnection();
            return bl.getStatusDataLPSalByPemda(pemdas, tahun, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    
    /**
     *
     * @param pemdas
     * @param tahun
     * @return List
     * @throws Exception
     * berfungsi untuk menampilkan semua status data Laporan Perubahan Ekuitas dari pemda
     */
    public List<StatusPemda> getStatusDataLpeByPemda(List<Pemda> pemdas, short tahun) throws Exception{
        Connection conn = null;
        try {
            conn = createConnection();
            return bl.getStatusDataLpeByPemda(pemdas, tahun, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    
    /**
     * method updateStatusDataLraByPemda
     * @param statusPemda
     * @param tahun
     * @param periode
     * @throws Exception
     * berfungsi untuk merubah status data LRA dari pemda
     */
    @SuppressWarnings("ConvertToTryWithResources")
    public void updateStatusDataLraByPemda(StatusPemda statusPemda, short tahun, short periode) throws Exception {
        Connection conn = null;
        try {
            conn = createConnection();
            bl.updateStatusDataLraByPemda(statusPemda, tahun, periode, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
//    @SuppressWarnings("ConvertToTryWithResources")
//    public void updateStatusDataLraByPemda(StatusPemda statusPemda, short tahun, short periode, short kodeData) throws Exception {
//        Connection conn = null;
//        try {
//            conn = createConnection();
//            bl.updateStatusDataLraByPemda(statusPemda, tahun, periode, kodeData, conn);
//        } catch (Exception ex) {
//            throw new Exception(ex.getMessage());
//        } finally {
//            if (conn != null) {
//                conn.close();
//            }
//        }
//    }
    
    /**
     *
     * @param statusPemda
     * @param tahun
     * @param semester
     * @throws Exception
     * berfungsi untuk merubah status data Neraca dari pemda
     */
    public void updateStatusDataNeracaByPemda(StatusPemda statusPemda, short tahun, short semester) throws Exception{
        Connection conn = null;
        try {
            conn = createConnection();
            bl.updateStatusDataNeracaByPemda(statusPemda, tahun, semester, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    
    /**
     *
     * @param statusPemda
     * @param tahun
     * @throws Exception
     * berfungsi untuk merubah status data Arus Kas dari pemda
     */
    public void updateStatusDataArusKasByPemda(StatusPemda statusPemda, short tahun) throws Exception{
        Connection conn = null;
        try {
            conn = createConnection();
            bl.updateStatusDataArusKasByPemda(statusPemda, tahun, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    
    /**
     *
     * @param statusPemda
     * @param tahun
     * @param kodeData
     * @throws Exception
     * berfungsi untuk merubah status data APBD dari pemda
     */
    public void updateStatusDataApbdByPemda(StatusPemda statusPemda, short tahun, short kodeData) throws Exception{
        Connection conn = null;
        try {
            conn = createConnection();
            bl.updateStatusDataApbdByPemda(statusPemda, tahun, kodeData, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    
    /**
     *
     * @param statusPemda
     * @param tahun
     * @param triwulan
     * @throws Exception
     * berfungsi untuk merubah status data LO dari pemda
     */
    public void updateStatusDataLOByPemda(StatusPemda statusPemda, short tahun, short triwulan) throws Exception{
        Connection conn = null;
        try {
            conn = createConnection();
            bl.updateStatusDataLOByPemda(statusPemda, tahun, triwulan, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    
    /**
     *
     * @param statusPemda
     * @param tahun
     * @param periode
     * @throws Exception
     * berfungsi untuk merubah status data DTH dari pemda
     */
    public void updateStatusDataDTHByPemda(StatusPemda statusPemda, short tahun, short periode) throws Exception{
        Connection conn = null;
        try {
            conn = createConnection();
            bl.updateStatusDataDTHByPemda(statusPemda, tahun, periode, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    
    /**
     *
     * @param statusPemda
     * @param tahun
     * @throws Exception
     * berfungsi untuk merubah status data PFK dari pemda
     */
    public void updateStatusDataPFKByPemda(StatusPemda statusPemda, short tahun) throws Exception{
        Connection conn = null;
        try {
            conn = createConnection();
            bl.updateStatusDataPFKByPemda(statusPemda, tahun, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    
    /**
     *
     * @param statusPemda
     * @param tahun
     * @throws Exception
     * berfungsi untuk merubah status data Pinjaman Daerah dari pemda
     */
    public void updateStatusDataPinjamanDaerahByPemda(StatusPemda statusPemda, short tahun) throws Exception{
        Connection conn = null;
        try {
            conn = createConnection();
            bl.updateStatusDataPinjamanDaerahByPemda(statusPemda, tahun, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    
    
    /**
     *
     * @param statusPemda
     * @param tahun
     * @throws Exception
     * berfungsi untuk merubah status data Laporan Perubahan Saldo dari pemda
     */
    public void updateStatusDataLPSalByPemda(StatusPemda statusPemda, short tahun) throws Exception{
        Connection conn = null;
        try {
            conn = createConnection();
            bl.updateStatusDataLPSalByPemda(statusPemda, tahun, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    
    /**
     *
     * @param statusPemda
     * @param tahun
     * @throws Exception
     * berfungsi untuk menampilkan semua status data Laporan Perubahan Ekuitas dari pemda
     */
    public void updateStatusDataLpeByPemda(StatusPemda statusPemda, short tahun) throws Exception{
        Connection conn = null;
        try {
            conn = createConnection();
            bl.updateStatusDataLpeByPemda(statusPemda, tahun, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    
    /**
     *
     * @param pemdas
     * @param tahun
     * @return List
     * @throws Exception
     * berfungsi untuk menampilkan semua status data dari pemda
     */
    public List<StatusPemda> getStatusDataByPemda(List<Pemda> pemdas, short tahun) throws Exception{
        Connection conn = null;
        try {
            conn = createConnection();
            return bl.getStatusDataByPemda(pemdas, tahun, conn);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    
}