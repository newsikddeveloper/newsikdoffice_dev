/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.sikd.office.ejb.newsession;

import app.sikd.entity.Waktu;
import app.sikd.entity.backoffice.setting.Account;
import app.sikd.entity.backoffice.setting.StatusData;
import java.util.List;
import javax.ejb.Remote;

/**
 *
 * @author sora
 */
@Remote
public interface NewSettingSessionBeanRemote {

    /**
     *
     * @return List<account>
     * @throws Exception
     */
    public List<Account> getSuperCashAccount() throws Exception;

    public List<Account> getSubCashAccount(long idParent) throws Exception;
    
    public List<StatusData> getStatusDatas() throws Exception;
    public List<StatusData> getStatusDatasByUserGroup(long idGroup) throws Exception;
    public StatusData getStatusDataByKode(short kode) throws Exception;
    public void createStatusData(StatusData obj) throws Exception;
    public void deleteStatusData(short kode) throws Exception;
    public void updateStatusData(StatusData oldObj, StatusData newObj) throws Exception;
    
    public List<Waktu> getWaktus() throws Exception;
    public Waktu getWaktuByIkd(String ikd) throws Exception;
    public void updateWaktu(Waktu obj) throws Exception;
}
