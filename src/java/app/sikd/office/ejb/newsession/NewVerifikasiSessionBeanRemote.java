/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.sikd.office.ejb.newsession;

import app.sikd.entity.Pemda;
import app.sikd.entity.backoffice.StatusPemda;
import java.util.List;
import javax.ejb.Remote;

/**
 *
 * @author sora
 */
@Remote
public interface NewVerifikasiSessionBeanRemote {
//    public List<StatusPemda> getStatusDataLraByPemda(List<Pemda> pemdas, short tahun, short periode, short kodeData) throws Exception;
    public List<StatusPemda> getStatusDataLraByPemda(List<Pemda> pemdas, short tahun, short periode) throws Exception;
    public List<StatusPemda> getStatusDataNeracaByPemda(List<Pemda> pemdas, short tahun, short semester) throws Exception;
    public List<StatusPemda> getStatusDataArusKasByPemda(List<Pemda> pemdas, short tahun) throws Exception;
    public List<StatusPemda> getStatusDataApbdByPemda(List<Pemda> pemdas, short tahun, short kodeData) throws Exception;
    public List<StatusPemda> getStatusDataLOByPemda(List<Pemda> pemdas, short tahun, short triwulan) throws Exception;
    public List<StatusPemda> getStatusDataDTHByPemda(List<Pemda> pemdas, short tahun, short periode) throws Exception;
    public List<StatusPemda> getStatusDataPFKByPemda(List<Pemda> pemdas, short tahun) throws Exception;
    public List<StatusPemda> getStatusDataPinjamanDaerahByPemda(List<Pemda> pemdas, short tahun) throws Exception;
    public List<StatusPemda> getStatusDataLPSalByPemda(List<Pemda> pemdas, short tahun) throws Exception;
    public List<StatusPemda> getStatusDataLpeByPemda(List<Pemda> pemdas, short tahun) throws Exception;
    
//    public void updateStatusDataLraByPemda(StatusPemda statusPemda, short tahun, short periode, short kodeData) throws Exception;
    public void updateStatusDataLraByPemda(StatusPemda statusPemda, short tahun, short periode) throws Exception;
    public void updateStatusDataNeracaByPemda(StatusPemda statusPemda, short tahun, short semester) throws Exception;
    public void updateStatusDataArusKasByPemda(StatusPemda statusPemda, short tahun) throws Exception;
    public void updateStatusDataApbdByPemda(StatusPemda statusPemda, short tahun, short kodeData) throws Exception;
    public void updateStatusDataLOByPemda(StatusPemda statusPemda, short tahun, short triwulan) throws Exception;
    public void updateStatusDataDTHByPemda(StatusPemda statusPemda, short tahun, short periode) throws Exception;
    public void updateStatusDataPFKByPemda(StatusPemda statusPemda, short tahun) throws Exception;
    public void updateStatusDataPinjamanDaerahByPemda(StatusPemda statusPemda, short tahun) throws Exception;
    public void updateStatusDataLPSalByPemda(StatusPemda statusPemda, short tahun) throws Exception;
    public void updateStatusDataLpeByPemda(StatusPemda statusPemda, short tahun) throws Exception;
    
    
    public List<StatusPemda> getStatusDataByPemda(List<Pemda> pemdas, short tahun) throws Exception;
    
    
}
