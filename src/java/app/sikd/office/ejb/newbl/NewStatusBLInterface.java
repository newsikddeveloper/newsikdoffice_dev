package app.sikd.office.ejb.newbl;

import app.sikd.entity.Pemda;
import app.sikd.entity.backoffice.StatusPemda;
import java.sql.Connection;
import java.util.List;

/**
 *
 * @author sora
 */
public interface NewStatusBLInterface {
//    public List<StatusPemda> getStatusDataLraByPemda(List<Pemda> pemdas, short tahun, short periode, short kodeData, Connection conn) throws Exception;
    public List<StatusPemda> getStatusDataLraByPemda(List<Pemda> pemdas, short tahun, short periode, Connection conn) throws Exception;
    public List<StatusPemda> getStatusDataNeracaByPemda(List<Pemda> pemdas, short tahun, short semester, Connection conn) throws Exception;
    public List<StatusPemda> getStatusDataArusKasByPemda(List<Pemda> pemdas, short tahun, Connection conn) throws Exception;
    public List<StatusPemda> getStatusDataApbdByPemda(List<Pemda> pemdas, short tahun, short kodeData, Connection conn) throws Exception;
    public List<StatusPemda> getStatusDataLOByPemda(List<Pemda> pemdas, short tahun, short triwulan, Connection conn) throws Exception;
    public List<StatusPemda> getStatusDataDTHByPemda(List<Pemda> pemdas, short tahun, short periode, Connection conn) throws Exception;
    public List<StatusPemda> getStatusDataPFKByPemda(List<Pemda> pemdas, short tahun, Connection conn) throws Exception;
    public List<StatusPemda> getStatusDataPinjamanDaerahByPemda(List<Pemda> pemdas, short tahun, Connection conn) throws Exception;
    public List<StatusPemda> getStatusDataLPSalByPemda(List<Pemda> pemdas, short tahun, Connection conn) throws Exception;
    public List<StatusPemda> getStatusDataLpeByPemda(List<Pemda> pemdas, short tahun, Connection conn) throws Exception;

//    public void updateStatusDataLraByPemda(StatusPemda statusPemda, short tahun, short periode, short kodeData, Connection conn) throws Exception;
    public void updateStatusDataLraByPemda(StatusPemda statusPemda, short tahun, short periode, Connection conn) throws Exception;
    public void updateStatusDataNeracaByPemda(StatusPemda statusPemda, short tahun, short semester, Connection conn) throws Exception;
    public void updateStatusDataArusKasByPemda(StatusPemda statusPemda, short tahun, Connection conn) throws Exception;
    public void updateStatusDataApbdByPemda(StatusPemda statusPemda, short tahun, short kodeData, Connection conn) throws Exception;
    public void updateStatusDataLOByPemda(StatusPemda statusPemda, short tahun, short triwulan, Connection conn) throws Exception;
    public void updateStatusDataDTHByPemda(StatusPemda statusPemda, short tahun, short periode, Connection conn) throws Exception;
    public void updateStatusDataPFKByPemda(StatusPemda statusPemda, short tahun, Connection conn) throws Exception;
    public void updateStatusDataPinjamanDaerahByPemda(StatusPemda statusPemda, short tahun, Connection conn) throws Exception;
    public void updateStatusDataLPSalByPemda(StatusPemda statusPemda, short tahun, Connection conn) throws Exception;
    public void updateStatusDataLpeByPemda(StatusPemda statusPemda, short tahun, Connection conn) throws Exception;

    
    public List<StatusPemda> getStatusDataByPemda(List<Pemda> pemdas, short tahun, Connection conn) throws Exception;
    
}
