package app.sikd.office.ejb.newbl;

import app.sikd.entity.Pemda;
import app.sikd.entity.backoffice.StatusPemda;
import app.sikd.office.ejb.newdbapi.NewStatusSQL;
import app.sikd.office.ejb.newdbapi.NewStatusSQLInterface;
import java.sql.Connection;
import java.util.List;

/**
 *
 * @author sora
 */
public class NewStatusBL implements NewStatusBLInterface{
    NewStatusSQLInterface sql;

    public NewStatusBL() {
        sql = new NewStatusSQL();
    }
    
    public List<StatusPemda> getStatusDataLraByPemda(List<Pemda> pemdas, short tahun, short periode, Connection conn) throws Exception{
        return sql.getStatusDataLraByPemda(pemdas, tahun, periode, conn);
    }
//    public List<StatusPemda> getStatusDataLraByPemda(List<Pemda> pemdas, short tahun, short periode, short kodeData, Connection conn) throws Exception{
//        return sql.getStatusDataLraByPemda(pemdas, tahun, periode, kodeData, conn);
//    }
    
    public List<StatusPemda> getStatusDataNeracaByPemda(List<Pemda> pemdas, short tahun, short semester, Connection conn) throws Exception {
        return sql.getStatusDataNeracaByPemda(pemdas, tahun, semester, conn);
    }
    public List<StatusPemda> getStatusDataArusKasByPemda(List<Pemda> pemdas, short tahun, Connection conn) throws Exception {
        return sql.getStatusDataArusKasByPemda(pemdas, tahun, conn);
    }
    
    public List<StatusPemda> getStatusDataApbdByPemda(List<Pemda> pemdas, short tahun, short kodeData, Connection conn) throws Exception{
        return sql.getStatusDataApbdByPemda(pemdas, tahun, kodeData, conn);
    }
    
    public List<StatusPemda> getStatusDataLOByPemda(List<Pemda> pemdas, short tahun, short triwulan, Connection conn) throws Exception{
        return sql.getStatusDataLOByPemda(pemdas, tahun, triwulan, conn);
    }
    
    public List<StatusPemda> getStatusDataDTHByPemda(List<Pemda> pemdas, short tahun, short periode, Connection conn) throws Exception{
        return sql.getStatusDataDTHByPemda(pemdas, tahun, periode, conn);
    }
    
    public List<StatusPemda> getStatusDataPFKByPemda(List<Pemda> pemdas, short tahun, Connection conn) throws Exception{
        return sql.getStatusDataPFKByPemda(pemdas, tahun, conn);
    }
    
    public List<StatusPemda> getStatusDataPinjamanDaerahByPemda(List<Pemda> pemdas, short tahun, Connection conn) throws Exception{
        return sql.getStatusDataPinjamanDaerahByPemda(pemdas, tahun, conn);
    }
    
    public List<StatusPemda> getStatusDataLPSalByPemda(List<Pemda> pemdas, short tahun, Connection conn) throws Exception{
        return sql.getStatusDataLPSalByPemda(pemdas, tahun, conn);
    }
    public List<StatusPemda> getStatusDataLpeByPemda(List<Pemda> pemdas, short tahun, Connection conn) throws Exception{
        return sql.getStatusDataLpeByPemda(pemdas, tahun, conn);
    }
    
    public void updateStatusDataLraByPemda(StatusPemda statusPemda, short tahun, short periode, Connection conn) throws Exception{
        sql.updateStatusDataLraByPemda(statusPemda, tahun, periode, conn);
    }
//    public void updateStatusDataLraByPemda(StatusPemda statusPemda, short tahun, short periode, short kodeData, Connection conn) throws Exception{
//        sql.updateStatusDataLraByPemda(statusPemda, tahun, periode, kodeData, conn);
//    }
    
    public void updateStatusDataNeracaByPemda(StatusPemda statusPemda, short tahun, short semester, Connection conn) throws Exception {
        sql.updateStatusDataNeracaByPemda(statusPemda, tahun, semester, conn);
    }
    public void updateStatusDataArusKasByPemda(StatusPemda statusPemda, short tahun, Connection conn) throws Exception {
        sql.updateStatusDataArusKasByPemda(statusPemda, tahun, conn);
    }
    
    public void updateStatusDataApbdByPemda(StatusPemda statusPemda, short tahun, short kodeData, Connection conn) throws Exception{
        sql.updateStatusDataApbdByPemda(statusPemda, tahun, kodeData, conn);
    }
    
    public void updateStatusDataLOByPemda(StatusPemda statusPemda, short tahun, short triwulan, Connection conn) throws Exception{
        sql.updateStatusDataLOByPemda(statusPemda, tahun, triwulan, conn);
    }
    
    public void updateStatusDataDTHByPemda(StatusPemda statusPemda, short tahun, short periode, Connection conn) throws Exception{
        sql.updateStatusDataDTHByPemda(statusPemda, tahun, periode, conn);
    }
    
    public void updateStatusDataPFKByPemda(StatusPemda statusPemda, short tahun, Connection conn) throws Exception{
        sql.updateStatusDataPFKByPemda(statusPemda, tahun, conn);
    }
    
    public void updateStatusDataPinjamanDaerahByPemda(StatusPemda statusPemda, short tahun, Connection conn) throws Exception{
        sql.updateStatusDataPinjamanDaerahByPemda(statusPemda, tahun, conn);
    }
    
    public void updateStatusDataLPSalByPemda(StatusPemda statusPemda, short tahun, Connection conn) throws Exception{
        sql.updateStatusDataLPSalByPemda(statusPemda, tahun, conn);
    }
    public void updateStatusDataLpeByPemda(StatusPemda statusPemda, short tahun, Connection conn) throws Exception{
        sql.updateStatusDataLpeByPemda(statusPemda, tahun, conn);
    }
    
    public List<StatusPemda> getStatusDataByPemda(List<Pemda> pemdas, short tahun, Connection conn) throws Exception{
        return sql.getStatusDataByPemda(pemdas, tahun, conn);
    }
    
    
}
