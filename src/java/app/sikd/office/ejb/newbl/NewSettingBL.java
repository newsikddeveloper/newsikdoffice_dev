package app.sikd.office.ejb.newbl;

import app.sikd.entity.Waktu;
import app.sikd.entity.backoffice.setting.Account;
import app.sikd.entity.backoffice.setting.StatusData;
import app.sikd.office.ejb.newdbapi.INewSettingSQL;
import app.sikd.office.ejb.newdbapi.NewSettingSQL;
import java.sql.Connection;
import java.util.List;

/**
 *
 * @author sora
 */
public class NewSettingBL implements NewSettingBLInterface{
    INewSettingSQL sql;

    public NewSettingBL() {
        sql = new NewSettingSQL();
    }
    
    /**
     *
     * @param conn
     * @return List<Account>
     * @throws Exception
     */
    public List<Account> getParentCashAccount(Connection conn) throws Exception{
        return sql.getParentCashAccount(conn);
    }
    public List<Account> getSubCashAccount(long idParent, Connection conn) throws Exception{
        return sql.getSubCashAccount(idParent, conn);
    }
    
    public List<StatusData> getStatusDatas(Connection conn) throws Exception{
        return sql.getStatusDatas(conn);
    }
    public StatusData getStatusDataByKode(short kode, Connection conn) throws Exception{
        return sql.getStatusDataByKode(kode, conn);
    }
    public List<StatusData> getStatusDatasByUserGroup(long idGroup, Connection conn) throws Exception{
        return sql.getStatusDatasByUserGroup(idGroup, conn);
    }
    public void createStatusData(StatusData obj, Connection conn) throws Exception{
        sql.createStatusData(obj, conn);
    }
    public void deleteStatusData(short kode, Connection conn) throws Exception{
        sql.deleteStatusData(kode, conn);
    }
    public void updateStatusData(StatusData oldObj, StatusData newObj, Connection conn) throws Exception{
        sql.updateStatusData(oldObj, newObj, conn);
    }
    
    public List<Waktu> getWaktus(Connection conn) throws Exception{
        return sql.getWaktus(conn);
    }
    
    public Waktu getWaktuByIkd(String ikd, Connection conn) throws Exception{
        return sql.getWaktuByIkd(ikd, conn);
    }
    
    public void updateWaktu(Waktu obj, Connection conn) throws Exception{
        sql.updateWaktu(obj, conn);
    }
    
}
