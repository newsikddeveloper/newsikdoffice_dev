package app.sikd.office.ejb.newbl;

import app.sikd.entity.Waktu;
import app.sikd.entity.backoffice.setting.Account;
import app.sikd.entity.backoffice.setting.StatusData;
import java.sql.Connection;
import java.util.List;

/**
 *
 * @author sora
 */
public interface NewSettingBLInterface {

    /**
     *
     * @param conn
     * @return List Account
     * @throws Exception
     */
    public List<Account> getParentCashAccount(Connection conn) throws Exception;
    public List<Account> getSubCashAccount(long idParent, Connection conn) throws Exception;
    
    public List<StatusData> getStatusDatas(Connection conn) throws Exception;
    public StatusData getStatusDataByKode(short kode, Connection conn) throws Exception;
    public void createStatusData(StatusData obj, Connection conn) throws Exception;
    public void deleteStatusData(short kode, Connection conn) throws Exception;
    public void updateStatusData(StatusData oldObj, StatusData newObj, Connection conn) throws Exception;
    public List<StatusData> getStatusDatasByUserGroup(long idGroup, Connection conn) throws Exception;
    
    public List<Waktu> getWaktus(Connection conn) throws Exception;
    public Waktu getWaktuByIkd(String ikd, Connection conn) throws Exception;
    public void updateWaktu(Waktu obj, Connection conn) throws Exception;
    
}
