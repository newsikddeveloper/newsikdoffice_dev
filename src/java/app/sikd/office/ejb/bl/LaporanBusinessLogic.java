/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.office.ejb.bl;

import app.sikd.entity.backoffice.ArusKasReport;
import app.sikd.entity.backoffice.LOReport;
import app.sikd.entity.backoffice.Neraca;
import app.sikd.entity.backoffice.NeracaAkunJenis;
import app.sikd.entity.backoffice.NeracaAkunKelompok;
import app.sikd.entity.backoffice.NeracaAkunObjek;
import app.sikd.entity.backoffice.NeracaAkunUtama;
import app.sikd.entity.backoffice.NeracaReport;
import app.sikd.entity.backoffice.PerubahanEkuitas;
import app.sikd.entity.backoffice.PerubahanSAL;
import app.sikd.office.ejb.dbapi.ILaporanSQL;
import app.sikd.office.ejb.dbapi.LaporanSQL;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author sora
 */
public class LaporanBusinessLogic implements ILaporanBusinessLogic{
    ILaporanSQL sql;
    
    public LaporanBusinessLogic(){
        sql = new LaporanSQL();        
    }
    
    @Override
    public List<NeracaReport> getNeracaReports(short year, short semester, String kodeSatker, String judulNeraca, Connection conn) throws Exception{
        return sql.getNeracaReports(year, semester, kodeSatker, judulNeraca, conn);
    }
    
    @Override
    public List<String> getNeracaLabels(short year, short semester, String kodeSatker, Connection conn) throws Exception{
        return sql.getJudulNeracas(year, semester, kodeSatker, conn);
    }
    
    /**
     *
     * @param year
     * @param semester
     * @param kodeSatker
     * @param judulNeraca
     * @param conn
     * @return
     * @throws Exception
     */
    @Override
    public Neraca getNeraca(short year, short semester, String kodeSatker, String judulNeraca, Connection conn) throws Exception{
        return sql.getNeraca(year, semester, kodeSatker, judulNeraca, conn);
    }
    
    /**
     *
     * @param neraca
     * @param conn
     * @return
     * @throws Exception
     */
    @SuppressWarnings("null")
    @Override
    public Neraca createNeracaStruktur(Neraca neraca, short iotype, Connection conn) throws Exception {
        long indexNeraca = 0;
        if (neraca != null) {
            indexNeraca = sql.createNeraca(neraca, iotype, conn);
            if (neraca.getAkunUtamas() != null) {
                for (NeracaAkunUtama akunUtama : neraca.getAkunUtamas()) {
                    long indexUtama = sql.createNeracaAkunUtama(indexNeraca, akunUtama, conn);
                    akunUtama.setIndex(indexUtama);
                    if (akunUtama.getAkunKelompoks() != null) {
                        for (NeracaAkunKelompok akunKelompok : akunUtama.getAkunKelompoks()) {
                            long indexKelompok = sql.createNeracaAkunKelompok(indexNeraca, akunKelompok, conn);
                            akunKelompok.setIndex(indexKelompok);
                            if (akunKelompok.getAkunJeniss() != null) {
                                for (NeracaAkunJenis akunJenis : akunKelompok.getAkunJeniss()) {
                                    long indexJenis = sql.createNeracaAkunJenis(indexNeraca, akunJenis, conn);
                                    akunJenis.setIndex(indexJenis);
                                    if (akunJenis.getAkunObjeks() != null) {
                                        for (NeracaAkunObjek akunObjek : akunJenis.getAkunObjeks()) {
                                            long indexObjek = sql.createNeracaAkunObjek(indexNeraca, akunObjek, conn);
                                            akunObjek.setIndex(indexObjek);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        neraca.setIndex(indexNeraca);
        return neraca;
    }

    /**
     *
     * @param neraca
     * @param conn
     * @return
     * @throws Exception
     */
    @Override
    public long createNeraca(Neraca neraca, short iotype, Connection conn) throws Exception {
            return sql.createNeraca(neraca, iotype, conn);
    }

    /**
     *
     * @param neraca
     * @param conn
     * @throws Exception
     */
    @Override
    public void updateNeraca(Neraca neraca, Connection conn) throws Exception{
        sql.updateNeraca(neraca, conn);
    }

    /**
     *
     * @param neraca
     * @param conn
     * @throws Exception
     */
    @Override
    public void deleteNeraca(Neraca neraca, Connection conn) throws Exception{
        sql.deleteNeraca(neraca, conn);
    }

    /**
     *
     * @param indexNeraca
     * @param obj
     * @param conn
     * @return
     * @throws Exception
     */
    @Override
    public long createNeracaAkunUtama(long indexNeraca, NeracaAkunUtama obj, Connection conn) throws Exception{
        return sql.createNeracaAkunUtama(indexNeraca, obj, conn);
    }
    
    /**
     *
     * @param indexNeraca
     * @param conn
     * @return
     * @throws Exception
     */
    @Override
    public List<NeracaAkunUtama> getNeracaAkunUtamas(long indexNeraca, Connection conn) throws Exception{
        return sql.getNeracaAkunUtamas(indexNeraca, conn);
    }

    /**
     *
     * @param obj
     * @param conn
     * @throws Exception
     */
    @Override
    public void updateNeracaAkunUtama(NeracaAkunUtama obj, Connection conn) throws Exception{
        sql.updateNeracaAkunUtama(obj, conn);
    }

    /**
     *
     * @param obj
     * @param conn
     * @throws Exception
     */
    @Override
    public void deleteNeracaAkunUtama(NeracaAkunUtama obj, Connection conn) throws Exception{
        sql.deleteNeracaAkunUtama(obj, conn);
    }
    
    /**
     *
     * @param indexNeracaAkunUtama
     * @param obj
     * @param conn
     * @return
     * @throws Exception
     */
    @Override
    public long createNeracaAkunKelompok(long indexNeracaAkunUtama, NeracaAkunKelompok obj, Connection conn) throws Exception{
        return sql.createNeracaAkunKelompok(indexNeracaAkunUtama, obj, conn);
    }
    
    /**
     *
     * @param indexNeracaAkunUtama
     * @param conn
     * @return
     * @throws Exception
     */
    @Override
    public List<NeracaAkunKelompok> getNeracaAkunKelompoks(long indexNeracaAkunUtama, Connection conn) throws Exception{
        return sql.getNeracaAkunKelompoks(indexNeracaAkunUtama, conn);
    }

    /**
     *
     * @param obj
     * @param conn
     * @throws Exception
     */
    @Override
    public void updateNeracaAkunKelompok(NeracaAkunKelompok obj, Connection conn) throws Exception{
        sql.updateNeracaAkunKelompok(obj, conn);
    }

    /**
     *
     * @param obj
     * @param conn
     * @throws Exception
     */
    @Override
    public void deleteNeracaAkunKelompok(NeracaAkunKelompok obj, Connection conn) throws Exception{
        sql.deleteNeracaAkunKelompok(obj, conn);
    }

    /**
     *
     * @param indexNeracaAkunKelompok
     * @param obj
     * @param conn
     * @return
     * @throws Exception
     */
    @Override
    public long createNeracaAkunJenis(long indexNeracaAkunKelompok, NeracaAkunJenis obj, Connection conn) throws Exception{
        return sql.createNeracaAkunJenis(indexNeracaAkunKelompok, obj, conn);
    }
    
    /**
     *
     * @param indexNeracaAkunKelompok
     * @param conn
     * @return
     * @throws Exception
     */
    @Override
    public List<NeracaAkunJenis> getNeracaAkunJeniss(long indexNeracaAkunKelompok, Connection conn) throws Exception{
        return sql.getNeracaAkunJeniss(indexNeracaAkunKelompok, conn);
    }

    /**
     *
     * @param obj
     * @param conn
     * @throws Exception
     */
    @Override
    public void updateNeracaAkunJenis(NeracaAkunJenis obj, Connection conn) throws Exception{
        sql.updateNeracaAkunJenis(obj, conn);
    }

    /**
     *
     * @param obj
     * @param conn
     * @throws Exception
     */
    @Override
    public void deleteNeracaAkunJenis(NeracaAkunJenis obj, Connection conn) throws Exception{
        sql.deleteNeracaAkunJenis(obj, conn);
    }

    /**
     *
     * @param indexNeracaAkunJenis
     * @param obj
     * @param conn
     * @return
     * @throws Exception
     */
    @Override
    public long createNeracaAkunObjek(long indexNeracaAkunJenis, NeracaAkunObjek obj, Connection conn) throws Exception{
        return sql.createNeracaAkunObjek(indexNeracaAkunJenis, obj, conn);
    }
    
    /**
     *
     * @param indexNeracaAkunJenis
     * @param conn
     * @return
     * @throws Exception
     */
    @Override
    public List<NeracaAkunObjek> getNeracaAkunObjeks(long indexNeracaAkunJenis, Connection conn) throws Exception{
        return sql.getNeracaAkunObjeks(indexNeracaAkunJenis, conn);
    }

    /**
     *
     * @param obj
     * @param conn
     * @throws Exception
     */
    @Override
    public void updateNeracaAkunObjek(NeracaAkunObjek obj, Connection conn) throws Exception{
        sql.updateNeracaAkunObjek(obj, conn);
    }

    /**
     *
     * @param obj
     * @param conn
     * @throws Exception
     */
    @Override
    public void deleteNeracaAkunObjek(NeracaAkunObjek obj, Connection conn) throws Exception{
        sql.deleteNeracaAkunObjek(obj, conn);
    }
    
    
    @Override
    public List<ArusKasReport> getArusKasReports(short year, String kodeSatker, String judulArusKas, Connection conn) throws Exception{
        return sql.getArusKasReports(year, kodeSatker, judulArusKas, conn);
    }
    
    @Override
    public List<String> getArusKasLabels(short year, String kodeSatker, Connection conn) throws Exception{
        return sql.getJudulArusKas(year, kodeSatker, conn);
    }
    
    @Override
    public PerubahanSAL getPerubahanSalReports(short year, String kodeSatker, Connection conn) throws Exception{
        return sql.getPerubahanSalReports(year, kodeSatker, conn);
    }

    /**
     *
     * @param sal
     * @param conn
     * @return
     * @throws Exception
     */
    @Override
    public long createPerubahanSalReports(PerubahanSAL sal, short iotype, Connection conn) throws Exception{
        return sql.createPerubahanSalReports(sal, iotype, conn);
    }

    /**
     *
     * @param year
     * @param kodeSatker
     * @param conn
     * @throws Exception
     */
    @Override
    public void deletePerubahanSalReports(short year, String kodeSatker, Connection conn) throws Exception{
        sql.deletePerubahanSalReports(year, kodeSatker, conn);
    }
    
    @Override
    public PerubahanEkuitas getPerubahanEkuitasReports(short year, String kodeSatker, Connection conn) throws Exception{
        return sql.getPerubahanEkuitasReports(year, kodeSatker, conn);
    }
    
    /**
     *
     * @param lpe
     * @param conn
     * @return
     * @throws Exception
     */
    @Override
    public long createPerubahanEkuitasReports(PerubahanEkuitas lpe, short iotype, Connection conn) throws Exception{
        return sql.createPerubahanEkuitasReports(lpe, iotype, conn);
    }

    /**
     *
     * @param year
     * @param kodeSatker
     * @param conn
     * @throws Exception
     */
    @Override
    public void deletePerubahanEkuitasReports(short year, String kodeSatker, Connection conn) throws Exception{
        sql.deletePerubahanEkuitasReports(year, kodeSatker, conn);
    }
    @Override
    public List<LOReport> getLaporanOperasionalReports(short year, short triwulan, String kodeSatker, Connection conn) throws Exception{
        return sql.getLaporanOperasionalReports(year, triwulan, kodeSatker, conn);
    }
    
}

