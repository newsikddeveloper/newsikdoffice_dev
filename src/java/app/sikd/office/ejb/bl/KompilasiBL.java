/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.office.ejb.bl;

import app.sikd.entity.backoffice.AkunKompilasi;
import app.sikd.entity.backoffice.AkunKompilasiGFS;
import app.sikd.entity.backoffice.KompilasiApbd1364;
import app.sikd.office.ejb.dbapi.IKompilasiSQL;
import app.sikd.office.ejb.dbapi.KompilasiSQL;
import java.sql.Connection;
import java.util.List;

/**
 *
 * @author sora
 */
public class KompilasiBL implements IKompilasiBL{
    IKompilasiSQL sql;

    public KompilasiBL() {
        sql = new KompilasiSQL();
    }
    public void deleteApbdKompilasi64(short tahun, short kodeData, Connection conn) throws Exception{
        sql.deleteApbdKompilasi64(tahun, kodeData, conn);
    }
    public List<KompilasiApbd1364> getApbd13Kompilasi64(short tahun, short kodeData, Connection conn) throws Exception{
        return sql.getApbd13Kompilasi64(tahun, kodeData, conn);
    }
    public KompilasiApbd1364 createApbdKompilasi64(KompilasiApbd1364 apbd1364, Connection conn) throws Exception{
        return sql.createApbdKompilasi64(apbd1364, conn);
    }
    
    public List<AkunKompilasiGFS> getApbdKompilasi64(short tahun, short kodeData, String kodepemda, Connection conn) throws Exception{
        return sql.getApbdKompilasi64(tahun, kodeData, kodepemda, conn);
    }
        
    public List<AkunKompilasi> getApbdKompilasiLangsungs(short tahun, List<String> pemdaCodes, Connection conn) throws Exception{
        return sql.getApbdKompilasiLangsungs(tahun, pemdaCodes, conn);
    }
    
}
