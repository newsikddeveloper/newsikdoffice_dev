/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.office.ejb.bl;

import app.sikd.entity.utilitas.SimpleAccount;
import app.sikd.office.ejb.dbapi.GfsSQL;
import app.sikd.office.ejb.dbapi.IGfsSQL;
import java.sql.Connection;
import java.util.List;

/**
 *
 * @author sora
 */
public class GfsBL implements IGfsBL{
    
    IGfsSQL sql;
    
    public GfsBL(){
        sql = new GfsSQL();        
    }
    
    public List<SimpleAccount> getGfsAkunStruktur( Connection conn) throws Exception{
        return sql.getGfsAkunStruktur(conn);
    }
}
