/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.office.ejb.bl;

import app.sikd.entity.backoffice.APBD;
import app.sikd.entity.backoffice.ArusKas;
import app.sikd.entity.backoffice.ArusKasSaldo;
import app.sikd.entity.backoffice.KegiatanAPBD;
import app.sikd.entity.backoffice.KodeRekeningAPBD;
import app.sikd.entity.backoffice.Neraca;
import app.sikd.entity.backoffice.NeracaAkunJenis;
import app.sikd.entity.backoffice.NeracaAkunKelompok;
import app.sikd.entity.backoffice.NeracaAkunObjek;
import app.sikd.entity.backoffice.NeracaAkunUtama;
import app.sikd.entity.backoffice.ObjArusKasAkun;
import app.sikd.entity.backoffice.RealisasiAPBD;
import app.sikd.entity.backoffice.RealisasiKegiatanAPBD;
import app.sikd.entity.backoffice.RealisasiKodeRekeningAPBD;
import java.sql.Connection;
import java.util.List;

/**
 *
 * @author sora
 */

public interface IDMBL {
    public Neraca getNeracaHeader(String kodeSatker, short tahun, short semester, String judul, Connection conn) throws Exception;
    public List<NeracaAkunUtama> getNeracaAkunStruktur(long indexNeraca, Connection conn) throws Exception;
    public Neraca createNeracaHeader(Neraca neraca, short iotype, Connection conn) throws Exception;
    public NeracaAkunUtama createNeracaAkunUtama(long indexNeraca, NeracaAkunUtama obj, Connection conn) throws Exception;
    public NeracaAkunKelompok createNeracaAkunKelompok(long indexAkunUtama, NeracaAkunKelompok obj, Connection conn) throws Exception;
    public NeracaAkunJenis createNeracaAkunJenis(long indexAkunKelompok, NeracaAkunJenis obj, Connection conn) throws Exception;
    public NeracaAkunObjek createNeracaAkunObjek(long indexAkunJenis, NeracaAkunObjek obj, Connection conn) throws Exception;
    public void deleteNeracaAkunObjek(long index, Connection conn) throws Exception;
    public void deleteNeracaAkunJenis(long index, Connection conn) throws Exception;
    public void deleteNeracaAkunKelompok(long index, Connection conn) throws Exception;
    public void deleteNeracaAkunUtama(long index, Connection conn) throws Exception;
    
    
    public void deleteArusKas(long index, Connection conn) throws Exception;
    public void deleteArusKasRinci(long index, String table, String field, Connection conn) throws Exception;
    public ArusKas createArusKasHeader(ArusKas arusKas, short iotype, Connection conn) throws Exception;
    public ObjArusKasAkun createArusKasRinci(long indexKas, ObjArusKasAkun obj, String table, String field, Connection conn) throws Exception;
    
    public ArusKas getArusKasHeader(String kodeSatker, short tahun, String judul, Connection conn) throws Exception;
    public List<ObjArusKasAkun> getArusKasRinci(long indexArusKas, String table, String field, Connection conn) throws Exception;    
    public ArusKasSaldo createArusKasSaldo(long indexKas, ArusKasSaldo obj, Connection conn) throws Exception;
    
    public APBD getAPBDHeader(short year, String kodeSatker, short kodeData, short jenisCOA, Connection conn) throws Exception;
    public APBD getApbdStruktur(String kodeSatker, short tahun, short kodeData, short jenisCOA, Connection conn) throws Exception;
    public APBD createAPBDHeader(APBD apbd, short iotype, Connection conn) throws Exception;
    public APBD updateAPBDHeader(APBD apbd, Connection conn) throws Exception;
    public void deleteAPBD(long id, Connection conn) throws Exception;    
    public KegiatanAPBD createKegiatanAPBD(long indexApbd, KegiatanAPBD kegiatan, Connection conn) throws Exception;
    public KegiatanAPBD updateKegiatanAPBD(KegiatanAPBD kegiatan, Connection conn) throws Exception;
    public void deleteKegiatanAPBD(long id, Connection conn) throws Exception;
    public void createRekeningAPBD(long indexKegiatan, KodeRekeningAPBD rekening, Connection conn) throws Exception;
    public void deleteRekeningAPBD(long indexKegiatan, KodeRekeningAPBD rekening, Connection conn) throws Exception;
    
    public RealisasiAPBD getRealisasiApbdHeader(String kodeSatker, short tahun, short bulan, short jenisCOA, Connection conn) throws Exception;
    public RealisasiAPBD getRealisasiApbdHeader(String kodeSatker, short tahun, short bulan, short kodeData, short jenisCOA, Connection conn) throws Exception;
    public RealisasiAPBD getRealisasiApbdStruktur(String kodeSatker, short tahun, short bulan, short kodeData, short jenisCOA, Connection conn) throws Exception;
    public RealisasiAPBD createRealiasiAPBDHeader(RealisasiAPBD apbd, short iotype, Connection conn) throws Exception;
    public RealisasiAPBD updateRealisasiAPBDHeader(RealisasiAPBD apbd, Connection conn) throws Exception;
    public void deleteRealisasiAPBD(long id, Connection conn) throws Exception;
    public RealisasiKegiatanAPBD createRealisasiKegiatanAPBD(long indexApbd, RealisasiKegiatanAPBD kegiatan, Connection conn) throws Exception;
    public RealisasiKegiatanAPBD updateRealisasiKegiatanAPBD(RealisasiKegiatanAPBD kegiatan, Connection conn) throws Exception;
    public void deleteRealisasiKegiatanAPBD(long id, Connection conn) throws Exception;
    public void createRealisasiRekeningAPBD(long indexKegiatan, RealisasiKodeRekeningAPBD rekening, Connection conn) throws Exception;
    public void deleteRealisasiRekeningAPBD(long indexKegiatan, RealisasiKodeRekeningAPBD rekening, Connection conn) throws Exception;
    
    
    
}
