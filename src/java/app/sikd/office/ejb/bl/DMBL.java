/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.office.ejb.bl;

import app.sikd.entity.backoffice.APBD;
import app.sikd.entity.backoffice.ArusKas;
import app.sikd.entity.backoffice.ArusKasSaldo;
import app.sikd.entity.backoffice.KegiatanAPBD;
import app.sikd.entity.backoffice.KodeRekeningAPBD;
import app.sikd.entity.backoffice.Neraca;
import app.sikd.entity.backoffice.NeracaAkunJenis;
import app.sikd.entity.backoffice.NeracaAkunKelompok;
import app.sikd.entity.backoffice.NeracaAkunObjek;
import app.sikd.entity.backoffice.NeracaAkunUtama;
import app.sikd.entity.backoffice.ObjArusKasAkun;
import app.sikd.entity.backoffice.RealisasiAPBD;
import app.sikd.entity.backoffice.RealisasiKegiatanAPBD;
import app.sikd.entity.backoffice.RealisasiKodeRekeningAPBD;
import app.sikd.office.ejb.dbapi.DMSQL;
import app.sikd.office.ejb.dbapi.IDMSQL;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author sora
 */
public class DMBL implements IDMBL{
    IDMSQL sql;
    
    public DMBL(){
        sql = new DMSQL();        
    }
    
    public Neraca getNeracaHeader(String kodeSatker, short tahun, short semester, String judul, Connection conn) throws Exception{
        return sql.getNeracaHeader(kodeSatker, tahun, semester, judul, conn);
    }
    public List<NeracaAkunUtama> getNeracaAkunStruktur(long indexNeraca, Connection conn) throws Exception{
        return sql.getNeracaAkunStruktur(indexNeraca, conn);
    }
    public Neraca createNeracaHeader(Neraca neraca, short iotype, Connection conn) throws Exception{
        return sql.createNeracaHeader(neraca, iotype, conn);
    }
    public NeracaAkunUtama createNeracaAkunUtama(long indexNeraca, NeracaAkunUtama obj, Connection conn) throws Exception{
        return sql.createNeracaAkunUtama(indexNeraca, obj, conn);
    }
    public NeracaAkunKelompok createNeracaAkunKelompok(long indexAkunUtama, NeracaAkunKelompok obj, Connection conn) throws Exception{
        return sql.createNeracaAkunKelompok(indexAkunUtama, obj, conn);
    }
    public NeracaAkunJenis createNeracaAkunJenis(long indexAkunKelompok, NeracaAkunJenis obj, Connection conn) throws Exception{
        return sql.createNeracaAkunJenis(indexAkunKelompok, obj, conn);
    }
    public NeracaAkunObjek createNeracaAkunObjek(long indexAkunJenis, NeracaAkunObjek obj, Connection conn) throws Exception{
        return sql.createNeracaAkunObjek(indexAkunJenis, obj, conn);
    }
    public void deleteNeracaAkunObjek(long index, Connection conn) throws Exception{
        sql.deleteNeracaAkunObjek(index, conn);
    }
    public void deleteNeracaAkunJenis(long index, Connection conn) throws Exception{
        sql.deleteNeracaAkunJenis(index, conn);
    }
    public void deleteNeracaAkunKelompok(long index, Connection conn) throws Exception{
        sql.deleteNeracaAkunKelompok(index, conn);
    }
    public void deleteNeracaAkunUtama(long index, Connection conn) throws Exception{
        sql.deleteNeracaAkunUtama(index, conn);
    }
    
    
    public void deleteArusKas(long index, Connection conn) throws Exception{
        sql.deleteArusKas(index, conn);
    }
    public void deleteArusKasRinci(long index, String table, String field, Connection conn) throws Exception{
        sql.deleteArusKasRinci(index, table, field, conn);
    }
    public ArusKas createArusKasHeader(ArusKas arusKas, short iotype, Connection conn) throws Exception{
        return sql.createArusKasHeader(arusKas, iotype, conn);
    }
    public ObjArusKasAkun createArusKasRinci(long indexKas, ObjArusKasAkun obj, String table, String field, Connection conn) throws Exception{
        return sql.createArusKasRinci(indexKas, obj, table, field, conn);
    }
    
    public ArusKas getArusKasHeader(String kodeSatker, short tahun, String judul, Connection conn) throws Exception{
        return sql.getArusKasHeader(kodeSatker, tahun, judul, conn);
    }
    public List<ObjArusKasAkun> getArusKasRinci(long indexArusKas, String table, String field, Connection conn) throws Exception{
        return sql.getArusKasRinci(indexArusKas, table, field, conn);
    }
    
    public ArusKasSaldo createArusKasSaldo(long indexKas, ArusKasSaldo obj, Connection conn) throws Exception{
        return sql.createArusKasSaldo(indexKas, obj, conn);
    }
    
    public APBD getAPBDHeader(short year, String kodeSatker, short kodeData, short jenisCOA, Connection conn) throws Exception{
        return sql.getAPBDHeader(year, kodeSatker, kodeData, jenisCOA, conn);
    }
    public APBD getApbdStruktur(String kodeSatker, short tahun, short kodeData, short jenisCOA, Connection conn) throws Exception{
        return sql.getApbdStruktur(kodeSatker, tahun, kodeData, jenisCOA, conn);
    }
    public APBD createAPBDHeader(APBD apbd, short iotype, Connection conn) throws Exception{
        return sql.createAPBDHeader(apbd, iotype, conn);
    }
    public APBD updateAPBDHeader(APBD apbd, Connection conn) throws Exception{
        return sql.updateAPBDHeader(apbd, conn);
    }
    public void deleteAPBD(long id, Connection conn) throws Exception{
        sql.deleteAPBD(id, conn);
    }
    public KegiatanAPBD createKegiatanAPBD(long indexApbd, KegiatanAPBD kegiatan, Connection conn) throws Exception{
        return sql.createKegiatanAPBD(indexApbd, kegiatan, conn);
    }
    public KegiatanAPBD updateKegiatanAPBD(KegiatanAPBD kegiatan, Connection conn) throws Exception{
        return sql.updateKegiatanAPBD(kegiatan, conn);
    }
    public void deleteKegiatanAPBD(long id, Connection conn) throws Exception{
        sql.deleteKegiatanAPBD(id, conn);
    }
    public void createRekeningAPBD(long indexKegiatan, KodeRekeningAPBD rekening, Connection conn) throws Exception{
        sql.createRekeningAPBD(indexKegiatan, rekening, conn);
    }
    public void deleteRekeningAPBD(long indexKegiatan, KodeRekeningAPBD rekening, Connection conn) throws Exception{
        sql.deleteRekeningAPBD(indexKegiatan, rekening, conn);
    }
    
    public RealisasiAPBD getRealisasiApbdHeader(String kodeSatker, short tahun, short bulan, short jenisCOA, Connection conn) throws Exception{
        return sql.getRealisasiApbdHeader(kodeSatker, tahun, bulan, jenisCOA, conn);
    }
    public RealisasiAPBD getRealisasiApbdHeader(String kodeSatker, short tahun, short bulan, short kodeData, short jenisCOA, Connection conn) throws Exception{
        return sql.getRealisasiApbdHeader(kodeSatker, tahun, bulan, kodeData, jenisCOA, conn);
    }
    public RealisasiAPBD getRealisasiApbdStruktur(String kodeSatker, short tahun, short bulan, short kodeData, short jenisCOA, Connection conn) throws Exception{
        return sql.getRealisasiApbdStruktur(kodeSatker, tahun, bulan, kodeData, jenisCOA, conn);
    }
    public RealisasiAPBD createRealiasiAPBDHeader(RealisasiAPBD apbd, short iotype, Connection conn) throws Exception{
        return sql.createRealiasiAPBDHeader(apbd, iotype, conn);
    }
    public RealisasiAPBD updateRealisasiAPBDHeader(RealisasiAPBD apbd,  Connection conn) throws Exception{
        return sql.updateRealisasiAPBDHeader(apbd, conn);
    }
    public void deleteRealisasiAPBD(long id, Connection conn) throws Exception{
        sql.deleteRealisasiAPBD(id, conn);
    }
    public RealisasiKegiatanAPBD createRealisasiKegiatanAPBD(long indexApbd, RealisasiKegiatanAPBD kegiatan, Connection conn) throws Exception{
        return sql.createRealisasiKegiatanAPBD(indexApbd, kegiatan, conn);
    }
    public RealisasiKegiatanAPBD updateRealisasiKegiatanAPBD(RealisasiKegiatanAPBD kegiatan, Connection conn) throws Exception{
        return sql.updateRealisasiKegiatanAPBD(kegiatan, conn);
    }
    public void deleteRealisasiKegiatanAPBD(long id, Connection conn) throws Exception{
        sql.deleteRealisasiKegiatanAPBD(id, conn);
    }
    public void createRealisasiRekeningAPBD(long indexKegiatan, RealisasiKodeRekeningAPBD rekening, Connection conn) throws Exception{
        sql.createRealisasiRekeningAPBD(indexKegiatan, rekening, conn);
    }
    public void deleteRealisasiRekeningAPBD(long indexKegiatan, RealisasiKodeRekeningAPBD rekening, Connection conn) throws Exception{
        sql.deleteRealisasiRekeningAPBD(indexKegiatan, rekening, conn);
    }
    
}
