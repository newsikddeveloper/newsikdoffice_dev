/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.office.ejb.bl;

import app.sikd.entity.backoffice.DataDaerah;
import app.sikd.entity.backoffice.DataDaerahDetail;
import app.sikd.entity.backoffice.PDRB;
import app.sikd.entity.backoffice.PDRBHarga;
import java.sql.Connection;

/**
 *
 * @author detra
 */
public interface INonFinansialBusinessLogic {
    public void createDataDaerah(DataDaerah obj, Connection conn) throws Exception;
    public void deleteDataDaerah(String kodeSatker, short tahunAnggaran, Connection conn) throws Exception;
    public  DataDaerahDetail getDataDaerahDetail(String kodeSatker, short tahunAnggaran, Connection conn) throws Exception;
    
    
    public void createPDRB(PDRB obj, Connection conn) throws Exception;
    public void deletePDRB(String kodeSatker, short tahunAnggaran, Connection conn) throws Exception;
    public  PDRBHarga getPDRBHargaBerlaku(String kodeSatker, short tahunAnggaran, Connection conn) throws Exception;
    public  PDRBHarga getPDRBHargaKonstan(String kodeSatker, short tahunAnggaran, Connection conn) throws Exception;
    
}
