/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.office.ejb.bl;

import app.sikd.entity.utilitas.MappingAccount;
import app.sikd.office.ejb.dbapi.INotifikasiSQL;
import app.sikd.office.ejb.dbapi.NotifikasiSQL;
import java.sql.Connection;
import java.util.List;

/**
 *
 * @author sora
 */
public class SIKDBusinessLogic implements ISIKDBusinessLogic{
    INotifikasiSQL notSQL;
    public SIKDBusinessLogic(){
        notSQL= new NotifikasiSQL();
    }
    
    /**
     *
     * @param tahun
     * @param kodeSatker
     * @param kodeData
     * @param jeniscoa
     * @param conn
     * @return
     * @throws Exception
     */
    @Override
    public short getApbdNotifikasi(short tahun, String kodeSatker, short kodeData, short jeniscoa, Connection conn) throws Exception{
        return notSQL.getApbdNotifikasi(tahun, kodeSatker, kodeData, jeniscoa, conn);
    }

    /**
     *
     * @param tahun
     * @param periode
     * @param kodeSatker
     * @param kodeData
     * @param jeniscoa
     * @param conn
     * @return
     * @throws Exception
     */
    @Override
    public short getLraNotifikasi(short tahun, short periode, String kodeSatker, short kodeData, short jeniscoa, Connection conn) throws Exception{
        return notSQL.getLraNotifikasi(tahun, periode, kodeSatker, kodeData, jeniscoa, conn);
    }

    /**
     *
     * @param tahun
     * @param periode
     * @param kodeSatker
     * @param conn
     * @return
     * @throws Exception
     */
    @Override
    public short getDthNotifikasi(short tahun, short periode, String kodeSatker, Connection conn) throws Exception{
        return notSQL.getDthNotifikasi(tahun, periode, kodeSatker, conn);
    }
    
    /**
     *
     * @param tahun
     * @param semester
     * @param kodeSatker
     * @param conn
     * @return
     * @throws Exception
     */
    @Override
    public short getNeracaNotifikasi(short tahun, short semester, String kodeSatker, Connection conn) throws Exception{
        return notSQL.getNeracaNotifikasi(tahun, semester, kodeSatker, conn);
    }
    
    /**
     *
     * @param tahun
     * @param kodeSatker
     * @param conn
     * @return
     * @throws Exception
     */
    @Override
    public short getArusKasNotifikasi(short tahun, String kodeSatker, Connection conn) throws Exception{
        return notSQL.getArusKasNotifikasi(tahun, kodeSatker, conn);
    }
    
    /**
     *
     * @param tahun
     * @param triwulan
     * @param kodeSatker
     * @param conn
     * @return
     * @throws Exception
     */
    @Override
    public short getLONotifikasi(short tahun, short triwulan, String kodeSatker, Connection conn) throws Exception{
        return notSQL.getLONotifikasi(tahun, triwulan, kodeSatker, conn);
    }
    
    /**
     *
     * @param tahun
     * @param kodeSatker
     * @param conn
     * @return
     * @throws Exception
     */
    @Override
    public short getEkuitasNotifikasi(short tahun, String kodeSatker, Connection conn) throws Exception{
        return notSQL.getEkuitasNotifikasi(tahun, kodeSatker, conn);
    }
    
    /**
     *
     * @param tahun
     * @param kodeSatker
     * @param conn
     * @return
     * @throws Exception
     */
    @Override
    public short getSalNotifikasi(short tahun, String kodeSatker, Connection conn) throws Exception{
        return notSQL.getSalNotifikasi(tahun, kodeSatker, conn);
    }
    
    /**
     *
     * @param tahun
     * @param kodeSatker
     * @param conn
     * @return
     * @throws Exception
     */
    @Override
    public short getPFKNotifikasi(short tahun, String kodeSatker, Connection conn) throws Exception{
        return notSQL.getPFKNotifikasi(tahun, kodeSatker, conn);
    }
    
    /**
     *
     * @param tahun
     * @param kodeSatker
     * @param conn
     * @return
     * @throws Exception
     */
    @Override
    public short getPinjamanDaerahNotifikasi(short tahun, String kodeSatker, Connection conn) throws Exception{
        return notSQL.getPinjamanDaerahNotifikasi(tahun, kodeSatker, conn);
    }
    
    /**
     *
     * @param tahun
     * @param kodeSatker
     * @param jenisCOA
     * @param conn
     * @return
     * @throws Exception
     */
    public List<MappingAccount> getAllNotifikasi(short tahun, String kodeSatker, short jenisCOA, Connection conn) throws Exception{
        return notSQL.getAllNotifikasi(tahun, kodeSatker, jenisCOA, conn);
    }
    
}
