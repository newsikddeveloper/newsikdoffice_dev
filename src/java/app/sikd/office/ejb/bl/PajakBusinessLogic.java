/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.office.ejb.bl;

import app.sikd.entity.backoffice.DTH;
import app.sikd.entity.backoffice.RTH;
import app.sikd.entity.backoffice.RincianBPHTB;
import app.sikd.entity.backoffice.RincianHiburan;
import app.sikd.entity.backoffice.RincianHotel;
import app.sikd.entity.backoffice.RincianIMB;
import app.sikd.entity.backoffice.RincianIzinUsaha;
import app.sikd.entity.backoffice.RincianKendaraan;
import app.sikd.entity.backoffice.RincianPajakDanRetribusi;
import app.sikd.entity.backoffice.RincianRestoran;
import app.sikd.office.ejb.dbapi.IOfficeSQL;
import app.sikd.office.ejb.dbapi.OfficeSQL;
import java.sql.Connection;
import java.util.List;

/**
 *
 * @author sora
 */
public class PajakBusinessLogic implements IPajakBusinessLogic{
    IOfficeSQL sql;
    
    public PajakBusinessLogic(){
        sql = new OfficeSQL();        
    }
    
    @Override
    public DTH getDTHReport(short year, String kodeSatker, short periode, String kodeSKPD, Connection conn) throws Exception{
        return sql.getDTHReport(year, kodeSatker, periode, kodeSKPD, conn);
    }
    
    @Override
    public List<RTH> getRTHReport(short year, String kodeSatker, short periode, Connection conn) throws Exception{
        return sql.getRTHReport(year, kodeSatker, periode, conn);
    }
    
    @Override
    public List<String> getDinasKirimDTH(short year, String kodeSatker, Connection conn) throws Exception{
        return sql.getDinasKirimDTH(year, kodeSatker, conn);
    }
    
    @Override
    public List<RincianKendaraan> getRincianKendaraan(String kodeSatker, short tahun, Connection conn) throws Exception{
        return sql.getRincianKendaraan(kodeSatker, tahun, conn);
    }
    @Override
    public List<RincianBPHTB> getRincianBPHTB(String kodeSatker, short tahun, Connection conn) throws Exception{
        return sql.getRincianBPHTB(kodeSatker, tahun, conn);
    }
    @Override
    public List<RincianHiburan> getRincianHiburan(String kodeSatker, short tahun, Connection conn) throws Exception{
        return sql.getRincianHiburan(kodeSatker, tahun, conn);
    }
    @Override
    public List<RincianHotel> getRincianHotel(String kodeSatker, short tahun, Connection conn) throws Exception{
        return sql.getRincianHotel(kodeSatker, tahun,conn);
    }
    @Override
    public List<RincianIMB> getRincianIMB(String kodeSatker, short tahun, Connection conn) throws Exception{
        return sql.getRincianIMB(kodeSatker, tahun, conn);
    }
    @Override
    public List<RincianRestoran> getRincianRestoran(String kodeSatker, short tahun, Connection conn) throws Exception{
        return sql.getRincianRestoran(kodeSatker, tahun, conn);
    }
    @Override
    public List<RincianIzinUsaha> getRincianIzinUsaha(String kodeSatker, short tahun, Connection conn) throws Exception{
        return sql.getRincianIzinUsaha(kodeSatker, tahun, conn);
    }
    
    @Override
    public List<RincianPajakDanRetribusi> getRincianPajakDanRetribusiDaerah(String kodeSatker, short tahun, Connection conn) throws Exception{
        return sql.getRincianPajakDanRetribusi(kodeSatker, tahun, conn);
    }
    
}
