/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.office.ejb.bl;

import app.sikd.entity.backoffice.DataDaerah;
import app.sikd.entity.backoffice.DataDaerahDetail;
import app.sikd.entity.backoffice.PDRB;
import app.sikd.entity.backoffice.PDRBHarga;
import app.sikd.office.ejb.dbapi.IOfficeSQL;
import app.sikd.office.ejb.dbapi.OfficeSQL;
import java.sql.Connection;
import java.sql.SQLException;

/**
 *
 * @author detra
 */
public class NonFinansialBusinessLogic implements INonFinansialBusinessLogic{
    IOfficeSQL sql;
    public NonFinansialBusinessLogic(){
        sql = new OfficeSQL();
    }
    
    @Override
    public void createDataDaerah(DataDaerah obj, Connection conn) throws Exception{
        try {
            int currentLevel = conn.getTransactionIsolation();
            conn.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
            conn.setAutoCommit(false);
            if (obj != null) {
                long id= sql.getDataDaerahIndex(obj.getKodeSatker(), obj.getTahunAnggaran(), conn);
                if( id <= 0 ) {
                    id = sql.createDataDaerah(obj, conn);
                    if( obj.getDetail() != null ){
                        DataDaerahDetail r = obj.getDetail();
                        sql.createDataDaerahDetail(r, id, conn);
                    }
                }
                else {
                    long detailId = sql.getDataDaerahDetailIndex(id, conn);
                    if (obj.getDetail() != null) {
                        DataDaerahDetail r = obj.getDetail();
                        if( detailId <= 0 )
                            sql.createDataDaerahDetail(r, id, conn);
                        else 
                            sql.updateDataDaerahDetail(r, id, conn);
                    }                    
                }
            }
            conn.commit();
            conn.setAutoCommit(true);
            conn.setTransactionIsolation(currentLevel);
            
        } catch (SQLException ex) {
            try {
                conn.rollback();
            } catch (SQLException exc) {
            }
            throw new Exception("Gagal input Pinjaman Daerah \n" + ex.getMessage());
        } finally {
            if (conn != null) {
                conn.setAutoCommit(true);
            }
        }        
    }
    
    @Override
    public void deleteDataDaerah(String kodeSatker, short tahunAnggaran, Connection conn) throws Exception{
        try {
            int currentLevel = conn.getTransactionIsolation();
            conn.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
            conn.setAutoCommit(false);
                sql.deleteDataDaerah(kodeSatker, tahunAnggaran, conn);
                
            conn.commit();
            conn.setAutoCommit(true);
            conn.setTransactionIsolation(currentLevel);
            
        } catch (SQLException ex) {
            try {
                conn.rollback();
            } catch (SQLException exc) {
            }
            throw new Exception("Gagal Menghapus Pinjaman Daerah \n" + ex.getMessage());
        } finally {
            if (conn != null) {
                conn.setAutoCommit(true);
            }
        }        
    }
    
    @Override
    public  DataDaerahDetail getDataDaerahDetail(String kodeSatker, short tahunAnggaran, Connection conn) throws Exception{
        try {
            int currentLevel = conn.getTransactionIsolation();
            conn.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
                long id = sql.getDataDaerahIndex(kodeSatker, tahunAnggaran, conn);
                DataDaerahDetail result = sql.getDataDaerahDetail(id, conn);
                conn.setTransactionIsolation(currentLevel);
                return result;
        } catch (SQLException ex) {
            try {
                conn.rollback();
            } catch (SQLException exc) {
            }
            throw new Exception("Gagal mengambil Data Daerah \n" + ex.getMessage());
        } finally {
            if (conn != null) {
                conn.setAutoCommit(true);
            }
        }        
    }
    
    
    @Override
    public void createPDRB(PDRB obj, Connection conn) throws Exception{
        try {
            int currentLevel = conn.getTransactionIsolation();
            conn.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
            conn.setAutoCommit(false);
            if (obj != null) {
                long id= sql.getPDRBIndex(obj.getKodeSatker(), obj.getTahunAnggaran(), conn);
                if(id > 0 ) sql.deletePDRB(obj.getKodeSatker(), obj.getTahunAnggaran(), conn);
                
                    id = sql.createPDRB(obj, conn);
                    if( obj.getHargaBerlaku()!= null ){                        
                        sql.createPDRBHargaBerlaku(obj.getHargaBerlaku(), id, conn);
                    }
                    if( obj.getHargaKonstan()!= null ){                        
                        sql.createPDRBHargaKonstant(obj.getHargaKonstan(), id, conn);
                    }
                
            }
            conn.commit();
            conn.setAutoCommit(true);
            conn.setTransactionIsolation(currentLevel);
            
        } catch (SQLException ex) {
            try {
                conn.rollback();
            } catch (SQLException exc) {
            }
            throw new Exception("Gagal input PDRB \n" + ex.getMessage());
        } finally {
            if (conn != null) {
                conn.setAutoCommit(true);
            }
        }        
    }
    
    @Override
    public void deletePDRB(String kodeSatker, short tahunAnggaran, Connection conn) throws Exception{
        try {
            int currentLevel = conn.getTransactionIsolation();
            conn.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
            conn.setAutoCommit(false);
                sql.deletePDRB(kodeSatker, tahunAnggaran, conn);
                
            conn.commit();
            conn.setAutoCommit(true);
            conn.setTransactionIsolation(currentLevel);
            
        } catch (SQLException ex) {
            try {
                conn.rollback();
            } catch (SQLException exc) {
            }
            throw new Exception("Gagal Menghapus PDRB \n" + ex.getMessage());
        } finally {
            if (conn != null) {
                conn.setAutoCommit(true);
            }
        }        
    }
    
    @Override
    public  PDRBHarga getPDRBHargaBerlaku(String kodeSatker, short tahunAnggaran, Connection conn) throws Exception{
        try {
            int currentLevel = conn.getTransactionIsolation();
            conn.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
                PDRBHarga result = sql.getPDRBHargaBerlaku(kodeSatker, tahunAnggaran, conn);
                conn.setTransactionIsolation(currentLevel);
                return result;
        } catch (SQLException ex) {
            try {
                conn.rollback();
            } catch (SQLException exc) {
            }
            throw new Exception("Gagal mengambil PDRB Harga Berlaku \n" + ex.getMessage());
        } finally {
            if (conn != null) {
                conn.setAutoCommit(true);
            }
        }        
    }
    
    
    @Override
    public  PDRBHarga getPDRBHargaKonstan(String kodeSatker, short tahunAnggaran, Connection conn) throws Exception{
        try {
            int currentLevel = conn.getTransactionIsolation();
            conn.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
                PDRBHarga result = sql.getPDRBHargaKonstan(kodeSatker, tahunAnggaran, conn);
                conn.setTransactionIsolation(currentLevel);
                return result;
        } catch (SQLException ex) {
            try {
                conn.rollback();
            } catch (SQLException exc) {
            }
            throw new Exception("Gagal mengambil PDRB Harga Konstant \n" + ex.getMessage());
        } finally {
            if (conn != null) {
                conn.setAutoCommit(true);
            }
        }        
    }
    
}
