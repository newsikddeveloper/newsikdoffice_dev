/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.office.ejb.bl;

import app.sikd.entity.utilitas.MappingAccount;
import java.sql.Connection;
import java.util.List;

/**
 *
 * @author sora
 */
public interface ISIKDBusinessLogic {
    public short getApbdNotifikasi(short tahun, String kodeSatker, short kodeData, short jeniscoa, Connection conn) throws Exception;
    public short getLraNotifikasi(short tahun, short periode, String kodeSatker, short kodeData, short jeniscoa, Connection conn) throws Exception;
    public short getDthNotifikasi(short tahun, short periode, String kodeSatker, Connection conn) throws Exception;
    
    public short getNeracaNotifikasi(short tahun, short semester, String kodeSatker, Connection conn) throws Exception;
    public short getArusKasNotifikasi(short tahun, String kodeSatker, Connection conn) throws Exception;
    public short getLONotifikasi(short tahun, short triwulan, String kodeSatker, Connection conn) throws Exception;
    public short getEkuitasNotifikasi(short tahun, String kodeSatker, Connection conn) throws Exception;
    public short getSalNotifikasi(short tahun, String kodeSatker, Connection conn) throws Exception;
    public short getPFKNotifikasi(short tahun, String kodeSatker, Connection conn) throws Exception;
    public short getPinjamanDaerahNotifikasi(short tahun, String kodeSatker, Connection conn) throws Exception;
    
    public List<MappingAccount> getAllNotifikasi(short tahun, String kodeSatker, short jenisCOA, Connection conn) throws Exception;
    
}
