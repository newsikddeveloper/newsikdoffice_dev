/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.office.ejb.bl;

import app.sikd.entity.backoffice.UploadFilePDF;
import app.sikd.entity.utilitas.UploadFile;
import app.sikd.office.ejb.dbapi.IUploadFileDAO;
import app.sikd.office.ejb.dbapi.UploadFileDAO;
import java.sql.Connection;
import java.util.List;

/**
 *
 * @author detra
 */
public class UploadFileBusinessLogic implements IUploadFileBusinessLogic{
    IUploadFileDAO dao;

    public UploadFileBusinessLogic() {
        dao = new UploadFileDAO();
    }
    
    @Override
    public void addUploadFile(UploadFile uploadFile, String tableName, Connection sikdConn) throws Exception{
        dao.addUploadFile(uploadFile, tableName, sikdConn);
    }
 
    @Override
     public void deleteUploadFile(long id, String tableName, Connection sikdConn) throws Exception{
         dao.deleteUploadFile(id, tableName, sikdConn);
     }
 
    @Override
     public void updateUploadFile(UploadFile uploadFile, String tableName, Connection sikdConn) throws Exception{
         dao.updateUploadFile(uploadFile, tableName, sikdConn);
     }
    @Override
     public List<UploadFile> getAllUploadFiles(String tableName, String kodeSatker, short tahun, Connection sikdConn) throws Exception{
         return dao.getAllUploadFiles(tableName, kodeSatker, tahun, sikdConn);
     }
    
    @Override
    public void addPdfUploadFile(UploadFilePDF uploadFile, String tableName, Connection sikdConn) throws Exception{
        dao.addPdfUploadFile(uploadFile, tableName, sikdConn);
    }
 
    @Override
     public void deletePdfUploadFile(long id, String tableName, Connection sikdConn) throws Exception{
         dao.deletePdfUploadFile(id, tableName, sikdConn);
     }
 
    @Override
     public List<UploadFilePDF> getAllPdfUploadFiles(String tableName, String kodeSatker, Connection sikdConn) throws Exception{
         return dao.getAllPdfUploadFiles(tableName, kodeSatker, sikdConn);
     }
 
    
}
