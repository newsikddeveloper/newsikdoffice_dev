/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.office.ejb.bl;

import app.sikd.entity.apbd.LampiranIIIPerdaAPBD;
import app.sikd.entity.apbd.LampiranIIPerdaAPBD;
import app.sikd.entity.apbd.LampiranILra;
import app.sikd.entity.apbd.LampiranIPerdaAPBD;
import app.sikd.entity.apbd.LampiranIVPerdaAPBD;
import app.sikd.entity.apbd.LampiranVPerdaAPBD;
import app.sikd.entity.backoffice.APBDPerda;
import app.sikd.entity.backoffice.NilaiFungsiPemda;
import app.sikd.entity.backoffice.PerhitunganFihakKetiga;
import app.sikd.entity.backoffice.PinjamanDaerah;
import app.sikd.entity.backoffice.RealisasiAPBD;
import app.sikd.entity.backoffice.RealisasiKegiatanAPBD;
import app.sikd.entity.backoffice.RealisasiKodeRekeningAPBD;
import app.sikd.entity.backoffice.RekapitulasiGajiPegawai;
import app.sikd.entity.backoffice.RincianPerhitunganFihakKetiga;
import app.sikd.entity.backoffice.RincianPinjamanDaerah;
import app.sikd.entity.utilitas.MappingAccount;
import app.sikd.entity.utilitas.MappingNonStandar;
import app.sikd.entity.utilitas.SimpleAccount;
import java.sql.Connection;
import java.sql.Timestamp;
import java.util.List;

/**
 *
 * @author detra
 */
public interface IAPBDBusinessLogic {
    public APBDPerda getAPBDPerdaNumber(short year, String kodeSatker, short kodeData, short jenisCOA, Connection conn) throws Exception;
    public APBDPerda getRealisasiAPBDPerdaNumber(short year, String kodeSatker, short kodeData, short jenisCOA, Connection conn) throws Exception;
    
    public List<LampiranIPerdaAPBD> getLampiranIPerdaAPBD(short year, String satkerCode, short kodeData, short jenisCOA, Connection conn) throws Exception;
    public List<LampiranIIPerdaAPBD> getLampiranIIPerdaAPBD(short year, String kodeSatker, short kodeData, short jenisCOA, Connection conn) throws Exception;
    public List<LampiranIIIPerdaAPBD> getLampiranIIIPerdaAPBD(short year, String satkerCode, short kodeData, String kodeSKPD, short jenisCOA, Connection conn) throws Exception;
    public List<LampiranIVPerdaAPBD> getLampiranIVPerdaAPBD(short year, String kodeSatker, short kodeData, short jenisCOA, Connection conn) throws Exception;
    public List<LampiranVPerdaAPBD> getLampiranVPerdaAPBD(short year, String satkerCode, short kodeData, short jenisCOA, Connection conn) throws Exception;
    
    public List<String> getDinasKirimAPBD(short year, String kodeSatker,  short kodeData, short jenisCOA, Connection conn) throws Exception;                
    public Timestamp getTanggalDinasKirimAPBD(short year, String kodeSatker, short kodeData, short jenisCOA, Connection conn) throws Exception ;
    
    public List<LampiranIPerdaAPBD> getKompilasiLampiranIPerdaAPBD(short year, List<String> kodePemdas, short kodeData, short jenisCOA, Connection conn) throws Exception;
    public List<LampiranIIPerdaAPBD> getKompilasiLampiranIIPerdaAPBD(short year, List<String> kodePemdas, short kodeData, short jenisCOA, Connection conn) throws Exception;
    
    public List<LampiranIPerdaAPBD> getRingkasanPembiayaan(short year, String satkerCode, short kodeData, short jenisCOA, Connection conn) throws Exception;
    public List<LampiranIPerdaAPBD> getRingkasanPembiayaan(short year, List<String> kodepemdas, short kodeData, short jenisCOA, Connection conn) throws Exception;
    
//    public List<LampiranIPerdaAPBD> getRealisasiAPBD(short year, String satkerCode, short kodeData, short month, short jenisCOA, Connection conn) throws Exception;
    public List<LampiranILra> getRealisasiAPBD(short year, String kodeSatker, short month, short jenisCOA, Connection conn) throws Exception;
    public List<LampiranILra> getRealisasiAPBD(short year, String satkerCode, short kodeData, short month, short jenisCOA, Connection conn) throws Exception;
    public List<LampiranIPerdaAPBD> getKompilasiRealisasiAPBD(short year, List<String> kodePemdas, short kodeData, short month, short jenisCOA, Connection conn) throws Exception;
    public List<String> getDinasKirimRealisasiAPBD(short year, String kodeSatker,  short kodeData, short jenisCOA, Connection conn) throws Exception;
    public Timestamp getTanggalDinasKirimRealisasiAPBD(short year, String kodeSatker, short kodeData, short jenisCOA, Connection conn) throws Exception ;
    
    public String getUrusan(String kodeUrusan, Connection conn) throws Exception;
    
    
    public List<RekapitulasiGajiPegawai> getRekapitulasiGajiPegawai(short year, String satkerCode, short month, Connection conn) throws Exception;
    
    public List<RincianPerhitunganFihakKetiga> getRincianPFK(short year, String satkerCode, Connection conn) throws Exception;
    public void createPerhitunganFihakKetiga(PerhitunganFihakKetiga obj, short iotype, Connection conn) throws Exception;
    public void updatePerhitunganFihakKetiga(PerhitunganFihakKetiga oldObj, PerhitunganFihakKetiga newObj, short iotype, Connection conn) throws Exception;
    public void deletePerhitunganFihakKetiga(PerhitunganFihakKetiga obj, Connection conn) throws Exception;
    
    public List<RincianPinjamanDaerah> getRincianPinjamanDaerah(short year, String satkerCode, Connection conn) throws Exception;
    public void createPinjamanDaerah(PinjamanDaerah obj, short iotype, Connection conn) throws Exception;
    public void updatePinjamanDaerah(PinjamanDaerah oldObj, PinjamanDaerah newObj, Connection conn) throws Exception;
    public void deletePinjamanDaerah(PinjamanDaerah obj, Connection conn) throws Exception;
    
    public List<NilaiFungsiPemda> getRealisasiKegiatanBerdasarkanFungsi(short tahun, short bulan, String kodeSatker, short kodeData, String kodeFungsi, Connection conn) throws Exception;
    public List<NilaiFungsiPemda> getRealisasiBelanjaBerdasarkanSumberDana(short tahun, short bulan, String kodeSatker, short sumberDana, Connection conn) throws Exception;
    public List<LampiranIPerdaAPBD> getLampiranIPenjabaranAPBDbyYear(short year, String kodeSatker, short kodeData, short jenisCOA, Connection conn) throws Exception;
    
    public void deleteMappingAccounts(MappingAccount ma, Connection conn) throws Exception;
    public void createMappingAccounts(MappingAccount ma, Connection conn) throws Exception;
    public List<MappingAccount> getMappingAccounts(short level, Connection conn) throws Exception;
    public List<SimpleAccount> getAccrualAccounts(short[] level, Connection conn) throws Exception;
    
    public void deleteGfsMappingAccounts(MappingAccount ma, Connection conn) throws Exception;
    public void createGfsMappingAccounts(MappingAccount ma, Connection conn) throws Exception;
    public List<MappingAccount> getGfsMappingAccounts(short[] levels, Connection conn) throws Exception;
    public List<SimpleAccount> getGfsAccounts(short[] level, Connection conn) throws Exception;
    
    public RealisasiAPBD getLRA(String kodeSatker, short tahun , short bulan, short kodeData, short jenisCOA, String userName, String password, Connection conn) throws Exception;
    public List<RealisasiKegiatanAPBD> getLRAKegiatan(long indexLRA, Connection conn) throws Exception;
    public List<RealisasiKodeRekeningAPBD> getLRAAkun(long indexKegiatanLRA, Connection conn) throws Exception;
    
    public List<MappingNonStandar> getMappingNonStandarAccountApbds(short level, short tahun, String kodeSatker, Connection conn) throws Exception;
    public List<MappingNonStandar> getMappingNonStandarAccountLras(short level, short tahun, String kodeSatker, Connection conn) throws Exception;
    public List<MappingNonStandar> getMappingNonStandarAccountDths(short level, short tahun, String kodeSatker, Connection conn) throws Exception;
    public List<MappingNonStandar> getMappingNonStandarAccountNeracas(short level, short tahun, String kodeSatker, Connection conn) throws Exception;
    public MappingNonStandar createMappingAccountNonStandars(MappingNonStandar ma, Connection conn) throws Exception;
    public void deleteMappingAccountNonStandars(MappingNonStandar ma, Connection conn) throws Exception;
    
}
