
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.office.ejb.bl;

import app.sikd.entity.backoffice.ArusKasReport;
import app.sikd.entity.backoffice.LOReport;
import app.sikd.entity.backoffice.LaporanOperasional;
import app.sikd.entity.backoffice.Neraca;
import app.sikd.entity.backoffice.NeracaAkunJenis;
import app.sikd.entity.backoffice.NeracaAkunKelompok;
import app.sikd.entity.backoffice.NeracaAkunObjek;
import app.sikd.entity.backoffice.NeracaAkunUtama;
import app.sikd.entity.backoffice.NeracaReport;
import app.sikd.entity.backoffice.PerubahanEkuitas;
import app.sikd.entity.backoffice.PerubahanSAL;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author sora
 */
public interface ILaporanBusinessLogic {
    public List<NeracaReport> getNeracaReports(short year, short semester, String kodeSatker, String judulNeraca, Connection conn) throws Exception;
    public List<String> getNeracaLabels(short year, short semester, String kodeSatker, Connection conn) throws Exception;
    
    public Neraca createNeracaStruktur(Neraca neraca, short iotype, Connection conn) throws Exception;
    public Neraca getNeraca(short year, short semester, String kodeSatker, String judulNeraca, Connection conn) throws Exception;
    public long createNeraca(Neraca neraca, short iotype, Connection conn) throws Exception;
    public void updateNeraca(Neraca neraca, Connection conn) throws Exception;
    public void deleteNeraca(Neraca neraca, Connection conn) throws Exception;
    
    public long createNeracaAkunUtama(long indexNeraca, NeracaAkunUtama obj, Connection conn) throws Exception;
    public List<NeracaAkunUtama> getNeracaAkunUtamas(long indexNeraca, Connection conn) throws Exception;
    public void updateNeracaAkunUtama(NeracaAkunUtama obj, Connection conn) throws Exception;
    public void deleteNeracaAkunUtama(NeracaAkunUtama obj, Connection conn) throws Exception;
    
    public long createNeracaAkunKelompok(long indexNeracaAkunUtama, NeracaAkunKelompok obj, Connection conn) throws Exception;
    public List<NeracaAkunKelompok> getNeracaAkunKelompoks(long indexNeracaAkunUtama, Connection conn) throws Exception;
    public void updateNeracaAkunKelompok(NeracaAkunKelompok obj, Connection conn) throws Exception;
    public void deleteNeracaAkunKelompok(NeracaAkunKelompok obj, Connection conn) throws Exception;
    
    public long createNeracaAkunJenis(long indexNeracaAkunKelompok, NeracaAkunJenis obj, Connection conn) throws Exception;
    public List<NeracaAkunJenis> getNeracaAkunJeniss(long indexNeracaAkunKelompok, Connection conn) throws Exception;
    public void updateNeracaAkunJenis(NeracaAkunJenis obj, Connection conn) throws Exception;
    public void deleteNeracaAkunJenis(NeracaAkunJenis obj, Connection conn) throws Exception;
    
    public long createNeracaAkunObjek(long indexNeracaAkunJenis, NeracaAkunObjek obj, Connection conn) throws Exception;
    public List<NeracaAkunObjek> getNeracaAkunObjeks(long indexNeracaAkunJenis, Connection conn) throws Exception;
    public void updateNeracaAkunObjek(NeracaAkunObjek obj, Connection conn) throws Exception;
    public void deleteNeracaAkunObjek(NeracaAkunObjek obj, Connection conn) throws Exception;
    
    public List<ArusKasReport> getArusKasReports(short year, String kodeSatker, String judulArusKas, Connection conn) throws Exception;
    public List<String> getArusKasLabels(short year, String kodeSatker, Connection conn) throws Exception;
    
    
    public PerubahanSAL getPerubahanSalReports(short year, String kodeSatker, Connection conn) throws Exception;
    public long createPerubahanSalReports(PerubahanSAL sal, short iotype, Connection conn) throws Exception;
    public void deletePerubahanSalReports(short year, String kodeSatker, Connection conn) throws Exception;
    public PerubahanEkuitas getPerubahanEkuitasReports(short year, String kodeSatker, Connection conn) throws Exception;
    public long createPerubahanEkuitasReports(PerubahanEkuitas lpe, short iotype, Connection conn) throws Exception;
    public void deletePerubahanEkuitasReports(short year, String kodeSatker, Connection conn) throws Exception;
    public List<LOReport> getLaporanOperasionalReports(short year, short triwulan, String kodeSatker, Connection conn) throws Exception;
}
