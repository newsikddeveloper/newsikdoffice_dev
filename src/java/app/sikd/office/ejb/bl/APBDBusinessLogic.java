/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.office.ejb.bl;

import app.sikd.entity.apbd.LampiranIIIPerdaAPBD;
import app.sikd.entity.apbd.LampiranIIPerdaAPBD;
import app.sikd.entity.apbd.LampiranILra;
import app.sikd.entity.apbd.LampiranIPerdaAPBD;
import app.sikd.entity.apbd.LampiranIVPerdaAPBD;
import app.sikd.entity.apbd.LampiranVPerdaAPBD;
import app.sikd.entity.backoffice.APBDPerda;
import app.sikd.entity.backoffice.NilaiFungsiPemda;
import app.sikd.entity.backoffice.PerhitunganFihakKetiga;
import app.sikd.entity.backoffice.PinjamanDaerah;
import app.sikd.entity.backoffice.RealisasiAPBD;
import app.sikd.entity.backoffice.RealisasiKegiatanAPBD;
import app.sikd.entity.backoffice.RealisasiKodeRekeningAPBD;
import app.sikd.entity.backoffice.RekapitulasiGajiPegawai;
import app.sikd.entity.backoffice.RincianPerhitunganFihakKetiga;
import app.sikd.entity.backoffice.RincianPinjamanDaerah;
import app.sikd.entity.utilitas.MappingAccount;
import app.sikd.entity.utilitas.MappingNonStandar;
import app.sikd.entity.utilitas.SimpleAccount;
import app.sikd.office.ejb.dbapi.IOfficeSQL;
import app.sikd.office.ejb.dbapi.OfficeSQL;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

/**
 *
 * @author detra
 */
public class APBDBusinessLogic implements IAPBDBusinessLogic{
    IOfficeSQL sql;
    
    public APBDBusinessLogic(){
        sql = new OfficeSQL();        
    }
    
    @Override
    public APBDPerda getAPBDPerdaNumber(short year, String kodeSatker, short kodeData, short jenisCOA, Connection conn) throws Exception{
        return sql.getAPBDPerdaNumber(year, kodeSatker, kodeData, jenisCOA, conn);        
    }
    @Override
    public APBDPerda getRealisasiAPBDPerdaNumber(short year, String kodeSatker, short kodeData, short jenisCOA, Connection conn) throws Exception{
        return sql.getLRAPerdaNumber(year, kodeSatker, kodeData, jenisCOA, conn);        
    }
    
    @Override
    public List<LampiranIPerdaAPBD> getLampiranIPerdaAPBD(short year, String satkerCode, short kodeData, short jenisCOA, Connection conn) throws Exception{
        List<LampiranIPerdaAPBD> xx=sql.getLampiranIPerdaAPBDbyYear(year, satkerCode, kodeData, jenisCOA, conn);

        return xx;
    }
    
    @Override
    public List<LampiranIPerdaAPBD> getKompilasiLampiranIPerdaAPBD(short year, List<String> kodePemdas, short kodeData, short jenisCOA, Connection conn) throws Exception{
        List<LampiranIPerdaAPBD> xx=sql.getKompilasiLampiranIPerdaAPBD(year, kodePemdas, kodeData, jenisCOA, conn);

        return xx;
    }
    
    @Override
    public List<LampiranIIPerdaAPBD> getLampiranIIPerdaAPBD(short year, String kodeSatker, short kodeData, short jenisCOA, Connection conn) throws Exception{
        return sql.getLampiranIIPerdaAPBDbyYear(year, kodeSatker, kodeData, jenisCOA, conn);
    }
    
    @Override
    public List<LampiranIIPerdaAPBD> getKompilasiLampiranIIPerdaAPBD(short year, List<String> kodePemdas, short kodeData, short jenisCOA, Connection conn) throws Exception{
        List<LampiranIIPerdaAPBD> xx=sql.getKompilasiLampiranIIPerdaAPBD(year, kodePemdas, kodeData, jenisCOA, conn);

        return xx;
    }
    
    @Override
    public List<String> getDinasKirimRealisasiAPBD(short year, String kodeSatker,  short kodeData, short jenisCOA, Connection conn) throws Exception{
        return sql.getDinasKirimRealisasiAPBD(year, kodeSatker, kodeData, jenisCOA, conn);
    }
    
    @Override
    public Timestamp getTanggalDinasKirimRealisasiAPBD(short year, String kodeSatker,  short kodeData, short jenisCOA, Connection conn) throws Exception{
        return sql.getTanggalDinasKirimRealisasiAPBD(year, kodeSatker, kodeData, jenisCOA, conn);
    }
    
    @Override
    public List<String> getDinasKirimAPBD(short year, String kodeSatker,  short kodeData, short jenisCOA, Connection conn) throws Exception{
        return sql.getDinasKirimAPBD(year, kodeSatker, kodeData, jenisCOA, conn);
    }
    
    @Override
    public Timestamp getTanggalDinasKirimAPBD(short year, String kodeSatker, short kodeData, short jenisCOA, Connection conn) throws Exception{
        return sql.getTanggalDinasKirimAPBD(year, kodeSatker, kodeData, jenisCOA, conn);
    }
    
    @Override
    public String getUrusan(String kodeUrusan, Connection conn) throws Exception{
        return sql.getUrusan(kodeUrusan, conn);
    }
    
    @Override
    public List<LampiranIIIPerdaAPBD> getLampiranIIIPerdaAPBD(short year, String kodeSatker, short kodeData, String kodeSKPD, short jenisCOA, Connection conn) throws Exception{
        return sql.getLampiranIIIPerdaAPBD(year, kodeSatker, kodeData, kodeSKPD, jenisCOA, conn);
    }
    
    @Override
    public List<LampiranIVPerdaAPBD> getLampiranIVPerdaAPBD(short year, String kodeSatker, short kodeData, short jenisCOA, Connection conn) throws Exception{
        return sql.getLampiranIVPerdaAPBD(year, kodeSatker, kodeData, jenisCOA, conn);
    }
    
    @Override
    public List<LampiranVPerdaAPBD> getLampiranVPerdaAPBD(short year, String kodeSatker, short kodeData, short jenisCOA, Connection conn) throws Exception{
        return sql.getLampiranVPerdaAPBD(year, kodeSatker, kodeData, jenisCOA, conn);
    }
    
    @Override
    public List<LampiranILra> getRealisasiAPBD(short year, String satkerCode, short kodeData, short month, short jenisCOA, Connection conn) throws Exception{
        List<LampiranILra> xx=sql.getRealisasiAPBD(year, satkerCode, kodeData, month, jenisCOA, conn);
        return xx;//result;
    }
    
    @Override
    public List<LampiranILra> getRealisasiAPBD(short year, String kodeSatker, short month, short jenisCOA, Connection conn) throws Exception{
        List<LampiranILra> xx=sql.getRealisasiAPBD(year, kodeSatker, month, jenisCOA, conn);
        return xx;
    }
    
//    @Override
//    public List<LampiranIPerdaAPBD> getRealisasiAPBD(short year, String satkerCode, short kodeData, short month, short jenisCOA, Connection conn) throws Exception{
//        List<LampiranIPerdaAPBD> xx=sql.getRealisasiAPBD(year, satkerCode, kodeData, month, jenisCOA, conn);
//        return xx;//result;
//    }
//    
    @Override
    public List<LampiranIPerdaAPBD> getKompilasiRealisasiAPBD(short year, List<String> kodePemdas, short kodeData, short month, short jenisCOA, Connection conn) throws Exception{
        List<LampiranIPerdaAPBD> xx=sql.getKompilasiRealisasiAPBD(year, kodePemdas, kodeData, month, jenisCOA, conn);
        return xx;//result;
    }
    
    @Override
    public List<LampiranIPerdaAPBD> getRingkasanPembiayaan(short year, String satkerCode, short kodeData, short jenisCOA, Connection conn) throws Exception{
        List<LampiranIPerdaAPBD> xx=sql.getRingkasanPembiayaan(year, satkerCode, kodeData, jenisCOA, conn);
        return xx;
    }
    
    @Override
    public List<LampiranIPerdaAPBD> getRingkasanPembiayaan(short year, List<String> kodepemdas, short kodeData, short jenisCOA, Connection conn) throws Exception{
        List<LampiranIPerdaAPBD> xx=sql.getRingkasanPembiayaan(year, kodepemdas, kodeData, jenisCOA, conn);
        return xx;
    }
    
    
    @Override
    public List<RekapitulasiGajiPegawai> getRekapitulasiGajiPegawai(short year, String satkerCode, short month, Connection conn) throws Exception{
        return sql.getRekapitulasiGajiPegawai(year, satkerCode, month, conn);
    }
    
    
    @Override
    public List<RincianPerhitunganFihakKetiga> getRincianPFK(short year, String satkerCode, Connection conn) throws Exception{
        return sql.getRincianPFK(year, satkerCode, conn);
    }
    
    @Override
    public void createPerhitunganFihakKetiga(PerhitunganFihakKetiga obj, short iotype, Connection conn) throws Exception{
        try {
            int currentLevel = conn.getTransactionIsolation();
            conn.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
            conn.setAutoCommit(false);
            if (obj != null) {
                long id;
                PerhitunganFihakKetiga pfk = sql.getPFK(obj.getTahunAnggaran(), obj.getKodeSatker(), conn);
                if( pfk == null || pfk.getId() <= 0 ) {
                    id = sql.createPFK(obj, iotype, conn);                    
                }
                else id = pfk.getId();
                if( obj.getRincians() != null && obj.getRincians().size() > 0 ){
                    for (int i = 0; i < obj.getRincians().size(); i++) {
                        RincianPerhitunganFihakKetiga r = obj.getRincians().get(i);                        
                        sql.createRincianPFK(id, r, conn);
                    }
                }
            }
            conn.commit();
            conn.setAutoCommit(true);
            conn.setTransactionIsolation(currentLevel);
            
        } catch (SQLException ex) {
            try {
                conn.rollback();
            } catch (SQLException exc) {
            }
            throw new Exception("Gagal input Perhitungan Fihak Ketiga \n" + ex.getMessage());
        } finally {
            if (conn != null) {
                conn.setAutoCommit(true);
            }
        }        
    }
    
    @Override
    public void updatePerhitunganFihakKetiga(PerhitunganFihakKetiga oldObj, PerhitunganFihakKetiga newObj, short iotype, Connection conn) throws Exception{
        try {
            int currentLevel = conn.getTransactionIsolation();
            conn.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
            conn.setAutoCommit(false);
            if (newObj != null) {
                long id;
                PerhitunganFihakKetiga pfk = sql.getPFK(newObj.getTahunAnggaran(), newObj.getKodeSatker(), conn);
                if( pfk == null || pfk.getId() <= 0 ) {
                    id = sql.createPFK(newObj, iotype, conn);
                }
                else id = pfk.getId();
                if( oldObj.getRincians() != null && oldObj.getRincians().size() > 0 ){
                    for (int i = 0; i < oldObj.getRincians().size(); i++) {
                        RincianPerhitunganFihakKetiga r = oldObj.getRincians().get(i);                        
                        sql.deleteRincianPFK(id, r, conn);
                    }
                }
                if( newObj.getRincians() != null && newObj.getRincians().size() > 0 ){
                    for (int i = 0; i < newObj.getRincians().size(); i++) {
                        RincianPerhitunganFihakKetiga r = newObj.getRincians().get(i);                        
                        sql.createRincianPFK(id, r, conn);
                    }
                }
            }
            conn.commit();
            conn.setAutoCommit(true);
            conn.setTransactionIsolation(currentLevel);
            
        } catch (SQLException ex) {
            try {
                conn.rollback();
            } catch (SQLException exc) {
            }
            throw new Exception("Gagal input Perhitungan Fihak Ketiga \n" + ex.getMessage());
        } finally {
            if (conn != null) {
                conn.setAutoCommit(true);
            }
        }        
    }
    
    @Override
    public void deletePerhitunganFihakKetiga(PerhitunganFihakKetiga obj, Connection conn) throws Exception{
        try {
            int currentLevel = conn.getTransactionIsolation();
            conn.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
            conn.setAutoCommit(false);
            if (obj != null) {
                long id = sql.getPFKIndex(obj.getTahunAnggaran(), obj.getKodeSatker(), conn);
    
                if( obj.getRincians() != null && obj.getRincians().size() > 0 ){
                    for (int i = 0; i < obj.getRincians().size(); i++) {
                        RincianPerhitunganFihakKetiga r = obj.getRincians().get(i);
                        sql.deleteRincianPFK(id, r, conn);
                    }
                }
            }
            conn.commit();
            conn.setAutoCommit(true);
            conn.setTransactionIsolation(currentLevel);
            
        } catch (SQLException ex) {
            try {
                conn.rollback();
            } catch (SQLException exc) {
            }
            throw new Exception("Gagal hapus rincian Perhitungan Fihak Ketiga\n" + ex.getMessage());
        } finally {
            if (conn != null) {
                conn.setAutoCommit(true);
            }
        }        
    }
    
    
    
    @Override
    public List<RincianPinjamanDaerah> getRincianPinjamanDaerah(short year, String satkerCode, Connection conn) throws Exception{
        return sql.getRincianPinjamanDaerahs(satkerCode, year, conn);
    }
    
    @Override
    public void createPinjamanDaerah(PinjamanDaerah obj, short iotype, Connection conn) throws Exception{
        try {
            int currentLevel = conn.getTransactionIsolation();
            conn.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
            conn.setAutoCommit(false);
            if (obj != null) {
                long id= sql.getPinjamanDaerahIndex(obj.getKodeSatker(), obj.getTahunAnggaran(), conn);
                if( id <= 0 ) {
                    id = sql.createPinjamanDaerah(obj, iotype, conn);
                }
                if( obj.getRincians() != null && obj.getRincians().size() > 0 ){
                    for (int i = 0; i < obj.getRincians().size(); i++) {
                        RincianPinjamanDaerah r = obj.getRincians().get(i);          
                        sql.createRincianPinjamanDaerah(r, id, conn);
                    }
                }
            }
            conn.commit();
            conn.setAutoCommit(true);
            conn.setTransactionIsolation(currentLevel);
            
        } catch (SQLException ex) {
            try {
                conn.rollback();
            } catch (SQLException exc) {
            }
            throw new Exception("Gagal input Pinjaman Daerah \n" + ex.getMessage());
        } finally {
            if (conn != null) {
                conn.setAutoCommit(true);
            }
        }        
    }
    
    @Override
    public void updatePinjamanDaerah(PinjamanDaerah oldObj, PinjamanDaerah newObj, Connection conn) throws Exception{
        try {
            int currentLevel = conn.getTransactionIsolation();
            conn.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
            conn.setAutoCommit(false);
            if (newObj != null) {
                long id = sql.getPinjamanDaerahIndex(newObj.getKodeSatker(), newObj.getTahunAnggaran(), conn);
                
                if( oldObj.getRincians() != null && oldObj.getRincians().size() > 0 ){
                    for (int i = 0; i < oldObj.getRincians().size(); i++) {
                        RincianPinjamanDaerah r = oldObj.getRincians().get(i);    
                        sql.deleteRincianPinjamanDaerah(oldObj.getKodeSatker(), oldObj.getTahunAnggaran(), r, conn);
                    }
                }
                if( newObj.getRincians() != null && newObj.getRincians().size() > 0 ){
                    for (int i = 0; i < newObj.getRincians().size(); i++) {
                        RincianPinjamanDaerah r = newObj.getRincians().get(i);
                        sql.createRincianPinjamanDaerah(r, id, conn);
                    }
                }
            }
            conn.commit();
            conn.setAutoCommit(true);
            conn.setTransactionIsolation(currentLevel);
            
        } catch (SQLException ex) {
            try {
                conn.rollback();
            } catch (SQLException exc) {
            }
            throw new Exception("Gagal Merubah Pinjaman Daerah \n" + ex.getMessage());
        } finally {
            if (conn != null) {
                conn.setAutoCommit(true);
            }
        }        
    }
    
    @Override
    public void deletePinjamanDaerah(PinjamanDaerah obj, Connection conn) throws Exception{
        try {
            int currentLevel = conn.getTransactionIsolation();
            conn.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
            conn.setAutoCommit(false);
            if (obj != null) {
                if( obj.getRincians() != null && obj.getRincians().size() > 0 ){
                    for (int i = 0; i < obj.getRincians().size(); i++) {
                        RincianPinjamanDaerah r = obj.getRincians().get(i);
                        sql.deleteRincianPinjamanDaerah(obj.getKodeSatker().trim(), obj.getTahunAnggaran(), r, conn);
                    }
                }
            }
            conn.commit();
            conn.setAutoCommit(true);
            conn.setTransactionIsolation(currentLevel);
            
        } catch (SQLException ex) {
            try {
                conn.rollback();
            } catch (SQLException exc) {
            }
            throw new Exception("Gagal hapus rincian Pinjaman Daerah \n" + ex.getMessage());
        } finally {
            if (conn != null) {
                conn.setAutoCommit(true);
            }
        }        
    }
    
    /**
     *
     * @param tahun
     * @param bulan
     * @param kodeSatker
     * @param kodeData
     * @param kodeFungsi
     * @param conn
     * @return
     * @throws Exception
     */
    @Override
    public List<NilaiFungsiPemda> getRealisasiKegiatanBerdasarkanFungsi(short tahun, short bulan, String kodeSatker, short kodeData, String kodeFungsi, Connection conn) throws Exception{
        return sql.getRealisasiKegiatanBerdasarkanFungsi(tahun, bulan, kodeSatker, kodeData, kodeFungsi, conn);
    }

    /**
     *
     * @param tahun
     * @param bulan
     * @param kodeSatker
     * @param sumberDana
     * @param conn
     * @return
     * @throws Exception
     */
    @Override
    public List<NilaiFungsiPemda> getRealisasiBelanjaBerdasarkanSumberDana(short tahun, short bulan, String kodeSatker, short sumberDana, Connection conn) throws Exception{
        return sql.getRealisasiBelanjaBerdasarkanSumberDana(tahun, bulan, kodeSatker, sumberDana, conn);
        
    }
    
    /**
     *
     * @param year
     * @param kodeSatker
     * @param kodeData
     * @param jenisCOA
     * @param conn
     * @return
     * @throws Exception
     */
    @Override
    public List<LampiranIPerdaAPBD> getLampiranIPenjabaranAPBDbyYear(short year, String kodeSatker, short kodeData, short jenisCOA, Connection conn) throws Exception{
        return sql.getLampiranIPenjabaranAPBDbyYear(year, kodeSatker, kodeData, jenisCOA, conn);
    }
    
    /**
     *
     * @param ma
     * @param conn
     * @throws Exception
     */
    @Override
    public void deleteMappingAccounts(MappingAccount ma, Connection conn) throws Exception{
        sql.deleteMappingAccounts(ma, conn);
    }
    
    /**
     *
     * @param ma
     * @param conn
     * @throws Exception
     */
    @Override
    public void createMappingAccounts(MappingAccount ma, Connection conn) throws Exception{
        sql.createMappingAccounts(ma, conn);
    }

    /**
     *
     * @param level
     * @param conn
     * @return
     * @throws Exception
     */
    @Override
    public List<MappingAccount> getMappingAccounts(short level, Connection conn) throws Exception{
        return sql.getMappingAccounts(level, conn);                
    }

    /**
     *
     * @param level
     * @param conn
     * @return
     * @throws Exception
     */
    @Override
    public List<SimpleAccount> getAccrualAccounts(short[] level, Connection conn) throws Exception{
        return sql.getAccrualAccounts(level, conn);
    }
    
    
    public void deleteGfsMappingAccounts(MappingAccount ma, Connection conn) throws Exception{
        sql.deleteGfsMappingAccounts(ma, conn);
    }
    public void createGfsMappingAccounts(MappingAccount ma, Connection conn) throws Exception{
        sql.createGfsMappingAccounts(ma, conn);
    }
    public List<MappingAccount> getGfsMappingAccounts(short[] levels, Connection conn) throws Exception{
        return sql.getGfsMappingAccounts(levels, conn);
    }
    public List<SimpleAccount> getGfsAccounts(short[] level, Connection conn) throws Exception{
        return sql.getGfsAccounts(level, conn);
    }
    
    /**
     *
     * @param kodeSatker
     * @param tahun
     * @param bulan
     * @param kodeData
     * @param jenisCOA
     * @param userName
     * @param password
     * @param conn
     * @return
     * @throws Exception
     */
    @Override
    public RealisasiAPBD getLRA(String kodeSatker, short tahun , short bulan, short kodeData, short jenisCOA, String userName, String password, Connection conn) throws Exception{
        return sql.getLRA(kodeSatker, tahun , bulan, kodeData, jenisCOA, userName, password, conn);
    }

    /**
     *
     * @param indexLRA
     * @param conn
     * @return
     * @throws Exception
     */
    @Override
    public List<RealisasiKegiatanAPBD> getLRAKegiatan(long indexLRA, Connection conn) throws Exception{
        return sql.getLRAKegiatan(indexLRA, conn);
    }

    /**
     *
     * @param indexKegiatanLRA
     * @param conn
     * @return
     * @throws Exception
     */
    @Override
    public List<RealisasiKodeRekeningAPBD> getLRAAkun(long indexKegiatanLRA, Connection conn) throws Exception{
        return sql.getLRAAkun(indexKegiatanLRA, conn);
    }
    
    public List<MappingNonStandar> getMappingNonStandarAccountApbds(short level, short tahun, String kodeSatker, Connection conn) throws Exception{
        return sql.getMappingNonStandarAccountApbds(level, tahun, kodeSatker, conn);
    }
    public List<MappingNonStandar> getMappingNonStandarAccountLras(short level, short tahun, String kodeSatker, Connection conn) throws Exception{
        return sql.getMappingNonStandarAccountLras(level, tahun, kodeSatker, conn);
    }
    public List<MappingNonStandar> getMappingNonStandarAccountDths(short level, short tahun, String kodeSatker, Connection conn) throws Exception{
        return sql.getMappingNonStandarAccountDths(level, tahun, kodeSatker, conn);
    }
    public List<MappingNonStandar> getMappingNonStandarAccountNeracas(short level, short tahun, String kodeSatker, Connection conn) throws Exception{
        return sql.getMappingNonStandarAccountNeracas(level, tahun, kodeSatker, conn);
    }
    public MappingNonStandar createMappingAccountNonStandars(MappingNonStandar ma, Connection conn) throws Exception{
        return sql.createMappingAccountNonStandars(ma, conn);
    }
    public void deleteMappingAccountNonStandars(MappingNonStandar ma, Connection conn) throws Exception{
        sql.deleteMappingAccountNonStandars(ma, conn);
    }
    
}
