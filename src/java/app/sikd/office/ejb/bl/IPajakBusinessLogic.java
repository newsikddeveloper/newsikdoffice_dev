/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.office.ejb.bl;

import app.sikd.entity.backoffice.DTH;
import app.sikd.entity.backoffice.RTH;
import app.sikd.entity.backoffice.RincianBPHTB;
import app.sikd.entity.backoffice.RincianHiburan;
import app.sikd.entity.backoffice.RincianHotel;
import app.sikd.entity.backoffice.RincianIMB;
import app.sikd.entity.backoffice.RincianIzinUsaha;
import app.sikd.entity.backoffice.RincianKendaraan;
import app.sikd.entity.backoffice.RincianPajakDanRetribusi;
import app.sikd.entity.backoffice.RincianRestoran;
import java.sql.Connection;
import java.util.List;

/**
 *
 * @author sora
 */
public interface IPajakBusinessLogic {
    public DTH getDTHReport(short year, String kodeSatker, short periode, String kodeSKPD, Connection conn) throws Exception;
    public List<RTH> getRTHReport(short year, String kodeSatker, short periode, Connection conn) throws Exception;
    public List<String> getDinasKirimDTH(short year, String kodeSatker, Connection conn) throws Exception;
    
    public List<RincianKendaraan> getRincianKendaraan(String kodeSatker, short tahun, Connection conn) throws Exception;
    public List<RincianBPHTB> getRincianBPHTB(String kodeSatker, short tahun, Connection conn) throws Exception;
    public List<RincianHiburan> getRincianHiburan(String kodeSatker, short tahun, Connection conn)  throws Exception;
    public List<RincianHotel> getRincianHotel(String kodeSatker, short tahun, Connection conn)  throws Exception;
    public List<RincianIMB> getRincianIMB(String kodeSatker, short tahun, Connection conn)  throws Exception;
    public List<RincianRestoran> getRincianRestoran(String kodeSatker, short tahun, Connection conn)  throws Exception;
    public List<RincianIzinUsaha> getRincianIzinUsaha(String kodeSatker, short tahun, Connection conn)  throws Exception;
    
    public List<RincianPajakDanRetribusi> getRincianPajakDanRetribusiDaerah(String kodeSatker, short tahun, Connection conn) throws Exception;
}
