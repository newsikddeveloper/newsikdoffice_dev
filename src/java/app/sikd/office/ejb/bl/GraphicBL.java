/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.office.ejb.bl;

import app.sikd.entity.backoffice.NilaiFungsiPemda;
import app.sikd.entity.graph.GraphGlobal;
import app.sikd.office.ejb.dbapi.GraphicSQL;
import app.sikd.office.ejb.dbapi.IGraphicSQL;
import java.sql.Connection;
import java.util.List;

/**
 *
 * @author sora
 */
public class GraphicBL implements IGraphicBL {
    IGraphicSQL sql;
    
    public GraphicBL(){
        sql = new GraphicSQL();        
    }
    
    /**
     *
     * @param year
     * @param kodeData
     * @param jenisCOA
     * @param kodePemdas
     * @param conn
     * @return
     * @throws Exception
     */
    @Override
    public List<GraphGlobal> getPadGraph(short year, short kodeData, short jenisCOA, List<String> kodePemdas, Connection conn) throws Exception {
        return sql.getPadGraph(year, kodeData, jenisCOA, kodePemdas, conn);
    }

    /**
     *
     * @param year
     * @param kodeData
     * @param jenisCOA
     * @param kodePemdas
     * @param conn
     * @return
     * @throws Exception
     */
    @Override
    public List<GraphGlobal> getFiskalGraph(short year, short kodeData, short jenisCOA, List<String> kodePemdas, Connection conn) throws Exception{
        return sql.getFiskalGraph(year, kodeData, jenisCOA, kodePemdas, conn);
    }

    /**
     *
     * @param year
     * @param kodeData
     * @param jenisCOA
     * @param kodePemdas
     * @param conn
     * @return
     * @throws Exception
     */
    @Override
    public List<GraphGlobal> getDanaBelanjaPengeluaranGraph(short year, short kodeData, short jenisCOA, List<String> kodePemdas, Connection conn) throws Exception{
        return sql.getDanaBelanjaPengeluaranGraph(year, kodeData, jenisCOA, kodePemdas, conn);
    }

    /**
     *
     * @param year
     * @param kodeData
     * @param jenisCOA
     * @param kodePemdas
     * @param conn
     * @return
     * @throws Exception
     */
    @Override
    public List<GraphGlobal> getTingkatBelanjaModalGraph(short year, short kodeData, short jenisCOA, List<String> kodePemdas, Connection conn) throws Exception{
        return sql.getTingkatBelanjaModalGraph(year, kodeData, jenisCOA, kodePemdas, conn);
    }

    /**
     *
     * @param year
     * @param kodeData
     * @param jenisCOA
     * @param kodePemdas
     * @param conn
     * @return
     * @throws Exception
     */
    @Override
    public List<GraphGlobal> getTingkatBelanjaPegawaiTdkLangsungGraph(short year, short kodeData, short jenisCOA, List<String> kodePemdas, Connection conn) throws Exception{
        return sql.getTingkatBelanjaPegawaiTdkLangsungGraph(year, kodeData, jenisCOA, kodePemdas, conn);
    }
    
    
    /**
     *
     * @param year
     * @param kodeData
     * @param jenisCOA
     * @param kodeFungsi
     * @param conn
     * @return
     * @throws Exception
     */
    @Override
    public List<GraphGlobal> getTop10Fungsi(short year, short kodeData, short jenisCOA, String kodeFungsi, Connection conn) throws Exception{
        return sql.getTop10Fungsi(year, kodeData, jenisCOA, kodeFungsi, conn);
    }
    
    /**
     *
     * @param year
     * @param kodeSatker
     * @param kodeData
     * @param jenisCOA
     * @param kodeFungsi
     * @param conn
     * @return
     * @throws Exception
     */
    @Override
    public List<NilaiFungsiPemda> getNilaiFungsiPemda(short year, String kodeSatker, short kodeData, short jenisCOA, String kodeFungsi, Connection conn) throws Exception{
        return sql.getNilaiFungsiPemda(year, kodeSatker, kodeData, jenisCOA, kodeFungsi, conn);
    }

    /**
     *
     * @param year
     * @param kodePemdas
     * @param kodeData
     * @param jenisCOA
     * @param kodeFungsi
     * @param conn
     * @return
     * @throws Exception
     */
    @Override
    public List<NilaiFungsiPemda> getKompilasiNilaiFungsiPemda(short year, List<String> kodePemdas, short kodeData, short jenisCOA, String kodeFungsi, Connection conn) throws Exception{
        return sql.getKompilasiNilaiFungsiPemda(year, kodePemdas, kodeData, jenisCOA, kodeFungsi, conn);
    }
    
    /**
     *
     * @param year
     * @param kodeData
     * @param jenisCOA
     * @param conn
     * @return
     * @throws Exception
     */
    @Override
    public int getJumlahDaerahKirimRealisasi(short year, short kodeData, short jenisCOA, Connection conn) throws Exception{
        return sql.getJumlahDaerahKirimRealisasi(year, kodeData, jenisCOA, conn);
    }
    
}
